<?php 

return [
    'usertypelist' => [
        '1' => 'ISO Service Seeker (Individual)',
//        '2' => 'Auditor - Service Provider',
//        '3' => 'Trainer - Service Provider',
        '4' => 'Examiner - Service Provider',
        '5' => 'ISO Service Seeker (Organization/Company)',
        '6' => 'Certification Bodies',
        '7' => 'Training Agencies',
        '8' => 'Auditor cum Trainer',
        '9' => 'Certification Company'
    ],
    'assignmentypelist' => [
        '1' => 'Audit',
        '2' => 'Training Program'
    ],
    'trainingtypelist' => [
        '1' => 'Lead auditor(most popular) Training',
        '2' => 'Internal auditor Training',
        '3' => 'Implementor Training',
        '4' => 'Lead Implementor Training',
        '5' => 'Awareness',
    ],
    'trainingmode' => [
        '1' => 'Face to Face',
        '2' => 'Online'
    ],
    'status' => [
        '1' => 'Active',
        '2' => 'Inactive'
    ],
    'assignmentstatus' => [
        '1' => 'Active',
        '2' => 'Draft/Inactive'
    ],
    'allowance' => [
        '1' => 'Yes',
        '2' => 'No'
    ],
    'codetype' => [
		'2' => 'NACE',
        '1' => 'ISIC',
        '3' => 'ANZSIC'
    ]
]

?>