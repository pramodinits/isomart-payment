<?php

return [
    "dateformat" => "d-M-Y",
    "price" => "$",
    "creditpoint" => "Credit Points",
    "refid" => "#",
    "effortdays" => " days",
    "modeonline" => " Online",
    "modef2f" => " Face to Face",
    "adddateformat" => "d-M-Y g:i a",
    "assignmentopen" => "Open / ",
    "assignmentdaysleft" => " days left"
        ]
?>

