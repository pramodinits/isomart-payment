<?php

return [
    'tasknamelist' => [
        '1' => 'Create Assignment',
        '2' => 'Send Proposal',
        '3' => 'Training Advertisement',
        '4' => 'Query Posting'
    ],
    'packagelist' => [
        '1' => 'Free Trial',
        '2' => 'Subscribed'
    ],
    'packageduration' => [
        '1' => '1 Month',
        '2' => '12 Months'
    ]
]
?>
