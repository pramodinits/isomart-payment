<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class UserCreditPointsTransaction extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'narration',
        'activity_date',
        'credit_point',
        'debit_point',
        'add_date',
        'status',
        'protocol',
    ];
    public $timestamps = false;
    protected $table = 'user_creditpoint_transaction';
}