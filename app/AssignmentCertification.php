<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentCertification extends Model {

    protected $fillable = [
        'ref_id',
        'assignment_id',
        'assignment_name',
        'sic_code',
        'auditor_name',
        'location',
        'effort_tentative',
        'budget_usd',
        'start_date',
        'end_date',
        'proposal_end_date',
        'local_travel_allowance',
        'local_stay_allowance',
        'air_fare',
        'assignment_status',
        'description',
        'query_first',
        'query_second',
        'query_second',
        'admin_approval',
        'add_date',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'assignment_request_certification';

    public function Assignment() {
        return $this->belongsTo('App\Assignment');
    }

}
