<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class CpdHours extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'cpd_title',
        'cpd_description',
        'cpd_date',
        'cpd_links',
        'status',
        'add_date',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'cpd_hours';
}
