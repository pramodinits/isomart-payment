<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingPost extends Model
{
    protected $fillable = [
        'ref_id',
        'traning_name',
        'batch_no',
        'start_date',
        'end_date',
        'standard',
        'training_type',
        'session_details',
        'mode',
        'location',
        'fee_range',
        'status',
        'add_date',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'training_post';
}
