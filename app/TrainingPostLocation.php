<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingPostLocation extends Model
{
    protected $fillable = [
        'training_post_id',
        'country_id',
        'state_id',
        'city_id',
        'city_name',
        'status',
        'add_date',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'trainingadv_location';
}
