<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class PaypalPayment extends Model
{
    protected $fillable = [
        'id',
        'paypal_payment_id',
        'user_payment_refid',
        'payer_id',
        'user_id',
        'payment_status',
        'payment_date',
        'protocol',
    ];
    public $timestamps = false;
    protected $table = 'paypal_payment';
}