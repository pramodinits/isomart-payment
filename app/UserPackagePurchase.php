<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class UserPackagePurchase extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'package_id',
        'package_price',
        'credit_points',
        'package_duration',
        'purchase_date',
        'expiry_date',
        'transaction_id',
        'payment_id',
        'payment_mode',
        'add_date',
        'status',
        'protocol',
    ];
    public $timestamps = false;
    protected $table = 'user_package_purchase';
}