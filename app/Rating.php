<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = [
        'id',
        'bid_id',
        'reference_id',
        'sender_id',
        'recive_id',
        'message',
        'add_date',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'rating';
    public function RatingAnswer() {
        return $this->hasMany('App\RatingAnswer');
    }
}
