<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class SubscriptionPackage extends Model
{
    protected $fillable = [
        'id',
        'package_type',
        'package_name',
        'package_price',
        'credit_points',
        'add_date',
        'status',
        'protocol',
    ];
    public $timestamps = false;
    protected $table = 'subscription_package';
}
