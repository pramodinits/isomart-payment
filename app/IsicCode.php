<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class IsicCode extends Model {

    protected $fillable = [
        'user_id',
        'code_sector',
        'code_description',
        'applied_date',
        'approved_date',
        'status',
        'add_date',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'users_isic_code';

    public function UserDocument() {
        return $this->hasMany('App\UserDocument');
    }
    
}
