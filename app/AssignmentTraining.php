<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class AssignmentTraining extends Model
{
    protected $fillable = [
        'assignment_id',
        'ref_id',
        'batch_no',
        'start_date',
        'end_date',
        'proposal_end_date',
        'standard',
        'training_type',
        'session_details',
        'mode',
        'location',
        'effort_tentative',
        'local_travel_allowance',
        'local_stay_allowance',
        'air_fare',
        'fee_range',
        'query',
        'assignment_status',
        'admin_approval',
        'add_date',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'assignment_request_training';
    
    public function Assignment()
    {
        return $this->belongsTo('App\Assignment');
    }
    
    public function AssignmentLocation() {
        return $this->hasMany('App\AssignmentLocation');
    }
}
