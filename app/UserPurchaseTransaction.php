<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class UserPurchaseTransaction extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'package_id',
        'package_price',
        'credit_points',
        'package_duration',
        'purchase_date',
        'expiry_date',
        'order_refid',
        'payment_id',
        'payer_id',
        'payment_status',
        'add_date',
        'status',
        'protocol',
    ];
    public $timestamps = false;
    protected $table = 'user_purchase_transaction';
}