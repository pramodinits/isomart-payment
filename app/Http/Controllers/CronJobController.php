<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Session;
use Mail;
use Config;
use App\EmailNotification;
use Carbon\Carbon;

class CronJobController extends Controller {

    public function cronAddAssignment() {
        $emailInfoList = EmailNotification::where('email_type', 1)->where('email_flag', '!=', 1)->orderBy('id', 'ASC')->limit(15)->get();
        foreach ($emailInfoList as $k => $v) {
            $emailArray = array();
            $emailArray['subject'] = $v->email_subject;
            $emailArray['body'] = $v->email_message;
            $emailArray['attachment'] = $v->email_attachment;
            $emailArray['toArrayname'] = explode(",", $v->email_name);
            $emailArray['toArrayemail'] = explode(",", $v->email_to);
            if ($v->email_cc) {
                $emailArray['ccArrayemail'] = explode(",", $v->email_cc);
            } else {
                $emailArray['ccArrayemail'] = array();
            }
            if (@$v->email_cc_name) {
                $emailArray['ccArrayname'] = explode(",", $v->email_cc_name);
            } else {
                $emailArray['ccArrayname'] = array();
            }
            $this->sendEmail($emailArray);
            EmailNotification::where('id', $v->id)->update(array(
                "email_flag" => 1
            ));
        }
    }
    
    public function cronEditAssignment() {
        $emailInfoList = EmailNotification::where('email_type', 2)->where('email_flag', '!=', 1)->orderBy('id', 'ASC')->limit(15)->get();
        foreach ($emailInfoList as $k => $v) {
            $emailArray = array();
            $emailArray['subject'] = $v->email_subject;
            $emailArray['body'] = $v->email_message;
            $emailArray['attachment'] = $v->email_attachment;
            $emailArray['toArrayname'] = explode(",", $v->email_name);
            $emailArray['toArrayemail'] = explode(",", $v->email_to);
            if ($v->email_cc) {
                $emailArray['ccArrayemail'] = explode(",", $v->email_cc);
            } else {
                $emailArray['ccArrayemail'] = array();
            }
            if (@$v->email_cc_name) {
                $emailArray['ccArrayname'] = explode(",", $v->email_cc_name);
            } else {
                $emailArray['ccArrayname'] = array();
            }
            $this->sendEmail($emailArray);
            EmailNotification::where('id', $v->id)->update(array(
                "email_flag" => 1
            ));
        }
    }
    
    public function cronDeleteAssignment() {
        $emailInfoList = EmailNotification::where('email_type', 3)->where('email_flag', '!=', 1)->orderBy('id', 'ASC')->limit(15)->get();
        foreach ($emailInfoList as $k => $v) {
            $emailArray = array();
            $emailArray['subject'] = $v->email_subject;
            $emailArray['body'] = $v->email_message;
            $emailArray['attachment'] = $v->email_attachment;
            $emailArray['toArrayname'] = explode(",", $v->email_name);
            $emailArray['toArrayemail'] = explode(",", $v->email_to);
            if ($v->email_cc) {
                $emailArray['ccArrayemail'] = explode(",", $v->email_cc);
            } else {
                $emailArray['ccArrayemail'] = array();
            }
            if (@$v->email_cc_name) {
                $emailArray['ccArrayname'] = explode(",", $v->email_cc_name);
            } else {
                $emailArray['ccArrayname'] = array();
            }
            $this->sendEmail($emailArray);
            EmailNotification::where('id', $v->id)->update(array(
                "email_flag" => 1
            ));
        }
    }
    
    public function cronExpireCreditPoint() {
        $emailInfoList = DB::table('users')->select('email', 'fname', 'lname', 'credit_points', 'id')
                ->where('credit_points', '<', 20)->where('credit_points', '!=', 0)
                ->where('email_creditpoint_flag', '!=', 1)
                ->orderBy('id', 'ASC')->limit(15)->get();
        foreach ($emailInfoList as $k => $v) {
            $emailArray = array();
            $emailArray['subject'] = "You are low on credit points";
            $emailArray['body'] = "<p>Hello $v->fname,</p>"
                    . "<p>You have only $v->credit_points credit points left in your account.</p>"
                    . "Thanks,<br />ISOMart Team";
            $emailArray['attachment'] = "";
            $emailArray['toArrayname'] = explode(",", $v->fname . " " . $v->lname);
            $emailArray['toArrayemail'] = explode(",", $v->email);
            $emailArray['ccArrayemail'] = array();
            $emailArray['ccArrayname'] = array();
            $this->sendEmail($emailArray);
            DB::table('users')->where('id', $v->id)->update(array(
                "email_creditpoint_flag" => 1
            ));
        }
    }
    
    public function cronExpirePackageDuration() {
        //DB::enableQueryLog(); 
        $emailInfoList = DB::table('users')->select('email', 'fname', 'lname', 'credit_points', 'expiry_date', 'id')
                ->where(DB::raw("(DATE(expiry_date))"), Carbon::now()->addDays(7)->format('Y-m-d'))
                ->where('email_packagedate_flag', '!=', 1)->orderBy('id', 'ASC')->limit(15)->get();
//        $query = DB::getQueryLog();
//        print "<pre>";
//        print_r($emailInfoList);
//        exit();
        foreach ($emailInfoList as $k => $v) {
            $emailArray = array();
            $emailArray['subject'] = "You are low on credit points";
            $emailArray['body'] = "<p>Hello $v->fname,</p>"
                    . "<p>Your $v->credit_points credit points are aout to expire on ".Carbon::parse(@$v->expiry_date)->format('d-M-Y')."</p>"
                    . "<p>Please buy more credit points and extend the expiry date of existing credit points for 1 more year.</p>"
                    . "Thanks,<br />ISOMart Team";
            $emailArray['attachment'] = "";
            $emailArray['toArrayname'] = explode(",", $v->fname . " " . $v->lname);
            $emailArray['toArrayemail'] = explode(",", $v->email);
            $emailArray['ccArrayemail'] = array();
            $emailArray['ccArrayname'] = array();
            $this->sendEmail($emailArray);
            DB::table('users')->where('id', $v->id)->update(array(
                "email_packagedate_flag" => 1
            ));
        }
    }
    
    public function cronNewQuery() {
        $emailInfoList = EmailNotification::where('email_type', 4)->where('email_flag', '!=', 1)->orderBy('id', 'ASC')->limit(15)->get();
        foreach ($emailInfoList as $k => $v) {
            $emailArray = array();
            $emailArray['subject'] = $v->email_subject;
            $emailArray['body'] = $v->email_message;
            $emailArray['attachment'] = $v->email_attachment;
            $emailArray['toArrayname'] = explode(",", $v->email_name);
            $emailArray['toArrayemail'] = explode(",", $v->email_to);
            if ($v->email_cc) {
                $emailArray['ccArrayemail'] = explode(",", $v->email_cc);
            } else {
                $emailArray['ccArrayemail'] = array();
            }
            if (@$v->email_cc_name) {
                $emailArray['ccArrayname'] = explode(",", $v->email_cc_name);
            } else {
                $emailArray['ccArrayname'] = array();
            }
            $this->sendEmail($emailArray);
            EmailNotification::where('id', $v->id)->update(array(
                "email_flag" => 1
            ));
        }
    }

    private function sendEmail($data) {
        $sendarray = array();
        $toArrayname = $data['toArrayname'];
        $toArrayemail = $data['toArrayemail'];
        $ccArrayemail = $data['ccArrayemail'];
        $ccArrayname = $data['ccArrayname'];
        $subject = $data['subject'];
        $messagebody = $data['body'];
        $attachment = $data['attachment'];
        Mail::send([], [], function($message) use ($messagebody, $toArrayemail, $toArrayname, $ccArrayemail, $ccArrayname, $subject, $attachment) {
            $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
            $message->subject($subject);
            foreach ($toArrayemail as $key => $val) {
                $message->to($val, @$toArrayname[$key]);
            }
            foreach ($ccArrayemail as $key => $val) {
                $message->bcc($val, @$ccArrayname[$key]);
            }
            $message->setBody($messagebody, 'text/html');
            if (@$attachment) {
                $message->attach($attachment);
            }
        });
    }
    
    

}
