<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Session;
use Mail;
use Config;
use App\AssignmentAudit;
use App\AssignmentTraining;


class NotificationController extends Controller {

    public function providerRequestEmail() {
        $providerList = DB::table('users')->select('email', 'fname', 'lname')
                        ->whereIn('usertype', array(4, 8))->get();
        $getAssignment = AssignmentAudit::where('email_flag', 0)->get();
        foreach ($getAssignment as $k => $v) {
            $seekername = DB::table('users')
                            ->join('assignment_request_audit', 'assignment_request_audit.user_id', '=', 'users.id')
                            ->select('users.fname', 'users.lname')
                            ->where('users.id', $v->user_id)->first();
            foreach ($providerList as $key => $val) {
                $data['ref_id'] = $v->ref_id;
                $data['seekername'] = $seekername->fname . " " . $seekername->lname;
                $data['fname'] = $val['fname'];
                $toemail = $val['email'];
                $toname = $val['fname'] . " " . $val['lname'];
                if ($v->user_id != $val->id) {
                    $messageBody = view('email.providerRequestEmail', $data);
                    $a = Mail::send('email.providerRequestEmail', $data, function($message) use ($toemail, $toname) {
                                $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                                $message->to($toemail, $toname)->subject("ISOMart : New Audit Assignment");
                            });
                }
            }
            AssignmentAudit::where('id', $v->id)->update(array(
                "email_flag" => 1
            ));
        }
    }

    public function trainingRequestEmail() {
        $userList = DB::table('users')->select('email', 'fname', 'lname')->get();


        $getAssignment = AssignmentTraining::where('email_flag', 0)->get();
        print "<pre>";
        print_r($userList);
        print_r($getAssignment);
        exit();
        foreach ($getAssignment as $k => $v) {
//            $trainingdata = DB::table('users')
//                            ->join('assignment_request_training', 'assignment_request_training.user_id', '=', 'users.id')
//                            ->select('users.company_name')
//                            ->where('users.id', $v->user_id)->first();
            foreach ($userList as $key => $val) {
                if ($v->user_id == $val->id) {
                    $data['company_name'] = $val->company_name;
                }
                $data['batch_no'] = $v->batch_no;
                $data['duration'] = $v->effort_tentative;
                $data['price'] = $v->fee_range;
                $data['fname'] = $val['fname'];
                $toemail = $val['email'];
                $toname = $val['fname'] . " " . $val['lname'];
                if ($v->user_id != $val->id) {
                    $messageBody = view('email.trainingRequestEmail', $data);
                    $a = Mail::send('email.trainingRequestEmail', $data, function($message) use ($toemail, $toname) {
                                $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                                $message->to($toemail, $toname)->subject("ISOMart : New Training Assignment");
                            });
                }
            }
            AssignmentTraining::where('id', $v->id)->update(array(
                "email_flag" => 1
            ));
        }
    }

    public function assignmentNotification($ref_id = "", $id_assignment = "", $assignment_type = "", $id_update = "", $id_delete = "", $fcm_token_bidder = "") {
        //notification send to bidder
        if (!empty($id_delete)) { //assignment delete
            $fcmdevice_token = $fcm_token_bidder;
            if ($assignment_type == 1) {
                $notification['action'] = "audit_delete_assignment";
            } else {
                $notification['action'] = "training_delete_assignment";
            }
            $msg = array(
                'header' => 'Assignment Deleted',
                'text' => ($assignment_type == 1 ? "Audit" : "Training") . " assignment deleted. #". $ref_id
            );
        } else {
            $fcm_token = array();
            $auditorList = DB::table('users')->select('fcm_device_token')->where('usertype', 8)->where('status', 1)->get();
            foreach ($auditorList as $k => $v) {
                $fcm_token[] = $v->fcm_device_token;
            }
            $fcmdevice_token = $fcm_token;
            if ($assignment_type == 1) {
                $notification['action'] = "audit_assignment";
            } else {
                $notification['action'] = "training_assignment";
            }
            $notification['assignment_id'] = $id_assignment; //Assignment ID
            if (!empty($id_update)) { //assignment update
                $msg = array(
                    'header' => 'Assignment Updated',
                    'text' => ($assignment_type == 1 ? "Audit" : "Training") . " assignment info updated. #". $ref_id
                );
            } else { //assignment create
                $msg = array(
                    'header' => 'New Assignment',
                    'text' => "New " . ($assignment_type == 1 ? "Audit" : "Training") . " assignment created. #". $ref_id
                );
            }
        }
        $body = json_encode($notification);
        $message = json_encode($msg);
        $this->sendNotification($message, $fcmdevice_token, $body);
    }

    public function trainingAdvNotification($ref_id = "", $id_advertisement = "") {
        $fcm_token = array();
        $auditorList = DB::table('users')->select('fcm_device_token')->where('usertype', 8)->where('status', 1)->get();
        foreach ($auditorList as $k => $v) {
            $fcm_token[] = $v->fcm_device_token;
        }
        $notification['adversitement_id'] = $id_advertisement; //Advertisement ID
        $notification['action'] = "training_advertisement"; //Action
        $body = json_encode($notification);
        $msg = array(
            'header' => 'Training Advertisement',
            'text' => "New training advertisement posted. #" . $ref_id
        );
        $message = json_encode($msg);
        $fcmdevice_token = $fcm_token;
        $this->sendNotification($message, $fcmdevice_token, $body);
    }
    
    public function newQueryNotification($id_query, $fcm_token) {
        $notification['query_id'] = $id_query; //Advertisement ID
        $notification['action'] = "new_query"; //Action
        $body = json_encode($notification);
        $msg = array(
            'header' => 'New Query Posted',
            'text' => "A new query has been posted related to your industry sector."
        );
        $message = json_encode($msg);
        $fcmdevice_token = $fcm_token;
        $this->sendNotification($message, $fcmdevice_token, $body);
    }
    
    public function sendNotification($message = "", $fcmdevice_token = "", $body = "") {
        $apiKey = "AAAAvFvOVqU:APA91bEGT9EcytGVoqNKnyVjNiof5eS_vpL0vHjj1tQhW1E7BobGbnELK2yJtklvnGd6y7QNBC5w_UNQ8ICJdsl3RSD0QoUbTNSSaWNoL3ZTHU_Jd8U3ZRkVxiD6hxWeJT0YFzwdlaqa";
        //fcm token id as recieptants
        $recieptants = $fcmdevice_token;
        $url = 'https://fcm.googleapis.com/fcm/send';
        //data set format
        if (is_array($fcmdevice_token)) {
            $to = "registration_ids";
        } else {
            $to = "to";
        }
        $fields = array
            (
            '' . $to . '' => $fcmdevice_token,
            'data' => array("Details" => json_decode($body, true),
                "Text" => json_decode($message, true))
        );
        //send notification curl
        $headers = array('Content-Type: application/json', 'Authorization: key=' . $apiKey);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = json_decode(curl_exec($ch), true);
        if (curl_errno($ch) || $result["failure"] != 0) {
            $flag = -1;
        } else {
            $flag = 1;
        }
        curl_close($ch);
        return $flag;
    }

}
