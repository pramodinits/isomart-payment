<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Session;
use Mail;
use Config;
use App\AssignmentAudit;
use App\AssignmentTraining;
use App\Message;
use App\SendProposal;
use Carbon\Carbon;
use App\TrainingPost; // for Training
use App\TrainingPostLocation; // for Training location
use App\EmailNotification; // for Email notification

class MessageController extends Controller {

    private $NotificationController;
    private $SubscriptionController;

    public function __construct() {
        $this->NotificationController = new NotificationController();
        $this->SubscriptionController = new SubscriptionController();
        $action = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);
    }

    public function messageDetails($id = null, $bidid = null, $refid = null) {
        $data = array();
        $data['reciveid'] = $id;
        $data['ref_id'] = $refid;
        $data['proposal_id'] = $bidid;
        $senderid = @Auth::user()->id;
        $data['senderid'] = @Auth::user()->id;
        //DB::enableQueryLog(); 
        $messagelist = DB::table('message')
                ->where("flag", 1)
//                ->where(function($q) use ($id) {
//                    $q->where('sender_id', $id)
//                    ->orWhere('recive_id', $id);
//                })
                ->where(function($q) use ($senderid) {
                    $q->where('sender_id', $senderid)
                    ->orWhere('recive_id', $senderid);
                })
                ->groupBy('sender_id', 'recive_id')
                ->groupBy('reference_id')
                ->orderBy('message_id', 'DESC')
                ->get();

        //->where("reference_id",$refid)
//        $query = DB::getQueryLog();
//        print "<pre>";
//        print_r($query);
//        exit;
        $data['messagelist'] = $messagelist;
        return view('message.messagedetails', $data);
    }

    public function sendMessage($id = null, $bidid = null, $refid = null) {
        $data = array();
        $ajax = @$_REQUEST['ajax'];
        $data['reciveid'] = $id;
        $data['ref_id'] = $refid;
        $data['proposal_id'] = $bidid;
        $senderid = @Auth::user()->id;
        $data['senderid'] = @Auth::user()->id;
        $messagelist = DB::table('message')
                ->where("reference_id", $refid)
                ->where(function($q) use ($id) {
                    $q->where('sender_id', $id)
                    ->orWhere('recive_id', $id);
                })
                ->where(function($q) use ($senderid) {
                    $q->where('sender_id', $senderid)
                    ->orWhere('recive_id', $senderid);
                })
                ->orderBy('message_id', 'ASC')
                ->get();
        //get endjob status
        $data['getEndJob'] = SendProposal::select('endjob_status')->where('id', $bidid)->first();
        //get endjob status
        //get sender name
        $data['getUserName'] = DB::table('users')->select('fname', 'lname')->where('id', $id)->first();
        //get sender name
        $data['messagelist'] = $messagelist;

        if (@$ajax) {
            return view('message.messagesection', $data);
        } else {
            return view('message.sendmessage', $data);
        }
    }

    public function insertMessage(Request $request) {
        $reqdata = $request->all();

        $noticeId = @$reqdata['notice_id'];
        $ins_data = array();
        $ins_data['recive_id'] = $reqdata['recive_id'];
        $ins_data['reference_id'] = @$reqdata['reference_id'];
        $ins_data['proposal_id'] = @$reqdata['proposal_id'];
        $ins_data['message'] = $reqdata['message'];
        $ins_data['sender_id'] = @Auth::user()->id;
        $ins_data['add_date'] = date("Y-m-d H:i:s");
        $ins_data['protocol'] = $_SERVER['REMOTE_ADDR'];
        $ins_data['flag'] = 1;
        Message::where('sender_id', $ins_data['sender_id'])
                ->where('recive_id', $ins_data['recive_id'])
                ->where('reference_id', $ins_data['reference_id'])
                ->update(array("flag" => 0));
        Message::where('sender_id', $ins_data['recive_id'])
                ->where('recive_id', $ins_data['sender_id'])
                ->where('reference_id', $ins_data['reference_id'])
                ->update(array("flag" => 0));
        if ($noticeId) {
            //update
        } else {
            //insert
            $id = Message::create($ins_data);
            //notification send to receiver
            $device_token = DB::table('users')->select('fcm_device_token')->where('id', $ins_data['recive_id'])->first();
            $notification['reference_id'] = $ins_data['reference_id']; //Ref ID
            $notification['sender_id'] = $ins_data['sender_id']; //Sender ID
            $notification['sender_name'] = @Auth::user()->fname . " " . @Auth::user()->lname; //Sender Name
            $notification['message'] = $ins_data['message']; //Message
            $notification['recive_id'] = $ins_data['recive_id']; //Receive ID
            $notification['action'] = "new_message"; //Proposal ID
            $body = json_encode($notification);
            $msg = array(
                'header' => 'You have a new Message',
                'text' => "You have a new Message from ISOMart."
            );
            $message = json_encode($msg);
            $fcmdevice_token = $device_token->fcm_device_token;
            $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);
        }
        $idre = $noticeId ? $noticeId : $id;
        if ($idre) {
            if ($noticeId) {
                print "update";
                exit;
            } else {
                print "insert";
                exit;
            }
        } else {
            print "error";
            exit;
        }
    }

    public function insertRating(Request $request) {
        $reqdata = $request->all();

        $ins_data = array();
        $ins_data['recive_id'] = $reqdata['user_id'];
        $ins_data['assignment_id'] = $reqdata['assignment_id'];
        $ins_data['reference_id'] = @$reqdata['reference_id'];
        $ins_data['message'] = $reqdata['message'];
        $ins_data['bid_id'] = $reqdata['bid_id'];
        $query_id = $reqdata['query_id'];
        $answer = $reqdata['answer'];
        $ins_data['sender_id'] = @Auth::user()->id;
        $ins_data['add_date'] = date("Y-m-d");
        $ins_data['protocol'] = $_SERVER['REMOTE_ADDR'];
        
        $id = DB::table('rating')->insertGetId($ins_data);
        foreach ($query_id as $k => $v) {

            $rat_data[$k]['rating_id'] = $id;
            $rat_data[$k]['query_id'] = $v;
            if (@$answer[$k]) {
                $rat_data[$k]['answer'] = $answer[$k];
            } else {
                $rat_data[$k]['answer'] = 0;
            }
            $id_insert = DB::table('rating_answer')->insertGetId($rat_data[$k]);
        }

        if (@$id_insert) {
            //notification send to receiver
            $notification['assignment_id'] = $reqdata['assignment_id']; //Assignment ID
            $notification['recive_id'] = $reqdata['user_id']; //Receiver ID
            $notification['reference_id'] = $reqdata['reference_id']; //Ref ID
            if (@$reqdata['assignment_type'] == 1) {
                $action = "audit_rating";
            } else {
                $action = "traning_rating";
            }
            $notification['action'] = $action; //Proposal ID
            $body = json_encode($notification);
            $msg = array(
                'header' => 'You have received a new rating',
                'text' => 'You have received a new rating from ISOMart'
            );
            $message = json_encode($msg);
            $fcmdevice_token = $reqdata['device_token'];
            $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);

            //mail send to receiver
            $receiver_info = DB::table('users')->select("fname", "lname", "email")->where("id", $reqdata['user_id'])->first();
            if (@$reqdata['type'] == 1) {
                $detail_url = asset('myjobDetails/' . $reqdata['assignment_id']);
            } else {
                $detail_url = asset('assignedjobDetails/' . $reqdata['assignment_id']);
            }
            $mail['fname'] = $receiver_info->fname;
            $mail['lname'] = $receiver_info->lname;
            $mail['email'] = $receiver_info->email;
            $mail['email_name'] = $receiver_info->fname . " " . $receiver_info->lname;
            $mail['subject'] = "You have received a new rating";
            $mail['msgbody'] = "<p>Hello " . $mail['email_name'] . "</p>"
                    . "<p>You have received a new rating.</p>"
                    . "<p>Please click <a href='$detail_url'>here</a> to view the details.</p>"
                    . "Thanks<br/>ISOMart Team";
            $this->messageEmailReceiver($mail);
            
            Session::flash('success_message', "Rated Successfully!");
            if (@$reqdata['type'] == 1) {
                return redirect("/myjobDetails/" . $reqdata['assignment_id']);
            } else {
                return redirect("/assignedjobDetails/" . $reqdata['assignment_id']);
            }
        } else {
            Session::flash('serror_message', "Failure in rate!");
            if (@$reqdata['type'] == 1) {
                return redirect("/myjobDetails/" . $reqdata['assignment_id']);
            } else {
                return redirect("/assignedjobDetails/" . $reqdata['assignment_id']);
            }
        }
    }
    
    public function messageEmailReceiver($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $email_name = $data['email_name'];
        $subject = $data['subject'];
        $msgbody = $data['msgbody'];
        Mail::send([], [], function($message) use ($toemail, $toname, $subject, $email_name, $msgbody) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                    $message->setBody($msgbody, 'text/html');
                });
    }

    //Traing Start
    public function addTrainingPost($training_id = null) {
        if (@Auth::user()->id) {
            $prefix = DB::getTablePrefix();
            if ($training_id) {
                $trainingpostinfo = TrainingPost::where('training_post_id', @$training_id)
                        ->first();
                $traininglocation = TrainingPostLocation::where('training_post_id', @$training_id)
                                ->select('countries.name as countryName', 'states.name as stateName', 'trainingadv_location.*')
                                ->join('countries', 'countries.id', '=', 'trainingadv_location.country_id')
                                ->join('states', 'states.id', '=', 'trainingadv_location.state_id')
                                ->orderBy('location_id', 'DESC')->get();
                $data['traininginfo'] = $trainingpostinfo;
                $data['traininglocation'] = $traininglocation;
            }
            $trainingpostlist = TrainingPost::where('user_id', @Auth::user()->id)
                            ->orderBy('training_post_id', 'DESC')->get();
            $data['traininglist'] = $trainingpostlist;

            $data['countryList'] = DB::table('countries')->orderBy('name', 'ASC')->get();
            $data['standardlist'] = Config::get('standards.standardlist');
            $data['trainingtypelist'] = Config::get('selecttype.trainingtypelist');
            $data['trainingmode'] = Config::get('selecttype.trainingmode');
            $data['status'] = Config::get('selecttype.assignmentstatus');
            $data['selectmenu'] = "addtrainingpost";
//            print "<pre>";
//            print_r(@$trainingpostinfo);exit;
            if (@Auth::user()->usertype == 7) {
                return view('pages.trainingpost.addtrainingpost', $data);
            } else {
                return redirect('/missing');
            }
        } else {
            return redirect("/signin");
        }
    }

    public function deletePostLocation(Request $request) {
        $reqdata = $request->all();
        $location_id = $reqdata['location_id'];
        if ($location_id) {
            $location_exist = TrainingPostLocation::where('location_id', $location_id)->first();
            if ($location_id) {
                TrainingPostLocation::where('location_id', $location_id)->delete();
                print "Success";
            } else {
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function insertTrainingPost(Request $request) {
        $reqdata = $request->all();
        $training = $reqdata['training'];
        $training['start_date'] = Carbon::parse($reqdata['startDate'])->format('Y-m-d');
        $training['end_date'] = Carbon::parse($reqdata['endDate'])->format('Y-m-d');
        $id_user = @$reqdata['user_id'];
        $id_update = @$reqdata['id_update'];

        $taskPoints = $this->SubscriptionController->getTaskPoint("Training Advertisement");
        $creditPoints = $this->SubscriptionController->getUserCreditPoint(@Auth::user()->id);
        if ($creditPoints < $taskPoints && empty($id_update)) {
            Session::flash('error_message', "You have not sufficient credit points available!");
            return redirect("/addTrainingPost");
        } else {
            //location info
            $cityname = @$reqdata['cityname'];
            $stateid = @$reqdata['stateid'];
            $countryid = @$reqdata['countryid'];
            $citylength = count($cityname);
            //location info
            $hidden_img = @$reqdata['photo_hid'];
            $img_file = $request->file('img');
            if ($img_file) {
                $nameimage = $img_file->getClientOriginalName();
                $nameimage = str_replace(" ", "_", $nameimage);
            }
            if (@$id_update) {
                if ($citylength) {
                    for ($i = 0; $i < $citylength; $i++) {
                        $location['city_id'] = null;
                        $cityId = DB::table('cities')->select("id")->where('name', $cityname[$i])
                                        ->where('country_id', $countryid[$i])->where('region_id', $stateid[$i])->first();
                        if ($cityId) {
                            $location['city_id'] = $cityId->id;
                        }
                        $location['training_post_id'] = $id_update;
                        $location['country_id'] = $countryid[$i];
                        $location['state_id'] = $stateid[$i];
                        $location['city_name'] = $cityname[$i];
                        $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                        $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                        $locid = TrainingPostLocation::insertGetId($location);
                    }
                }
                if ($img_file) {
                    if (file_exists(public_path('training_image/' . $hidden_img))) {
                        @unlink(public_path('training_image/' . $hidden_img));
                    }
                    @$name = $id_update . "_" . $nameimage;
                    @$cp = $img_file->move(public_path('training_image'), $name);
                    $training['image'] = $name;
                }
                $id = TrainingPost::where('training_post_id', $id_update)->update($training);
                if (@$id || @$locid) {
                    Session::flash('success_message', "Training advertisement successfully updated.");
                    return redirect("/addTrainingPost/" . $id_update);
                } else {
                    Session::flash('error_message', "Failure in update");
                    return redirect("/addTrainingPost");
                }
            } else {
                $training['user_id'] = @Auth::user()->id;
                $training['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $training['protocol'] = $_SERVER['REMOTE_ADDR'];
//            print "<pre>";
//            print_r($training);
//            exit();
                //transaction
                $debitPoints = $this->SubscriptionController->updateUserCreditPoint("", @Auth::user()->id, $taskPoints, "");
                if ($debitPoints) {
                    $updateTransaction = $this->SubscriptionController->updateUserTransaction(@Auth::user()->id, "Training Advertisement", Carbon::now()->format('Y-m-d'), "", $taskPoints);
                }

                $id = TrainingPost::insertGetId($training);

                //insert location
                for ($i = 0; $i < $citylength; $i++) {
                    $location['city_id'] = null;
                    $cityId = DB::table('cities')->select("id")
                                    ->where('name', $cityname[$i])
                                    ->where('country_id', $countryid[$i])
                                    ->where('region_id', $stateid[$i])->first();
                    if ($cityId) {
                        $location['city_id'] = $cityId->id;
                    }
                    $location['training_post_id'] = $id;
                    $location['country_id'] = $countryid[$i];
                    $location['state_id'] = $stateid[$i];
                    $location['city_name'] = $cityname[$i];
                    $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                    $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                    TrainingPostLocation::create($location);
                }
                //insert location

                if ($id) {
                    $ref_id = "TA" . date("Y") . date("m") . date("d") . $id;
                    TrainingPost::where('training_post_id', $id)->update(array(
                        "ref_id" => $ref_id
                    ));
                    if ($img_file) {
                        @$name = $id . "_" . $nameimage;
                        @$cp = $img_file->move(public_path('training_image'), $name);
                        TrainingPost::where('training_post_id', $id)->update(array("image" => $name));
                    }
                    //send notification to auditor
                    if ($training['status'] == 1) {
                        $this->NotificationController->trainingAdvNotification($ref_id, $id);
                    }
                    Session::flash('success_message', "Training advertisement posted Successfully!.");
                    return redirect("/addTrainingPost");
                } else {
                    Session::flash('error_message', "Failure in insert!");
                    return redirect("/addTrainingPost");
                }
            }
        }
    }

    public function deleteTrainingPost(Request $request) {
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            TrainingPost::where('training_post_id', $id)->delete();
            print "Success";
        } else {
            print "Error";
        }
        exit();
    }

    public function trainingPostList() {
        $trainingpostinfo = TrainingPost::where('status', '=', 1)
                        ->orderBy('training_post_id', 'DESC')->limit(3)->get();
        $data['trainingpostinfo'] = $trainingpostinfo;
        return view('pages.trainingpost.trainingpostlist', $data);
    }

    public function trainingPostDetail($id = null) {
        $trainingpostdetail = TrainingPost::where('training_post_id', $id)->first();
        $data['trainingpostdetail'] = $trainingpostdetail;
        $userdetails = DB::table('users')->where("id", @$trainingpostdetail->user_id)->first();
        $data['userdetails'] = $userdetails;
        $data['trainingtypelist'] = Config::get('selecttype.trainingtypelist');
        $data['trainingmode'] = Config::get('selecttype.trainingmode');
        $data['status'] = Config::get('selecttype.assignmentstatus');
        //get city name
        $locationname = DB::table('trainingadv_location')
                        ->select('countries.name as countryName', 'states.name as stateName', 'trainingadv_location.*')
                        ->join('countries', 'countries.id', '=', 'trainingadv_location.country_id')
                        ->join('states', 'states.id', '=', 'trainingadv_location.state_id')
                        ->where('trainingadv_location.training_post_id', $id)->get();
        $data['locationname'] = $locationname;
        $data['standardlist'] = Config::get('standards.standardlist');
        return view('pages.trainingpost.trainingpostdetail', $data);
    }

    public function addQuery($query_id = null) {
        if (@Auth::user()->id) {
            $prefix = DB::getTablePrefix();
            $data['codetype'] = Config::get('selecttype.codetype');
            $data['countryList'] = DB::table('countries')->orderBy('name', 'ASC')->get();
            $data['status'] = Config::get('selecttype.assignmentstatus');
            $data['codeDetail'] = DB::table('international_standards_section')->pluck("name", "id");
//            print "<pre>";
//            print_r(@$trainingpostinfo);exit;
            if ($query_id) {
                $querypostinfo = DB::table('query')->where('query_id', @$query_id)
                        ->first();
                $data['queryinfo'] = $querypostinfo;
                $statelist = DB::table('states')->where('country_id', @$querypostinfo->country)
                                ->orderBy('name', 'ASC')->get();

                $data['states'] = $statelist;
            }

            return view('pages.query.addquery', $data);
        } else {
            return redirect("/signin");
        }
    }

    public function insertQuery(Request $request) {
        $reqdata = $request->all();
        $query['startDate'] = Carbon::parse($reqdata['startDate'])->format('Y-m-d');
        $query['endDate'] = Carbon::parse($reqdata['endDate'])->format('Y-m-d');
        $query['user_id'] = @$reqdata['user_id'];
        $query['usertype'] = @$reqdata['user_type'];
        $query['title'] = @$reqdata['title'];
        $query['codetype'] = @$reqdata['codetype'];
        $query['sic_code'] = @$reqdata['sic_code'];
        $id_update = @$reqdata['id_update'];

        //check credit points
        $taskPoints = $this->SubscriptionController->getTaskPoint("Query Posting");
        $creditPoints = $this->SubscriptionController->getUserCreditPoint(@Auth::user()->id);
        if ($creditPoints < $taskPoints && empty($id_update)) {
            Session::flash('error_message', "You have not sufficient credit points available!");
            return redirect("/addQuery");
        } else {
            //location info
            $cityId = DB::table('cities')->select("id")
                            ->where('name', @$reqdata['city'])
                            ->where('country_id', @$reqdata['country'])
                            ->where('region_id', @$reqdata['states'])->first();
            if ($cityId) {
                $query['city'] = $cityId->id;
            }
            $query['city_name'] = @$reqdata['city'];
            $query['states'] = @$reqdata['states'];
            $query['country'] = @$reqdata['country'];
            //location info

            $query['status'] = @$reqdata['status'];
            $query['effort_mandays'] = @$reqdata['effort_mandays'];
            $query['lookingfor'] = @$reqdata['lookingfor'];
            $query['description'] = @$reqdata['description'];
            if (@$reqdata['lookingfor'] == 1) {
                $query['travelinfo'] = @$reqdata['travelinfo'];
                $query['budgetinfo'] = @$reqdata['budgetinfo'];
            } else if (@$reqdata['lookingfor'] == 2) {
                $query['perday_charge'] = @$reqdata['perday_charge'];
                $query['radious'] = @$reqdata['radious'];
            }

            if (@$id_update) {
                $id = DB::table('query')->where('query_id', $id_update)->update($query);
                if ($id || $id == "") {
                    Session::flash('success_message', "Query successfully updated.");
                    return redirect("/addQuery/" . $id_update);
                } else {
                    Session::flash('error_message', "Failure in update");
                    return redirect("/addQuery");
                }
            } else {
                $query['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $query['protocol'] = $_SERVER['REMOTE_ADDR'];
                $debitPoints = $this->SubscriptionController->updateUserCreditPoint("", @Auth::user()->id, $taskPoints, "");
                if ($debitPoints) {
                    $updateTransaction = $this->SubscriptionController->updateUserTransaction(@Auth::user()->id, "Query Posting", Carbon::now()->format('Y-m-d'), "", $taskPoints);
                }
                $id = DB::table('query')->insertGetId($query);
                
                if ($id && $query['status'] == 1) {
                    //store email notification
                    $detail_url = asset('queryPostDetail/' . $id);
                    $msgbody = "<p>A new query has been posted related to your industry sector.</p>"
                            . "<p>To view or respond to this query, please click <a href='$detail_url'>here</a>.</p>"
                            . "Thanks,<br />ISOMart Team";
                    $providerList = DB::table('users')
                                    ->join('users_isic_code as ic', 'ic.user_id', '=', 'users.id')
                                    ->select('users.email', 'users.fname', 'users.lname', 'users.fcm_device_token')
                                    ->where('ic.codetype', $query['codetype'])
                                    ->whereIn('users.usertype', array(5, 6))->get();
                    $fcm_token = array();
                    foreach ($providerList as $k => $v) {
                        $fcm_token[] = $v->fcm_device_token;
                        $ins_email['email_type'] = 4; //3->New query
                        $ins_email['email_to'] = $v->email;
                        $ins_email['email_name'] = $v->fname . " " . $v->lname;
                        $ins_email['email_subject'] = "New Query Posted";
                        $ins_email['email_message'] = "<p>Hello " . $ins_email['email_name'] . "</p>" . $msgbody;
                        $ins_email['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                        $ins_email['protocol'] = $_SERVER['REMOTE_ADDR'];
                        EmailNotification::insertGetId($ins_email);
                    }
                    
                    //send app notification
                    $id_query = $id;
                    $this->NotificationController->newQueryNotification($id_query, $fcm_token);
                }

                if ($id) {
                    Session::flash('success_message', "Query posted Successfully!.");
                    return redirect("/myQuery");
                } else {
                    Session::flash('error_message', "Failure in insert!");
                    return redirect("/addQuery");
                }
            }
        }
    }

    public function myQuery() {
        if (@Auth::user()->id) {
            $prefix = DB::getTablePrefix();

            $querypostlist = DB::table('query')->where('user_id', @Auth::user()->id)
                            ->orderBy('query_id', 'DESC')->get();
            $data['querylist'] = $querypostlist;
            $data['codeDetail'] = DB::table('international_standards_section')->pluck("name", "id");
            $data['codetype'] = Config::get('selecttype.codetype');
            $data['countryList'] = DB::table('countries')->orderBy('name', 'ASC')->get();
            $data['status'] = Config::get('selecttype.assignmentstatus');
            $data['selectmenu'] = "myquery";
            return view('pages.query.myquery', $data);
        } else {
            return redirect("/signin");
        }
    }

    public function deleteQueryPost(Request $request) {
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            DB::table('query')->where('query_id', $id)->delete();
            print "Success";
        } else {
            print "Error";
        }
        exit();
    }

    public function queryPostList() {
        $querypostinfo = DB::table('query')->where('status', '=', 1)
                        ->orderBy('query_id', 'DESC')->get();
        $data['querypostinfo'] = $querypostinfo;
        $data['country'] = DB::table('countries')->pluck("name", "id");
        $data['state'] = DB::table('states')->pluck("name", "id");
        return view('pages.query.querypostlist', $data);
    }

    public function queryPostDetail($id = null) {
        $querypostdetail = DB::table('query')->where('query_id', $id)->first();
        $data['querypostdetail'] = $querypostdetail;
        $userdetails = DB::table('users')->where("id", @$querypostdetail->user_id)->first();
        $data['codeDetail'] = DB::table('international_standards_section')->pluck("name", "id");
        $data['codetype'] = Config::get('selecttype.codetype');
        $data['status'] = Config::get('selecttype.assignmentstatus');
        $data['country'] = DB::table('countries')->pluck("name", "id");
        $data['state'] = DB::table('states')->pluck("name", "id");
        $data['selectmenu'] = "querydetail";
        return view('pages.query.querypostdetail', $data);
    }

}
