<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Session;
use Mail;
use Config;
use App\AssignmentAudit;
use App\AssignmentTraining;
use App\AssignmentReport;
use App\AssignmentCertification;
use App\Assignment;
use App\SendProposal;
use Carbon\Carbon;
use App\Http\Controllers\NotificationController;

class CalenderController extends UserController {

    private $NotificationController;

    public function __construct() {
        //$this->middleware('auth');
        $this->NotificationController = new NotificationController();
    }

    public function myCalender() {
        $list = DB::table('send_proposal')
                        ->join('assignment_request_audit', 'send_proposal.assignment_id', '=', 'assignment_request_audit.assignment_id')
                        ->select('assignment_request_audit.assignment_name', 'assignment_request_audit.ref_id', 'assignment_request_audit.assignment_id',
                                'assignment_request_audit.start_date as assignment_startdate', 'assignment_request_audit.end_date as assignment_enddate', 
                                'send_proposal.id as proposal_id')
                ->where('send_proposal.approval_status', 1)->where("send_proposal.user_id", @Auth::user()->id)->get();
//        print "<pre>";
//       print_r($list);
//       exit();
        $data['list'] = $list;
        $data['defaultDate'] = date('Y-m-d');
        $data['selectmenu'] = "calendar";
        return view('pages.calender.mycalender', $data);
    }

}
