<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Session;
use Mail;
use Config;
use App\AssignmentAudit;
use App\AssignmentTraining;
use App\TrainingPost;
use App\Assignment;
use App\AssignmentLocation;
use App\SendProposal;
use App\Rating;
use App\RatingAnswer;
use App\CpdHours;
use App\TaskCredit;
use App\SubscriptionPackage;
use Carbon\Carbon;

class AdminController extends Controller {

    public function index() {
        $auditcount = AssignmentAudit::count();
        $activeAuditCount = Assignment::where('assignment_type', 1)->where('status', 1)
                ->where('close_job', 0)->where('approval_status', '!=', 2)->count();
        $activeTrainingCount = Assignment::where('assignment_type', 2)->where('status', 1)
                ->where('close_job', 0)->where('approval_status', '!=', 2)->count();
        $trainingcount = AssignmentTraining::count();
        $usercount = DB::table('users')->where("is_admin", "0")->count();
        
        $latestcount = DB::table('users')
                ->where('reg_date', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 30 DAY)'))
                ->count();
        
        $data['auditcount'] = $auditcount;
        $data['trainingcount'] = $trainingcount;
        $data['usercount'] = $usercount;
        $data['activeAuditCount'] = $activeAuditCount;
        $data['activeTrainingCount'] = $activeTrainingCount;
        $data['latestcount'] = $latestcount;
        
        $getuser = DB::table('users')->select(DB::raw('DATE(reg_date) as reg_date,COUNT(id) as user_count'))
                    ->where('reg_date', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 30 DAY)'))
                    ->groupBy(DB::raw('DATE(reg_date)'))
                    ->orderby('reg_date', 'DESC')
                    ->get();
        $getuserArray = array();

        foreach ($getuser as $key => $val) {
            $getuserArray[$val->reg_date] = $val->user_count;
        }
        //DB::enableQueryLog(); 
        $getassignment = DB::table('assignment')->select(DB::raw('DATE(add_date) as add_date,COUNT(id) as assignment_count'))
                    ->where('add_date', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 30 DAY)'))
                    ->where('approval_status', '!=', 2)
                    ->groupBy(DB::raw('DATE(add_date)'))
                    ->orderby('add_date', 'DESC')
                    ->get();
        $getassignArray = array();

        foreach ($getassignment as $key => $val) {
            $getassignArray[$val->add_date] = $val->assignment_count;
        }
//        print "<pre>";
//        print_r(DB::getQueryLog());exit;
        
        $date[0] = date('Y-m-d');
        for ($i = 1; $i <= 30; $i++) {
            $date[$i] = date('Y-m-d', strtotime("-$i days"));
        }
        $datestring =array();
        $userstring =array();
        $assignmentstring =array();
        foreach ($date as $k => $v) {
            $match30daysarray[$k]['reg_date'] = date('d-m-Y', strtotime($v));
            $match30daysarray[$k]['user_count'] = @$getuserArray[$v] ? $getuserArray[$v] : 0;
            $match30daysarray[$k]['assignment_count'] = @$getassignArray[$v] ? $getassignArray[$v] : 0;
            
            array_push($datestring, $match30daysarray[$k]['reg_date']);
            array_push($userstring, $match30daysarray[$k]['user_count']);
            array_push($assignmentstring, $match30daysarray[$k]['assignment_count']);
        }
        $data['datestring'] = $datestring;
        $data['userstring'] = $userstring;
        $data['assignmentstring'] = $assignmentstring;
        
        return view('home', $data);
    }

    public function logout(Request $request) {
        if (@Auth::id()) {
            Session::flush();
            Auth::logout();
        }
//        Auth::logout();
        $request->session()->flash('success_message', "Successfully loggedout");
        return redirect("/admin");
    }

    public function userListing(Request $request) {
        $searchinput = $request->all();
        $email = isset($searchinput['email']) ? trim($searchinput['email']) : "";
        $phone = isset($searchinput['phone']) ? trim($searchinput['phone']) : "";
        $usertype = isset($searchinput['usertype']) ? trim($searchinput['usertype']) : "";
        $arraysearch = array(
            array("phone", "LIKE", "%$phone%"),
            array("email", "LIKE", "%$email%"),
            array("usertype", "LIKE", "%$usertype%"),
        );
        $arraysearchappendurl = array(
            "phone" => $phone,
            "email" => $email,
            "usertype" => $usertype,
        );
        $userlist = DB::table('users')
                ->where($arraysearch)
                ->where("is_admin", "0")
                ->orderBy('id', 'DESC')
                ->paginate(10);
        $userlist->appends($arraysearchappendurl)->links();
        $data['userlist'] = $userlist;
        return view('vendor.adminlte.user.userlisting', $data)->with($arraysearchappendurl);
    }
    
    public function updateUserStatus(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['user_id'];
        $status_val = $reqdata['status_val'];
        if($user_id) {
            $updateStatus = DB::table('users')->where("id", $user_id)
                    ->update(
                    array(
                        "status" => $status_val,
                        "updated_at" => Carbon::now()->format("Y-m-d H:i:s")
                    )
            );
            if ($updateStatus) {
                print "success";
                exit;
            } else {
                print "failure";
                exit;
            }
        } else {
            print "failure";
            exit;
        }
    }
    
    public function userDetail($user_id) {
        $averagerating = 0;
        $gettotalaverage = 0;
        $valStatus = 1;
            $prefix = DB::getTablePrefix();
            $data['userdetails'] = DB::table('users')->where('id', $user_id)->first();
            $data['contactdetails'] = DB::table('users_contact_details')->where('user_id', $user_id)->first();

            $data['addressdetails'] = DB::table('users_contact_address')
                            ->select('countries.name as countryName', 'states.name as stateName', 'users_contact_address.*')
                            ->join('countries', 'countries.id', '=', 'users_contact_address.country_id')
                            ->join('states', 'states.id', '=', 'users_contact_address.state_id')
                            ->where('users_contact_address.user_id', $user_id)
                            ->where('users_contact_address.available_status', $valStatus)->first();

            $data['generaldetails'] = DB::table('users_general_info')->where('user_id', $user_id)->first();
            $data['cpddetails'] = CpdHours::where('user_id', $user_id)->limit(3)->get();
            $data['isiccodedetails'] = DB::table('users_isic_code')
                            ->join('international_standards_section', 'users_isic_code.code_sector', '=', 'international_standards_section.id')
                            ->select('users_isic_code.*', 'international_standards_section.name as codeName', 'international_standards_section.type', 'international_standards_section.code')
                            ->where('users_isic_code.user_id', $user_id)->limit(3)->get();
            
            $ratingreview = Rating::with('RatingAnswer')
                            ->join('users', 'rating.sender_id', '=', 'users.id')
                            ->leftJoin('assignment_request_audit as ar', function($join) {
                                $join->on('ar.ref_id', '=', 'rating.reference_id');
                            })
                            ->leftJoin('assignment_request_training as at', function($join) {
                                $join->on('at.ref_id', '=', 'rating.reference_id');
                            })
                            ->leftjoin('international_standards_section as is', 'is.id', '=', 'ar.sic_code')
                            ->leftjoin('assignment_location as al', 'al.assignment_id', '=', 'rating.assignment_id')
                            ->select('ar.assignment_name', 'ar.sic_code', 'ar.start_date AS auditStart', 'ar.end_date AS auditEnd', 'ar.description',
                                    'at.batch_no', 'at.training_type', 'at.mode', 'at.start_date AS trainingStart', 'at.end_date AS trainingEnd', 'at.session_details',
                                    'users.fname', 'users.lname', 'rating.*', 
                                    'is.code', 'is.name', 'is.type', 
                                    DB::raw("(GROUP_CONCAT(city_name SEPARATOR ', ')) as `cities`"))
                            ->groupBy('al.assignment_id')->where('recive_id', $user_id)->get();

            if ($data['userdetails']->usertype == 6 || $data['userdetails']->usertype == 5 || $data['userdetails']->usertype == 1) {
                $data['jobCount'] = DB::table('assignment')
                                ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                                ->where(function($q) use ($valStatus) {
                                    $q->where('assignment.approval_status', $valStatus)
                                    ->orWhere('send_proposal.approval_status', $valStatus);
                                })
                                ->where('assignment.user_id', $user_id)->groupBy('assignment.id')->count();
            } else {
                $data['jobCount'] = SendProposal::where('user_id', $user_id)->where('approval_status', 1)->count();
            }
            $data['queryname'] = DB::table('query_master')->pluck("query", "id");

            foreach ($ratingreview as $k => $v) {
                $sumrating = 0;
                foreach ($ratingreview[$k]['RatingAnswer'] as $key => $val) {
                    $sumrating += $val->answer;
                }
                $averagerating += $sumrating / count($data['queryname']);
            }
            if ($averagerating > 0) {
                $data['gettotalaverage'] = round($averagerating / count($ratingreview));
            }
            $data['ratingreview'] = $ratingreview;
            return view('vendor.adminlte.user.userdetail', $data);
    }
    
    public function assignmentListing() {
        $prefix = DB::getTablePrefix();
        $auditlist = AssignmentAudit::
                orderBy('id', 'DESC')
                ->paginate(10);
//        $traininglist = AssignmentTraining::orderBy('id', 'DESC')->paginate(10);
        $traininglist = Assignment::with('AssignmentLocation')
                        ->join('assignment_request_training', DB::raw($prefix . 'assignment_request_training.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                        ->select("assignment_request_training.*", "assignment.*")->where('assignment.approval_status', '!=', 2)
                        ->orderBy('assignment.id', 'DESC')->paginate(10);
        $data['auditlist'] = $auditlist;
        $data['traininglist'] = $traininglist;
//        print "<pre>";
//        print_r($data['traininglist']);
//        exit();
        return view('vendor.adminlte.assignment.assignmentListing', $data);
    }
    
    public function auditDetail($assignment_id) {
        $auditdetails = AssignmentAudit::where('assignment_id', $assignment_id)->first();
        $data['auditdetails'] = $auditdetails;
        //get city name
        $locationname = DB::table('assignment_location')
                        ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                        ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                        ->join('states', 'states.id', '=', 'assignment_location.state_id')
                        ->where('assignment_location.assignment_id', $assignment_id)->get();
        $data['locationname'] = $locationname;
        //standard details
        $standardDeatil = DB::table('international_standards_section')->where('id', $auditdetails->sic_code)->first();
        $data['standardDeatil'] = $standardDeatil;
        //get assignment creator & assignment approval info
        $userdetails = Assignment::select('users.fname', 'users.lname', 'users.id', 'users.email', 'assignment.approval_status', 'assignment.user_id')
                        ->leftJoin('users', function($join) {
                            $join->on('assignment.user_id', '=', 'users.id');
                        })
                        ->where("assignment.id", $auditdetails->assignment_id)->get();
        $data['userdetails'] = $userdetails;                
        //get assignment creator & assignment approval info      
        //get count of total list
        $data['countTotalBid'] = SendProposal::where('assignment_id', '=', $assignment_id)->count();                
        return view('vendor.adminlte.assignment.auditdetail', $data);
    }
    
    public function trainingDetail($assignment_id) {
        $trainingdetails = AssignmentTraining::where('assignment_id', $assignment_id)->first();
        $data['trainingdetails'] = $trainingdetails;
        //get city name
        if ($trainingdetails->mode == 1) {
            $locationname = DB::table('assignment_location')
                            ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                            ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                            ->join('states', 'states.id', '=', 'assignment_location.state_id')
                            ->where('assignment_location.assignment_id', $assignment_id)->get();
            $data['locationname'] = $locationname;
        }
        //get assignment creator & assignment approval info
        $userdetails = Assignment::select('users.fname', 'users.lname', 'users.id', 'users.email', 'assignment.approval_status', 'assignment.user_id')
                        ->leftJoin('users', function($join) {
                            $join->on('assignment.user_id', '=', 'users.id');
                        })
                        ->where("assignment.id", $trainingdetails->assignment_id)->get();
        $data['userdetails'] = $userdetails;
        //get count of total list
        $data['countTotalBid'] = SendProposal::where('assignment_id', '=', $assignment_id)->count();                
        return view('vendor.adminlte.assignment.trainingdetail', $data);
    }
    
    public function trainingPostListing() {
        $trainingpostlist = TrainingPost::orderBy('training_post_id', 'DESC')->paginate(10);;
        $data['traininglist'] = $trainingpostlist;
        $data['countryList'] = DB::table('countries')->orderBy('name', 'ASC')->get();
        $data['standardlist'] = Config::get('standards.standardlist');
        $data['trainingtypelist'] = Config::get('selecttype.trainingtypelist');
        $data['trainingmode'] = Config::get('selecttype.trainingmode');
        $data['status'] = Config::get('selecttype.assignmentstatus');
        $userdetails = DB::table('users')->get();
        foreach($userdetails as $key => $val)
        {
        $data['userdetails'][$val->id] = $val->fname." ".$val->lname;
        }
        return view('vendor.adminlte.advertisement.trainingpostlisting', $data);
        
    }
    
    public function advertisementShape($id = null) {
        $data = array();
        $shapelist = DB::table('advt_shape')->orderBy('shape_id', 'DESC')->paginate(10);
        $data['shapelist'] = $shapelist;
        if ($id) {
            $shapeinfo = DB::table('advt_shape')->where('shape_id', @$id)->first();
            $data['shapeinfo'] = $shapeinfo;
        }
        
        return view('vendor.adminlte.advertisement.advertisementshape', $data);
        
    }
    
    public function insertShape(Request $request) {
        $reqdata = $request->all();
        $shape = $reqdata['shape'];
        $id_update = @$reqdata['id_update'];
        
        if (@$id_update) {
            $id = DB::table('advt_shape')->where('shape_id', $id_update)->update($shape);
            if ($id) {
                Session::flash('success_message', "Advertisement shape successfully updated.");
                return redirect("/admin/advertisementShape");
            } else {
                Session::flash('error_message', "Failure in update");
                return redirect("/admin/advertisementShape");
            }
        } else {
            $shape['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $shape['protocol'] = $_SERVER['REMOTE_ADDR'];
            
            $id = DB::table('advt_shape')->insertGetId($shape);
            if ($id) {
                Session::flash('success_message', "Advertisement shape posted Successfully!.");
                return redirect("/admin/advertisementShape");
            } else {
                Session::flash('error_message', "Failure in insert!");
                return redirect("/admin/advertisementShape");
            }
        }
    }
    
    public function deleteShape(Request $request) {
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            DB::table('advt_shape')->where('shape_id', $id)->delete();

            print "Success";
        } else {
            print "Error";
        }
        exit();
    }
    
    public function advertisementLocation($id = null) {
        $data = array();
        $loclist = DB::table('advt_location')->orderBy('loc_id', 'DESC')->paginate(10);
        $data['loclist'] = $loclist;
        if ($id) {
            $locinfo = DB::table('advt_location')->where('loc_id', @$id)->first();
            $data['locinfo'] = $locinfo;
        }
        
        return view('vendor.adminlte.advertisement.advertisementlocation', $data);
        
    }
    
    public function insertLocation(Request $request) {
        $reqdata = $request->all();
        $location = $reqdata['location'];
        $id_update = @$reqdata['id_update'];
        
        if (@$id_update) {
            $id = DB::table('advt_location')->where('loc_id', $id_update)->update($location);
            if ($id) {
                Session::flash('success_message', "Advertisement location successfully updated.");
                return redirect("/admin/advertisementLocation");
            } else {
                Session::flash('error_message', "Failure in update");
                return redirect("/admin/advertisementLocation");
            }
        } else {
            $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $location['protocol'] = $_SERVER['REMOTE_ADDR'];
            
            $id = DB::table('advt_location')->insertGetId($location);
            if ($id) {
                Session::flash('success_message', "Advertisement location posted Successfully!.");
                return redirect("/admin/advertisementLocation");
            } else {
                Session::flash('error_message', "Failure in insert!");
                return redirect("/admin/advertisementLocation");
            }
        }
    }
    
    public function deleteLocation(Request $request) {
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            DB::table('advt_location')->where('loc_id', $id)->delete();

            print "Success";
        } else {
            print "Error";
        }
        exit();
    }
    
    public function addAdvertisement($id = null) {
        $data = array();
        $data['shapelist'] = DB::table('advt_shape')->pluck('shape_name', 'shape_id');
        $data['locationlist'] = DB::table('advt_location')->pluck('loc_name', 'loc_id');
        $data['status'] = Config::get('selecttype.assignmentstatus');
        if ($id) {
            $advtinfo = DB::table('advt_banner')->where('id', @$id)->first();
            $data['advtinfo'] = $advtinfo;
            $data['shapeinfo'] = DB::table('advt_shape')->where('shape_id', @$advtinfo->shape)->first();
        }
        
        return view('vendor.adminlte.advertisement.addadvertisement', $data);
        
    }
    
    public function insertAdvertisement(Request $request) {
        $reqdata = $request->all();
        $advt = $reqdata['advt'];
        $id_update = @$reqdata['id_update'];
        if (!@$id_update) {
		$data_exists = DB::table('advt_banner')
                    ->select('id')
                    ->where('shape', $advt['shape'])
                    ->where('location', $advt['location'])
                    ->count();
        
        if ($data_exists > 0) {
            Session::flash('error_message', "Advertisement already present!.");
            return redirect("/admin/advertisementListing");
        }
		}
		
        $hidden_img = @$reqdata['photo_hid'];
        $img_file = $request->file('image');
        if ($img_file) {
            $nameimage = $img_file->getClientOriginalName();
            $nameimage = str_replace(" ", "_", $nameimage);
        }
        
        if (@$id_update) {
            if ($img_file) {
                if (file_exists(public_path('training_image/' . $hidden_img))) {
                    @unlink(public_path('training_image/' . $hidden_img));
                }
                @$name = $id_update . "_" . $nameimage;
                @$cp = $img_file->move(public_path('training_image'), $name);
                $advt['image'] = $name;
            }
            //DB::enableQueryLog(); 
            $id = DB::table('advt_banner')->where('id', $id_update)->update($advt);
//            $query = DB::getQueryLog();
//        print "<pre>";
//        print_r($query);
//        exit;
            Session::flash('success_message', "Advertisement location successfully updated.");
            return redirect("/admin/advertisementListing");
            
        } else {
            $advt['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $advt['protocol'] = $_SERVER['REMOTE_ADDR'];
            
            $id = DB::table('advt_banner')->insertGetId($advt);
            if ($img_file) {
                    @$name = $id . "_" . $nameimage;
                    @$cp = $img_file->move(public_path('training_image'), $name);
                    DB::table('advt_banner')->where('id', $id)->update(array("image" => $name));
                }
            if ($id) {
                Session::flash('success_message', "Advertisement posted Successfully!.");
                return redirect("/admin/advertisementListing");
            } else {
                Session::flash('error_message', "Failure in insert!");
                return redirect("/admin/advertisementListing");
            }
        }
    }
    
    public function advertisementListing($id = null) {
        $data = array();
        $data['shapelist'] = DB::table('advt_shape')->pluck('shape_name', 'shape_id');
        $data['locationlist'] = DB::table('advt_location')->pluck('loc_name', 'loc_id');
        $data['status'] = Config::get('selecttype.assignmentstatus');
        $advtlist = DB::table('advt_banner')->orderBy('id', 'DESC')->paginate(10);
        $data['advtlist'] = $advtlist;
        
        
        return view('vendor.adminlte.advertisement.advertisementlisting', $data);
        
    }
    
    public function deleteAdvertisement(Request $request) {
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            DB::table('advt_banner')->where('id', $id)->delete();

            print "Success";
        } else {
            print "Error";
        }
        exit();
    }
    
    public function changeNote(Request $request) {
        $reqdata = $request->all();
        $shape = $reqdata['shape'];
        if ($shape) {
            $data['shapelist'] = DB::table('advt_shape')->where('shape_id', $shape)->first();
            
           // echo "NOTE:Banner size is ".$shapelist->width." * ".$shapelist->height;
        }
        return view('vendor.adminlte.advertisement.changenote', $data);
    }
    
    public function queryListing() {
        $querypostinfo = DB::table('query')->orderBy('query_id', 'DESC')->paginate(10);
        $data['querypostinfo'] = $querypostinfo;
        $data['codeDetail'] = DB::table('international_standards_section')->pluck("name", "id");
        $data['codetype'] = Config::get('selecttype.codetype');
        $data['status'] = Config::get('selecttype.assignmentstatus');
        $data['country'] = DB::table('countries')->pluck("name", "id");
        $data['state'] = DB::table('states')->pluck("name", "id");
        $userdetails = DB::table('users')->get();
        foreach($userdetails as $key => $val)
        {
        $data['userdetails'][$val->id] = $val->fname." ".$val->lname;
        }
        return view('vendor.adminlte.query.querypostlist', $data);
    }
    
    //subscription package
    public function taskcreditListing() {
        $data = array();
        return view('vendor.adminlte.subscription.taskcreditlisting', $data);
    }
    
    public function addTaskcredit() {
        $data = array();
        $data['taskcreditlist'] = TaskCredit::where('status', 1)->get();
        return view('vendor.adminlte.subscription.addtaskcredit', $data);
    }
    
    public function insertCreditTask(Request $request) {
        $reqdata = $request->all();
        $updatlen = 0;
        $task_name = $reqdata['task_name'];
        $task_point = $reqdata['task_point'];
        $id_update = @$reqdata['id_update'];
//        if (array_key_exists('Query Posting', $task_name)) {
//            $updatlen = sizeof($id_update);
//        }
        if (!empty($id_update)) {
            $updatlen = count($id_update);
        }
        $tasklength = count($task_name);
        for ($i = 0; $i < $tasklength; $i++) {
            if ($updatlen > 0) {
                $task_ins['task_name'] = $task_name[$i];
                $task_ins['task_point'] = $task_point[$i];
                $task_id = $id_update[$i];
                $update_point = TaskCredit::where("id", $task_id)->update($task_ins);
            } else {
                $task_ins['task_name'] = $task_name[$i];
                $task_ins['task_point'] = $task_point[$i];
                $task_ins['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $task_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
                $task_id = TaskCredit::insertGetId($task_ins);
            }
        }
        if (@$task_id || @$update_point) {
            Session::flash('success_message', "Task Credit info successfully saved.");
            return redirect("/admin/addTaskcredit");
        } else {
            Session::flash('error_message', "Failure in save");
            return redirect("/admin/addTaskcredit");
        }
    }
    
    public function addPackage($id = null) {
        $data = array();
        if ($id) {
            $packageinfo = SubscriptionPackage::where('id', @$id)->first();
            $data['packageinfo'] = $packageinfo;
        }
        $data['package_list'] = SubscriptionPackage::where('status', 1)->orderBy('package_price', 'ASC')->get();
        return view('vendor.adminlte.subscription.addPackage', $data);
    }
    
    public function insertPackage(Request $request) {
        $reqdata = $request->all();
        $package_ins = $reqdata['package'];
        $id_update = @$reqdata['id_update'];
        if ($id_update) {
            $package_upd = SubscriptionPackage::where('id', $id_update)->update($package_ins);
        } else {
            $package_ins['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $package_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
            $package_id = SubscriptionPackage::insertGetId($package_ins);
        }
        if (@$package_id || @$package_upd) {
            Session::flash('success_message', "Package successfully saved.");
            return redirect("/admin/addPackage");
        } else {
            Session::flash('error_message', "Failure in save");
            return redirect("/admin/addPackage");
        }
    }
    
    public function deletePackage(Request $request) {
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            SubscriptionPackage::where('id', $id)->delete();
            print "Success";
        } else {
            print "Error";
        }
        exit();
    }

}
