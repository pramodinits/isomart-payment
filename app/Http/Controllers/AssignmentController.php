<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Session;
use Mail;
use Config;
use App\AssignmentAudit;
use App\AssignmentTraining;
use App\AssignmentReport;
use App\AssignmentCertification;
use App\Assignment;
use App\SendProposal;
use App\AssignmentLocation;
use App\EmailNotification;
use Carbon\Carbon;
use App\Http\Controllers\NotificationController;

class AssignmentController extends UserController {

    private $NotificationController;
    private $SubscriptionController;

    public function __construct() {
        //$this->middleware('auth');
        $this->NotificationController = new NotificationController();
        $this->SubscriptionController = new SubscriptionController();
        $action = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);
    }

    public function requestassignment($assignment_id = null) {
        if (@Auth::user()->id) {
            $prefix = DB::getTablePrefix();
            if ($assignment_id) {
                $assignmentType = Assignment::select('assignment_type')->where('id', $assignment_id)->first();
                if ($assignmentType->assignment_type == 1) {
                    $auditdetails = AssignmentAudit::where('assignment_id', $assignment_id)->first();
                    $locationname = DB::table('assignment_location')
                                    ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                                    ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                                    ->join('states', 'states.id', '=', 'assignment_location.state_id')
                                    ->where('assignment_location.assignment_id', $assignment_id)->get();
                    $data['locationname'] = $locationname;
                    $data['codeDetail'] = DB::table('international_standards_section')->where('id', $auditdetails->sic_code)->first();
                    $data['auditdetails'] = $auditdetails;
                } else if ($assignmentType->assignment_type == 2) {
                    $trainingdetails = AssignmentTraining::where('assignment_id', $assignment_id)->first();
                    if ($trainingdetails->mode == 1) {
                        $locationname = DB::table('assignment_location')
                                        ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                                        ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                                        ->join('states', 'states.id', '=', 'assignment_location.state_id')
                                        ->where('assignment_location.assignment_id', $assignment_id)->get();
                        $data['locationname'] = $locationname;
                    } else {
                        $data['locationname'] = "";
                    }
                    $data['trainingdetails'] = $trainingdetails;
                }
            }

            $data['assignmenttype'] = Config::get('selecttype.assignmentypelist');
            $data['trainingtypelist'] = Config::get('selecttype.trainingtypelist');
            $data['trainingmode'] = Config::get('selecttype.trainingmode');
            $data['status'] = Config::get('selecttype.assignmentstatus');
            $data['codetype'] = Config::get('selecttype.codetype');
            $data['allowance'] = Config::get('selecttype.allowance');
            $data['standardlist'] = Config::get('standards.standardlist');
            $data['countryList'] = DB::table('countries')->orderBy('name', 'ASC')->get();
            if (@Auth::user()->usertype == 6 || @Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) {
                return view('pages.assignment.requestassignment', $data);
            } else {
                return redirect('/missing');
            }
        } else {
            return redirect("/signin");
        }
    }

    public function searchContent(Request $request) {
        $reqdata = $request->all();
        $search['searchValue'] = @$reqdata['searchValue'];
        $search['tabActive'] = @$reqdata['tabActive'];
        $search['selectSearch'] = @$reqdata['selectSearch'];
        if ($search['selectSearch'] == 1) {
            $data = $this->searchAssignment($search);
            return view('pages.assignment.assignmentlist', $data);
        } else if ($search['selectSearch'] == 2) {
            $data = $this->searchQuery($search);
            return view('pages.query.querypostlist', $data);
        }
    }

    private function searchAssignment($search) {
        $searchValue = @$search['searchValue'];
        $tabActive = @$search['tabActive'];
        $prefix = DB::getTablePrefix();
        $arraysearch = array();
        //get audit info
        $resaudit = Assignment::with('AssignmentLocation')
                        ->join('assignment_request_audit', DB::raw($prefix . 'assignment_request_audit.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                        ->select("assignment_request_audit.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                        ->where('approval_status', '!=', 2)->where('close_job', '=', 0)->where('status', '=', 1);

        if ($searchValue && $tabActive == 1) {
            $arraysearch = array(array("assignment_name", "LIKE", "%$searchValue%"));
            $arraysearchdesc = array(array("description", "LIKE", "%$searchValue%"));
            $resaudit->where($arraysearch)->orWhere($arraysearchdesc);
            Session::put('tabActive', "1");
        }
        $assignmentaudit = $resaudit->orderBy('assignment.id', 'DESC')->get();

        //get training info
        $restraining = Assignment::with('AssignmentLocation')
                        ->join('assignment_request_training', DB::raw($prefix . 'assignment_request_training.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                        ->select("assignment_request_training.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                        ->where('approval_status', '!=', 2)->where('close_job', '=', 0)->where('status', '=', 1);

        if ($searchValue && $tabActive == 2) {
            $arraysearch = array(array("session_details", "LIKE", "%$searchValue%"));
            $restraining->where($arraysearch);
            Session::put('tabActive', "2");
        }
        $assignmenttraining = $restraining->orderBy('assignment.id', 'DESC')->get();

        $data['tabActive'] = Session::get('tabActive', 1);
        $data['searchInput'] = $searchValue;
        $data['assignmentaudit'] = $assignmentaudit;
        $data['assignmenttraining'] = $assignmenttraining;
        $data['selectSearch'] = 1;
        return $data;
    }

    private function searchQuery($search) {
        $searchValue = @$search['searchValue'];
        $resquery = DB::table('query')->where('status', '=', 1);
        if ($searchValue) {
            $arraysearch = array(array("title", "LIKE", "%$searchValue%"));
            $arraysearchdesc = array(array("description", "LIKE", "%$searchValue%"));
            $resquery->where($arraysearch)->orWhere($arraysearchdesc);
        }
        $querypostinfo = $resquery->orderBy('query_id', 'DESC')->get();
        $data['querypostinfo'] = $querypostinfo;
        $data['country'] = DB::table('countries')->pluck("name", "id");
        $data['state'] = DB::table('states')->pluck("name", "id");
        $data['searchInput'] = $searchValue;
        $data['selectSearch'] = 2;
        return $data;
    }

    public function assignmentlist(Request $request) {
        //Search function
        $reqdata = $request->all();
        $searchValue = @$reqdata['searchValue'];
        $tabActive = @$reqdata['tabActive'];
        $prefix = DB::getTablePrefix();
        $arraysearch = array();
        //get audit info

        $resaudit = Assignment::with('AssignmentLocation')
                        ->join('assignment_request_audit', DB::raw($prefix . 'assignment_request_audit.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                        ->select("assignment_request_audit.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                        ->where('approval_status', '!=', 2)->where('close_job', '=', 0)->where('status', '=', 1);

        if ($searchValue && $tabActive == 1) {
            $arraysearch = array(array("assignment_name", "LIKE", "%$searchValue%"));
            $arraysearchdesc = array(array("description", "LIKE", "%$searchValue%"));
            $resaudit->where($arraysearch)->orWhere($arraysearchdesc);
            Session::put('tabActive', "1");
        }
        $assignmentaudit = $resaudit->orderBy('assignment.id', 'DESC')->get();
        //get audit info
        //get training info
        $restraining = Assignment::with('AssignmentLocation')
                        ->join('assignment_request_training', DB::raw($prefix . 'assignment_request_training.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                        ->select("assignment_request_training.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                        ->where('approval_status', '!=', 2)->where('close_job', '=', 0)->where('status', '=', 1);

        if ($searchValue && $tabActive == 2) {
            $arraysearch = array(array("session_details", "LIKE", "%$searchValue%"));
            $restraining->where($arraysearch);
            Session::put('tabActive', "2");
        }
        $assignmenttraining = $restraining->orderBy('assignment.id', 'DESC')->get();
        //get training info

        if ($searchValue && $tabActive == 3) {
            $assignmentcertification = Assignment::leftJoin('assignment_request_certification', function($join) {
                                $join->on('assignment.id', '=', 'assignment_request_certification.assignment_id');
                            })
                            ->where('assignment.assignment_type', '3')
                            ->where("assignment.approval_status", '!=', 2)
                            ->where('assignment_request_certification.assignment_name', 'like', '%' . $searchValue . '%')
                            ->orWhere('assignment_request_certification.description', 'like', '%' . $searchValue . '%')->get();

            Session::put('tabActive', "3");
        } else {
            $assignmentcertification = Assignment::leftJoin('assignment_request_certification', function($join) {
                        $join->on('assignment.id', '=', 'assignment_request_certification.assignment_id');
                    })->where('assignment.assignment_type', '3')->orderBy('assignment.id', 'DESC')->get();
        }
        $data['tabActive'] = Session::get('tabActive', 1);
        $data['searchInput'] = $searchValue;
        $data['assignmentaudit'] = $assignmentaudit;
        $data['assignmenttraining'] = $assignmenttraining;
        $data['assignmentcertification'] = $assignmentcertification;
        return view('pages.assignment.assignmentlist', $data);
    }

    public function allassignment(Request $request) {
        $prefix = DB::getTablePrefix();
        $data['assignmentlistaudit'] = array();
        $data['assignmentlisttraining'] = array();
        $assignmentlistaudit = Assignment::with('AssignmentLocation')
                        ->join('assignment_request_audit', DB::raw($prefix . 'assignment_request_audit.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                        ->select("assignment_request_audit.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                        ->where('assignment.user_id', @Auth::user()->id)->where('assignment.approval_status', '==', 0)
                        ->where('assignment.close_job', '=', 0)->orderBy('assignment.id', 'DESC')->get();

        $assignmentlisttraining = Assignment::with('AssignmentLocation')
                        ->join('assignment_request_training', DB::raw($prefix . 'assignment_request_training.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                        ->select("assignment_request_training.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                        ->where('assignment.user_id', @Auth::user()->id)->where('assignment.approval_status', '==', 0)
                        ->where('assignment.close_job', '=', 0)->orderBy('assignment.id', 'DESC')->get();

//        $assignmentlisttraining = Assignment::with('assignmenttraining')->where('approval_status', '==', 0)
//                        ->where('user_id', @Auth::user()->id)->orderBy('id', 'DESC')->get();

        $assignmentcertification = Assignment::with('assignmentcertification')
                        ->where('user_id', @Auth::user()->id)
                        ->where('approval_status', '==', 0)->orderBy('id', 'DESC')->get();
        //get cities
        $id_cities = $cityNameArray = "";
        foreach ($assignmentcertification as $k => $v) {
            foreach ($v->assignmentcertification as $key => $val) {
                @$id_cities .= $val->location . ",";
            }
        }
        $id_cities = rtrim($id_cities, ",");
        if ($id_cities) {
            $id_citiesArray = explode(",", $id_cities);
            $cityNameArray = DB::table('cities')->whereIn('id', $id_citiesArray)->pluck("name", "id");
            $data['cityNameArray'] = $cityNameArray;
        }
        //get cities
        //active tab usertype validation
//        if (@Auth::user()->usertype == 6) {
//            $data['tabActive'] = Session::get('tabActive', 1);
//        } else if (@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) {
//            $data['tabActive'] = Session::get('tabActive', 2);
//        } 
//        else if (@Auth::user()->usertype == 5 || @Auth::user()->usertype == 6 || @Auth::user()->usertype == 7) {
//            $data['tabActive'] = Session::get('tabActive', 2);
//        }
        //active tab usertype validation
        $data['tabActive'] = Session::get('tabActive', 1);
        $data['assignmentlistaudit'] = $assignmentlistaudit;
        $data['assignmentlisttraining'] = $assignmentlisttraining;
        $data['assignmentcertification'] = $assignmentcertification;
        $data['selectmenu'] = "myassignment";
        return view('pages.assignment.allassignment', $data);
    }

    public function assignmentrequest(Request $request) {
        $user = UserController::validuser();
        $reqdata = $request->all();
        $assignment = $reqdata['audit'];
        $assignment['start_date'] = Carbon::parse($reqdata['startDate'])->format('Y-m-d');
        $assignment['end_date'] = Carbon::parse($reqdata['endDate'])->format('Y-m-d');
        $assignment['proposal_end_date'] = Carbon::parse($reqdata['proposalEndDate'])->format('Y-m-d');
        $id_user = @$reqdata['user_id'];
        $id_update = @$reqdata['id_update'];
        $assignment_id = @$reqdata['assignment_id'];
        $assignment_type = 1;
        //location info
        $cityname = @$reqdata['cityname'];
        $stateid = @$reqdata['stateid'];
        $countryid = @$reqdata['countryid'];
        $effort = @$reqdata['effort'];
        $delete_location = @$reqdata['delete_location'];
        $citylength = count($cityname);
        $dellength = count($delete_location);
        //location info

        $taskPoints = $this->SubscriptionController->getTaskPoint("Create Assignment");
        $creditPoints = $this->SubscriptionController->getUserCreditPoint(@Auth::user()->id);
        if ($creditPoints < $taskPoints && empty($id_update)) {
            Session::flash('error_message', "You have not sufficient credit points available!");
            return redirect("/requestassignment");
        } else {
            if (@$id_update) {
                if ($dellength) {
                    for ($i = 0; $i < $dellength; $i++) {
                        AssignmentLocation::where('id', $delete_location[$i])->delete();
                    }
                }
                if ($citylength) {
                    for ($i = 0; $i < $citylength; $i++) {
                        $cityId = DB::table('cities')->select("id")->where('name', $cityname[$i])
                                        ->where('country_id', $countryid[$i])->where('region_id', $stateid[$i])->first();
                        if ($cityId) {
                            $location['city_id'] = $cityId->id;
                        }
                        $location['assignment_id'] = $assignment_id;
                        $location['country_id'] = $countryid[$i];
                        $location['state_id'] = $stateid[$i];
                        $location['city_name'] = $cityname[$i];
                        $location['effort'] = $effort[$i];
                        $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                        $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                        AssignmentLocation::create($location);
                    }
                }
                Assignment::where('id', $assignment_id)->update(array("status" => $assignment['assignment_status']));
                $id_upd = AssignmentAudit::where('id', $id_update)->update($assignment);
                if ($assignment['assignment_status'] == 1) {
                    $ref_id = @$reqdata['ref_id'];
                    $this->NotificationController->assignmentNotification($ref_id, $assignment_id, $assignment_type, @$id_update);
                }
            } else {
                //insert to Assignment
                $assignmentParent['user_id'] = @Auth::user()->id;
                $assignmentParent['assignment_type'] = 1;
                $assignmentParent['status'] = $assignment['assignment_status'];
                $assignmentParent['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $assignmentParent['protocol'] = $_SERVER['REMOTE_ADDR'];
                $assignmentId = Assignment::create($assignmentParent);
                $id_assignment = $assignmentId->id;
                //insert to Assignment
                //insert location
                for ($i = 0; $i < $citylength; $i++) {
                    $cityId = DB::table('cities')->select("id")
                                    ->where('name', $cityname[$i])
                                    ->where('country_id', $countryid[$i])
                                    ->where('region_id', $stateid[$i])->first();
                    if ($cityId) {
                        $location['city_id'] = $cityId->id;
                    }
                    $location['assignment_id'] = $id_assignment;
                    $location['country_id'] = $countryid[$i];
                    $location['state_id'] = $stateid[$i];
                    $location['city_name'] = $cityname[$i];
                    $location['effort'] = $effort[$i];
                    $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                    $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                    AssignmentLocation::create($location);
                }
                //insert location

                $assignment['assignment_id'] = $id_assignment;
                $assignment['admin_approval'] = 1;
                $assignment['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $assignment['protocol'] = $_SERVER['REMOTE_ADDR'];

                //update transaction
                $debitPoints = $this->SubscriptionController->updateUserCreditPoint("", @Auth::user()->id, $taskPoints, "");
                if ($debitPoints) {
                    $updateTransaction = $this->SubscriptionController->updateUserTransaction(@Auth::user()->id, "Create Audit Assignment", Carbon::now()->format('Y-m-d'), "", $taskPoints);
                }

                $res = AssignmentAudit::create($assignment);
                $id_ins = $res->id;
                if ($id_ins) {
                    $ref_id = "AU" . date("Y") . date("m") . date("d") . $id_assignment;
                    AssignmentAudit::where('id', $id_ins)->update(array(
                        "ref_id" => $ref_id
                    ));
                    $data_report['user_id'] = @Auth::user()->id;
                    $data_report['assignment_id'] = $id_assignment;
                    $data_report['assignment_type'] = 1;
                    $data_report['status'] = $assignment['assignment_status'];
                    $data_report['description'] = $assignment['description'];
                    $data_report['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                    $data_report['protocol'] = $_SERVER['REMOTE_ADDR'];
                    AssignmentReport::create($data_report);

                    //send notification to bidder
                    if ($assignment['assignment_status'] == 1) {
                        $this->NotificationController->assignmentNotification($ref_id, $id_assignment, $assignment_type);
                    }
                }
            }
            
            //store email notification info
            $emailInfo['assignment_id'] = @$id_update ? $assignment_id : $id_assignment;
            $emailInfo['id_update'] = @$id_update ? @$id_update : "";
            $emailInfo['ref_id'] = $ref_id;
            $emailInfo['assignment_name'] = $assignment['assignment_name'];
            $emailInfo['effort_tentative'] = $assignment['effort_tentative'];
            $emailInfo['budget_usd'] = $assignment['budget_usd'];
            $emailInfo['detail_url'] = asset('assignmentAuditDetail/' . $emailInfo['assignment_id']);
            $this->emailNotificationSave($emailInfo); 
            
            if (@$id_update) {
                if (@$id_upd) { //for update
                    Session::flash('success_message', "Assignment details successfully updated.");
                    return redirect("assignmentAuditDetail/" . $assignment_id);
                } else {
                    Session::flash('error_message', "Failure in update");
                    return redirect("assignmentAuditDetail/" . $assignment_id);
                }
            } else {
                if ($id_ins) { //for insert
                Session::put('tabActive', "1");
                Session::flash('success_message', "Assignment created Successfully!");
                return redirect("/allassignment");
            } else {
                Session::flash('error_message', "Failure in insert!");
                return redirect("/requestassignment");
            }
            }
        }
    }

    public function deleteLocation(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $location_id = $reqdata['location_id'];
        if ($location_id) {
            $location_exist = AssignmentLocation::where('id', $location_id)->first();
            if ($location_id) {
                AssignmentLocation::where('id', $location_id)->delete();
                print "Success";
            } else {
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function requestassignmenttraining(Request $request) {
        $user = UserController::validuser();
        $reqdata = $request->all();
        $training = $reqdata['training'];
        $training['start_date'] = Carbon::parse($reqdata['startDate'])->format('Y-m-d');
        $training['end_date'] = Carbon::parse($reqdata['endDate'])->format('Y-m-d');
        $training['proposal_end_date'] = Carbon::parse($reqdata['proposalEndDate'])->format('Y-m-d');
        $id_user = @$reqdata['user_id'];
        $id_update = @$reqdata['id_update'];
        $assignment_id = @$reqdata['assignment_id'];
        $delete_location = @$reqdata['delete_location'];
        $dellength = count($delete_location);
        $assignment_type = 2;
        //get effort
        if ($training['mode'] == 2) {
            $training['effort_tentative'] = $reqdata['effort_tentative_online'];
        }
        //location info
        $cityname = @$reqdata['cityname'];
        $stateid = @$reqdata['stateid'];
        $countryid = @$reqdata['countryid'];
        $effort = @$reqdata['effort'];
        $citylength = count($cityname);
        //location info
        //check
        $taskPoints = $this->SubscriptionController->getTaskPoint("Create Assignment");
        $creditPoints = $this->SubscriptionController->getUserCreditPoint(@Auth::user()->id);
        if ($creditPoints < $taskPoints && empty($id_update)) {
            Session::flash('error_message', "You have not sufficient credit points available!");
            return redirect("/requestassignment");
        } else {
            if (@$id_update) {
                if ($dellength) {
                    for ($i = 0; $i < $dellength; $i++) {
                        AssignmentLocation::where('id', $delete_location[$i])->delete();
                    }
                }
                if ($citylength) {
                    for ($i = 0; $i < $citylength; $i++) {
                        $location['city_id'] = null;
                        $cityId = DB::table('cities')->select("id")->where('name', $cityname[$i])
                                        ->where('country_id', $countryid[$i])->where('region_id', $stateid[$i])->first();
                        if ($cityId) {
                            $location['city_id'] = $cityId->id;
                        }
                        $location['assignment_id'] = $assignment_id;
                        $location['country_id'] = $countryid[$i];
                        $location['state_id'] = $stateid[$i];
                        $location['city_name'] = $cityname[$i];
                        $location['effort'] = $effort[$i];
                        $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                        $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                        $locid = AssignmentLocation::insertGetId($location);
//                    AssignmentLocation::create($location);
                    }
                }
                Assignment::where('id', $assignment_id)->update(array("status" => $training['assignment_status']));
                $id_upd = AssignmentTraining::where('id', $id_update)->update($training);
                if ($training['assignment_status'] == 1) {
                    $ref_id = @$reqdata['ref_id'];
                    $result = $this->NotificationController->assignmentNotification($ref_id, $assignment_id, $assignment_type, @$id_update);
                }
            } else {
                //insert to Assignment
                $assignmentParent['user_id'] = @Auth::user()->id;
                $assignmentParent['assignment_type'] = 2;
                $assignmentParent['status'] = $training['assignment_status'];
                $assignmentParent['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $assignmentParent['protocol'] = $_SERVER['REMOTE_ADDR'];
                $assignmentId = Assignment::create($assignmentParent);
                $id_assignment = $assignmentId->id;
                //insert to Assignment
                //insert location
                for ($i = 0; $i < $citylength; $i++) {
                    $location['city_id'] = null;
                    $cityId = DB::table('cities')->select("id")
                                    ->where('name', $cityname[$i])
                                    ->where('country_id', $countryid[$i])
                                    ->where('region_id', $stateid[$i])->first();
                    if ($cityId) {
                        $location['city_id'] = $cityId->id;
                    }
                    $location['assignment_id'] = $id_assignment;
                    $location['country_id'] = $countryid[$i];
                    $location['state_id'] = $stateid[$i];
                    $location['city_name'] = $cityname[$i];
                    $location['effort'] = $effort[$i];
                    $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                    $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                    AssignmentLocation::create($location);
                }
                //insert location
                $training['assignment_id'] = $id_assignment;
                $training['admin_approval'] = 1;
                $training['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $training['protocol'] = $_SERVER['REMOTE_ADDR'];

                //update transaction
                $debitPoints = $this->SubscriptionController->updateUserCreditPoint("", @Auth::user()->id, $taskPoints, "");
                if ($debitPoints) {
                    $updateTransaction = $this->SubscriptionController->updateUserTransaction(@Auth::user()->id, "Create Training Assignment", Carbon::now()->format('Y-m-d'), "", $taskPoints);
                }

                $res = AssignmentTraining::create($training);
                $id = $res->id;
                if ($id) {
                    $ref_id = "TR" . date("Y") . date("m") . date("d") . $id_assignment;
                    AssignmentTraining::where('id', $id)->update(array(
                        "ref_id" => $ref_id
                    ));
                    $data_report['user_id'] = @Auth::user()->id;
                    $data_report['assignment_id'] = $id_assignment;
                    $data_report['assignment_type'] = 2;
                    $data_report['status'] = $training['assignment_status'];
                    $data_report['description'] = $training['session_details'];
                    $data_report['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                    $data_report['protocol'] = $_SERVER['REMOTE_ADDR'];
                    AssignmentReport::create($data_report);
                    
                    if ($training['assignment_status'] == 1) {
                        $this->NotificationController->assignmentNotification($ref_id, $id_assignment, $assignment_type);
                    }
                }
            }
            //store email notification info
            $emailInfo['assignment_id'] = @$id_update ? $assignment_id : $id_assignment;
            $emailInfo['id_update'] = @$id_update ? @$id_update : "";
            $emailInfo['ref_id'] = $ref_id;
            $emailInfo['assignment_name'] = $training['batch_no'];
            $emailInfo['effort_tentative'] = $training['effort_tentative'];
            $emailInfo['budget_usd'] = $training['fee_range'];
            $emailInfo['detail_url'] = asset('assignmentTrainingDetail/' . $emailInfo['assignment_id']);
            $this->emailNotificationSave($emailInfo); 
            
            if (@$id_update) { //for update
                if (@$id_upd || @$locid) {
                    Session::flash('success_message', "Assignment details successfully updated.");
                    return redirect("/assignmentTrainingDetail/" . $emailInfo['assignment_id']);
                } else {
                    Session::flash('error_message', "Failure in update");
                    return redirect("/requestassignment");
                }
            } else { //for insert
                if (@$id) {
                    Session::put('tabActive', "2");
                    Session::flash('success_message', "Assignment created Successfully!");
                    return redirect("/allassignment");
                } else {
                    Session::flash('error_message', "Failure in insert!");
                    return redirect("/requestassignment");
                }
            }
           
        }
    }

    public function emailNotificationSave($emailInfo) {
        $ref_id = $emailInfo['ref_id'];
        $assignment_name = $emailInfo['assignment_name'];
        $effort_tentative = $emailInfo['effort_tentative'];
        $budget_usd = $emailInfo['budget_usd'];
        $detail_url = $emailInfo['detail_url'];
        $id_update = $emailInfo['id_update'];
        if (@$id_update) {
            $msgbody = "<p>Assignment $ref_id has been updated by the owner.</p>"
                    . "<p>The details are as follows: <br/>"
                    . "<strong>Assignment Details</strong><br/>"
                    . "Assignment Name - " . $assignment_name . "<br/>"
                    . "Effort - " . $effort_tentative . "<br/>"
                    . "Budget - " . $budget_usd . "</p>"
                    . "<p>Please click <a href='$detail_url'>here</a> to view the details and bid for the same</p><br />"
                    . "Thanks,<br />ISOMart Team";
        } else {
            $msgbody = "<p>A new assignment has been posted that matches your industry sector or location.</p>"
                    . "<p>The details are as follows: <br/>"
                    . "<strong>Assignment Details</strong><br/>"
                    . "Assignment Name - " . $assignment_name . "<br/>"
                    . "Assignment Ref ID - $ref_id<br/>"
                    . "Effort - " . $effort_tentative . "<br/>"
                    . "Budget - " . $budget_usd . "</p>"
                    . "<p>Please click <a href='$detail_url'>here</a> to view the details and bid for the same</p>"
                    . "Thanks,<br />ISOMart Team";
        }

        $providerList = DB::table('users')->select('email', 'fname', 'lname')->whereIn('usertype', array(4, 8))->where('status', 1)->get();
        foreach ($providerList as $k => $v) {
            $ins_email['email_type'] = @$id_update ? 2 : 1; //2->update, 1->insert
            $ins_email['email_to'] = $v->email;
            $ins_email['email_name'] = $v->fname . " " . $v->lname;
            $ins_email['email_subject'] = @$id_update ? "Assignment $ref_id Updated" : "New ISO Assignment";
            $ins_email['email_message'] = "<p>Hello " . $ins_email['email_name'] . "</p>" . $msgbody;
            $ins_email['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $ins_email['protocol'] = $_SERVER['REMOTE_ADDR'];
            EmailNotification::insertGetId($ins_email);
        }
    }
    

    public function assignmentAuditDetail($assignment_id) {
        $prefix = DB::getTablePrefix();
        $codematch = 0;
        $auditdetails = AssignmentAudit::
                        select("assignment_request_audit.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment_request_audit.assignment_id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment_request_audit.assignment_id ) as jobEffort"))
                        ->where('assignment_request_audit.assignment_id', $assignment_id)->first();
        //get city name
        $locationname = DB::table('assignment_location')
                        ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                        ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                        ->join('states', 'states.id', '=', 'assignment_location.state_id')
                        ->where('assignment_location.assignment_id', $assignment_id)->get();
        $data['locationname'] = $locationname;
        //get city name
        //standard details
        $standardDeatil = DB::table('international_standards_section')->where('id', $auditdetails->sic_code)->first();
        $data['standardDeatil'] = $standardDeatil;
        //standard details
        //send proposal by standard & location
        $data['countisicCode'] = DB::table('users_isic_code')->where('user_id', @Auth::user()->id)->count();
        $getisicCode = DB::table('users_isic_code')
                        ->join('international_standards_section', 'users_isic_code.code_sector', '=', 'international_standards_section.id')
                        ->select('users_isic_code.*', 'international_standards_section.name as codeName', 'international_standards_section.type', 'international_standards_section.code')
                        ->where('users_isic_code.user_id', @Auth::user()->id)->get();

        foreach ($getisicCode as $k => $v) {
            $rescodematch = DB::table('international_standards_mapping')
                            ->where($v->type, $v->code)->where($standardDeatil->type, $standardDeatil->code)->first();
            if ($rescodematch) {
                $codematch++;
            }
        }
        if (@Auth::user()->id) {
            $data['codeMapping'] = $codematch;
        } else {
            $data['codeMapping'] = "";
        }
        $data['standard'] = DB::table('users_isic_code')->where('user_id', @Auth::user()->id)
                        ->where('code_sector', @$auditdetails->sic_code)->count();
        //send proposal by standard & location (no address validation for now)
        //get assignment creator & assignment approval info
        $userdetails = Assignment::select('users.fcm_device_token', 'users.fname', 'users.lname', 'users.id', 'users.email', 'assignment.approval_status', 'assignment.user_id', 'assignment.assignment_type')
                        ->leftJoin('users', function($join) {
                            $join->on('assignment.user_id', '=', 'users.id');
                        })
                        ->where("assignment.id", $auditdetails->assignment_id)->get();
        //get assignment creator & assignment approval info
        //get bid lists by bid sender & assignment creator
        if (@$userdetails[0]->user_id == @Auth::user()->id) { //get bid list by assignment creator id
            $proposaldetails = DB::table('send_proposal')
                            ->select('users.fname', 'users.lname', 'users.profile_img', 'users.id AS userId', 'users.gender', 'send_proposal.*')
                            ->join('users', 'send_proposal.user_id', '=', 'users.id')
                            ->where('send_proposal.assignment_id', $assignment_id)->get();
        } else if (@Auth::user()->id) { //get bid list by bid sender id
            $proposaldetails = DB::table('send_proposal')
                            ->select('users.fname', 'users.lname', 'users.profile_img', 'users.id AS userId', 'users.gender', 'send_proposal.*')
                            ->join('users', 'send_proposal.user_id', '=', 'users.id')->where('send_proposal.assignment_id', $assignment_id)
                            ->where('send_proposal.approval_status', '!=', 3)->where('send_proposal.user_id', @Auth::user()->id)->get();
        } else {
            $proposaldetails = array();
        }
        //get bid lists by bid sender & assignment creator
        //get count of total list
        $data['countTotalBid'] = SendProposal::where('assignment_id', '=', $assignment_id)->count();
        //get count of total list
        //get proposal data using Auth ID
        $proposalEdit = SendProposal::where('user_id', @Auth::user()->id)->where('approval_status', '!=', 3)
                        ->where('assignment_id', '=', $assignment_id)->first();
//                        ->where('approval_status', '!=', 2)
        $data['proposalEdit'] = $proposalEdit;
        //get proposal data using Auth ID

        $data['auditdetails'] = $auditdetails;
        $data['userdetails'] = $userdetails;
        //$data['awarddetails'] = $awarddetails; //TODO : get awarded user info as assignment assigned to multiple auditor
        $data['proposaldetails'] = $proposaldetails;
        return view('pages.assignment.assignmentauditdetail', $data);
    }

    public function assignmentTrainingDetail($assignment_id) {
        $prefix = DB::getTablePrefix();
        $trainingdetails = AssignmentTraining::
                        select("assignment_request_training.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment_request_training.assignment_id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment_request_training.assignment_id ) as jobEffort"))
                        ->where('assignment_request_training.assignment_id', $assignment_id)->first();

        //get city name
        if ($trainingdetails->mode == 1) {
            $locationname = DB::table('assignment_location')
                            ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                            ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                            ->join('states', 'states.id', '=', 'assignment_location.state_id')
                            ->where('assignment_location.assignment_id', $assignment_id)->get();
            $data['locationname'] = $locationname;
        }
        //get city name
        //get assignment creator & assignment approval info
        $userdetails = Assignment::select('users.fcm_device_token', 'users.fname', 'users.lname', 'users.id', 'users.email', 'assignment.approval_status', 'assignment.user_id', 'assignment.assignment_type')
                        ->leftJoin('users', function($join) {
                            $join->on('assignment.user_id', '=', 'users.id');
                        })
                        ->where("assignment.id", $trainingdetails->assignment_id)->get();
        //get assignment creator & assignment approval info
        //get bid lists by bid sender & assignment creator
        if (@$userdetails[0]->user_id == @Auth::user()->id) { //get bid list by assignment creator id
            $proposaldetails = DB::table('send_proposal')
                            ->select('users.fname', 'users.lname', 'users.profile_img', 'users.id AS userId', 'users.gender', 'send_proposal.*')
                            ->join('users', 'send_proposal.user_id', '=', 'users.id')
                            ->where('send_proposal.assignment_id', $assignment_id)->get();
        } else if (@Auth::user()->id) { //get bid list by bid sender id
            $proposaldetails = DB::table('send_proposal')
                            ->select('users.fname', 'users.lname', 'users.profile_img', 'users.id AS userId', 'users.gender', 'send_proposal.*')
                            ->join('users', 'send_proposal.user_id', '=', 'users.id')->where('send_proposal.assignment_id', $assignment_id)
                            ->where('send_proposal.approval_status', '!=', 3)->where('send_proposal.user_id', @Auth::user()->id)->get();
        } else {
            $proposaldetails = array();
        }
        //get bid lists by bid sender & assignment creator
        //get count of total list
        $data['countTotalBid'] = SendProposal::where('assignment_id', '=', $assignment_id)->count();
        //get count of total list
        //get proposal data using Auth ID
        $proposalEdit = SendProposal::where('user_id', @Auth::user()->id)->where('approval_status', '!=', 3)
                        ->where('assignment_id', '=', $assignment_id)->first();
//                        ->where('approval_status', '!=', 2)
        $data['proposalEdit'] = $proposalEdit;
        //get proposal data using Auth ID

        $data['trainingdetails'] = $trainingdetails;
        $data['userdetails'] = $userdetails;
        $data['proposaldetails'] = $proposaldetails;
        return view('pages.assignment.assignmenttrainingdetail', $data);
    }

    public function checkRespondBid(Request $request) {
        $reqdata = $request->all();
        $assignment_id = $reqdata['assignmentid'];
        $getDates = AssignmentAudit::select('start_date', 'end_date')->where('assignment_id', $assignment_id)->first();
        if ($getDates) {
            $getAcceptedBid = SendProposal::select('send_proposal.user_id', 'assignment_request_audit.assignment_id')
                    ->join('assignment_request_audit', 'assignment_request_audit.assignment_id', '=', 'send_proposal.assignment_id')
                    ->where('send_proposal.user_id', @Auth::user()->id)->where('send_proposal.approval_status', '!=', 2)
                    ->where('send_proposal.endjob_status', '=', 0)
                    ->where('assignment_request_audit.start_date', '>=', $getDates->start_date)
                    ->where('assignment_request_audit.end_date', '<=', $getDates->end_date)
                    ->get();
            $getBidCount = count($getAcceptedBid);
            if ($getBidCount > 0) {
                print "success";
                exit;
            } else {
                print "failure";
                exit;
            }
        } else {
            print "failure";
            exit;
        }
    }

    public function sendproposal(Request $request) {
        $user = UserController::validuser();
        $reqdata = $request->all();
        $ins_proposal = $reqdata['proposal'];
        $locid = @$reqdata['locationId'];
        if ($locid) {
            $ins_proposal['assignment_location_id'] = implode(",", @$reqdata['locationId']);
        }
        $proposal_id = @$reqdata['proposal_id'];
        $assignment_type = @$reqdata['assignment_type'];

        //check credit point
        $taskPoints = $this->SubscriptionController->getTaskPoint("Send Proposal");
        $creditPoints = $this->SubscriptionController->getUserCreditPoint(@Auth::user()->id);
        if ($creditPoints < $taskPoints && empty($proposal_id)) {
            Session::flash('error_message', "You have not sufficient credit points available!");
            if ($assignment_type == 1) {
                return redirect("/assignmentAuditDetail/" . $ins_proposal['assignment_id']);
            } else if ($assignment_type == 2) {
                return redirect("/assignmentTrainingDetail/" . $ins_proposal['assignment_id']);
            }
        } else {
            if ($proposal_id) {
                SendProposal::where('id', $proposal_id)->update($ins_proposal);
                Session::flash('success_message', "Bid updated successfully!");
                if ($assignment_type == 1) {
                    return redirect("/assignmentAuditDetail/" . $ins_proposal['assignment_id']);
                } else if ($assignment_type == 2) {
                    return redirect("/assignmentTrainingDetail/" . $ins_proposal['assignment_id']);
                }

                //TODO: send emil in update
            } else {
                //in rejection case overlap existing bid
                $bid_exists = SendProposal::select('id')->where('user_id', @Auth::user()->id)->where('approval_status', '=', 2)
                                ->where('assignment_id', $ins_proposal['assignment_id'])->first();
                if (count($bid_exists) > 0) {
                    SendProposal::where('user_id', @Auth::user()->id)->where('id', $bid_exists->id)->delete();
                }
                //in rejection case overlap existing bid
                $ins_proposal['user_id'] = @Auth::user()->id;
                $ins_proposal['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
                $ins_proposal['protocol'] = $_SERVER['REMOTE_ADDR'];

                //update transaction
                $debitPoints = $this->SubscriptionController->updateUserCreditPoint("", @Auth::user()->id, $taskPoints, "");
                if ($debitPoints) {
                    $updateTransaction = $this->SubscriptionController->updateUserTransaction(@Auth::user()->id, "Send Proposal", Carbon::now()->format('Y-m-d'), "", $taskPoints);
                }
                //update transaction
                $res = SendProposal::create($ins_proposal);
                $id = $res->id;
                if ($id) {
                    //send mail to seeker
                    $mail['fname'] = $reqdata['fname'];
                    $mail['lname'] = $reqdata['lname'];
                    $mail['email'] = $reqdata['email'];
                    $mail['ref_id'] = $reqdata['ref_id'];
                    $mail['assignment_title'] = $reqdata['assignment_name'];
                    $mail['assignment_id'] = $ins_proposal['assignment_id'];
                    $mail['bid_price'] = $ins_proposal['estimated_budget'];
                    $mail['bid_effort'] = $ins_proposal['estimated_effort'];
                    $mail['subject'] = "New Response To Your Assignment " . $reqdata['ref_id'];
                    $mail['assignment_type'] = $assignment_type;
                    $this->proposalEmailSeeker($mail);
                    //send mail to seeker
                    //notification send to seeker
                    $notification['assignment_id'] = $ins_proposal['assignment_id']; //Proposal ID
                    if ($assignment_type = 1) {
                        $notificationAction = "audit_send_proposal";
                    } else {
                        $notificationAction = "training_send_proposal";
                    }
                    $notification['assignment_title'] = $reqdata['assignment_name'];
                    $notification['bid_price'] = $ins_proposal['estimated_budget'];
                    $notification['bid_effort'] = $ins_proposal['estimated_effort'];
                    $notification['action'] = $notificationAction; //Notification action
                    $body = json_encode($notification);
                    $msg = array(
                        'header' => "New Response To Your Assignment " . $reqdata['ref_id'],
                        'text' => "We have received a new response to your assignment " . $reqdata['ref_id']
                    );
                    $message = json_encode($msg);
                    $fcmdevice_token = $reqdata['fcm_device_token'];
                    $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);

                    Session::flash('success_message', "Bid sent successfully!");
                    if ($assignment_type == 1) {
                        return redirect("/assignmentAuditDetail/" . $ins_proposal['assignment_id']);
                    } else if ($assignment_type == 2) {
                        return redirect("/assignmentTrainingDetail/" . $ins_proposal['assignment_id']);
                    }
                } else {
                    Session::flash('error_message', "Failure in Bid sending!");
                    return redirect("/assignmentAuditDetail/" . $ins_proposal['assignment_id']);
                }
            }
        }
    }

    public function proposalEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $messageBody = view('email.proposalSendEmail', $data);
        $a = Mail::send('email.proposalSendEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function proposalEmailProvider($data) {
        $toemail = @Auth::user()->email;
        $toname = @Auth::user()->fname . " " . @Auth::user()->lname;
        $data['fname'] = @Auth::user()->fname;
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $proposaltype = $data['proposaltype'];
        $messageBody = view('email.proposalSendEmail', $data);
        $a = Mail::send('email.proposalSendEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function allproposal() {
        UserController::validuser();
        $data['proposalList'] = array();
        $proposalListAudit = DB::table('assignment_request_audit')
                ->select('send_proposal.estimated_effort', 'send_proposal.estimated_budget', 'assignment_request_audit.ref_id', 'assignment_request_audit.*', 'assignment.approval_status')
                ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment_request_audit.assignment_id')
                ->join('assignment', 'assignment.id', '=', 'assignment_request_audit.assignment_id')
                ->where('send_proposal.user_id', @Auth::user()->id)
                ->where('send_proposal.approval_status', '!=', 1)->where('assignment.approval_status', '=', 0)
                ->get();
        $proposalLisTraining = DB::table('assignment_request_training')
                ->select('send_proposal.estimated_effort', 'send_proposal.estimated_budget', 'assignment_request_training.ref_id', 'assignment_request_training.*', 'assignment.approval_status')
                ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment_request_training.assignment_id')
                ->join('assignment', 'assignment.id', '=', 'assignment_request_training.assignment_id')
                ->where('send_proposal.user_id', @Auth::user()->id)
                ->where('send_proposal.approval_status', '!=', 1)->where('assignment.approval_status', '=', 0)
                ->get();
        $data['proposalListAudit'] = $proposalListAudit;
        $data['proposalLisTraining'] = $proposalLisTraining;
        $data['selectmenu'] = "allproposal";
        return view('pages.assignment.allproposal', $data);
    }

    public function rejectProposal(Request $request) {
        UserController::validuser();
        $reqdata = $request->all();
        $id = $reqdata['id'];
        $assignmentType = $reqdata['assignmentType'];
        if ($id) {
            if (@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) { //seeker
                $document = DB::table('assignment_request_training')
                        ->select('assignment_request_training.ref_id', 'assignment_request_training.assignment_id', 'send_proposal.id', 'send_proposal.user_id')
                        ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment_request_training.assignment_id')
                        ->where('send_proposal.id', $id)
                        ->first();
            } else if (@Auth::user()->usertype == 6) { //CB
                $document = DB::table('assignment_request_audit')
                                ->select('assignment_request_audit.ref_id', 'assignment_request_audit.assignment_id', 'send_proposal.id', 'send_proposal.user_id')
                                ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment_request_audit.assignment_id')
                                ->where('send_proposal.id', $id)->first();
            }

            if ($document) {
                $details = DB::table('users')->where('id', $document->user_id)->first();
                $id = SendProposal::where("id", $id)->update(array('approval_status' => 2));
                //send mail to seeker
//                $mail['fname'] = @Auth::user()->fname;
//                $mail['lname'] = @Auth::user()->lname;
//                $mail['email'] = @Auth::user()->email;
//                $mail['ref_id'] = $document->ref_id;
//                $mail['subject'] = "ISOMart : Bid Rejection!";
//                $mail['proposaltype'] = 1;
//                $this->proposalRejectEmailSeeker($mail);
                //send mail to seeker
                //send mail to provider
                $mail['fname'] = $details->fname;
                $mail['lname'] = $details->lname;
                $mail['email'] = $details->email;
                $mail['ref_id'] = $document->ref_id;
                $mail['assignment_type'] = $assignmentType;
                $mail['assignment_id'] = $document->assignment_id;
                $mail['subject'] = "Update on your bid " . $document->ref_id;
                $this->proposalRejectEmailProvider($mail);

                //notification send to provider
                $notification['assignment_id'] = $document->assignment_id; //Assignemnt ID
                $notification['action'] = $assignmentType = 1 ? "audit_reject_proposal" : "training_reject_proposal"; //Proposal ID
                $body = json_encode($notification);
                $msg = array(
                    'header' => "Update on your bid $document->ref_id",
                    'text' => "Your bid against assignment : <strong>$document->ref_id</strong> has been rejected by the owner."
                );
                $message = json_encode($msg);
                $fcmdevice_token = $details->fcm_device_token;
                $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);
                print "Success";
            } else {
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function proposalRejectEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $proposaltype = $data['proposaltype'];
        $messageBody = view('email.proposalRejectEmail', $data);
        $a = Mail::send('email.proposalRejectEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function proposalRejectEmailProvider($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $messageBody = view('email.proposalRejectEmail', $data);
        $a = Mail::send('email.proposalRejectEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function acceptProposal(Request $request) {
        UserController::validuser();
        $reqdata = $request->all();
        $id = $reqdata['id'];
        $refId = $reqdata['refId'];
        $assignmentId = $reqdata['assignmentId'];
        $assignmentType = $reqdata['assignmentType'];
        if ($id) {
            $isjobmoveonline = false;
            $assignedLocation = false; //get assigned location
            //get location ID from bid id
            $resbidinfo = SendProposal::select("assignment_location_id", "user_id")->where("id", $id)->first();
            $difflocation = $resbidinfo->assignment_location_id;
            if (!empty($difflocation)) { //if location available for assignment
                $arraylocation = explode(",", $difflocation);
                $bidlocationcount = count($arraylocation);
                //get location ID from bid id, count bid location id
                //get all location id of assignment
                $resAL = AssignmentLocation::select("id", "whomtoassign_id")->where("assignment_id", $assignmentId)->pluck("whomtoassign_id", "id");
                $isjobmove = false; //check whether location is assigned or not
                $count = 0; //remaining location ids which are not assigned

                foreach ($resAL as $key => $val) {
                    if (!$val) {   //if assigned id is null in assignment_location
                        $count++;
                    }
                    if (in_array($key, $arraylocation) && ($val)) {  //get assigned location id
                        $assignedLocation = $key;
                    }
                }
                AssignmentLocation::whereIn('id', $arraylocation)->update(array('whomtoassign_id' => $resbidinfo->user_id));
                if ($count == $bidlocationcount) {  //update assignment status to job if all location assigned
                    $isjobmove = true;
                    Assignment::where("id", $assignmentId)->update(array("approval_status" => 1));
                }
            } else { //move to job for online training mode
                $isjobmoveonline = true;
                Assignment::where("id", $assignmentId)->update(array("approval_status" => 1));
            }
            if ($assignedLocation) {
                $assignedcityname = AssignmentLocation::select('city_name')->where('id', $assignedLocation)->first();
                print json_encode($assignedcityname->city_name);
            } else {
                //get bidder details
                $details = DB::table('users')->select("fname", "lname", "email", "fcm_device_token")->where('id', $resbidinfo->user_id)->first();
                SendProposal::where("id", $id)->update(array('approval_status' => 1));
                //send mail to seeker
//                $mail['fname'] = @Auth::user()->fname;
//                $mail['lname'] = @Auth::user()->lname;
//                $mail['email'] = @Auth::user()->email;
//                $mail['ref_id'] = $refId;
//                $mail['subject'] = "ISOMart : Bid Accepted!";
//                $mail['proposaltype'] = 1;
//                $this->proposalAccpetEmailSeeker($mail);
                //send mail to seeker
                //send mail to provider
                $mail['fname'] = $details->fname;
                $mail['lname'] = $details->lname;
                $mail['email'] = $details->email;
                $mail['ref_id'] = $refId;
                $mail['assignment_id'] = $assignmentId;
                $mail['subject'] = "Update on your bid " . $refId;
                $this->proposalAccpetEmailProvider($mail);

                //notification send to provider
                $notification['assignment_id'] = $assignmentId; //Proposal ID
                $notification['action'] = $assignmentType = 1 ? "audit_accept_proposal" : "training_accept_proposal"; //Proposal ID
                $body = json_encode($notification);
                $msg = array(
                    'header' => "Update on your bid " . $refId,
                    'text' => "Your bid against assignment : <strong>$refId</strong> has been accepted by the owner."
                );
                $message = json_encode($msg);
                $fcmdevice_token = $details->fcm_device_token;
                $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);
                print "Success";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function proposalAccpetEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $proposaltype = $data['proposaltype'];
        $messageBody = view('email.proposalAcceptEmail', $data);
        $a = Mail::send('email.proposalAcceptEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function proposalAccpetEmailProvider($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $messageBody = view('email.proposalAcceptEmail', $data);
        $a = Mail::send('email.proposalAcceptEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function alljobslist() {
        UserController::validuser();
        $data['jobslist'] = array();
        $data['certificationjob'] = array();
        //get job of CB
        if (@Auth::user()->usertype == 6) { //CB
            $jobslist = DB::table('assignment')
                    ->join('assignment_request_audit', 'assignment.id', '=', 'assignment_request_audit.assignment_id')
                    ->select('assignment_request_audit.*')
                    ->where('assignment.approval_status', '=', 1)
                    ->where('assignment.assignment_type', '=', 1)
                    ->where('assignment.user_id', @Auth::user()->id)
//                    ->orWhere('assignment.awarded_to', @Auth::user()->id)
                    ->orderBy('id', 'DESC')
                    ->get();
            $certificationjob = DB::table('assignment')
                    ->join('assignment_request_certification', 'assignment.id', '=', 'assignment_request_certification.assignment_id')
                    ->select('assignment_request_certification.*')
                    ->where('assignment.approval_status', '=', 1)
                    ->where('assignment.assignment_type', '=', 3)
                    ->where('assignment.awarded_to', @Auth::user()->id)
//                    ->orWhere('assignment.awarded_to', @Auth::user()->id)
                    ->orderBy('id', 'DESC')
                    ->get();
            $data['certificationjob'] = $certificationjob;
            $data['jobslist'] = $jobslist;
        } else if (@Auth::user()->usertype == 1 || @Auth::user()->usertype == 5) { //seeker
            $certificationjob = DB::table('assignment')
                    ->join('assignment_request_certification', 'assignment.id', '=', 'assignment_request_certification.assignment_id')
                    ->select('assignment_request_certification.*')
                    ->where('assignment.approval_status', '=', 1)
                    ->where('assignment.user_id', @Auth::user()->id)
                    ->orderBy('id', 'DESC')
                    ->get();
            $data['certificationjob'] = $certificationjob;
        } else if (@Auth::user()->usertype == 8) { //Auditor
            $jobslist = DB::table('assignment')
                    ->join('assignment_request_audit', 'assignment.id', '=', 'assignment_request_audit.assignment_id')
                    ->select('assignment_request_audit.*')
                    ->where('assignment.approval_status', '=', 1)
                    ->where('assignment.awarded_to', @Auth::user()->id)
                    ->orderBy('id', 'DESC')
                    ->get();
            $data['jobslist'] = $jobslist;
        }
        return view('pages.assignment.alljobslist', $data);
    }

    public function deleteProposal(Request $request) {
        UserController::validuser();
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            $proposalDetail = SendProposal::where('id', $id)->where('user_id', @Auth::user()->id)->first();
            $document = AssignmentAudit::select('ref_id')
                            ->where('assignment_id', $proposalDetail->assignment_id)->first();
            if ($document) {
//                $details = DB::table('users')->where('id', $document->user_id)->first();
                SendProposal::where('user_id', @Auth::user()->id)->where('id', $id)->delete();
                //send mail to provider
                $mail['fname'] = @Auth::user()->fname;
                $mail['lname'] = @Auth::user()->lname;
                $mail['email'] = @Auth::user()->email;
                $mail['ref_id'] = $document->ref_id;
                $mail['subject'] = "ISOMart : Bid Deleted!";
                $this->proposalDeleteEmailProvider($mail);
                //send mail to provider
                print "Success";
            } else {
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function proposalDeleteEmailProvider($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $messageBody = view('email.deleteProposalEmail', $data);
        $a = Mail::send('email.deleteProposalEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function deleteAssignment(Request $request) {
        UserController::validuser();
        $reqdata = $request->all();
        $id = $reqdata['id'];
        $id_delete = $id;
        if ($id) {
            $assignmentDetail = Assignment::where('id', $id)->first();
            $bidExists = SendProposal::where('assignment_id', $id)->first();
            
            if ($bidExists) {
                SendProposal::where("assignment_id", $id)->update(array('approval_status' => 3));
            }
            if ($assignmentDetail) {
                //Delete assignment accroding to type
                if ($assignmentDetail->assignment_type == 1) {
                    $auditdetails = AssignmentAudit::select('ref_id')
                                    ->where('assignment_request_audit.assignment_id', $id)->first();
                    Assignment::where("id", $id)->where('user_id', @Auth::user()->id)->update(array('approval_status' => 2));
                } else if ($assignmentDetail->assignment_type == 2) {
                    $auditdetails = AssignmentTraining::select('ref_id')
                                    ->where('assignment_request_training.assignment_id', $id)->first();
                    Assignment::where("id", $id)->where('user_id', @Auth::user()->id)->update(array('approval_status' => 2));
                }
                
                $msgbody = "<p>Assignment $auditdetails->ref_id has been cancelled by the owner.</p>"
                    . "<p>Subsequently, we have cancelled your bid associated with this assignment.</p>"
                    . "Thanks,<br />ISOMart Team";
                
                $providerList = DB::table('send_proposal')
                        ->join('users as u', 'u.id', '=', 'send_proposal.user_id')
                        ->select('u.email', 'u.fname', 'u.lname', 'u.fcm_device_token')
                        ->where('send_proposal.assignment_id', $id)->get();
                $fcm_token = array();
                foreach ($providerList as $k => $v) {
                    $fcm_token[] = $v->fcm_device_token;
                    $ins_email['email_type'] = 3; //3->delete
                    $ins_email['email_to'] = $v->email;
                    $ins_email['email_name'] = $v->fname . " " . $v->lname;
                    $ins_email['email_subject'] = "Assignment $auditdetails->ref_id Cancelled";
                    $ins_email['email_message'] = "<p>Hello " . $ins_email['email_name'] . "</p>" . $msgbody;
                    $ins_email['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                    $ins_email['protocol'] = $_SERVER['REMOTE_ADDR'];
                    EmailNotification::insertGetId($ins_email);
                }
                //notification to auditors
                $id_update = "";
                $assignment_type = $assignmentDetail->assignment_type;
                $ref_id = $auditdetails->ref_id;
                $id_assignment = $id;
                $fcm_token_bidder = $fcm_token;
                $this->NotificationController->assignmentNotification($ref_id, $id_assignment, $assignment_type, $id_update, $id_delete, $fcm_token_bidder);
                print "Success";
            } else {
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function assignmentDeleteEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $messageBody = view('email.deleteAssignmentEmail', $data);
        $a = Mail::send('email.deleteAssignmentEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function requestassignmentcertification(Request $request) {
        $user = UserController::validuser();
        $reqdata = $request->all();
        $assignment = $reqdata['certification'];
        $assignment['location'] = implode(",", $reqdata['location']);
        $assignment['proposal_end_date'] = Carbon::parse($reqdata['proposalEndDate'])->format('Y-m-d');
        $id_user = @$reqdata['user_id'];
        $id_update = @$reqdata['id_update'];
        if (@$id_update) {
            $id = AssignmentCertification::where('id', $id_update)->update($assignment);
            if ($id) {
                Session::flash('success_message', "Assignment details successfully updated.");
                return redirect("/allassignment");
            } else {
                Session::flash('error_message', "Failure in update");
                return redirect("/requestassignment");
            }
        } else {
            //insert to Assignment
            $assignmentParent['user_id'] = @Auth::user()->id;
            $assignmentParent['assignment_type'] = 3;
            $assignmentParent['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
            $assignmentParent['protocol'] = $_SERVER['REMOTE_ADDR'];
            $assignmentId = Assignment::create($assignmentParent);
            $id_assignment = $assignmentId->id;
            //insert to Assignment

            $assignment['assignment_id'] = $id_assignment;
            $assignment['admin_approval'] = 1;
            $assignment['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
            $assignment['protocol'] = $_SERVER['REMOTE_ADDR'];
            $res = AssignmentCertification::create($assignment);
            $id = $res->id;
            if ($id) {
                $ref_id = "CE" . date("Y") . date("m") . date("d") . $id;
                AssignmentCertification::where('id', $id)->update(array(
                    "ref_id" => $ref_id
                ));
                $data_report['user_id'] = @Auth::user()->id;
                $data_report['assignment_id'] = $id_assignment;
                $data_report['assignment_type'] = 3;
                $data_report['status'] = $assignment['assignment_status'];
                $data_report['description'] = $assignment['description'];
                $data_report['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
                $data_report['protocol'] = $_SERVER['REMOTE_ADDR'];
                AssignmentReport::create($data_report);
                //send mail to admin
                $mail['ref_id'] = $ref_id;
                $mail['subject'] = "ISOMart : New Certification Assignment";
                $mail['assignment'] = 3;
                $this->assignmentRequestEmail($mail);
                //send mail to admin
                Session::put('tabActive', "3");
                Session::flash('success_message', "Assignement created Successfully!");
                return redirect("/allassignment");
            } else {
                Session::flash('error_message', "Failure in insert!");
                return redirect("/requestassignment");
            }
        }
    }

    public function assignmentCertificationDetail($assignment_id) {
        $certificationdetails = AssignmentCertification::where('assignment_request_certification.assignment_id', $assignment_id)->first();

        //get location names
        $citynames = array();
        @$locationIds = explode(",", @$certificationdetails->location);
        $cityNameArray = DB::table('cities')
                        ->select('countries.name as countryName', 'states.name as stateName', 'cities.name as cityName')
                        ->join('countries', 'countries.id', '=', 'cities.country_id')
                        ->join('states', 'states.id', '=', 'cities.region_id')
                        ->whereIn('cities.id', @$locationIds)->get();

        foreach ($cityNameArray as $key => $val) {
            $citynames[] = $val->cityName . ", " . $val->stateName . ", " . $val->countryName;
        }
        $data['locationname'] = $citynames;
        //get location names
        //standard details code
        $standardDetail = DB::table('international_standards_section')
                        ->where('id', $certificationdetails->sic_code)->first();
        $data['standardDeatil'] = $standardDetail;
        //standard details code
        //send proposal by standard & location
        $data['isicCode'] = DB::table('users_isic_code')->where('user_id', @Auth::user()->id)->count();
        $data['standard'] = DB::table('users_isic_code')->where('user_id', @Auth::user()->id)
                        ->where('code_sector', @$certificationdetails->sic_code)->count();
        $data['address'] = DB::table('users_contact_address')
                        ->whereIn('city', @$locationIds)->where('status_current', 1)
                        ->where('user_id', @Auth::user()->id)->count();
        //send proposal by standard & location
        //user details
        $userdetails = Assignment::select('users.fname', 'users.lname', 'users.id', 'users.email', 'assignment.user_id', 'assignment.approval_status', 'assignment.assignment_type')
                        ->leftJoin('users', function($join) {
                            $join->on('assignment.user_id', '=', 'users.id');
                        })
                        ->where("assignment.id", $certificationdetails->assignment_id)->get();
        //user details
        //awarded details
        $awarddetails = Assignment::select('users.fname', 'users.lname', 'assignment.awarded_to')
                        ->leftJoin('users', function($join) {
                            $join->on('assignment.awarded_to', '=', 'users.id');
                        })
                        ->where("assignment.id", $certificationdetails->assignment_id)->get();
        //awarded details
        //get proposal details if found 
        $proposaldetails = DB::table('send_proposal')
                ->select('users.fname', 'users.lname', 'users.profile_img', 'users.id AS userId', 'users.gender', 'send_proposal.*')
                ->join('users', 'send_proposal.user_id', '=', 'users.id')
                ->where('send_proposal.assignment_id', $assignment_id)
                ->where('send_proposal.approval_status', '=', 0)
                ->get();
        //get proposal details if found
        //to edit proposal
        $editProposal = SendProposal::select('user_id')->where('user_id', @Auth::user()->id)
                ->where('approval_status', '=', 0)
                ->where('assignment_id', '=', $assignment_id)
                ->count();
        if ($editProposal > 0) {
            $data['editProposal'] = $editProposal;
        }
        //to edit proposal
        //get proposal data using Auth ID
        $proposalEdit = SendProposal::where('user_id', @Auth::user()->id)
                        ->where('assignment_id', '=', $assignment_id)
                        ->where('approval_status', '!=', 2)->first();
        $data['proposalEdit'] = $proposalEdit;
        //get proposal data using Auth ID

        $data['certificationdetails'] = $certificationdetails;
        $data['userdetails'] = $userdetails;
        $data['awarddetails'] = $awarddetails;
        $data['proposaldetails'] = $proposaldetails;
        return view('pages.assignment.assignmentcertificationdetail', $data);
    }

    public function startJob(Request $request) {
        UserController::validuser();
        $reqdata = $request->all();
        $assignment_id = $reqdata['assignment_id'];
        $ref_id = $reqdata['ref_id'];
        if ($assignment_id && $ref_id) {
            $update_id = Assignment::where('id', $assignment_id)->where('user_id', @Auth::user()->id)
                    ->update(array(
                "approval_status" => 1,
                "start_job_at" => Carbon::now()->format("Y-m-d H:i:s")
            ));
            if ($update_id) {
                //send mail to seeker
                $mail['fname'] = @Auth::user()->fname;
                $mail['lname'] = @Auth::user()->lname;
                $mail['email'] = @Auth::user()->email;
                $mail['ref_id'] = $ref_id;
                $mail['subject'] = "ISOMart : New Job!";
                $mail['proposaltype'] = 1;
                $this->startJobEmailSeeker($mail);
                //send mail to seeker
                print "Success";
            } else {
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function startJobEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $messageBody = view('email.startJobEmail', $data);
        $a = Mail::send('email.startJobEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function shareAssignment(Request $request) {
        $reqdata = $request->all();
        $data['assignment_id'] = $reqdata['assignment_id'];
        $data['ref_id'] = $reqdata['ref_id'];
        $data['fname'] = @Auth::user()->fname;
        $data['lname'] = @Auth::user()->lname;
        $toemail = $reqdata['share_email'];
        $subject = "Share Assignment";
        $messageBody = view('email.shareAssignmentEmail', $data);
        $a = Mail::send('email.shareAssignmentEmail', $data, function($message) use ($toemail, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail)->subject($subject);
                });
        Session::flash('success_message', "Assignment shared successfully!");
        return redirect("/assignmentAuditDetail/" . $reqdata['assignment_id']);
    }

}
