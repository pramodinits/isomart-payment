<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Session;
use Mail;
use Config;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use App\Assignment;
use App\AssignmentAudit;
use App\AssignmentTraining;
use App\AssignmentCertification;
use App\AssignmentLocation;
use App\UserDocument;
use App\IsicCode;
use App\SendProposal;
use App\Rating;
use App\RatingAnswer;
use App\CpdHours;
use App\SubscriptionPackage;
use App\TrainingPost; // for Training

class UserController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        $prefix = DB::getTablePrefix();
        $assignmentaudit = Assignment::with('AssignmentLocation')
                        ->join('assignment_request_audit', DB::raw($prefix . 'assignment_request_audit.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                        ->select("assignment_request_audit.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                        ->where('status', '=', 1)->where('close_job', '=', 0)
                        ->where('approval_status', '!=', 2)->orderBy('assignment.id', 'DESC')->limit(3)->get();

        $assignmenttraining = Assignment::with('AssignmentLocation')
                        ->join('assignment_request_training', DB::raw($prefix . 'assignment_request_training.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                        ->select("assignment_request_training.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                        ->where('status', '=', 1)->where('close_job', '=', 0)
                        ->where('approval_status', '!=', 2)->orderBy('assignment.id', 'DESC')->limit(3)->get();

//        $assignmenttraining = AssignmentTraining::orderBy('id', 'DESC')->limit(3)->get();

        $assignmentcertification = Assignment::leftJoin('assignment_request_certification', function($join) {
                            $join->on('assignment.id', '=', 'assignment_request_certification.assignment_id');
                        })
                        ->where('assignment.approval_status', '!=', 2)
                        ->where('assignment.assignment_type', '3')
                        ->orderBy('assignment.id', 'DESC')->limit(3)->get();
        //get cities
        $id_cities = "";
        $cityNameArray = "";
        foreach ($assignmentcertification as $key => $val) {
            @$id_cities .= $val->location . ",";
        }

        $id_cities = rtrim($id_cities, ",");
        if ($id_cities) {
            $id_citiesArray = explode(",", $id_cities);
            $data['cityNameArray'] = DB::table('cities')->whereIn('id', $id_citiesArray)->pluck("name", "id");
        }
        //get cities
        $data['assignmentaudit'] = $assignmentaudit;
        $data['assignmenttraining'] = $assignmenttraining;
        $data['assignmentcertification'] = $assignmentcertification;
//        $data['assignmentLocation'] = $assignmentLocation;
        //start Training
        $trainingpostinfo = TrainingPost::where('status', '=', 1)
                        ->orderBy('training_post_id', 'DESC')->limit(3)->get();
        $data['trainingpostinfo'] = $trainingpostinfo;
        //end Training
        $data['advtinfo'] = DB::table('advt_banner')->where('status', '=', 1)
                        ->where('location', '=', 1)->pluck("image", "shape");

        //start query
        $querypostinfo = DB::table('query')->where('status', '=', 1)
                        ->orderBy('startDate', 'ASC')->limit(2)->get();
        $data['querypostinfo'] = $querypostinfo;

        $data['country'] = DB::table('countries')->pluck("name", "id");
        $data['state'] = DB::table('states')->pluck("name", "id");
        //end query
        //subscription packages
        $data['package_list'] = SubscriptionPackage::where('status', 1)->orderBy('package_price', 'ASC')->limit(3)->get();
        return view('pages.home', $data);
    }

    public function signin($phone = null, $assignment_id = null) {
        if (Auth::id() && Auth::user()->phone_verify_status == 0) {
            Session::flush();
            Auth::logout();
        }
        if (Auth::id()) {
            return redirect("/dashboard");
        }
        $data['selectmenu'] = "signin";
        if (@$phone != "phone") {
            $data['phone'] = $phone;
        }
        if (@$assignment_id != "assignment_id") {
            $data['assignment_id'] = $assignment_id;
        }
        return view('pages.login', $data);
    }

    public function signup($phone = null) {
        if (Auth::id()) {
            return redirect("/");
        }

        $action = app('request')->route()->getAction();
        $data['selectmenu'] = "signup";
        $data['phone'] = $phone;
        $data['usertype'] = Config::get('selecttype.usertypelist');
        return view('pages.signup', $data);
    }

    public function validuser() {
        $phoneverify = Auth::user()->phone_verify_status;

        $status = Auth::user()->status;
        if ($status == 1) {
            return true;
        }
//        if ($phoneverify != 1) {
//            $request->session()->flash('error_message', trans('registration.verifyemail'));
//        } elseif ($status == 2) {
//            $request->session()->flash('error_message', trans('registration.waitforadmin'));
//        } elseif ($status == 3) {
//            $request->session()->flash('error_message', trans('registration.invalidID'));
//        } else {
//            return true;
//        }
        Auth::logout();
        return redirect("/signin");
    }

    public function registration(Request $request) {
        $reqdata = $request->all();
        $trimPhone = str_replace(' ', '', $reqdata['Phone']);
        $phone_exists = DB::table('users')->select('phone')
                ->where('phone', $trimPhone)
                ->where('countrycode', trim($reqdata['countrycode']))
                ->count();
        if ($phone_exists > 0) {
            Session::flash('error_message', 'Phone Exists.');
            return redirect("/signup");
        }
        $ins_data = array();
        $ins_data['fname'] = $reqdata['fName'];
        $ins_data['lname'] = $reqdata['lName'];
        $ins_data['email'] = $reqdata['email'];
        $ins_data['gender'] = $reqdata['cgender'];
        $ins_data['phone'] = $reqdata['Phone'];
        $countrycode = $reqdata['countrycode'];
        $ins_data['countrycode'] = trim($countrycode, "+");
        $ins_data['company_name'] = $reqdata['Cname'];
        $ins_data['password'] = bcrypt($reqdata['Password']);
        $ins_data['usertype'] = $reqdata['Usertype'];
        $ins_data['verify_otp'] = rand(1000, 9999);
//        $ins_data['verify_otp'] = 1234; //TODO : generate & send otp to user
        $ins_data['otp_created_at'] = Carbon::now()->format("Y-m-d H:i:s");
        $ins_data['status'] = 0;
        $ins_data['reg_date'] = Carbon::now()->format("Y-m-d H:i:s");
        $ins_data['platform'] = 1;
        $ins_data['protocol'] = $_SERVER['REMOTE_ADDR'];
        $ins_data['remember_token'] = $reqdata['_token'];
        $id = DB::table('users')->insertGetId($ins_data);

        if ($id) {
            $this->sendOTPtoUser($ins_data); //TODO : generate & send otp to user
            $ins_data['subject'] = "ISOMart : Successfully registered!";
            $ins_data['flag'] = 1;
            $this->otpEmailtoUser($ins_data);
            Auth::loginUsingId($id);
            Session::flash('success_message', 'Registered Successfully. Verify your phone no through OTP.');
            return redirect("/verifyotp");
        } else {
            return redirect("/signup");
        }
    }

    public function otpEmailtoUser($data) {
        $toemail = $data['email'];
        $fname = $data['fname'];
        $toname = $data['fname'] . " " . $data['lname'];
        $otp = $data['verify_otp'];
        $subject = $data['subject'];
        $flag = $data['flag'];
        $messageBody = view('email.registration', $data);
        $a = Mail::send('email.registration', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function sendOTPtoUser($data) {
        $phone = $data['phone'];
        $otp = $data['verify_otp'];
        $countrycode = $data['countrycode'];
        $tophone = "+" . $countrycode . $phone;
        $msg = $otp . " is your OTP";
        $message = urlencode($msg);
        $url = "http://sandeshlive.in/API/WebSMS/Http/v1.0a/index.php?username=1orimarkt%20&password=0Ri1@m7r4&sender=SURPLU&to=$tophone&message=$message&reqid=1&format=%7bjson%7Ctext%7d&route_id=23";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
    }

    public function verifyotp() {
        $action = app('request')->route()->getAction();
        $data['controller'] = class_basename($action['controller']);
        $data['selectmenu'] = "verifyotp";
        return view('pages.verifyotp', $data);
    }

    public function submitverifyotp(Request $request) {
        $reqdata = $request->all();
        $submit_otp = $reqdata['otp'];

        if (@Auth::user()->verify_otp == $submit_otp) {
            DB::table('users')->where("id", @Auth::user()->id)
                    ->update(
                            array(
                                "phone_verify_status" => 1,
                                "status" => 1,
                                "updated_at" => Carbon::now()->format("Y-m-d H:i:s"),
                                "profile_visited" => 1,
                            )
            );
            Session::flash('success_message', 'Phone verified Successfully.');
            return redirect("/profile");
        } else {
            $request->session()->flash('error_message', 'OTP mismatched.');
            return redirect("/verifyotp");
        }
    }

    public function login(Request $request) {
        $reqdata = $request->all();
        $assignment_id = @$reqdata['assignment_id'];
        $phone = str_replace(" ", "", $reqdata['Phone']);
        $countrycode = trim($reqdata['countrycode'], "+");
        $password = $reqdata['Password'];

        if (Auth::attempt(['countrycode' => $countrycode, 'phone' => $phone, 'password' => $password, 'is_admin' => 0])) {

//            Session::set('returnurl', 'dashboard');
//            if (@$request->session()->has('returnurl')) {
//                $url = Session::get('returnurl') ? Session::get('returnurl') : "/";
//                Session::forget('returnurl');
//                return redirect($url);
//            } 
            if (@Auth::user()->status == 0) {
                Session::flush();
                Auth::logout();
                Session::flash('error_message', "Inactive user");
                return redirect("/signin");
            }
            if (@$assignment_id) {
                $assignmentDetails = Assignment::where("id", $assignment_id)->first();
                $request->session()->flash('success_message', trans('Successfully login. Now you can send proposal for this assignment.'));
                //to check assignment type
                if ($assignmentDetails->assignment_type == 1) {
                    return redirect("assignmentAuditDetail/" . $assignment_id);
                } else if ($assignmentDetails->assignment_type == 2) {
                    return redirect("assignmentTrainingDetail/" . $assignment_id);
                }
                //to check assignment type
            } elseif (@Auth::user()->phone_verify_status == 0) {
                $verify_otp = rand(1000, 9999);
                //send SMS
                $ins_data['phone'] = $phone;
                $ins_data['verify_otp'] = $verify_otp;
                $ins_data['countrycode'] = $countrycode;
                $this->sendOTPtoUser($ins_data);
                //send SMS
                //send email
                $email_data['email'] = @Auth::user()->email;
                $email_data['verify_otp'] = $verify_otp;
                $email_data['fname'] = @Auth::user()->fname;
                $email_data['lname'] = @Auth::user()->lname;
                $email_data['flag'] = 3;
                $email_data['subject'] = "ISOMart : Mobile Verfication!";
                $this->otpEmailtoUser($email_data);
                //send email
                $update = DB::table('users')->where("countrycode", $countrycode)->where("phone", $phone)
                        ->update(
                        array(
                            "verify_otp" => $verify_otp
                        )
                );
                Session::flash('success_message', "Login successful. Kindly verify phone number.");
                return redirect("/verifyotp"); //TODO : generate & send otp to user
            } elseif (@Auth::user()->profile_visited == 1) {
                Session::flash('success_message', "Successfully login!");
                return redirect("/dashboard");
            } else {
                Session::flash('success_message', "Successfully login!");
                return redirect("/profile");
            }
        } else {
            Session::flash('error_message', "Invalid password");
            return redirect("/signin");
        }
    }

    public function forgotpassword() {
        $selectmenu = "forgotpassword";
        return view('pages.forgotpassword', compact('selectmenu'));
    }

    public function checkphone(Request $request) {
        $reqdata = $request->all();
        $phoneNo = $reqdata['phone'];
        $countrycode = trim($reqdata['countrycode'], "+");
        $phone_exists = DB::table('users')->select('phone')
                ->where('countrycode', $countrycode)
                ->where('phone', $phoneNo)
                ->count();
        if ($phone_exists > 0) {
            print "success";
            exit;
        } else {
            print "failure";
            exit;
        }
    }

    public function setPasswordOtp(Request $request) {
        $reqdata = $request->all();
        $phoneNo = $reqdata['phone'];
        $countrycode = $reqdata['countrycode'];
        $showOtp = @$reqdata['showOtp'];
        $setPassword = @$reqdata['setPassword'];

        if (@$phoneNo && @$setPassword) {
            $updatePwd = DB::table('users')->where("countrycode", $countrycode)->where("phone", $phoneNo)
                    ->update(
                    array(
                        "password" => bcrypt($setPassword),
                        "phone_verify_status" => 1,
                        "status" => 1,
                        "updated_at" => Carbon::now()->format("Y-m-d H:i:s")
                    )
            );
            if ($updatePwd) {
                print "success";
                exit;
            } else {
                print "failure";
                exit;
            }
        } elseif (@$phoneNo && @$showOtp) {
            $userdata = DB::table('users')->where("countrycode", $countrycode)->where('phone', $phoneNo)
                            ->where('verify_otp', $showOtp)->first();
            if ($userdata) {
                print "success";
                exit;
            } else {
                print "failure";
                exit;
            }
        } else {
            if ($phoneNo) { //TODO : generate & send otp to user
                $userdata = DB::table('users')->where('countrycode', $countrycode)->where('phone', $phoneNo)->first();
                $verify_otp = rand(1000, 9999);
                $update = DB::table('users')->where("countrycode", $countrycode)->where("phone", $phoneNo)
                        ->update(array("verify_otp" => $verify_otp));
                if ($update) {
                    print "success";
                    $ins_data['phone'] = $phoneNo;
                    $ins_data['verify_otp'] = $verify_otp;
                    $ins_data['countrycode'] = $countrycode;
                    $this->sendOTPtoUser($ins_data);
                    //send email
                    $email_data['email'] = $userdata->email;
                    $email_data['verify_otp'] = $verify_otp;
                    $email_data['fname'] = $userdata->fname;
                    $email_data['lname'] = $userdata->lname;
                    $email_data['flag'] = 2;
                    $email_data['subject'] = "ISOMart : Secure One-Time-Password for Reset Password";
                    $this->otpEmailtoUser($email_data);
                    //send email
//                    print "success";
                    exit;
                } else {
                    print "failure";
                    exit;
                }
            } else {
                print "failure";
                exit;
            }
        }
    }

    public function profile() {
        if (@Auth::user()->id) {
            $data['contactDetails'] = array();
            $data['generalDetails'] = array();
            $data['addressDetails'] = array();
            $data['sicCodeDetails'] = array();
            $data['standardDetails'] = array();
            $data['industryDetails'] = array();
            $data['stateNameArray'] = array();

            //standards list
            $data['standardlist'] = Config::get('standards.standardlist');
            //standards list
            //country details
            $data['countryList'] = DB::table('countries')->orderBy('name', 'ASC')->get();
            $data['countryName'] = $data['countryList']->pluck('name', 'id');
            //country details
            //general details
            $data['generalstatuscolor'] = "#222222";
            $generalDetails = DB::table('users_general_info')
                    ->where('user_id', @Auth::user()->id)
                    ->first();

            if (@$generalDetails->id) {
//                $generalstatuscolor = "Blue";
                $data['generalDetails'] = $generalDetails;
                $data['generalstatuscolor'] = "#4782d3";
                $generaldocuments = DB::table('users_document')->where('user_id', Auth::user()->id)->get();

                $regDateProof = $trainingCertificate = $aggrement = $verificationReport = $cv = $codeClaim = $regCertificate = $auditLog = $appreciationLetter = array();
                foreach ($generaldocuments as $key => $val) {
                    $flag = $val->flag;
                    switch ($flag) {
                        case "1":
                            $regDateProof[] = $val;
                            break;
                        case "2":
                            $cv[] = $val;
                            break;
                        case "3":
                            $codeClaim[] = $val;
                            break;
                        case "4":
                            $trainingCertificate[] = $val;
                            break;
                        case "5":
                            $regCertificate[] = $val;
                            break;
                        case "6":
                            $aggrement[] = $val;
                            break;
                        case "7":
                            $auditLog[] = $val;
                            break;
                        case "8":
                            $verificationReport[] = $val;
                            break;
                        case "9":
                            $appreciationLetter[] = $val;
                            break;
                        default:
                            break;
                    }
                }
                $data['regDateProof'] = $regDateProof;
                $data['trainingCertificate'] = $trainingCertificate;
                $data['aggrement'] = $aggrement;
                $data['verificationReport'] = $verificationReport;
                $data['cv'] = $cv;
                $data['codeClaim'] = $codeClaim;
                $data['regCertificate'] = $regCertificate;
                $data['auditLog'] = $auditLog;
                $data['appreciationLetter'] = $appreciationLetter;
            }
            //general details end
            
            //update available address
            $availableCurrent = DB::table('users_contact_address')->where('user_id', Auth::user()->id)
                   ->where('available_fromdate', '==', Carbon::now()->format("Y-m-d"))->first();
            if($availableCurrent) {
               DB::table('users_contact_address')->where("available_status", 1)
                                ->where("user_id", @Auth::user()->id)->update(array("available_status" => 0));
              DB::table('users_contact_address')->where("id", $availableCurrent->id)->update(array("available_status" => 0));
            }
            
            //contact info
            $data['contactstatuscolor'] = "#222222";
            $addressDetails = DB::table('users_contact_address')->where('user_id', Auth::user()->id)->get();
            $data['addressDetails'] = $addressDetails;

            //get state name
            $id_states = "";
            foreach ($addressDetails as $key => $val) {
                @$id_states .= $val->state_id . ",";
            }
            $id_states = rtrim($id_states, ",");
            if ($id_states) {
                $id_statesArray = explode(",", $id_states);
                $data['stateNameArray'] = DB::table('states')->whereIn('id', $id_statesArray)->pluck("name", "id");
            }
//get state name

            $contactDetails = DB::table('users_contact_details')->where('user_id', Auth::user()->id)->first();
            $data['contactDetails'] = $contactDetails;
            if (count(@$addressDetails) > 0 || @$contactDetails->id) {
                $data['contactstatuscolor'] = "#4782d3";
            }
            //contact info
            //sic code info
            $data['sicstatuscolor'] = "#222222";
            $sicCodeDetails = DB::table('users_isic_code')
                            ->join('international_standards_section', 'users_isic_code.code_sector', '=', 'international_standards_section.id')
                            ->select('users_isic_code.*', 'international_standards_section.name as codeName', 'international_standards_section.type', 'international_standards_section.code')
                            ->where('users_isic_code.user_id', Auth::user()->id)->get();
            $sicDocDetails = UserDocument::where('user_id', Auth::user()->id)->where('flag', 10)->get();
            if (count($sicCodeDetails) > 0) {
                $data['sicstatuscolor'] = "#4782d3";
                $data['sicCodeDetails'] = $sicCodeDetails;
                $data['sicDocDetails'] = $sicDocDetails;
            }
            //sic code info
            //sic code info
            $data['standardstatuscolor'] = "#222222";
            $standardDetails = DB::table('users_standards')->where('user_id', Auth::user()->id)->get();
            if (count($standardDetails) > 0) {
                $data['standardstatuscolor'] = "#4782d3";
                $data['standardDetails'] = $standardDetails;
            }
            //sic code info
            //industry info
            $data['industrystatuscolor'] = "#222222";
            $industryDetails = DB::table('users_industry_sector')->where('user_id', Auth::user()->id)->get();
            if (count($industryDetails) > 0) {
                $data['industrystatuscolor'] = "#4782d3";
                $data['industryDetails'] = $industryDetails;
            }
            //industry info

            $data['profile_activetab'] = Session::get('profile_activetab', 1);
            $data['selectmenu'] = "profile";
            return view('pages.profile', $data);
        } else {
            return redirect("/signin");
        }
    }

    public function getLocationInfo(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $tbl = $reqdata['tbl'];
        $selected_val = $reqdata['selected_val'];
        $id = $reqdata['id'];
        if ($tbl == "states") {
            $res = DB::table($tbl)->where('country_id', $selected_val)
                            ->orderBy('name', 'ASC')->get();
            $str = "<option value=''>Select State</option>";
            foreach ($res as $k => $v) {
                $str.="<option value='{$v->id}'>" . ucwords(strtolower($v->name)) . "</option>";
            }
            echo $str;
        } else {
            $res = DB::table($tbl)->where('region_id', $selected_val)
                            ->orderBy('name', 'ASC')->pluck('name');
            echo json_encode($res);
        }
        exit();
    }

    public function getStateList(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $tbl = $reqdata['tbl'];
        $selected_val = $reqdata['selected_val'];
        $id = $reqdata['id'];
        if ($id) {
            $res = DB::table($tbl)->where('country_id', $selected_val)
                            ->orderBy('name', 'ASC')->get();
            $str = "<option value=''>Select State</option>";
            foreach ($res as $k => $v) {
                $str.="<option value='{$v->id}'>" . ucwords(strtolower($v->name)) . "</option>";
            }
            echo $str;
        } else {
            echo "Error";
        }
        exit();
    }

    public function getCityList(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $tbl = $reqdata['tbl'];
        $selected_val = $reqdata['selected_val'];
        $id = $reqdata['id'];
        if ($id) {
            $res = DB::table($tbl)->where('region_id', $selected_val)
                            ->orderBy('name', 'ASC')->get();
            $str = "<option value=''>Select City</option>";
            foreach ($res as $k => $v) {
                $str.="<option value='{$v->id}'>" . ucwords(strtolower($v->name)) . "</option>";
            }
            echo $str;
        } else {
            echo "Error";
        }
        exit();
    }

    public function getStandards(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $selected_val = $reqdata['selected_val'];
        $select_sic_code = @$reqdata['select_sic_code'];
        if ($selected_val) {
            if ($selected_val == 1) {
                $standards = DB::table('international_standards_section')->where('type', 'ISIC')
                                ->orderBy('code', 'ASC')->get();
            } else if ($selected_val == 2) {
                $standards = DB::table('international_standards_section')->where('type', 'NACE')
                                ->orderBy('code', 'ASC')->get();
            } else if ($selected_val == 3) {
                $standards = DB::table('international_standards_section')->where('type', 'ANZSIC')
                                ->orderBy('code', 'ASC')->get();
            }
            $str = "<option value=''>Select Code</option>";
            foreach ($standards as $k => $v) {
                $selected = ($v->id == $select_sic_code) ? "selected" : "";
                $str.="<option value='{$v->id}' $selected >" . $v->code . ": " . ucwords(strtolower($v->name)) . "</option>";
            }
            echo $str;
        } else {
            echo "Error";
        }
        exit();
    }

    public function logout(Request $request) {
        if (@Auth::id()) {
            Session::flush();
            Auth::logout();
        }
        $request->session()->flash('success_message', 'Successfully logged out.');
        return redirect("/");
    }

    public function updateprofile(Request $request) {
        $reqdata = $request->all();
        if ($reqdata['update'] == 'profileimg') {
            $img_file = $request->file('profileImg');
            $req_data = $request->all();
            @$hidden_img = @$req_data['hidden_img'];
            if ($img_file) {
                $upd = array();
                $id = Auth::user()->id;
                if ($img_file) {
                    $nameimage = $img_file->getClientOriginalName();
                    $nameimage = str_replace(" ", "_", $nameimage);
                    if (@$hidden_img) {
                        if (file_exists(public_path('profile_image/' . $id . "_" . $hidden_img))) {
                            @unlink(public_path('profile_image/' . $id . "_" . $hidden_img));
                        }
                    }
                    @$name = $id . "_" . $nameimage;
                    @$cp = $img_file->move(public_path('profile_image'), $name);
                    $upd['profile_img'] = $name;
                }
                DB::table('users')->where("id", $id)->update($upd);
                $request->session()->flash('success_message', "Profile image successfully updated.");
            } else {
                $request->session()->flash('error_message', "Failure in update");
            }
            return redirect("profile");
        } elseif ($reqdata['update'] == 'profilename') {
            $updateName['fname'] = $reqdata['fName'];
            $updateName['lname'] = $reqdata['lName'];
            $updateName['updated_at'] = Carbon::now()->format("Y-m-d H:i:s");
            $id = DB::table('users')->where("id", Auth::user()->id)->update($updateName);
            if ($id) {
                $request->session()->flash('success_message', "Name successfully updated.");
            } else {
                $request->session()->flash('error_message', "Failure in update");
            }
            return redirect("profile");
        } elseif ($reqdata['update'] == 'profilepassword') {
            $update_pwd = $reqdata['newPassword'];
            $oldPwd = $reqdata['oldPassword'];

            $userdata = DB::table('users')->where('id', Auth::user()->id)->first();
            if (Hash::check($oldPwd, $userdata->password)) {
//                $id = DB::table('users')->where("id", Auth::user()->id)->update(bcrypt($updatePwd));
                $id = DB::table('users')->where("id", Auth::user()->id)
                        ->update(
                        array(
                            "password" => bcrypt($update_pwd),
                            "updated_at" => Carbon::now()->format("Y-m-d H:i:s")
                        )
                );
                if ($id) {
                    $request->session()->flash('success_message', "Password successfully updated.");
                } else {
                    $request->session()->flash('error_message', "Failure in update");
                }
            } else {
                $request->session()->flash('error_message', "Current password is not correct.");
            }
            return redirect("profile");
        }
    }

    public function updateProfileGeneral(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $profile = $reqdata['profile'];
        $profile['registration_date'] = Carbon::parse($profile['registration_date'])->format('Y-m-d');
        $id_user = @$reqdata['user_id'];
        $id_update = @$reqdata['id'];

        $userdata = DB::table('users_general_info')->where('user_id', $id_user)->first();
        if ($userdata) {
            $res = DB::table('users_general_info')->where("id", $id_update)->update($profile);
            $messageid = $id_update;
        } else {
            $profile['user_id'] = $id_user;
            $profile['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
            $profile['protocol'] = $_SERVER['REMOTE_ADDR'];
            $id_insert = DB::table('users_general_info')->insertGetId($profile);
            $messageid = $id_update = $id_insert;
        }
        //upload documents
        $document_file = array();
        $document_file['date_proof'] = $request->file('regDateProof');
        $document_file['training_certificate'] = $request->file('trainingCertificate');
        $document_file['aggrement_doc'] = $request->file('aggrement');
        $document_file['verification_report'] = $request->file('verificationReport');
        $document_file['cv_doc'] = $request->file('cv');
        $document_file['code_claim'] = $request->file('codeClaim');
        $document_file['appreciation_letter'] = $request->file('appreciationLetter');
        $audit_log = $request->file('auditLog');
        $reg_certificate = $request->file('regCertificate');
        if (@$id_insert) {
//            $path = public_path() . '/profile_document/' . $id_user;
            $path = public_path() . '/profile_document/' . $id_insert;
            File::makeDirectory($path, $mode = 0777, true, true);
            if ($document_file) {
                foreach ($document_file as $key => $val) {
                    if (!empty($val)) {
                        $getNumeric1 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
                        $getNumeric2 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);

                        $nameimage = $val->getClientOriginalName();
                        $extension = pathinfo($nameimage, PATHINFO_EXTENSION);
//                        $nameimage = str_replace(" ", "_", $nameimage);
                        if ($key == 'date_proof') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            $docinsert['flag'] = 1;
                        }
                        if ($key == 'training_certificate') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            $docinsert['flag'] = 4;
                        }
                        if ($key == 'aggrement_doc') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            $docinsert['flag'] = 6;
                        }
                        if ($key == 'verification_report') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            $docinsert['flag'] = 8;
                        }
                        if ($key == 'cv_doc') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            $docinsert['flag'] = 2;
                        }
                        if ($key == 'code_claim') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            $docinsert['flag'] = 3;
                        }
                        if ($key == 'appreciation_letter') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            $docinsert['flag'] = 9;
                        }
                        @$cp = $val->move($path, $name);
                        $docinsert['user_id'] = $id_user;
                        $docinsert['name'] = $name;
                        $docinsert['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
                        $docinsert['protocol'] = $_SERVER['REMOTE_ADDR'];
                        $docinsert['status'] = 1;
                        $iddoc = DB::table('users_document')->insertGetId($docinsert);
                    }
                }
            }
        } else if (@$id_update) {
//            $path = public_path() . '/profile_document/' . $id_user . "/";
            $path = public_path() . '/profile_document/' . $id_update . "/";
            if ($document_file) {
                $docins['user_id'] = @$id_user;
                $docins['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
                $docins['protocol'] = $_SERVER['REMOTE_ADDR'];
                $docins['status'] = 1;
                foreach ($document_file as $key => $val) {
                    if (!empty($val)) {
                        $getNumeric1 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
                        $getNumeric2 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);

                        $nameimage = $val->getClientOriginalName();
                        $extension = pathinfo($nameimage, PATHINFO_EXTENSION);
                        if ($key == 'date_proof') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            if ($reqdata['regDateProof_prev']) {
                                $hpath = public_path() . '/profile_document/' . $id_update . "/" . $reqdata['regDateProof_prev'];
                                File::delete($hpath);
                                DB::table('users_document')->where('name', 'LIKE', $reqdata['regDateProof_prev'])->update(array("name" => $name));
                            } else {
                                $docins['name'] = $name;
                                $docins['flag'] = 1;
                                DB::table('users_document')->insertGetId($docins);
                            }
                        }
                        if ($key == 'training_certificate') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            if ($reqdata['trainingCertificate_prev']) {
                                $mpath = public_path() . '/profile_document/' . $id_update . "/" . $reqdata['trainingCertificate_prev'];
                                File::delete($mpath);
                                DB::table('users_document')->where('name', 'LIKE', $reqdata['trainingCertificate_prev'])->update(array("name" => $name));
                            } else {
                                $docins['name'] = $name;
                                $docins['flag'] = 4;
                                DB::table('users_document')->insertGetId($docins);
                            }
                        }
                        if ($key == 'aggrement_doc') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            if ($reqdata['aggrement_prev']) {
                                $mpath = public_path() . '/profile_document/' . $id_update . "/" . $reqdata['aggrement_prev'];
                                File::delete($mpath);
                                DB::table('users_document')->where('name', 'LIKE', $reqdata['aggrement_prev'])->update(array("name" => $name));
                            } else {
                                $docins['name'] = $name;
                                $docins['flag'] = 6;
                                DB::table('users_document')->insertGetId($docins);
                            }
                        }
                        if ($key == 'verification_report') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            if ($reqdata['verificationReport_prev']) {
                                $mpath = public_path() . '/profile_document/' . $id_update . "/" . $reqdata['verificationReport_prev'];
                                File::delete($mpath);
                                DB::table('users_document')->where('name', 'LIKE', $reqdata['verificationReport_prev'])->update(array("name" => $name));
                            } else {
                                $docins['name'] = $name;
                                $docins['flag'] = 8;
                                DB::table('users_document')->insertGetId($docins);
                            }
                        }
                        if ($key == 'cv_doc') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            if ($reqdata['cv_prev']) {
                                $mpath = public_path() . '/profile_document/' . $id_update . "/" . $reqdata['cv_prev'];
                                File::delete($mpath);
                                DB::table('users_document')->where('name', 'LIKE', $reqdata['cv_prev'])->update(array("name" => $name));
                            } else {
                                $docins['name'] = $name;
                                $docins['flag'] = 2;
                                DB::table('users_document')->insertGetId($docins);
                            }
                        }
                        if ($key == 'code_claim') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            if ($reqdata['codeClaim_prev']) {
                                $mpath = public_path() . '/profile_document/' . $id_update . "/" . $reqdata['codeClaim_prev'];
                                File::delete($mpath);
                                DB::table('users_document')->where('name', 'LIKE', $reqdata['codeClaim_prev'])->update(array("name" => $name));
                            } else {
                                $docins['name'] = $name;
                                $docins['flag'] = 3;
                                DB::table('users_document')->insertGetId($docins);
                            }
                        }
                        if ($key == 'appreciation_letter') {
                            @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                            if ($reqdata['appreciationLetter_prev']) {
                                $mpath = public_path() . '/profile_document/' . $id_update . "/" . $reqdata['appreciationLetter_prev'];
                                File::delete($mpath);
                                DB::table('users_document')->where('name', 'LIKE', $reqdata['appreciationLetter_prev'])->update(array("name" => $name));
                            } else {
                                $docins['name'] = $name;
                                $docins['flag'] = 9;
                                DB::table('users_document')->insertGetId($docins);
                            }
                        }
                        @$cp = $val->move($path, $name);
                    }
                }
            }
        }
        if ($audit_log) {
            foreach ($audit_log as $key => $val) {
                $getNumeric1 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
                $getNumeric2 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);

                $nameimage = $val->getClientOriginalName();
                $extension = pathinfo($nameimage, PATHINFO_EXTENSION);
                @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                $cp = $val->move(public_path() . '/profile_document/' . $id_update . "/", $name);
                if ($cp) {
                    $docinsert['user_id'] = $id_user;
                    $docinsert['name'] = $name;
                    $docinsert['flag'] = 7;
                    $docinsert['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
                    $docinsert['protocol'] = $_SERVER['REMOTE_ADDR'];
                    $docinsert['status'] = 1;
                    $res = DB::table('users_document')->insertGetId($docinsert);
                }
            }
        }
        if ($reg_certificate) {
            foreach ($reg_certificate as $key => $val) {
                $getNumeric1 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
                $getNumeric2 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);

                $nameimage = $val->getClientOriginalName();
                $extension = pathinfo($nameimage, PATHINFO_EXTENSION);
                $nameimage = $val->getClientOriginalName();
                $nameimage = str_replace(" ", "_", $nameimage);
                @$name = $getNumeric1 . $id_user . $getNumeric2 . "." . $extension;
                $cp = $val->move(public_path() . '/profile_document/' . $id_update . "/", $name);
                if ($cp) {
                    $docinsert['user_id'] = $id_user;
                    $docinsert['name'] = $name;
                    $docinsert['flag'] = 5;
                    $docinsert['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
                    $docinsert['protocol'] = $_SERVER['REMOTE_ADDR'];
                    $docinsert['status'] = 1;
                    $res = DB::table('users_document')->insertGetId($docinsert);
                }
            }
        }
        if ($messageid) {
            Session::put('profile_activetab', "1");
            Session::flash('success_message', "Information successfully updated.");
            return redirect("/profile");
        } else {
            Session::flash('error_message', "Failure in insert");
            return redirect("/profile");
        }
    }

    public function updateProfileContact(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $id_update = @$reqdata['id_update'];
        $profile = $reqdata['profile'];
        if ($id_update) {
            $id = DB::table('users_contact_details')
                    ->where("id", $id_update)
                    ->where("user_id", Auth::user()->id)
                    ->update($profile);
            if ($id) {
                Session::put('profile_activetab', "2");
                Session::flash('success_message', "Contact details successfully updated.");
                return redirect("/profile");
            } else {
                Session::put('profile_activetab', "2");
                Session::flash('error_message', "Failure in update");
                return redirect("/profile");
            }
        } else {
            $profile['user_id'] = Auth::user()->id;
            $profile['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
            $profile['protocol'] = $_SERVER['REMOTE_ADDR'];
            $profile['status'] = 1;
            $id = DB::table('users_contact_details')
                    ->insertGetId($profile);
            if ($id) {
                Session::put('profile_activetab', "2");
                Session::flash('success_message', "Contact details inserted successfully!.");
                return redirect("/profile");
            } else {
                Session::put('profile_activetab', "2");
                Session::flash('error_message', "Failure in insert");
                return redirect("/profile");
            }
        }
    }

    public function downloaddocument($id = null, $uid = null) {
        $this->validuser();
        if ($id) {
            $document = DB::table('users_document')->where(DB::raw("md5(id)"), $id)->first();
            if ($document) {
                if ($document->flag == 10) {
                    $path = public_path() . '/isic_document/' . $uid . "/" . $document->name;
                } else {
                    $path = public_path() . '/profile_document/' . $uid . "/" . $document->name;
                }
                return response()->download($path);
            } else {
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function deletedocument(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $id = $reqdata['id'];
        $uid = $reqdata['uid'];
        if ($id) {
            $doc = DB::table('users_document')->where('id', $id)
                    ->first();
            if ($doc) {
                $path = public_path() . '/profile_document/' . $uid . "/" . $doc->name;
                File::delete($path);
                DB::table('users_document')->where('id', $id)->delete();
                Session::put('profile_activetab', "1");
                Session::flash('success_message', "Document deleted successfully.");
                print "Success";
            } else {
                Session::put('profile_activetab', "1");
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function checkAvailableDate(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $checkedAvailable = $reqdata['checkedAvailable'];
        if (@$reqdata['fromdate']) {
            $fromdate = Carbon::parse(@$reqdata['fromdate'])->format('Y-m-d');
        }
        if (@$reqdata['todate']) {
            $todate = Carbon::parse(@$reqdata['todate'])->format('Y-m-d');
        }
        $currentdate = Carbon::now()->format("Y-m-d");
        $userid = @Auth::user()->id;
        if ($checkedAvailable && empty($fromdate)) { //date not selected but only selected availability
            print "available";
        } else if ($fromdate == $currentdate) { //current date as equals to form date
            print "samestartdate";
        } else if ($fromdate > $currentdate) { //form date exceeds from current date
            print "exceedstartdate";
        }
        exit();
    }

    public function addAddress(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $user_id = @$reqdata['user_id'];
        $id_update = @$reqdata['id_update'];
        $availableMatch = @$reqdata['availableMatch'];
        $address = $reqdata['address'];
        $statusval = 1;
        if (@$reqdata['available_fromdate']) {
            $address['available_fromdate'] = Carbon::parse(@$reqdata['available_fromdate'])->format('Y-m-d');
        }
        if (@$reqdata['available_todate']) {
            $address['available_todate'] = Carbon::parse(@$reqdata['available_todate'])->format('Y-m-d');
        }
        //get city id
        $cityId = DB::table('cities')->select("id")
                        ->where('name', $address['city_name'])
                        ->where('country_id', $address['country_id'])
                        ->where('region_id', $address['state_id'])->first();
        if ($cityId) {
            $address['city_id'] = $cityId->id;
        }
        //get city id
        //set primary address if no address for user
        $addressExists = DB::table('users_contact_address')->where('user_id', $user_id)->first();
        if (empty($addressExists)) {
            $address['primary_status'] = 1;
            $address['available_status'] = 1;
        } else {
            //to set primary status if record exists
            if (!empty($address['primary_status'])) {
                $primaryExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('primary_status', $statusval)->first();
                if ($primaryExists) {
                    $upd_primary = DB::table('users_contact_address')->where("id", $primaryExists->id)
                                    ->where("user_id", @Auth::user()->id)->update(array("primary_status" => 0));
                }
            }
            //if primary address not selected
            if (empty($address['primary_status'])) {
                $address['primary_status'] = 0;
            }
            //set available status as diffrent scenario
            if (!empty($address['available_status'])) {
                //current date as equals to start date
                if ($availableMatch == 1) {
                    $address['available_status'] = 1;
                    $availableExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('available_status', $statusval)->first();
                    if ($availableExists) {
                        $upd_available = DB::table('users_contact_address')->where("id", $availableExists->id)
                                        ->where("user_id", @Auth::user()->id)->update(array("available_status" => 0));
                    }
                } else if ($availableMatch == 2) { //it will set to available on selected date
                    $primaryExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('primary_status', 1)->first();
                    if($primaryExists) {
                        $upd_primary = DB::table('users_contact_address')->where("id", $primaryExists->id)
                                    ->where("user_id", @Auth::user()->id)->update(array("available_status" => 1));
                    }
                    $address['available_status'] = 0;
                } else if ($availableMatch == 3) { //no date selected remove available existing available location and set current to available
                    $availableExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('available_status', $statusval)->first();
                    if ($availableExists) {
                        $upd_available = DB::table('users_contact_address')->where("id", $availableExists->id)
                                ->where("user_id", @Auth::user()->id)
                                ->update(array(
                            "available_status" => 0,
                            "available_fromdate" => null,
                            "available_todate" => null,
                        ));
                    }
                    $address['available_status'] = 1;
                }
            } else {
                $getTotalRow = DB::table('users_contact_address')->where('user_id', @Auth::user()->id)->count();
                if($getTotalRow == 1) {
                    $address['available_status'] = 1;
                } else if ($getTotalRow > 1) {
                    $primaryExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('primary_status', 1)->first();
                    if($primaryExists) {
                        $upd_primary = DB::table('users_contact_address')->where("id", $primaryExists->id)
                                    ->where("user_id", @Auth::user()->id)->update(array("available_status" => 1));
                    }
                    $address['available_status'] = 0;
                } else {
                    $address['available_status'] = 0;
                }
//                $availableExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('available_status', 1)->first();
//                $primaryExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('primary_status', 1)->first();
//                if($availableExists) {
//                    if($availableExists->id != $id_update) {
//                        
//                    } else if ($availableExists->id == $id_update) {
//                        
//                    }
//                } else if (empty($availableExists)) {
//                    $upd_primary = DB::table('users_contact_address')->where("id", $primaryExists->id)
//                                    ->where("user_id", @Auth::user()->id)->update(array("primary_status" => 0));
//                } else {
//                    $address['available_status'] = 0;
//                }
            }
        }
        if ($id_update) {
            $id = DB::table('users_contact_address')->where("id", $id_update)->update($address);
            if ($id || @$upd_available || @$upd_primary) {
                Session::put('profile_activetab', "2");
                Session::flash('success_message', "Information successfully updated.");
                return redirect("/profile");
            } else {
                Session::put('profile_activetab', "2");
                Session::flash('success_message', "Failure in update.");
                return redirect("/profile");
            }
        } elseif ($user_id) {
            $address['user_id'] = Auth::user()->id;
            $address['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
            $address['protocol'] = $_SERVER['REMOTE_ADDR'];
            $address['status'] = 1;
            $id_ins = DB::table('users_contact_address')->insertGetId($address);
            if ($id_ins) {
                Session::put('profile_activetab', "2");
                Session::flash('success_message', "Information successfully inserted.");
                return redirect("/profile");
            } else {
                Session::put('profile_activetab', "2");
                Session::flash('error_message', "Failure in insert.");
                return redirect("/profile");
            }
        } else {
            Session::put('profile_activetab', "2");
            Session::flash('error_message', "Error.");
            return redirect("/profile");
        }
    }

    public function getAddress(Request $request) {
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            $address = DB::table('users_contact_address')->where('id', $id)
                    ->first();
            if ($address) {
                echo json_encode($address);
            } else {
                echo "Error";
            }
        } else {
            echo "Error";
        }
        exit();
    }

    public function setCurrentAddress($id) {
        if ($id) {
            $default_exist = DB::table('users_contact_address')->where("user_id", @Auth::user()->id)->where('status_current', 1)
                    ->first();
            if ($default_exist) {
                $upd_default = DB::table('users_contact_address')->where("id", $default_exist->id)
                        ->where("user_id", @Auth::user()->id)
                        ->update(array("status_current" => 0));
            }
            $upd_id = DB::table('users_contact_address')->where("id", $id)
                    ->where("user_id", @Auth::user()->id)
                    ->update(
                    array(
                        "status_current" => 1
                    )
            );
            if ($upd_id) {
                Session::put('profile_activetab', "2");
                Session::flash('success_message', "Current address updated.");
                return redirect("/profile");
            } else {
                Session::put('profile_activetab', "2");
                Session::flash('success_message', "Error.");
                return redirect("/profile");
            }
        } else {
            Session::put('profile_activetab', "2");
            Session::flash('success_message', "Error.");
            return redirect("/profile");
        }
    }

    public function deleteAddress(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            $doc = DB::table('users_contact_address')->where('id', $id)->first();
            if ($doc) {
                $availableStatus = $doc->available_status;
                $primaryStatus = $doc->primary_status;
                DB::table('users_contact_address')->where('id', $id)->delete();
                if ($primaryStatus == 1) {
                    $getlastrow = DB::table('users_contact_address')->where("user_id", @Auth::user()->id)->orderBy('id', 'DESC')->first();
                    if ($getlastrow) {
                        $upd_primary = DB::table('users_contact_address')->where("id", $getlastrow->id)
                                        ->where("user_id", @Auth::user()->id)->update(array("primary_status" => 1));
                    }
                }
                if ($availableStatus == 1) {
                    $getlastrow = DB::table('users_contact_address')->where('primary_status', 1)->where("user_id", @Auth::user()->id)->first();
                    if ($getlastrow) {
                        $upd_available = DB::table('users_contact_address')->where("id", $getlastrow->id)
                                        ->where("user_id", @Auth::user()->id)->update(array("available_status" => 1));
                    }
                }
                print "Success";
            } else {
                Session::put('profile_activetab', "2");
                print "Error";
            }
        } else {
            Session::put('profile_activetab', "2");
            print "Error";
        }
        exit();
    }

    public function addIsicCode(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $user_id = @$reqdata['user_id'];
        $id_update = @$reqdata['id_code'];
        $code_doc = $request->file('codeDocument');
        $code_ins = array();
        $code_ins['codetype'] = $reqdata['codetype'];
        $code_ins['code_sector'] = $reqdata['codeNumber'];
        $code_ins['applied_date'] = Carbon::parse($reqdata['applied_date'])->format('Y-m-d');
        $code_ins['approved_date'] = Carbon::parse($reqdata['approved_date'])->format('Y-m-d');
        $code_ins['code_description'] = $reqdata['code_description'];
        if ($id_update) {
            $id = DB::table('users_isic_code')->where("id", $id_update)->where("user_id", Auth::user()->id)
                    ->update($code_ins);
            $codeId = $id;
        } else {
            $code_ins['user_id'] = Auth::user()->id;
            $code_ins['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
            $code_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
            $code_ins['status'] = 1;
            $id_ins = DB::table('users_isic_code')->insertGetId($code_ins);
            $codeId = $id_ins;
        }

        //upload document
        if ($code_doc) {
            foreach ($code_doc as $key => $val) {
                $getNumeric1 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
                $getNumeric2 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);

                $namefile = $val->getClientOriginalName();
                $extension = pathinfo($namefile, PATHINFO_EXTENSION);
                @$name = $getNumeric1 . $user_id . $getNumeric2 . "." . $extension;
                $cp = $val->move(public_path() . '/isic_document/' . $user_id . "/", $name);
                if ($cp) {
                    $docinsert['user_id'] = $user_id;
                    if ($id_update) {
                        $docinsert['code_id'] = $id_update;
                    } else {
                        $docinsert['code_id'] = $id_ins;
                    }
                    $docinsert['name'] = $name;
                    $docinsert['flag'] = 10;
                    $docinsert['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
                    $docinsert['protocol'] = $_SERVER['REMOTE_ADDR'];
                    $docinsert['status'] = 1;
                    $res = DB::table('users_document')->insertGetId($docinsert);
                }
            }
        }
        //upload document

        if (@$id_ins) {
            Session::put('profile_activetab', "3");
            Session::flash('success_message', "Information successfully saved.");
            return redirect("/profile");
        } elseif (@$id) {
            Session::put('profile_activetab', "3");
            Session::flash('success_message', "Information successfully updated.");
            return redirect("/profile");
        } else {
            Session::put('profile_activetab', "3");
            Session::flash('success_message', "You are not updated anything.");
            return redirect("/profile");
        }
    }

    public function getIsicCode(Request $request) {
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            $res['code'] = DB::table('users_isic_code')
                            ->join('international_standards_section', 'users_isic_code.code_sector', '=', 'international_standards_section.id')
                            ->select('users_isic_code.*', 'international_standards_section.name as codeName', 'international_standards_section.type', 'international_standards_section.code')
                            ->where('users_isic_code.id', $id)->first();
            $res['document'] = DB::table('users_document')->where('code_id', $id)->get();
            if ($res) {
                echo json_encode($res);
            } else {
                echo "Error";
            }
        } else {
            echo "Error";
        }
        exit();
    }

    public function deleteSicCode(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            $doc = DB::table('users_isic_code')->where('id', $id)->first();
            $code_doc = DB::table('users_document')->where('code_id', $id)->where('flag', 10)->get();
            foreach ($code_doc as $k => $v) {
                $path = public_path() . '/isic_document/' . $v->user_id . "/" . $v->name;
                File::delete($path);
            }
            if ($doc) {
                DB::table('users_isic_code')->where('id', $id)->delete();
                DB::table('users_document')->where('code_id', $id)->delete();
                Session::put('profile_activetab', "3");
                print "Success";
            } else {
                Session::put('profile_activetab', "3");
                print "Error";
            }
        } else {
            Session::put('profile_activetab', "3");
            print "Error";
        }
        exit();
    }

    public function deletedocIsic(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $id = $reqdata['id'];
        $user_id = $reqdata['user_id'];
        if ($id) {
            $doc = DB::table('users_document')->where('id', $id)->where('flag', 10)
                    ->first();
            if ($doc) {
                $path = public_path() . '/isic_document/' . $user_id . "/" . $doc->name;
                File::delete($path);
                DB::table('users_document')->where('id', $id)->delete();
                Session::put('profile_activetab', "3");
                Session::flash('success_message', "Document deleted successfully.");
                print "Success";
            } else {
                Session::put('profile_activetab', "3");
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function addStandard(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $user_id = @$reqdata['user_id'];
        $id_update = @$reqdata['id_standard'];
        $std_ins = array();
        $std_ins['standard_no'] = $reqdata['standardNo'];
        $std_ins['approved_date'] = Carbon::parse($reqdata['approvedDate'])->format('Y-m-d');
        $std_ins['expired_date'] = Carbon::parse($reqdata['expiryDate'])->format('Y-m-d');
        $std_ins['reinstated_date'] = Carbon::parse($reqdata['reinstatedDate'])->format('Y-m-d');
        $std_ins['verification_audit_date'] = Carbon::parse($reqdata['verAuditdDate'])->format('Y-m-d');
        $std_ins['removed_date'] = Carbon::parse($reqdata['removedDate'])->format('Y-m-d');
        $std_ins['standard_description'] = $reqdata['stdDescription'];
        $std_ins['standard_status'] = $reqdata['stdStatus'];
        if ($id_update) {
            $id = DB::table('users_standards')->where("id", $id_update)
                    ->where("user_id", Auth::user()->id)
                    ->update($std_ins);
            if ($id) {
                Session::put('profile_activetab', "4");
                Session::flash('success_message', "Information successfully updated.");
                return redirect("/profile");
            } else {
                Session::put('profile_activetab', "4");
                Session::flash('success_message', "Failure in update.");
                return redirect("/profile");
            }
        } elseif ($user_id) {
            $std_ins['user_id'] = Auth::user()->id;
            $std_ins['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
            $std_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
            $id_ins = DB::table('users_standards')
                    ->insertGetId($std_ins);
            if ($id_ins) {
                Session::put('profile_activetab', "4");
                Session::flash('success_message', "Information successfully inserted.");
                return redirect("/profile");
            } else {
                Session::put('profile_activetab', "4");
                Session::flash('success_message', "Failure in insert.");
                return redirect("/profile");
            }
        } else {
            Session::put('profile_activetab', "4");
            Session::flash('success_message', "Error.");
            return redirect("/profile");
        }
    }

    public function deleteStandard(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            $doc = DB::table('users_standards')->where('id', $id)
                    ->first();
            if ($doc) {
                DB::table('users_standards')->where('id', $id)->delete();
                Session::put('profile_activetab', "4");
                print "Success";
            } else {
                Session::put('profile_activetab', "4");
                print "Error";
            }
        } else {
            Session::put('profile_activetab', "4");
            print "Error";
        }
        exit();
    }

    public function addIndustryCode(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $isic_ins = array();
        $isic_ins['user_id'] = Auth::user()->id;
        $isic_ins['isic_code'] = $reqdata['isicCode'];
        $isic_ins['status'] = 1;
        $isic_ins['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
        $isic_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
        $id_ins = DB::table('users_industry_sector')
                ->insertGetId($isic_ins);
        if ($id_ins) {
            Session::put('profile_activetab', "5");
            Session::flash('success_message', "ISIC Code successfully inserted.");
            return redirect("/profile");
        } else {
            Session::put('profile_activetab', "5");
            Session::flash('success_message', "Failure in insert.");
            return redirect("/profile");
        }
    }

    public function deleteIsicCode(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            $doc = DB::table('users_industry_sector')->where('id', $id)
                    ->first();
            if ($doc) {
                DB::table('users_industry_sector')->where('id', $id)->delete();
                Session::put('profile_activetab', "5");
                print "Success";
            } else {
                Session::put('profile_activetab', "5");
                print "Error";
            }
        } else {
            Session::put('profile_activetab', "5");
            print "Error";
        }
        exit();
    }

    public function dashboard() {
        if (@Auth::user()->id) {
            $prefix = DB::getTablePrefix();
            $assignmentaudit = Assignment::with('AssignmentLocation')
                            ->join('assignment_request_audit', DB::raw($prefix . 'assignment_request_audit.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                            ->select("assignment_request_audit.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                            ->where('status', '=', 1)->where('close_job', '=', 0)
                            ->where('approval_status', '!=', 2)->orderBy('assignment.id', 'DESC')->limit(3)->get();

            $assignmenttraining = Assignment::with('AssignmentLocation')
                            ->join('assignment_request_training', DB::raw($prefix . 'assignment_request_training.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                            ->select("assignment_request_training.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                            ->where('status', '=', 1)->where('close_job', '=', 0)
                            ->where('approval_status', '!=', 2)->orderBy('assignment.id', 'DESC')->limit(3)->get();

            $assignmentcertification = Assignment::leftJoin('assignment_request_certification', function($join) {
                                $join->on('assignment.id', '=', 'assignment_request_certification.assignment_id');
                            })
                            ->where('assignment.assignment_type', '3')
                            ->orderBy('assignment.id', 'DESC')->limit(3)->get();
            //get cities
            $id_cities = "";
            $cityNameArray = "";
            foreach ($assignmentcertification as $key => $val) {
                @$id_cities .= $val->location . ",";
            }
            $id_cities = rtrim($id_cities, ",");
            if ($id_cities) {
                $id_citiesArray = explode(",", $id_cities);
                $data['cityNameArray'] = DB::table('cities')->whereIn('id', $id_citiesArray)->pluck("name", "id");
            }
            //get cities
            $data['assignmentaudit'] = $assignmentaudit;
            $data['assignmenttraining'] = $assignmenttraining;
            $data['assignmentcertification'] = $assignmentcertification;

            //message
            $senderid = @Auth::user()->id;
            //DB::enableQueryLog(); 
            $data['messagelist'] = DB::table('message')
                    ->where("flag", 1)
                    ->where(function($q) use ($senderid) {
                        $q->where('sender_id', $senderid)
                        ->orWhere('recive_id', $senderid);
                    })
                    ->groupBy('sender_id', 'recive_id')
                    ->groupBy('reference_id')
                    ->orderBy('message_id', 'DESC')
                    ->get();
            //message
//start Training
            $trainingpostinfo = TrainingPost::where('status', '=', 1)
                            ->orderBy('training_post_id', 'DESC')->limit(3)->get();
            $data['trainingpostinfo'] = $trainingpostinfo;
            //end Training
            $data['advtinfo'] = DB::table('advt_banner')->where('status', '=', 1)
                            ->where('location', '=', 1)->pluck("image", "shape");
            //start query
            $querypostinfo = DB::table('query')->where('status', '=', 1)
                            ->orderBy('startDate', 'ASC')->limit(2)->get();
            $data['querypostinfo'] = $querypostinfo;

            $data['country'] = DB::table('countries')->pluck("name", "id");
            $data['state'] = DB::table('states')->pluck("name", "id");
            //end query
            
            //subscription packages
        $data['package_list'] = SubscriptionPackage::where('status', 1)->orderBy('package_price', 'ASC')->limit(3)->get();
            return view('pages.dashboard', $data);
        } else {
            return redirect("/signin");
        }
    }

    public function publicProfileView($user_id) {
//        $sumrating = 0;
        $averagerating = 0;
        $gettotalaverage = 0;
        $valStatus = 1;
        if (@Auth::user()->id) {
            $prefix = DB::getTablePrefix();
            $data['userdetails'] = DB::table('users')->where('id', $user_id)->first();
            $data['contactdetails'] = DB::table('users_contact_details')->where('user_id', $user_id)->first();

            $data['addressdetails'] = DB::table('users_contact_address')
                            ->select('countries.name as countryName', 'states.name as stateName', 'users_contact_address.*')
                            ->join('countries', 'countries.id', '=', 'users_contact_address.country_id')
                            ->join('states', 'states.id', '=', 'users_contact_address.state_id')
                            ->where('users_contact_address.user_id', $user_id)
                            ->where('users_contact_address.available_status', $valStatus)->first();

            $data['generaldetails'] = DB::table('users_general_info')->where('user_id', $user_id)->first();
            $data['cpddetails'] = CpdHours::where('user_id', $user_id)->limit(3)->get();
            $data['isiccodedetails'] = DB::table('users_isic_code')
                            ->join('international_standards_section', 'users_isic_code.code_sector', '=', 'international_standards_section.id')
                            ->select('users_isic_code.*', 'international_standards_section.name as codeName', 'international_standards_section.type', 'international_standards_section.code')
                            ->where('users_isic_code.user_id', $user_id)->limit(3)->get();
//            $ratingreview = Rating::with('RatingAnswer')
//                            ->join('users', 'rating.sender_id', '=', 'users.id')
//                            ->join('assignment_request_audit as ar', 'ar.ref_id', '=', 'rating.reference_id')
//                            ->join('international_standards_section as is', 'is.id', '=', 'ar.sic_code')
//                            ->leftjoin('assignment_location as al', 'al.assignment_id', '=', 'ar.assignment_id')
//                            ->select('ar.assignment_name', 'ar.sic_code', 'ar.start_date', 'ar.end_date', 'ar.description', 'users.fname', 'users.lname', 'rating.*', 'is.code', 'is.name', 'is.type', DB::raw("(GROUP_CONCAT(city_name SEPARATOR ', ')) as `cities`"))
//                            ->groupBy('al.assignment_id')->where('recive_id', $user_id)->get();

            $ratingreview = Rating::with('RatingAnswer')
                            ->join('users', 'rating.sender_id', '=', 'users.id')
                            ->leftJoin('assignment_request_audit as ar', function($join) {
                                $join->on('ar.ref_id', '=', 'rating.reference_id');
                            })
                            ->leftJoin('assignment_request_training as at', function($join) {
                                $join->on('at.ref_id', '=', 'rating.reference_id');
                            })
                            ->leftjoin('international_standards_section as is', 'is.id', '=', 'ar.sic_code')
                            ->leftjoin('assignment_location as al', 'al.assignment_id', '=', 'rating.assignment_id')
                            ->select('ar.assignment_name', 'ar.sic_code', 'ar.start_date AS auditStart', 'ar.end_date AS auditEnd', 'ar.description', 'at.batch_no', 'at.training_type', 'at.mode', 'at.start_date AS trainingStart', 'at.end_date AS trainingEnd', 'at.session_details', 'users.fname', 'users.lname', 'rating.*', 'is.code', 'is.name', 'is.type', DB::raw("(GROUP_CONCAT(city_name SEPARATOR ', ')) as `cities`"))
                            ->groupBy('al.assignment_id')->where('recive_id', $user_id)->get();

//                print "<pre>";
//                print_r($ratingreview); exit();

            if ($data['userdetails']->usertype == 6 || $data['userdetails']->usertype == 5 || $data['userdetails']->usertype == 1) {
                $data['jobCount'] = DB::table('assignment')
                                ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                                ->where(function($q) use ($valStatus) {
                                    $q->where('assignment.approval_status', $valStatus)
                                    ->orWhere('send_proposal.approval_status', $valStatus);
                                })
                                ->where('assignment.user_id', $user_id)->groupBy('assignment.id')->count();
            } else {
                $data['jobCount'] = SendProposal::where('user_id', $user_id)->where('approval_status', 1)->count();
            }
            $data['queryname'] = DB::table('query_master')->pluck("query", "id");

            foreach ($ratingreview as $k => $v) {
                $sumrating = 0;
                foreach ($ratingreview[$k]['RatingAnswer'] as $key => $val) {
                    $sumrating += $val->answer;
                }
                $averagerating += $sumrating / count($data['queryname']);
            }
            if ($averagerating > 0) {
                $data['gettotalaverage'] = round($averagerating / count($ratingreview));
            }
            $data['ratingreview'] = $ratingreview;
            return view('pages.publicprofileview', $data);
        } else {
            return redirect("/signin");
        }
    }

    public function cpdHours() {
        if (@Auth::user()->id) {
            $data['getcpddetails'] = CpdHours::where('user_id', @Auth::user()->id)->orderBy('id', 'DESC')->get();
            $data['selectmenu'] = "cpdhours";
            return view('pages.cpdHours', $data);
        } else {
            return redirect("/signin");
        }
    }

    public function addcpdHours(Request $request) {
        $this->validuser();
        $reqdata = $request->all();
        $id_cpd = @$reqdata['id_cpd'];
        $cpd_ins['cpd_title'] = $reqdata['cpd_title'];
        $cpd_ins['cpd_description'] = $reqdata['cpd_description'];
        $cpd_ins['cpd_date'] = Carbon::parse($reqdata['cpd_date'])->format('Y-m-d');
//        $cpd_ins['cpd_links'] = implode(',', @$reqdata['cpdlinks']);
        $cpd_links = implode(',', @$reqdata['cpdlinks']);
        $cpd_ins['cpd_links'] = implode(',', array_filter(explode(',', $cpd_links)));
//        print_r($cpd_ins); exit();
        if ($id_cpd) {
            $id = CpdHours::where("id", $id_cpd)->where("user_id", Auth::user()->id)->update($cpd_ins);
            if ($id) {
                Session::flash('success_message', "CPD Hours updated Successfully!");
                return redirect("/cpdHours");
            } else {
                Session::flash('success_message', "Failure in update!");
                return redirect("/cpdHours");
            }
        } elseif (@Auth::user()->id) {
            $cpd_ins['user_id'] = @Auth::user()->id;
            $cpd_ins['status'] = 1;
            $cpd_ins['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $cpd_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
            $ins_id = CpdHours::create($cpd_ins);
            $id_insert = $ins_id->id;
            if (@$id_insert) {
                Session::flash('success_message', "CPD Hours saved Successfully!");
                return redirect("/cpdHours");
            } else {
                Session::flash('serror_message', "Failure in save!");
                return redirect("/cpdHours");
            }
        }
    }

    public function getCPD(Request $request) {
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            $res['cpdDetails'] = CpdHours::where('id', $id)->where('user_id', @Auth::user()->id)->first();
            if ($res) {
                echo json_encode($res);
            } else {
                echo "Error";
            }
        } else {
            echo "Error";
        }
        exit();
    }

    public function deleteCPD(Request $request) {
        $reqdata = $request->all();
        $id = $reqdata['id'];
        if ($id) {
            $res_info = CpdHours::where('id', $id)->where('user_id', @Auth::user()->id)->first();
            if ($res_info) {
                CpdHours::where('id', $id)->delete();
                print "Success";
            } else {
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    /* static pages */

    public function aboutus() {
        return view('pages.aboutus');
    }

    public function contactus() {
        return view('pages.contactus');
    }

    public function support() {
        return view('pages.support');
    }

    public function faq() {
        return view('pages.faq');
    }

    public function termcondition() {
        return view('pages.termcondition');
    }

    /* static pages */
}
