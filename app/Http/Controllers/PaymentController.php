<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use Mail;
use Config;
use URL;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Carbon\Carbon;
use App\PaypalPayment;
use App\SubscriptionPackage;
use App\UserPurchaseTransaction;


class PaymentController extends Controller
{
    private $SubscriptionController;
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
        
        $this->SubscriptionController = new SubscriptionController();
    }
    
    public function index() {
        return view('pages.paywithpaypal');
    }
    
    
    
    public function payWithpaypalPost(Request $request) {
        $package_id = Input::get('package_id');
        $amount = Input::get('amount');
        $user_id = Input::get('user_id');
        $this->payWithpaypal();
    }
    
    public function payWithpaypal($order_refid, $id_purchase, $package_price) {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName('Item 1') /** item name * */
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($package_price);/** unit price * */
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $amount->setCurrency('USD')
                ->setTotal($package_price);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('status')) /** Specify return URL * */
                ->setCancelUrl(URL::to('status'));
        $payment = new Payment();
        $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; * */
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::to('/testPayment');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/testPayment');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session * */
        Session::put('paypal_payment_id', $payment->getId());
        Session::put('order_refid', $order_refid);
        Session::put('purchase_id', $id_purchase);
        if (isset($redirect_url)) {
            /** redirect to paypal * */
            return Redirect::away($redirect_url);
        }
        \Session::put('error', 'Unknown error occurred');
        return Redirect::to('/');
    }

    public function getPaymentStatus() {
        /** Get the payment ID before session clear * */
        $payment_id = Session::get('paypal_payment_id');
        $order_refid = Session::get('order_refid');
        $purchase_id = Session::get('purchase_id');
        /** clear the session payment ID * */
        Session::forget('paypal_payment_id');
        Session::forget('order_refid');
        Session::forget('purchase_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::to('/testPayment');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /*         * Execute the payment * */
        $result = $payment->execute($execution, $this->_api_context);
        
        /*update payment info in purchase transaction*/
        $upd_transaction['order_refid'] = $order_refid;
        $upd_transaction['payment_id'] = $payment_id;
        $upd_transaction['payer_id'] = Input::get('PayerID');
        $upd_transaction['payment_status'] = $result->getState() == 'approved' ? 1 : 2;
        $update_transaction = UserPurchaseTransaction::where("id", $purchase_id)->update($upd_transaction);
        
        $purchase_info = UserPurchaseTransaction::where('id', $purchase_id)->first();
        if ($result->getState() == 'approved') {
            //update credit point in user
            $actionFlag = 1;
            $this->SubscriptionController->updateUserCreditPoint($actionFlag, $purchase_info->user_id, "", $purchase_info->credit_points);
            
            //insert into package purchase
            $this->SubscriptionController->subscriptionPurchase($purchase_info->user_id, $purchase_info->package_id, $purchase_info->package_price, 
                    $purchase_info->credit_points, $purchase_info->package_duration, $purchase_info->purchase_date, $purchase_info->expiry_date);
            
            //insert into creditpoint transaction
            $narration = "On subscription purchase";
            $res_transaction = $this->SubscriptionController->updateUserTransaction($purchase_info->user_id, $narration, 
                    $purchase_info->purchase_date, $purchase_info->credit_points, "");
            
             //send mail to buyer
            $packagename = SubscriptionPackage::select('package_name')->where('id', $purchase_info->package_id)->first();
            $creditInfo = DB::table('users')->select('credit_points', 'expiry_date', 'email', 'fname', 'lname')->where('id', $purchase_info->user_id)->first();
            $mail['email'] = $creditInfo->email;
            $mail['fname'] = $creditInfo->fname;
            $mail['lname'] = $creditInfo->lname;
            $mail['package_price'] = $purchase_info->package_price;
            $mail['credits_points'] = $purchase_info->credit_points;
            $mail['totalcredits_points'] = $creditInfo->credit_points;
            $mail['package_name'] = $packagename->package_name;
            $mail['expiry_date'] = $creditInfo->expiry_date;
            $mail['subject'] = "ISOMart : Subscription Package Purchase";
            $this->SubscriptionController->purchaseEmailBuyer($mail);
            
            \Session::flash('success_message', "Package subscribed Successfully!");
            return redirect("/mySubscription");
        }
        Session::flash('error_message', "Failure in subscription!");
        return redirect("/mySubscription");
    }

}
