<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use DB;
use Auth;
use Mail;
use Config;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use App\Assignment;
use App\AssignmentAudit;
use App\AssignmentTraining;
use App\AssignmentReport;
use App\SendProposal;
use App\AssignmentLocation;
use App\AssignmentCertification;
use App\AssignmentTracking;
use App\Rating;
use App\RatingAnswer;
use App\Message;
use App\CpdHours;
use App\TrainingPost;
use App\TrainingPostLocation;
use App\SubscriptionPackage;
use App\UserPurchaseTransaction;
use App\UserCreditPointsTransaction;
use App\UserPackagePurchase;
use App\TaskCredit;
use App\EmailNotification;

class ApiController extends Controller {

    private $NotificationController;
    private $SubscriptionController;

    public function __construct() {
        $this->NotificationController = new NotificationController();
        $this->SubscriptionController = new SubscriptionController();
        $action = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);
    }

    public function getUsertype() {
        $usertype = Config::get('selecttype.usertypelist');
        if ($usertype) {
            $data['Message'] = "Success";
            $data['StatusCode'] = 200;
            $data['Results'] = $usertype;
        } else {
            $data['StatusCode'] = 9999;
            $data['Message'] = "No list found.";
        }
        echo json_encode($data);
        exit();
    }

    public function getIsoStandards() {
        $iso_standards = Config::get('standards.standardlist');
        if ($iso_standards) {
            $data['Message'] = "Success";
            $data['StatusCode'] = 200;
            $data['Results'] = $iso_standards;
        } else {
            $data['StatusCode'] = 9999;
            $data['Message'] = "No list found.";
        }
        echo json_encode($data);
        exit();
    }

    public function sendOTPtoUser($data) {
        $phone = $data['phone'];
        $otp = $data['verify_otp'];
        $countrycode = $data['countrycode'];
        $tophone = "+" . $countrycode . $phone;
        $msg = $otp . " is your OTP";
        $message = urlencode($msg);
        $url = "http://sandeshlive.in/API/WebSMS/Http/v1.0a/index.php?username=1orimarkt%20&password=0Ri1@m7r4&sender=SURPLU&to=$tophone&message=$message&reqid=1&format=%7bjson%7Ctext%7d&route_id=23";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
    }

    public function otpEmailtoUser($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $otp = $data['verify_otp'];
        $subject = $data['subject'];
        $flag = $data['flag'];
        $messageBody = view('email.registration', $data);
        $a = Mail::send('email.registration', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function checkMobileNumber(Request $request) {
        $reqdata = $request->all();
        $phone = $reqdata['MobileNumber'];
        $countrycode = $reqdata['CountryCode'];
        $device_id = @$reqdata['DeviceId'] ? $reqdata['DeviceId'] : "";
        $platform = @$reqdata['Platform'] ? $reqdata['Platform'] : "";

        if ($phone) {
            $phone_exist = DB::table('users')->where('phone', $phone)
                            ->where('countrycode', $countrycode)->first();
            if ($phone_exist) {
                $data['Message'] = "Mobile number exist.";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Mobile number not found.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function login(Request $request) {
        $reqdata = $request->all();
        $usertype = Config::get('selecttype.usertypelist');
        $phone = $reqdata['MobileNumber'];
        $countrycode = $reqdata['CountryCode'];
        $password = $reqdata['Password'];
        $device_id = $reqdata['DeviceId'];
        $platform = $reqdata['Platform'];
        $fcm_token = $reqdata['FCMRegToken'];
        if (!$phone || !$password) {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
            echo json_encode($data);
            exit();
        }
        $userdata = DB::table('users')
                ->where('phone', $phone)
                ->where('countrycode', $countrycode)
                ->first();
        if ($userdata) {
            $data['Message'] = "Success";
            $data['StatusCode'] = 200;
        } else {
            $data['Message'] = "Invalid phone no or password.";
            $data['StatusCode'] = 9999;
        }
        if (Auth::attempt(['countrycode' => $countrycode, 'phone' => $phone, 'password' => $password, 'is_admin' => 0, 'status' => 1])) {
            //existing device id
            $exist_deviceid = DB::table('users')->where('device_id', "LIKE", "%$device_id%")->first();
            if (!empty($exist_deviceid)) {
                DB::table('users')->where("id", $exist_deviceid->id)
                        ->update(array(
                            "device_id" => NULL,
                            "fcm_device_token" => NULL
                ));
            } else { //if previous login device changed
                DB::table('users')->where("id", $userdata->id)
                        ->update(array(
                            "device_id" => NULL,
                            "fcm_device_token" => NULL
                ));
            }
            $update = DB::table('users')->where("id", $userdata->id)
                    ->update(array(
                "device_id" => $device_id,
                "fcm_device_token" => $fcm_token
            ));
//            exit();
            $data['Message'] = "Login successfully.";
            $data['StatusCode'] = 200;
            $data['Results'] = $userdata;
            $data['Usertype'] = $usertype[$userdata->usertype];
        } else {
            $data['Message'] = "Invalid password.";
            $data['StatusCode'] = 9999;
        }
        echo json_encode($data);
        exit();
    }

    public function testPrivate(Request $request) {
        $reqdata = $request->all();
        $data = array();
        $ins_data['testid'] = $reqdata['testid'];
        $message = "";
//        $fcmdevice_token = "edxNrHkpd4o:APA91bHh8S-6H4axYNUm4LtvWENjdRS-buFfITDFRhjiEEBMjTZN7ekWHJMc3nGUQfcRFciWvMtugmOTdVtXUV92yFPYhgG-ylxZKEXi9W6sQBQ2rQ8VkrqG-ldUvcP9JF_wH7qfIf-x";
        $fcmdevice_token = array("edxNrHkpd4o:APA91bHh8S-6H4axYNUm4LtvWENjdRS-buFfITDFRhjiEEBMjTZN7ekWHJMc3nGUQfcRFciWvMtugmOTdVtXUV92yFPYhgG-ylxZKEXi9W6sQBQ2rQ8VkrqG-ldUvcP9JF_wH7qfIf-x",
            "fRj31y-duq0:APA91bHSYqy26n9n1rcsmO-8dbCY_aU17GfrqfncMabQTtXzeNuLZdSDh2BRskLAALjz4XAHeQ7uaz7lAqyeueRG4RDIXWN9k7HA6tBU2wG__N3EVVrVd5EjmFjf0Xx4v2_59A4aO2re");
        $body = "";
        $this->testNotification($message, $fcmdevice_token, $body);
        $data['Message'] = "Notification sent successfully.";
        $data['StatusCode'] = 200;
        echo json_encode($data);
        exit();
    }

    public function testNotification($message = "", $registration_ids = "", $body = "") {
        $apiKey = "AAAAvFvOVqU:APA91bEGT9EcytGVoqNKnyVjNiof5eS_vpL0vHjj1tQhW1E7BobGbnELK2yJtklvnGd6y7QNBC5w_UNQ8ICJdsl3RSD0QoUbTNSSaWNoL3ZTHU_Jd8U3ZRkVxiD6hxWeJT0YFzwdlaqa";
        //fcm token id as recieptants
//        $recieptants = "cddSs0ef-TU:APA91bH8PfFwme1JAcrgKUvoB-MesP_JHkd_q7DvSc3jOtIuY9koc4kRsq9qIX7PBjb0bCBaGTap5kEu2tbsIuh_gAgeJFHPOqgSHwjYsLU3WNhcbxPnRdP9YG_5lOHjqmNhfyQ6YpoC";
        $url = 'https://fcm.googleapis.com/fcm/send';
        $res['ref_id'] = 147;
        $body = json_encode($res);
        $msg = array(
            'header' => 'Test',
            'text' => "test notification for ref id " . $res['ref_id']
        );
        if (is_array($registration_ids)) {
            $to = "registration_ids";
        } else {
            $to = "to";
        }
        $message = json_encode($msg);
        $fields = array
            (
            '' . $to . '' => $registration_ids,
            'data' => array("Details" => json_decode($body, true),
                "Text" => json_decode($message, true))
        );
        $headers = array('Content-Type: application/json', 'Authorization: key=' . $apiKey);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = json_decode(curl_exec($ch), true);
        if (curl_errno($ch) || $result["failure"] != 0) {
            $flag = -1;
        } else {
            $flag = 1;
        }
        curl_close($ch);
        print "<pre>";
        print_r($result);
        echo $flag;
    }

    public function requestOTP(Request $request) {
        /* generate otp & send to user */
        $reqdata = $request->all();
        $phone = $reqdata['MobileNumber'];
        $countrycode = $reqdata['CountryCode'];
        $device_id = @$reqdata['DeviceId'] ? $reqdata['DeviceId'] : "";
        $platform = @$reqdata['Platform'] ? $reqdata['Platform'] : "";
        if ($phone) {
            $userdata = DB::table('users')
                    ->where('phone', $phone)
                    ->where('countrycode', $countrycode)
                    ->first();
            if ($userdata) {
                $verify_otp = rand(1000, 9999);
                $update = DB::table('users')->where("phone", $phone)
                        ->update(
                        array(
                            "verify_otp" => $verify_otp
                        )
                );
                if ($update) {
                    $ins_data['verify_otp'] = $verify_otp;
                    $ins_data['phone'] = $phone;
                    $ins_data['countrycode'] = $countrycode;
                    $this->sendOTPtoUser($ins_data);
                    //send email
                    $email_data['email'] = $userdata->email;
                    $email_data['verify_otp'] = $verify_otp;
                    $email_data['fname'] = $userdata->fname;
                    $email_data['lname'] = $userdata->lname;
                    $email_data['flag'] = 2;
                    $email_data['subject'] = "Secure One-Time-Password for Reset Password";
                    $this->otpEmailtoUser($email_data);
                    //send email
                    $data['Message'] = "OTP sent successfully.";
                    $data['StatusCode'] = 200;
                    $data['OTP'] = $verify_otp;
                } else {
                    $data['Message'] = "Error in OTP request.";
                    $data['StatusCode'] = 9999;
                }
            } else {
                $data['Message'] = "Mobile no is not registered.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function verifyOTP(Request $request) {
        $reqdata = $request->all();
        $phone = $reqdata['MobileNumber'];
        $countrycode = $reqdata['CountryCode'];
        $otp = $reqdata['OTP'];
        $device_id = @$reqdata['DeviceId'] ? $reqdata['DeviceId'] : "";
        $platform = @$reqdata['Platform'] ? $reqdata['Platform'] : "";
        if (!$phone || !$otp) {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
            echo json_encode($data);
            exit();
        }
        $userdata = DB::table('users')
                ->where('countrycode', $countrycode)
                ->where('phone', $phone)
                ->where('verify_otp', $otp)
                ->first();
        if ($userdata) {
            DB::table('users')->where("id", $userdata->id)
                    ->update(
                            array(
                                "phone_verify_status" => 1,
                                "status" => 1, //alwaya active
                                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
                                "profile_visited" => 1 //need to discuss
                            )
            );
            $data['Message'] = "OTP verified successfully!";
            $data['StatusCode'] = 200;
            $data['Results'] = $userdata;
        } else {
            $data['Message'] = "Invalid OTP.";
            $data['StatusCode'] = 9999;
        }
        echo json_encode($data);
        exit();
    }

    public function signup(Request $request) {
        $reqdata = $request->all();
        $phone_exists = DB::table('users')->select('phone')->where('phone', $reqdata['MobileNumber'])->count();
        if ($phone_exists > 0) {
            $data['Message'] = "Phone exists.";
            $data['StatusCode'] = 9999;
            echo json_encode($data);
            exit();
        }

        $ins_data = array();
        $ins_data['fname'] = $reqdata['Fname'];
        $ins_data['lname'] = $reqdata['Lname'];
        $ins_data['email'] = $reqdata['Email'];
        $ins_data['countrycode'] = $reqdata['CountryCode'];
        $ins_data['phone'] = $reqdata['MobileNumber'];
        $ins_data['gender'] = $reqdata['Gender'];
        $ins_data['company_name'] = @$reqdata['OrgName'] ? $reqdata['OrgName'] : "";
        $ins_data['password'] = bcrypt($reqdata['Password']);
        $ins_data['usertype'] = $reqdata['UserTypeId'];
        $ins_data['verify_otp'] = rand(1000, 9999); //TODO  : to generate otp dynamically & integrate sms api
        $ins_data['otp_created_at'] = Carbon::now()->format('Y-m-d H:i:s');
        $ins_data['status'] = 0;
        $ins_data['reg_date'] = Carbon::now()->format('Y-m-d H:i:s');
        $ins_data['platform'] = 2;
        $ins_data['protocol'] = @$reqdata['DeviceId'] ? $reqdata['DeviceId'] : "";
        $ins_data['fcm_device_token'] = @$reqdata['FCMRegToken'] ? $reqdata['FCMRegToken'] : "";
        $get_ins_id = DB::table('users')->insertGetId($ins_data);

        if ($get_ins_id) {
            $this->sendOTPtoUser($ins_data);
            $ins_data['subject'] = "ISOMart : Successfully registered!";
            $ins_data['flag'] = 1;
            $this->otpEmailtoUser($ins_data);
            $data['Message'] = "Registered successfully!";
            $data['StatusCode'] = 200;
            $data['Results'] = $ins_data;
            $data['Results']['userID'] = $get_ins_id;
            /* TODO  : otp sent pending */
        } else {
            $data['Message'] = "Registered failure.";
            $data['StatusCode'] = 9999;
        }
        echo json_encode($data);
        exit();
    }

    public function setPassword(Request $request) {
        $reqdata = $request->all();
        $phone = $reqdata['MobileNumber'];
        $countrycode = $reqdata['CountryCode'];
        $password = $reqdata['Password'];
        $device_id = @$reqdata['DeviceId'] ? $reqdata['DeviceId'] : "";
        $platform = @$reqdata['Platform'] ? $reqdata['Platform'] : "";
        if (!$phone || !$password) {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
            echo json_encode($data);
            exit();
        }
        $userdata = DB::table('users')->where('phone', $phone)->where('countrycode', $countrycode)->first();
        if ($userdata) {
            $update = DB::table('users')->where("id", $userdata->id)
                    ->update(
                    array(
                        "password" => bcrypt($password),
                        "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
                    )
            );
            if ($update) {
                $data['Message'] = "Password set successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in password reset.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function updateinfo(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $fname = $reqdata['Fname'];
        $lname = $reqdata['Lname'];
        $updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $device_id = @$reqdata['DeviceId'] ? $reqdata['DeviceId'] : "";
        $platform = @$reqdata['Platform'] ? $reqdata['Platform'] : "";
        if ($user_id && $fname) {
            $uid = DB::table('users')
                    ->where("id", $user_id)
                    ->update(array("fname" => $fname, "lname" => $lname, "updated_at" => $updated_at));
            if ($uid) {
                $data['LastUpdatedAt'] = $updated_at;
                $data['Message'] = "Profile update successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in profile update.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function updateProfileImage(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $image = $reqdata['ProfilePicBase64'];
        $device_id = @$reqdata['DeviceId'] ? $reqdata['DeviceId'] : "";
        $platform = @$reqdata['Platform'] ? $reqdata['Platform'] : "";
        $path = public_path('profile_image');
        //image insert 
        if ($user_id && $image) {
            //get user data
            $user_data = DB::table('users')->where("id", $user_id)->first();
            //update img
            $img = $this->base64_to_jpeg($image);
            $imagename = $user_data->fname . "_" . $user_id . '.jpg';
            $file = $path . "/" . $imagename;
            if (file_exists($file)) {
                @unlink($file);
            }
            file_put_contents($file, $img);
            $upd['profile_img'] = $imagename;
            $upd['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $id = DB::table('users')->where("id", $user_id)->update($upd);
            if ($id) {
                $data['Message'] = "Profile update successfully!";
                $data['StatusCode'] = 200;
                $data['ProfilePicUrl'] = $imagename;
                $data['LastUpdatedAt'] = $upd['updated_at'];
            } else {
                $data['Message'] = "Failure in profile update.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    private function base64_to_jpeg($base64_string) {
        $filter_data = explode(",", $base64_string);
        $data = str_replace(' ', '+', $filter_data[1]);
        $data = base64_decode($data);
        return $data;
    }

    public function getLocationList(Request $request) {
        $reqdata = $request->all();
        $country_id = @$reqdata['CountryID'];
        $state_id = @$reqdata['StateID'];
        if (!empty($state_id)) {
            $cityList = DB::table('cities')
                            ->where('region_id', $state_id)
                            ->orderBy('name', 'ASC')->get();
            if (count($cityList) == 0) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
                echo json_encode($data);
                exit();
            } else {
                $data['Message'] = "City list found!";
                $data['StatusCode'] = 200;
                $data['Results'] = $cityList;
            }
        } else if (!empty($country_id)) {
            $stateList = DB::table('states')->where('country_id', $country_id)->orderBy('name', 'ASC')->get();
            if (count($stateList) == 0) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
                echo json_encode($data);
                exit();
            } else {
                $data['Message'] = "State list found!";
                $data['StatusCode'] = 200;
                $data['Results'] = $stateList;
            }
        } else {
            $countryList = DB::table('countries')->orderBy('name', 'ASC')->get();
            if (count($countryList) == 0) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
                echo json_encode($data);
                exit();
            } else {
                $data['Message'] = "Country list found!";
                $data['StatusCode'] = 200;
                $data['Results'] = $countryList;
            }
        }
        echo json_encode($data);
        exit();
    }

    public function getStandardsSection() {
        $standards = DB::table('international_standards_section')->get();
        if ($standards) {
            $data['Message'] = "Success";
            $data['StatusCode'] = 200;
            $data['Results'] = $standards;
        } else {
            $data['StatusCode'] = 9999;
            $data['Message'] = "No list found.";
        }
        echo json_encode($data);
        exit();
    }

    public function addAddress(Request $request) {
        $reqdata = $request->all();
        $ins_data = array();
        $statusval = 1;
        $user_id = $reqdata['UserId'];
        $confirmFlag = @$reqdata['confirmFlag'] ? @$reqdata['confirmFlag'] : 0;
        $currentdate = Carbon::now()->format("Y-m-d");
        $address = @$reqdata['Address'] ? $reqdata['Address'] : "";
        $id_update = @$reqdata['UpdateId'] ? $reqdata['UpdateId'] : "";
//        $fromdate = $address['available_fromdate'];
//        $todate = $address['available_todate'];
        if ($address['available_fromdate'] == "null" && $address['available_fromdate'] == "null") {
            $address['available_fromdate'] = null;
            $address['available_todate'] = null;
        } else {
            $address['available_fromdate'] = Carbon::parse($address['available_fromdate'])->format('Y-m-d');
            $address['available_todate'] = Carbon::parse($address['available_todate'])->format('Y-m-d');
        }
//        print "<pre>";
//        print_r($address);
//        exit();
        if (empty($user_id)) {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
            echo json_encode($data);
            exit();
        }

        //set available & rpimary status
        $addressExists = DB::table('users_contact_address')->where('user_id', $user_id)->first();
        if (empty($addressExists)) {
//            echo "no address";exit();
            $address['primary_status'] = 1;
            $address['available_status'] = 1;
        } else {
//            echo "address available";exit();
            //to set primary status if record exists
            if ($address['primary_status'] == 1) {
                $primaryExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('primary_status', $statusval)->first();
                if ($primaryExists) {
                    $upd_primary = DB::table('users_contact_address')->where("id", $primaryExists->id)
                                    ->where("user_id", $user_id)->update(array("primary_status" => 0));
                }
            }
            //check availability status & confirm flag
            if ($address['available_status'] == 1 && empty($address['available_fromdate']) && $confirmFlag == 0) { //date not selected but only selected availability
                $data['Message'] = "This location is set to your available location.";
                $data['StatusCode'] = 200;
                $data['confirmFlag'] = 3;
                echo json_encode($data);
                exit();
            } else if ($address['available_fromdate'] == $currentdate && $confirmFlag == 0) { //current date as equals to form date
                $data['Message'] = "Your previously set available location cancelled as you start this from today.";
                $data['StatusCode'] = 200;
                $data['confirmFlag'] = 1;
                echo json_encode($data);
                exit();
            } else if ($address['available_fromdate'] > $currentdate && $confirmFlag == 0) { //form date exceeds from current date
                $data['Message'] = "This location will set to your available location on the selected from date only.";
                $data['StatusCode'] = 200;
                $data['confirmFlag'] = 2;
                echo json_encode($data);
                exit();
            }
            //set available status as diffrent scenario
            if ($address['available_status'] == 1 && $confirmFlag) {
                if ($confirmFlag == 1) { //current date as equals to start date
                    $address['available_status'] = 1;
                    $availableExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('available_status', $statusval)->first();
                    if ($availableExists) {
                        $upd_available = DB::table('users_contact_address')->where("id", $availableExists->id)
                                        ->where("user_id", $user_id)->update(array("available_status" => 0));
                    }
                } else if ($confirmFlag == 2) { //it will set to available on selected date
                    $primaryExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('primary_status', 1)->first();
                    if ($primaryExists) {
                        $upd_primary = DB::table('users_contact_address')->where("id", $primaryExists->id)
                                        ->where("user_id", $user_id)->update(array("available_status" => 1));
                    }
                    $address['available_status'] = 0;
                } else if ($confirmFlag == 3) { //no date selected remove available existing available location and set current to available
                    $address['available_status'] = 1;
                    $availableExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('available_status', $statusval)->first();
                    if ($availableExists) {
                        $upd_available = DB::table('users_contact_address')->where("id", $availableExists->id)
                                ->where("user_id", $user_id)
                                ->update(array(
                            "available_status" => 0,
                            "available_fromdate" => null,
                            "available_todate" => null,
                        ));
                    }
                }
            } else {
                $getTotalRow = DB::table('users_contact_address')->where('user_id', $user_id)->count();
                if ($getTotalRow == 1) {
                    $address['available_status'] = 1;
                } else if ($getTotalRow > 1) {
                    $primaryExists = DB::table('users_contact_address')->where('user_id', $user_id)->where('primary_status', 1)->first();
                    if ($primaryExists) {
                        $upd_primary = DB::table('users_contact_address')->where("id", $primaryExists->id)
                                        ->where("user_id", $user_id)->update(array("available_status" => 1));
                    }
                    $address['available_status'] = 0;
                } else {
                    $address['available_status'] = 0;
                }
            }
        }
        if ($user_id && $id_update) { //update address
            $uid = DB::table('users_contact_address')
                    ->where("id", $id_update)
                    ->update($address);
            if ($uid) {
                $data['Message'] = "Address update successfully!";
                $data['StatusCode'] = 200;
                $data['Updation'] = 1;
            } else {
                $data['Message'] = "Failure in address update.";
                $data['StatusCode'] = 9999;
            }
        } else { //insert address
//            echo $confirmFlag; exit();
            $address['user_id'] = $user_id;
            $address['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $address['protocol'] = $_SERVER['REMOTE_ADDR'];
            $address['status'] = 1;
            $ins_id = DB::table('users_contact_address')->insertGetId($address);
            if ($ins_id) {
                $data['Message'] = "Address inserted successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in insert.";
                $data['StatusCode'] = 9999;
            }
        }
        echo json_encode($data);
        exit();
    }

    public function setCurrentAddress(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $address_id = $reqdata['AddressId'];
        if ($user_id && $address_id) {
            $default_exist = DB::table('users_contact_address')->where('user_id', $user_id)->where('status_current', 1)
                    ->first();
            if ($default_exist) {
                $upd_default = DB::table('users_contact_address')->where("id", $default_exist->id)
                                ->where("user_id", $user_id) > update(array("status_current" => 0));
            }
            $upd_id = DB::table('users_contact_address')->where("id", $address_id)
                            ->where("user_id", $user_id)->update(array("status_current" => 1));
            if ($upd_id) {
                $data['Message'] = "Current Address set successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in set current address.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad Request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function deleteAddress(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $id_delete = $reqdata['DeleteId'];
        if ($user_id && $id_delete) {
            $address = DB::table('users_contact_address')->where('id', $id_delete)->where('user_id', $user_id)
                    ->first();
            if ($address) {
                $availableStatus = $address->available_status;
                $primaryStatus = $address->primary_status;
                DB::table('users_contact_address')->where('id', $id_delete)->delete();
                if ($primaryStatus == 1) {
                    $getlastrow = DB::table('users_contact_address')->where("user_id", $user_id)->orderBy('id', 'DESC')->first();
                    if ($getlastrow) {
                        $upd_primary = DB::table('users_contact_address')->where("id", $getlastrow->id)
                                        ->where("user_id", $user_id)->update(array("primary_status" => 1));
                    }
                }
                if ($availableStatus == 1) {
                    $getlastrow = DB::table('users_contact_address')->where('primary_status', 1)->where("user_id", $user_id)->first();
                    if ($getlastrow) {
                        $upd_available = DB::table('users_contact_address')->where("id", $getlastrow->id)
                                        ->where("user_id", $user_id)->update(array("available_status" => 1));
                    }
                }
                $data['Message'] = "Address deleted successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in address delete.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad Request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function getAddress(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $address_id = @$reqdata['AddressId'] ? $reqdata['AddressId'] : "";
        if ($user_id && $address_id) {
            $addressDetail = DB::table('users_contact_address')
                            ->where('user_id', $user_id)
                            ->where('id', $address_id)->first();
            if ($addressDetail) {
                $data['Message'] = "Address info found!";
                $data['StatusCode'] = 200;
                $data['Results'] = $addressDetail;
            } else {
                $data['Message'] = "No address info found.";
                $data['StatusCode'] = 9999;
            }
        } else if ($user_id) {
            $addressDetail = DB::table('users_contact_address')
                            ->join('countries', 'countries.id', '=', 'users_contact_address.country_id')
                            ->join('states', 'states.id', '=', 'users_contact_address.state_id')
                            ->select('users_contact_address.*', 'countries.name AS countryName', 'states.name AS stateName')
                            ->where('user_id', $user_id)->orderBy('id', 'ASC')->get();
            if (count($addressDetail) == 0) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
                echo json_encode($data);
                exit();
            } else {
                $data['Message'] = "Address list found!";
                $data['StatusCode'] = 200;
                $data['Results'] = $addressDetail;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function contactDetails(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $id_update = @$reqdata['UpdateId'] ? $reqdata['UpdateId'] : "";
        $data_con['landline'] = @$reqdata['Landline'] ? $reqdata['Landline'] : "";
        $data_con['alternate_number'] = @$reqdata['AlternateNo'] ? $reqdata['AlternateNo'] : "";
        $data_con['skype'] = @$reqdata['Skype'] ? $reqdata['Skype'] : "";
        $data_con['instagram'] = @$reqdata['Instagram'] ? $reqdata['Instagram'] : "";
        $data_con['twitter'] = @$reqdata['Twitter'] ? $reqdata['Twitter'] : "";
        $data_con['whatsapp'] = @$reqdata['Whatsapp'] ? $reqdata['Whatsapp'] : "";
        if ($id_update && $user_id) {
            $id = DB::table('users_contact_details')
                    ->where("id", $id_update)
                    ->where("user_id", $user_id)
                    ->update($data_con);
            if ($id) {
                $data['Message'] = "Contact Details updated successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in update.";
                $data['StatusCode'] = 9999;
            }
        } elseif ($user_id) {
            $contactDetails = DB::table('users_contact_details')->where('user_id', $user_id)->first();
            if (empty($contactDetails)) {
                $data_con['user_id'] = $user_id;
                $data_con['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $data_con['protocol'] = $_SERVER['REMOTE_ADDR'];
                $data_con['status'] = 1;
                $id = DB::table('users_contact_details')
                        ->insertGetId($data_con);
                if ($id) {
                    $data['Message'] = "Contact Details inserted successfully!";
                    $data['StatusCode'] = 200;
                } else {
                    $data['Message'] = "Failure in insert.";
                    $data['StatusCode'] = 9999;
                }
            } else {
                $data['StatusCode'] = 200;
                $data['Message'] = "Contact Details exist!";
                $data['Results'] = $contactDetails;
            }
        } else {
            $data['Message'] = "Bad Request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function addIsicCode(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $id_update = @$reqdata['UpdateId'] ? $reqdata['UpdateId'] : "";
        $data_con['codetype'] = @$reqdata['codeType'] ? $reqdata['codeType'] : "";
        $data_con['code_sector'] = @$reqdata['codeSector'] ? $reqdata['codeSector'] : "";
        $data_con['code_description'] = @$reqdata['codeDesc'] ? $reqdata['codeDesc'] : "";
        $data_con['applied_date'] = @$reqdata['appliedDate'] ? $reqdata['appliedDate'] : NULL;
        $data_con['approved_date'] = @$reqdata['approvedDate'] ? $reqdata['approvedDate'] : NULL;
        $code_document = @$reqdata['codeDocument'];
        if (empty($user_id)) {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
            echo json_encode($data);
            exit();
        } elseif ($user_id && $id_update) {
            $uid = DB::table('users_isic_code')->where("id", $id_update)->update($data_con);
            $codeId = $uid;
            if ($uid) {
                $data['Message'] = "ISIC Code update successfully!";
                $data['StatusCode'] = 200;
                $data['Updation'] = 1;
            } else {
                $data['Message'] = "Failure in ISIC Code update.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data_con['user_id'] = $user_id;
            $data_con['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $data_con['protocol'] = $_SERVER['REMOTE_ADDR'];
            $data_con['status'] = 1;
            $ins_id = DB::table('users_isic_code')->insertGetId($data_con);
            $codeId = $ins_id;
            if ($ins_id) {
                $sicDetail = DB::table('users_isic_code')
                                ->join('users_document', 'users_document.code_id', '=', 'users_isic_code.id')
                                ->select('users_isic_code.*', 'users_document.name')
                                ->where('users_isic_code.user_id', $user_id)->orderBy('users_isic_code.id', 'DESC')->get();
                $data['Message'] = "SIC Code inserted successfully!";
                $data['StatusCode'] = 200;
                $data['Results'] = $sicDetail;
            } else {
                $data['Message'] = "Failure in insert.";
                $data['StatusCode'] = 9999;
            }
        }
        if ($code_document) {
            $docins = array();
            $docins['user_id'] = $user_id;
            $docins['code_id'] = $codeId;
            $docins['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $docins['protocol'] = $_SERVER['REMOTE_ADDR'];
            $docins['status'] = 1;
            foreach ($code_document as $key => $val) {
                if (!empty($val)) {
                    if (file_exists(public_path('isic_document/tempdocument') . "/" . $val)) {
                        $docins['flag'] = 10;
                        $docins['name'] = $val;
                        $source = public_path('isic_document/tempdocument') . "/" . $val;
                        $destination = public_path('isic_document') . "/" . $user_id . "/" . $val;
                        @rename($source, $destination);
                        @unlink($source);
                        $iddoc = DB::table('users_document')->insertGetId($docins);
                    }
                }
            }
        }
        echo json_encode($data);
        exit();
    }

    public function deleteIsicCode(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $id_delete = $reqdata['DeleteId'];
        if ($user_id && $id_delete) {
            $code_delete = DB::table('users_isic_code')->where('id', $id_delete)->where('user_id', $user_id)
                    ->first();
            if ($code_delete) {
                DB::table('users_isic_code')->where('id', $id_delete)->delete();
                DB::table('users_document')->where('code_id', $id_delete)->delete();
                $data['Message'] = "ISIC Code deleted successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in ISIC Code delete.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad Request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function getIsicCode(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $code_id = @$reqdata['CodeId'] ? $reqdata['CodeId'] : "";
        if ($user_id && $code_id) { //gets ingle code info
            $sicDetail = DB::table('users_isic_code')
                            ->leftJoin('users_document', function($join) {
                                $join->on('users_document.code_id', '=', 'users_isic_code.id');
                            })
                            ->join('international_standards_section', 'users_isic_code.code_sector', '=', 'international_standards_section.id')
                            ->select('users_isic_code.*', 'international_standards_section.name as codeName', 'international_standards_section.type', 'international_standards_section.code', DB::raw("(GROUP_CONCAT(iso_users_document.name SEPARATOR ', ')) as doc_name"), DB::raw("(GROUP_CONCAT(iso_users_document.id SEPARATOR ', ')) as doc_id"))
                            ->groupBy('users_isic_code.id')
                            ->where('users_isic_code.user_id', $user_id)->where('users_document.code_id', $code_id)
                            ->orderBy('users_isic_code.id', 'DESC')->get();
            if ($sicDetail) {
                $data['Message'] = "ISIC info found!";
                $data['StatusCode'] = 200;
                $data['Results'] = $sicDetail;
            } else {
                $data['Message'] = "No ISIC info found.";
                $data['StatusCode'] = 9999;
            }
        } else if ($user_id) {
            $sicDetail = DB::table('users_isic_code')
                            ->leftJoin('users_document', function($join) {
                                $join->on('users_document.code_id', '=', 'users_isic_code.id');
                            })
                            ->join('international_standards_section', 'users_isic_code.code_sector', '=', 'international_standards_section.id')
                            ->select('users_isic_code.*', 'international_standards_section.name as codeName', 'international_standards_section.type', 'international_standards_section.code', DB::raw("(GROUP_CONCAT(iso_users_document.name SEPARATOR ', ')) as doc_name"), DB::raw("(GROUP_CONCAT(iso_users_document.id SEPARATOR ', ')) as doc_id"))
                            ->groupBy('users_isic_code.id')
                            ->where('users_isic_code.user_id', $user_id)
                            ->orderBy('users_isic_code.id', 'DESC')->get();
            if (count($sicDetail) == 0) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
                echo json_encode($data);
                exit();
            } else {
                $data['Message'] = "ISIC code list found!";
                $data['StatusCode'] = 200;
                $data['Results'] = $sicDetail;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function addStandards(Request $request) {
        $reqdata = $request->all();
        $ins_data = array();
        $user_id = $reqdata['UserId'];
        $id_update = @$reqdata['UpdateId'] ? $reqdata['UpdateId'] : "";
        $data_con['standard_no'] = @$reqdata['standardNo'] ? $reqdata['standardNo'] : "";
        $data_con['standard_description'] = @$reqdata['stdDescription'] ? $reqdata['stdDescription'] : "";
        $data_con['standard_status'] = @$reqdata['stdStatus'] ? $reqdata['stdStatus'] : "";
        $data_con['approved_date'] = @$reqdata['approvedDate'] ? $reqdata['approvedDate'] : NULL;
        $data_con['expired_date'] = @$reqdata['expiryDate'] ? $reqdata['expiryDate'] : NULL;
        $data_con['reinstated_date'] = @$reqdata['reinstatedDate'] ? $reqdata['reinstatedDate'] : NULL;
        $data_con['verification_audit_date'] = @$reqdata['verAuditdDate'] ? $reqdata['verAuditdDate'] : "";
        $data_con['removed_date'] = @$reqdata['removedDate'] ? $reqdata['removedDate'] : "";
        $id_delete = @$reqdata['DeleteId'] ? $reqdata['DeleteId'] : "";
        if (empty($user_id)) {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
            echo json_encode($data);
            exit();
        } elseif ($user_id && $id_update) {
            $uid = DB::table('users_standards')
                    ->where("id", $id_update)
                    ->update($data_con);
            if ($uid) {
                $data['Message'] = "Standard update successfully!";
                $data['StatusCode'] = 200;
                $data['Updation'] = 1;
            } else {
                $data['Message'] = "Failure in Standard update.";
                $data['StatusCode'] = 9999;
            }
        } elseif ($user_id && $id_delete) {
            $address = DB::table('users_standards')->where('id', $id_delete)->where('user_id', $user_id)
                    ->first();
            if ($address) {
                DB::table('users_standards')->where('id', $id_delete)->delete();
                $data['Message'] = "Standard deleted successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in Standard delete.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data_con['user_id'] = $user_id;
            $data_con['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $data_con['protocol'] = $_SERVER['REMOTE_ADDR'];
            $data_con['status'] = 1;
            $ins_id = DB::table('users_standards')->insertGetId($data_con);
            if ($ins_id) {
                $stdDetail = DB::table('users_standards')
                        ->where('user_id', $user_id)
                        ->orderBy('id', 'ASC')
                        ->get();
                $data['Message'] = "Standard inserted successfully!";
                $data['StatusCode'] = 200;
                $data['Results'] = $stdDetail;
            } else {
                $data['Message'] = "Failure in insert.";
                $data['StatusCode'] = 9999;
            }
        }
        echo json_encode($data);
        exit();
    }

    public function getStandards(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $standard_id = @$reqdata['StandardId'] ? $reqdata['StandardId'] : "";
        if ($user_id && $standard_id) { //gets ingle standard info
            $stdDetail = DB::table('users_standards')
                            ->where('user_id', $user_id)
                            ->where('id', $standard_id)->first();
            if ($stdDetail) {
                $data['Message'] = "Standard info found!";
                $data['StatusCode'] = 200;
                $data['Results'] = $stdDetail;
            } else {
                $data['Message'] = "No Standard info found.";
                $data['StatusCode'] = 9999;
            }
        } else if ($user_id) {
            $stdDetail = DB::table('users_standards')
                    ->where('user_id', $user_id)
                    ->orderBy('id', 'ASC')
                    ->get();
            if (count($stdDetail) == 0) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
                echo json_encode($data);
                exit();
            } else {
                $data['Message'] = "Standard list found!";
                $data['StatusCode'] = 200;
                $data['Results'] = $stdDetail;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function uploadDocument(Request $request) {
        $reqdata = $request->all();
        $userId = $reqdata['userId'];
        $document_type = trim($reqdata['docType']); // aggrement,cv doc etc
        $document = $reqdata['doc'];
        if ($userId) {
            if ($document_type && $document) {
                $getNumeric1 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
                $getNumeric2 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
                $nameimage = $document->getClientOriginalName();
                $extension = pathinfo($nameimage, PATHINFO_EXTENSION);
                $filename = $getNumeric1 . $userId . $getNumeric2 . "." . $extension;
                $path = public_path() . '/job_document/tempdocument';
                File::makeDirectory($path, $mode = 0777, true, true);
                if ($document_type == "code") {
                    @$cp = $document->move(public_path('isic_document/tempdocument'), $filename);
                } else if ($document_type == "job") {
                    @$cp = $document->move($path, $filename);
                } else {
                    @$cp = $document->move(public_path('profile_document/tempdocument'), $filename);
                }

                $data['Message'] = "File uploaded successfully!";
                $data['StatusCode'] = 200;
                $data['Result'] = $filename;
            } else {
                $data['StatusCode'] = 9999;
                $data['Message'] = "Failure in upload.";
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function deletedocument(Request $request) {
        $reqdata = $request->all();
        $id = $reqdata['id'];
        $uid = $reqdata['uid'];
        if ($id) {
            $doc = DB::table('users_document')->where('id', $id)
                    ->first();
            if ($doc) {
                $path = public_path() . '/profile_document/' . $uid . "/" . $doc->name;
                File::delete($path);
                DB::table('users_document')->where('id', $id)->delete();
                Session::put('profile_activetab', "1");
                Session::flash('success_message', "Document deleted successfully.");
                print "Success";
            } else {
                Session::put('profile_activetab', "1");
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function testDocument(Request $request) {
        $reqdata = $request->all();
        $document = $reqdata['Document'];
        if ($document) {
            $getNumeric1 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
            $getNumeric2 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
            $nameimage = $document->getClientOriginalName();
            $extension = pathinfo($nameimage, PATHINFO_EXTENSION);
            $filename = $getNumeric1 . "10" . $getNumeric2 . "." . $extension;
            @$cp = $document->move(public_path('profile_document/tempdocument'), $filename);

            $data['Message'] = "File uploaded successfully!";
            $data['StatusCode'] = 200;
            $data['Result'] = $filename;
        } else {
            $data['StatusCode'] = 9999;
            $data['Message'] = "Failure in upload.";
        }
        echo json_encode($data);
        exit();
    }

    public function generalDetails(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $id_update = @$reqdata['UpdateId'] ? $reqdata['UpdateId'] : "";
        $data_ins['iso_standards'] = @$reqdata['isoStandards'] ? $reqdata['isoStandards'] : "";
        $data_ins['registration_date'] = @$reqdata['regDate'] ? $reqdata['regDate'] : NULL;
        $data_ins['brief_info'] = @$reqdata['briefInfo'] ? $reqdata['briefInfo'] : "";
        $data_ins['status'] = @$reqdata['profileStatus'] ? $reqdata['profileStatus'] : "";

        if ($id_update) {
            $res = DB::table('users_general_info')->where("id", $id_update)->update($data_ins);
            $id = $id_update;
//            $data['Results'] = $res;
//            echo json_encode($data);
//            exit();
        } elseif ($user_id) {
            $generalDetails = DB::table('users_general_info')->where('user_id', $user_id)->first();
            $generalDocuments = DB::table('users_document')->where('user_id', $user_id)->get();
//            if (empty($generalDetails)) {
//                $data['StatusCode'] = 9999;
//                $data['Message'] = "General Information not found!";
//                echo json_encode($data);
//                exit();
//            } else 
            if ($generalDetails) {
                $data['StatusCode'] = 200;
                $data['Message'] = "General Information Details exist!";
                $data['Results'] = $generalDetails;
                $data['ResultDocument'] = $generalDocuments;
                echo json_encode($data);
                exit();
            } else {
                $data_ins['user_id'] = $user_id;
                $data_ins['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $data_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
                $data_ins['status'] = 1;
                $id_insert = DB::table('users_general_info')
                        ->insertGetId($data_ins);
                $id = $id_insert;
            }
        }
        $document_file = array();
        $document_file['date_proof'] = @$reqdata['regDateProof'];
        $document_file['training_certificate'] = @$reqdata['trainingCertificate'];
        $document_file['aggrement_doc'] = @$reqdata['aggrement'];
        $document_file['verification_report'] = @$reqdata['verificationReport'];
        $document_file['cv_doc'] = @$reqdata['cv'];
        $document_file['code_claim'] = @$reqdata['codeClaim'];
        $document_file['appreciation_letter'] = @$reqdata['appreciationLetter'];
        $audit_log = @$reqdata['auditLog'];
        $reg_certificate = @$reqdata['regCertificate'];

        //document insert
        if (@$id_insert) {
            $path = public_path() . '/profile_document/' . $id;
            File::makeDirectory($path, $mode = 0777, true, true);
            //document upload
            if ($document_file) {
                $docins = array();
                $docins['user_id'] = $user_id;
                $docins['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $docins['protocol'] = $_SERVER['REMOTE_ADDR'];
                $docins['status'] = 1;
                foreach ($document_file as $key => $val) {
                    if (!empty($val)) {
                        if (file_exists(public_path('profile_document/tempdocument') . "/" . $val)) {
                            if ($key == 'date_proof') {
                                $docins['flag'] = 1;
                            }
                            if ($key == 'training_certificate') {
                                $docins['flag'] = 4;
                            }
                            if ($key == 'aggrement_doc') {
                                $docins['flag'] = 6;
                            }
                            if ($key == 'verification_report') {
                                $docins['flag'] = 8;
                            }
                            if ($key == 'cv_doc') {
                                $docins['flag'] = 2;
                            }
                            if ($key == 'code_claim') {
                                $docins['flag'] = 3;
                            }
                            if ($key == 'appreciation_letter') {
                                $docins['flag'] = 9;
                            }
                            if ($key == 'audit_log') {
                                $docins['flag'] = 7;
                            }
                            if ($key == 'reg_certificate') {
                                $docins['flag'] = 5;
                            }
//                                $document_name = substr($val, strpos($val, "__") + 2);
                            $docins['name'] = $val;
                            $source = public_path('profile_document/tempdocument') . "/" . $val;
                            $destination = public_path('profile_document') . "/" . @$id . "/" . $val;
                            @rename($source, $destination);
                            @unlink($source);
                            $iddoc = DB::table('users_document')->insertGetId($docins);
                        }
                    }
                }
            }
            $data['Message'] = "General Information inserted successfully!";
            $data['StatusCode'] = 200;
        } elseif (@$id_update) {
            $path = public_path() . '/profile_document/' . @$id . "/";
            if ($document_file) {
                foreach ($document_file as $key => $val) {
                    if (!empty($val)) {
                        if (file_exists(public_path('profile_document/tempdocument') . "/" . $val)) {
                            if ($key == 'date_proof') {
                                DB::table('users_document')
                                        ->where("user_id", $user_id)
                                        ->where("flag", 1)
                                        ->update(array("name" => $val));
                            }
                            if ($key == 'training_certificate') {
                                DB::table('users_document')
                                        ->where("user_id", $user_id)
                                        ->where("flag", 4)
                                        ->update(array("name" => $val));
                            }
                            if ($key == 'aggrement_doc') {
                                DB::table('users_document')
                                        ->where("user_id", $user_id)
                                        ->where("flag", 6)
                                        ->update(array("name" => $val));
                            }
                            if ($key == 'verification_report') {
                                DB::table('users_document')
                                        ->where("user_id", $user_id)
                                        ->where("flag", 8)
                                        ->update(array("name" => $val));
                            }
                            if ($key == 'cv_doc') {
                                DB::table('users_document')
                                        ->where("user_id", $user_id)
                                        ->where("flag", 2)
                                        ->update(array("name" => $val));
                            }
                            if ($key == 'code_claim') {
                                DB::table('users_document')
                                        ->where("user_id", $user_id)
                                        ->where("flag", 3)
                                        ->update(array("name" => $val));
                            }
                            if ($key == 'appreciation_letter') {
                                DB::table('users_document')
                                        ->where("user_id", $user_id)
                                        ->where("flag", 9)
                                        ->update(array("name" => $val));
                            }
                            $source = public_path('profile_document/tempdocument') . "/" . $val;
                            $destination = public_path('profile_document') . "/" . @$id . "/" . $val;
                            @rename($source, $destination);
                            @unlink($source);
                        }
                    }
                }
            }
            $data['Message'] = "General Information updated successfully!";
            $data['StatusCode'] = 200;
        }
        if ($audit_log) {
//                    if (!empty($audit_log)) {
            $docins = array();
            $docins['user_id'] = $user_id;
            $docins['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $docins['protocol'] = $_SERVER['REMOTE_ADDR'];
            $docins['status'] = 1;
            foreach ($audit_log as $key => $val) {
                if (!empty($val)) {
                    if (file_exists(public_path('profile_document/tempdocument') . "/" . $val)) {
                        $docins['flag'] = 7;
                        $docins['name'] = $val;
                        $source = public_path('profile_document/tempdocument') . "/" . $val;
                        $destination = public_path('profile_document') . "/" . @$id . "/" . $val;
                        @rename($source, $destination);
                        @unlink($source);
                        $iddoc = DB::table('users_document')->insertGetId($docins);
                    }
                }
            }
        }
        if ($reg_certificate) {
            $docins = array();
            $docins['user_id'] = $user_id;
            $docins['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $docins['protocol'] = $_SERVER['REMOTE_ADDR'];
            $docins['status'] = 1;
            foreach ($reg_certificate as $key => $val) {
                if (!empty($val)) {
                    if (file_exists(public_path('profile_document/tempdocument') . "/" . $val)) {
                        $docins['flag'] = 5;
                        $docins['name'] = $val;
                        $source = public_path('profile_document/tempdocument') . "/" . $val;
                        $destination = public_path('profile_document') . "/" . @$id . "/" . $val;
                        @rename($source, $destination);
                        @unlink($source);
                        $iddoc = DB::table('users_document')->insertGetId($docins);
                    }
                }
            }
        }
        //document insert
        if (empty($user_id)) {
            $data['Message'] = "Bad requst";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function assignmentCertification(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $id_update = @$reqdata['UpdateId'];
        $assignment = $reqdata['certification'];
        if (empty($user_id)) {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
            echo json_encode($data);
            exit();
        } else {
            if ($id_update) {
                $id = AssignmentCertification::where('id', $id_update)->update($assignment);
                if ($id) {
                    Session::flash('success_message', "Assignment details successfully updated.");
                    return redirect("/allassignment");
                } else {
                    Session::flash('error_message', "Failure in update");
                    return redirect("/requestassignment");
                }
            } else {
                //insert to Assignment
                $assignmentParent['user_id'] = $user_id;
                $assignmentParent['assignment_type'] = 3;
                $assignmentParent['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $assignmentParent['protocol'] = $_SERVER['REMOTE_ADDR'];
                $assignmentId = Assignment::create($assignmentParent);
                $id_assignment = $assignmentId->id;
                //insert to Assignment

                $assignment['assignment_id'] = $id_assignment;
                $assignment['admin_approval'] = 1;
                $assignment['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $assignment['protocol'] = $_SERVER['REMOTE_ADDR'];
                $res = AssignmentCertification::create($assignment);
                $id = $res->id;

                if ($id) {
                    $ref_id = "CE" . date("Y") . date("m") . date("d") . $id;
                    AssignmentCertification::where('id', $id)->update(array(
                        "ref_id" => $ref_id
                    ));
                    $data_report['user_id'] = $user_id;
                    $data_report['assignment_id'] = $id_assignment;
                    $data_report['assignment_type'] = 3;
                    $data_report['status'] = $assignment['assignment_status'];
                    $data_report['description'] = $assignment['description'];
                    $data_report['add_date'] = date("Y-m-d H:i:s");
                    $data_report['protocol'] = $_SERVER['REMOTE_ADDR'];
                    AssignmentReport::create($data_report);
                    //send mail to admin
                    $mail['ref_id'] = $ref_id;
                    $mail['subject'] = "ISOMart : New Certification Assignment";
                    $mail['assignment'] = 3;
                    $mail['fname'] = $reqdata['Fname'];
                    $mail['lname'] = $reqdata['Lname'];
                    $mail['email'] = $reqdata['Email'];
                    $this->assignmentRequestEmail($mail);
                    //send mail to admin
                    $data['Message'] = "Assignement created Successfully!";
                    $data['StatusCode'] = 200;
                } else {
                    $data['Message'] = "Failure in insert!";
                    $data['StatusCode'] = 9999;
                }
            }
        }
        echo json_encode($data);
        exit();
    }

    public function deleteLocation(Request $request) {
        $reqdata = $request->all();
        $location_id = $reqdata['locationId'];
        if ($location_id) {
            $location_exist = AssignmentLocation::where('id', $location_id)->first();
            if ($location_exist) {
                AssignmentLocation::where('id', $location_id)->delete();
                $data['Message'] = "Location deleted successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in Location delete.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad Request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function requestAssignmentAudit(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $email = $reqdata['Email'];
        $fname = $reqdata['Fname'];
        $lname = $reqdata['Lname'];
        $id_update = @$reqdata['UpdateId']; //assignemnt_id
        $assignment = @$reqdata['audit'];
        $audit_location = @$reqdata['auditlocation'];
        $assignment_type = 1;
        $location = array();
        if (empty($user_id)) {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
            echo json_encode($data);
            exit();
        } else {
            $taskPoints = $this->SubscriptionController->getTaskPoint("Create Assignment");
            $creditPoints = $this->SubscriptionController->getUserCreditPoint($user_id);
            if ($creditPoints < $taskPoints && empty($id_update)) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "You have not sufficient credit points available!";
                echo json_encode($data);
                exit();
            } else {
                if (@$id_update) {
                    //insert to location
                    if (count($audit_location) > 0) {
                        foreach ($audit_location as $key => $val) {
                            if ($val['loc_id']) {
                                
                            } else {
                                $location['country_id'] = $val['country_id'];
                                $location['state_id'] = $val['state_id'];
                                $location['city_name'] = $val['city_name'];
                                $location['city_id'] = $val['city_id'];
                                $location['effort'] = $val['effort'];
                                $location['assignment_id'] = $id_update;
                                $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                                $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                                $locId = AssignmentLocation::create($location);
                                $id_location = $locId->id;
                            }
                        }
                    }
                    Assignment::where('id', $id_update)->update(array("status" => $assignment['assignment_status']));
                    $id = AssignmentAudit::where('assignment_id', $id_update)->update($assignment);
                    if ($assignment['assignment_status'] == 1) {
                        $ref_id = @$reqdata['ref_id'];
                        $this->NotificationController->assignmentNotification($ref_id, $id_update, $assignment_type, @$id_update);
                    }
                } else {
                    //insert to Assignment
                    $assignmentParent['user_id'] = $user_id;
                    $assignmentParent['assignment_type'] = 1;
                    $assignmentParent['status'] = $assignment['assignment_status'];
                    $assignmentParent['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                    $assignmentParent['protocol'] = $_SERVER['REMOTE_ADDR'];
                    $assignmentId = Assignment::create($assignmentParent);
                    $id_assignment = $assignmentId->id;
                    //insert to Assignment
                    //insert to location
                    foreach ($audit_location as $key => $val) {
                        $location['country_id'] = $val['country_id'];
                        $location['state_id'] = $val['state_id'];
                        $location['city_name'] = $val['city_name'];
                        $location['city_id'] = $val['city_id'];
                        $location['effort'] = $val['effort'];
                        $location['assignment_id'] = $id_assignment;
                        $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                        $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                        AssignmentLocation::create($location);
                    }
                    //insert to location
                    $assignment['assignment_id'] = $id_assignment;
                    $assignment['user_id'] = $user_id;
                    $assignment['admin_approval'] = 1;
                    $assignment['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                    $assignment['protocol'] = $_SERVER['REMOTE_ADDR'];

                    //update transaction
                    $debitPoints = $this->SubscriptionController->updateUserCreditPoint("", $user_id, $taskPoints, "");
                    if ($debitPoints) {
                        $updateTransaction = $this->SubscriptionController->updateUserTransaction($user_id, "Create Audit Assignment", Carbon::now()->format('Y-m-d'), "", $taskPoints);
                    }

                    $res = AssignmentAudit::create($assignment);
                    $id_ins = $res->id;
                    if ($id_ins) {
                        $ref_id = "AU" . Carbon::now()->format("Y") . Carbon::now()->format("m") . Carbon::now()->format("d") . $id_assignment;
                        AssignmentAudit::where('id', $id_ins)->update(array(
                            "ref_id" => $ref_id
                        ));
                        $data_report['user_id'] = $user_id;
                        $data_report['assignment_id'] = $id_assignment;
                        $data_report['assignment_type'] = 1;
                        $data_report['status'] = $assignment['assignment_status'];
                        $data_report['description'] = $assignment['description'];
                        $data_report['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                        $data_report['protocol'] = $_SERVER['REMOTE_ADDR'];
                        AssignmentReport::create($data_report);
                        
                        //send notification to bidder
                        if ($assignment['assignment_status'] == 1) {
                            $this->NotificationController->assignmentNotification($ref_id, $id_assignment, $assignment_type);
                        }
                    }
                }
                
                //store email notification info
                $emailInfo['assignment_id'] = @$id_update ? $id_update : $id_assignment;
                $emailInfo['id_update'] = @$id_update ? @$id_update : "";
                $emailInfo['ref_id'] = $ref_id;
                $emailInfo['assignment_name'] = $assignment['assignment_name'];
                $emailInfo['effort_tentative'] = $assignment['effort_tentative'];
                $emailInfo['budget_usd'] = $assignment['budget_usd'];
                $emailInfo['detail_url'] = asset('assignmentAuditDetail/' . $emailInfo['assignment_id']);
                $this->emailNotificationSave($emailInfo);
                
                if (@$id_update) {
                    if (@$id || @$id_location) {
                        $data['Message'] = "Assignement updated Successfully!";
                        $data['StatusCode'] = 200;
                    } else {
                        $data['Message'] = "You are not updated anything.";
                        $data['StatusCode'] = 9999;
                    }
                } else {
                    if ($id_ins) { //for insert
                        $data['Message'] = "Assignement inserted Successfully!";
                        $data['StatusCode'] = 200;
                    } else {
                        $data['Message'] = "Failure in insert!";
                        $data['StatusCode'] = 9999;
                    }
                }
            }
        }
        echo json_encode($data);
        exit();
    }

    public function requestAssignmentTraining(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $email = $reqdata['Email'];
        $fname = $reqdata['Fname'];
        $lname = $reqdata['Lname'];
        $id_update = @$reqdata['UpdateId'];
        $assignment = @$reqdata['training'];
        $training_location = @$reqdata['traininglocation'];
        $assignment_type = 2;
        $location = array();
        if (empty($user_id)) {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
            echo json_encode($data);
            exit();
        } else {
            $taskPoints = $this->SubscriptionController->getTaskPoint("Create Assignment");
            $creditPoints = $this->SubscriptionController->getUserCreditPoint($user_id);
            if ($creditPoints < $taskPoints && empty($id_update)) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "You have not sufficient credit points available!";
                echo json_encode($data);
                exit();
            }
            if (@$id_update) { //in case of update
                //insert to location
                if (count($training_location) > 0) {
                    foreach ($training_location as $key => $val) {
                        if ($val['loc_id']) {
                            
                        } else {
                            $location['assignment_id'] = $id_update;
                            $location['country_id'] = $val['country_id'];
                            $location['state_id'] = $val['state_id'];
                            $location['city_name'] = $val['city_name'];
                            $location['city_id'] = $val['city_id'];
                            $location['effort'] = $val['effort'];
                            $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                            $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                            $id_location = AssignmentLocation::insertGetId($location);
//                            $id_location = $locId->id;
                        }
                    }
                }
                Assignment::where('id', $id_update)->update(array("status" => $assignment['assignment_status']));
                $id = AssignmentTraining::where('assignment_id', $id_update)->update($assignment);
                if ($training['assignment_status'] == 1) {
                    $ref_id = @$reqdata['ref_id'];
                    $result = $this->NotificationController->assignmentNotification($ref_id, $id_update, $assignment_type, @$id_update);
                }
            } else {
                //insert to Assignment
                $assignmentParent['user_id'] = $user_id;
                $assignmentParent['assignment_type'] = 2;
                $assignmentParent['status'] = $assignment['assignment_status'];
                $assignmentParent['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $assignmentParent['protocol'] = $_SERVER['REMOTE_ADDR'];
                $id_assignment = Assignment::insertGetId($assignmentParent);
//                $id_assignment = $assignmentId->id;
                //insert to location
                foreach ($training_location as $key => $val) {
                    $location['country_id'] = $val['country_id'];
                    $location['state_id'] = $val['state_id'];
                    $location['city_name'] = $val['city_name'];
                    $location['city_id'] = $val['city_id'];
                    $location['effort'] = $val['effort'];
                    $location['assignment_id'] = $id_assignment;
                    $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                    $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                    AssignmentLocation::create($location);
                }

                //insert into assignment audit
                $assignment['assignment_id'] = $id_assignment;
                $assignment['admin_approval'] = 1;
                $assignment['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $assignment['protocol'] = $_SERVER['REMOTE_ADDR'];

                //update transaction
                $debitPoints = $this->SubscriptionController->updateUserCreditPoint("", $user_id, $taskPoints, "");
                if ($debitPoints) {
                    $updateTransaction = $this->SubscriptionController->updateUserTransaction($user_id, "Create Training Assignment", Carbon::now()->format('Y-m-d'), "", $taskPoints);
                }
                $id_ins = AssignmentTraining::insertGetId($assignment);

                if ($id_ins) {
                    $ref_id = "TR" . Carbon::now()->format("Y") . Carbon::now()->format("m") . Carbon::now()->format("d") . $id_assignment;
                    AssignmentTraining::where('id', $id_ins)->update(array(
                        "ref_id" => $ref_id
                    ));
                    $data_report['user_id'] = $user_id;
                    $data_report['assignment_id'] = $id_assignment;
                    $data_report['assignment_type'] = 2;
                    $data_report['status'] = $assignment['assignment_status'];
                    $data_report['description'] = $assignment['session_details'];
                    $data_report['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                    $data_report['protocol'] = $_SERVER['REMOTE_ADDR'];
                    AssignmentReport::create($data_report);
                }
            }
            
            //store email notification info
            $emailInfo['assignment_id'] = @$id_update ? @$id_update : $id_assignment;
            $emailInfo['id_update'] = @$id_update ? @$id_update : "";
            $emailInfo['ref_id'] = $ref_id;
            $emailInfo['assignment_name'] = $assignment['batch_no'];
            $emailInfo['effort_tentative'] = $assignment['effort_tentative'];
            $emailInfo['budget_usd'] = $assignment['fee_range'];
            $emailInfo['detail_url'] = asset('assignmentTrainingDetail/' . $emailInfo['assignment_id']);
            $this->emailNotificationSave($emailInfo); 
            
            if (@$id_update) {
                if (@$id || @$id_location) {
                    $data['Message'] = "Assignement updated Successfully!";
                    $data['StatusCode'] = 200;
                } else {
                    $data['Message'] = "You are not updated anything.";
                    $data['StatusCode'] = 9999;
                }
            } else {
                if (@$id_ins) {
                    $data['Message'] = "Assignement inserted Successfully!";
                    $data['StatusCode'] = 200;
                } else {
                    $data['Message'] = "Failure in insert!";
                    $data['StatusCode'] = 9999;
                }
                
            }
        }
        echo json_encode($data);
        exit();
    }
    
    public function emailNotificationSave($emailInfo) {
        $ref_id = $emailInfo['ref_id'];
        $assignment_name = $emailInfo['assignment_name'];
        $effort_tentative = $emailInfo['effort_tentative'];
        $budget_usd = $emailInfo['budget_usd'];
        $detail_url = $emailInfo['detail_url'];
        $id_update = $emailInfo['id_update'];
        if (@$id_update) {
            $msgbody = "<p>Assignment $ref_id has been updated by the owner.</p>"
                    . "<p>The details are as follows: <br/>"
                    . "<strong>Assignment Details</strong><br/>"
                    . "Assignment Name - " . $assignment_name . "<br/>"
                    . "Effort - " . $effort_tentative . "<br/>"
                    . "Budget - " . $budget_usd . "</p>"
                    . "<p>Please click <a href='$detail_url'>here</a> to view the details and bid for the same</p><br />"
                    . "Thanks,<br />ISOMart Team";
        } else {
            $msgbody = "<p>A new assignment has been posted that matches your industry sector or location.</p>"
                    . "<p>The details are as follows: <br/>"
                    . "<strong>Assignment Details</strong><br/>"
                    . "Assignment Name - " . $assignment_name . "<br/>"
                    . "Assignment Ref ID - $ref_id<br/>"
                    . "Effort - " . $effort_tentative . "<br/>"
                    . "Budget - " . $budget_usd . "</p>"
                    . "<p>Please click <a href='$detail_url'>here</a> to view the details and bid for the same</p>"
                    . "Thanks,<br />ISOMart Team";
        }

        $providerList = DB::table('users')->select('email', 'fname', 'lname')->whereIn('usertype', array(4, 8))->where('status', 1)->get();
        foreach ($providerList as $k => $v) {
            $ins_email['email_type'] = @$id_update ? 2 : 1; //2->update, 1->insert
            $ins_email['email_to'] = $v->email;
            $ins_email['email_name'] = $v->fname . " " . $v->lname;
            $ins_email['email_subject'] = @$id_update ? "Assignment $ref_id Updated" : "New ISO Assignment";
            $ins_email['email_message'] = "<p>Hello " . $ins_email['email_name'] . "</p>" . $msgbody;
            $ins_email['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $ins_email['protocol'] = $_SERVER['REMOTE_ADDR'];
            EmailNotification::insertGetId($ins_email);
        }
    }

    public function myAssignmentList(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $flag = $reqdata['assignmentType'];
        $assignmentdetail['assignmentInfo'] = array();
        $prefix = DB::getTablePrefix();
        if ($user_id && $flag) {
            if ($flag == 1) {
                $assignmentlist = Assignment::with('AssignmentLocation')
                                ->join('assignment_request_audit', DB::raw($prefix . 'assignment_request_audit.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                                ->select("assignment.assignment_type", "assignment_request_audit.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                                ->where('user_id', $user_id)->where('assignment_type', $flag)
                                ->where('approval_status', '!=', 2)->where('close_job', '=', 0)
                                ->orderBy('assignment.id', 'DESC')->get();
                foreach ($assignmentlist as $k => $v) {
//                    $now = time(); // or your date as well
//                    $your_date = strtotime($v['proposal_end_date']);
//                    $datediff = $your_date - $now;
//                    $daysleft = round($datediff / (60 * 60 * 24));
                    //TODO: days difference in carbon
                    $now = Carbon::now()->format('Y-m-d'); // or your date as well
                    $end_date = Carbon::parse($v['proposal_end_date']);
                    $now_date = Carbon::parse($now);
                    $daysleft = $now_date->diffInDays($end_date, false);
                    $assignmentlist[$k]['openDaysLeft'] = $daysleft;
                }
            } else if ($flag == 2) {
//                $assignmentlist = AssignmentTraining::orderBy('id', 'ASC')
//                        ->where('user_id', $user_id)
//                        ->get();
                $assignmentlist = Assignment::with('AssignmentLocation')
                                ->join('assignment_request_training', DB::raw($prefix . 'assignment_request_training.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                                ->select("assignment.assignment_type", "assignment_request_training.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort"))
                                ->where('user_id', $user_id)->where('assignment_type', $flag)
                                ->where('approval_status', '!=', 2)->where('close_job', '=', 0)
                                ->orderBy('assignment.id', 'DESC')->get();

                foreach ($assignmentlist as $k => $v) {
//                    $now = time(); 
//                    $your_date = strtotime($v['proposal_end_date']);
//                    $datediff = $your_date - $now;
//                    $daysleft = round($datediff / (60 * 60 * 24));
                    //TODO: days difference in crbon
                    $now = Carbon::now()->format('Y-m-d'); // or your date as well
                    $end_date = Carbon::parse($v['proposal_end_date']);
                    $now_date = Carbon::parse($now);
                    $daysleft = $now_date->diffInDays($end_date, false);
                    $assignmentlist[$k]['openDaysLeft'] = $daysleft;
                }
            } else if ($flag == 3) {
                $assignmentlist = Assignment::leftJoin('assignment_request_certification', function($join) {
                                    $join->on('assignment.id', '=', 'assignment_request_certification.assignment_id');
                                })
                                ->where('assignment.approval_status', '!=', 2)
                                ->where('assignment.assignment_type', $flag)
                                ->where('assignment.user_id', $user_id)
                                ->orderBy('assignment.id', 'DESC')->get();
                //get location array
                $id_cities = $cityNameArray = "";
                foreach ($assignmentlist as $key => $val) {
                    @$id_cities .= $val->location . ",";
                }
                $id_cities = rtrim($id_cities, ",");
                if ($id_cities) {
                    $id_citiesArray = explode(",", $id_cities);
                    $cityNameArray = DB::table('cities')->whereIn('id', $id_citiesArray)->pluck("name", "id");
                }
                //get location array
                $assignmentlist['locationArray'] = $cityNameArray;
            }
            if (count($assignmentlist) == 0) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
                echo json_encode($data);
                exit();
            } else {
                $data['Message'] = "Assignment list found!";
                $data['StatusCode'] = 200;
                $data['Results'] = $assignmentlist;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function allAssignmentList(Request $request) {
        $reqdata = $request->all();
        $flag = $reqdata['assignmentType'];
        $search_text = @$reqdata['searchText'] ? $reqdata['searchText'] : "";
        $arraysearch = $arraysearchdesc = array();
        $prefix = DB::getTablePrefix();
        $banner_images = array(
            0 => asset('banner-app/iso1.jpg'),
            1 => asset('banner-app/iso2.jpg'),
            2 => asset('banner-app/iso3.jpg')
        );
        if ($flag) {
            if ($flag == 1) {
                //search result
                if ($search_text) {
                    $arraysearch = array(
                        array("assignment_name", "LIKE", "%$search_text%"),
                    );
                    $arraysearchdesc = array(
                        array("description", "LIKE", "%$search_text%"),
                    );
                }
                $assignmentlist = Assignment::with('AssignmentLocation')
                                ->join('assignment_request_audit', DB::raw($prefix . 'assignment_request_audit.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                                ->select("assignment_request_audit.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort")
                                )
                                ->orWhere($arraysearch)->orWhere($arraysearchdesc)->where('approval_status', '!=', 2)->where('close_job', '=', 0)
                                ->where('assignment_request_audit.assignment_status', '=', 1)->where('assignment_type', $flag)->orderBy('assignment.id', 'DESC')->get();
                foreach ($assignmentlist as $k => $v) {
//                    $now = time(); // or your date as well
//                    $your_date = strtotime($v->proposal_end_date);
//                    $datediff = $your_date - $now;
//                    $daysleft = round($datediff / (60 * 60 * 24));
                    //TODO: days difference in crbon
                    $now = Carbon::now()->format('Y-m-d'); // or your date as well
                    $end_date = Carbon::parse($v->proposal_end_date);
                    $now_date = Carbon::parse($now);
                    $daysleft = $now_date->diffInDays($end_date, false);
                    $assignmentlist[$k]['openDaysLeft'] = $daysleft;
                }
            } else if ($flag == 2) {
                if ($search_text) {
                    $arraysearch = array(
                        array("session_details", "LIKE", "%$search_text%"),
                    );
                }
                $assignmentlist = Assignment::with('AssignmentLocation')
                                ->join('assignment_request_training', DB::raw($prefix . 'assignment_request_training.assignment_id'), '=', DB::raw($prefix . 'assignment.id'))
                                ->select("assignment_request_training.*", "assignment.*", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobEffort")
                                )
                                ->orWhere($arraysearch)->where('approval_status', '!=', 2)->where('close_job', '=', 0)
                                ->where('assignment_request_training.assignment_status', '=', 1)->where('assignment_type', $flag)->orderBy('assignment.id', 'DESC')->get();
                foreach ($assignmentlist as $k => $v) {
//                    $now = time(); // or your date as well
//                    $your_date = strtotime($v->proposal_end_date);
//                    $datediff = $your_date - $now;
//                    $daysleft = round($datediff / (60 * 60 * 24));
                    //TODO: days difference in crbon
                    $now = Carbon::now()->format('Y-m-d'); // or your date as well
                    $end_date = Carbon::parse($v->proposal_end_date);
                    $now_date = Carbon::parse($now);
                    $daysleft = $now_date->diffInDays($end_date, false);
                    $assignmentlist[$k]['openDaysLeft'] = $daysleft;
                }
//                $assignmentlist = AssignmentTraining::orderBy('id', 'DESC')->get();
            } else if ($flag == 3) {
                $res = Assignment::leftJoin('assignment_request_certification', function($join) {
                            $join->on('assignment.id', '=', 'assignment_request_certification.assignment_id');
                        })
                        ->where('assignment.approval_status', '!=', 2)
                        ->where('assignment.assignment_type', $flag);
                if ($search_text) {
                    $res->where(function($q) use ($search_text) {
                        $q->orWhere("assignment_request_certification.assignment_name", "LIKE", "%$search_text%")
                                ->orWhere("assignment_request_certification.description", "LIKE", "%$search_text%");
                    });
                }

                $assignmentlist = $res->orderBy('assignment.id', 'DESC')->get();
                //get location array

                foreach ($assignmentlist as $key => $val) {
                    @$id_cities .= $val->location . ",";
                }
                $id_cities = rtrim($id_cities, ",");
                if ($id_cities) {
                    $id_citiesArray = explode(",", $id_cities);
                    $cityNameArray = DB::table('cities')->whereIn('id', $id_citiesArray)->pluck("name", "id");
                }
                //get location array
            }
            if (count($assignmentlist) == 0) {
                $data['StatusCode'] = 9999;
                $data['BannerImage'] = $banner_images;
                $data['Message'] = "No list found.";
                echo json_encode($data);
                exit();
            } else {
                $data['Message'] = "Assignment list found!";
                $data['StatusCode'] = 200;
                $data['Results']['AssignmentList'] = $assignmentlist;
                $data['Results']['BannerImage'] = $banner_images;
//                $data['Results']['locationArray'] = $cityNameArray;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function deleteAssignment(Request $request) {
        $reqdata = $request->all();
        $assignment_id = $reqdata['assignmentId'];
        $assignment_type = $reqdata['assignmentType'];
        $ref_id = $reqdata['ref_id'];
        if ($assignment_id && $assignment_type) {
            //Delete assignment accroding to type
            if ($assignment_type == 1) {
                Assignment::where("id", $assignment_id)->update(array('approval_status' => 2));
            } else if ($assignment_type == 2) {
                Assignment::where("id", $assignment_id)->update(array('approval_status' => 2));
            }
            
            $msgbody = "<p>Assignment $ref_id has been cancelled by the owner.</p>"
                    . "<p>Subsequently, we have cancelled your bid associated with this assignment.</p>"
                    . "Thanks,<br />ISOMart Team";
            $providerList = DB::table('send_proposal')
                        ->join('users as u', 'u.id', '=', 'send_proposal.user_id')
                        ->select('u.email', 'u.fname', 'u.lname', 'u.fcm_device_token')
                        ->where('send_proposal.assignment_id', $assignment_id)->get();
            $fcm_token = array();
            foreach ($providerList as $k => $v) {
                $fcm_token[] = $v->fcm_device_token;
                $ins_email['email_type'] = 3; //3->delete
                $ins_email['email_to'] = $v->email;
                $ins_email['email_name'] = $v->fname . " " . $v->lname;
                $ins_email['email_subject'] = "Assignment $ref_id Cancelled";
                $ins_email['email_message'] = "<p>Hello " . $ins_email['email_name'] . "</p>" . $msgbody;
                $ins_email['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $ins_email['protocol'] = $_SERVER['REMOTE_ADDR'];
                EmailNotification::insertGetId($ins_email);
            }
            //notification to auditors
            $id_update = "";
            $fcm_token_bidder = $fcm_token;
            $this->NotificationController->assignmentNotification($ref_id, $assignment_id, $assignment_type, $id_update, $assignment_id, $fcm_token_bidder);
            $data['Message'] = "Assignment deleted successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 9999;
            $data['Message'] = "Error in deletion.";
        }
        echo json_encode($data);
        exit();
    }

    public function assignmentDeleteEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $messageBody = view('email.deleteAssignmentEmail', $data);
        $a = Mail::send('email.deleteAssignmentEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    private function assignmentAuditDetail($details) {
        $assignment_id = $details['assignment_id'];
        $user_id = $details['user_id'];
        $prefix = DB::getTablePrefix();
        $assignmentdetails = AssignmentAudit::
                        join('international_standards_section', DB::raw($prefix . 'international_standards_section.id'), '=', DB::raw($prefix . 'assignment_request_audit.sic_code'))
                        ->join('assignment', 'assignment.id', '=', 'assignment_request_audit.assignment_id')
                        ->select("assignment_request_audit.*", 'assignment.user_id', 'assignment.approval_status', 'assignment.close_job', "international_standards_section.code", "international_standards_section.name AS codeName", "international_standards_section.type AS codeType", DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment_request_audit.assignment_id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment_request_audit.assignment_id ) as jobEffort")
                        )
                        ->where('assignment_request_audit.assignment_id', $assignment_id)->first();
        if (count($assignmentdetails) == 0) {
            $data['StatusCode'] = 9999;
            $data['Message'] = "No details found.";
        } else {
            $codematch = 0;
            $locationname = DB::table('assignment_location')
                            ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                            ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                            ->join('states', 'states.id', '=', 'assignment_location.state_id')
                            ->where('assignment_location.assignment_id', $assignment_id)->get();
            $assignmentdetails['locationNames'] = $locationname;
            //send bid by standard & location
            $assignmentdetails['codeAvailability'] = DB::table('users_isic_code')->where('user_id', $user_id)->count();
            //code mapping check
            $standardDeatil = DB::table('international_standards_section')->where('id', $assignmentdetails->sic_code)->first();
            $getisicCode = DB::table('users_isic_code')
                            ->join('international_standards_section', 'users_isic_code.code_sector', '=', 'international_standards_section.id')
                            ->select('users_isic_code.*', 'international_standards_section.name as codeName', 'international_standards_section.type', 'international_standards_section.code')
                            ->where('users_isic_code.user_id', $user_id)->get();
            foreach ($getisicCode as $k => $v) {
                $codematch = DB::table('international_standards_mapping')
                                ->where($v->type, $v->code)->where($standardDeatil->type, $standardDeatil->code)->first();
            }
            if ($user_id) {
                $assignmentdetails['codeSearch'] = count($codematch);
            } else {
                $assignmentdetails['codeSearch'] = 0;
            }

            //$assignmentdetails mapping check
            //send bid by standard & location
            //get bid details
            if ($assignmentdetails->user_id == $user_id) {
                $proposaldetails = DB::table('send_proposal')
                                ->select('users.fname', 'users.lname', 'users.profile_img', 'users.id AS userId', 'users.gender', 'send_proposal.*')
                                ->join('users', 'send_proposal.user_id', '=', 'users.id')
                                ->where('send_proposal.assignment_id', $assignment_id)->get();
//                                ->where('send_proposal.approval_status', '=', 0)
            } else if ($user_id) {
                $proposaldetails = DB::table('send_proposal')
                                ->select('users.fname', 'users.lname', 'users.profile_img', 'users.id AS userId', 'users.gender', 'send_proposal.*')
                                ->join('users', 'send_proposal.user_id', '=', 'users.id')
                                ->where('send_proposal.assignment_id', $assignment_id)
                                ->where('send_proposal.approval_status', '!=', 3)
                                ->where('send_proposal.user_id', $user_id)->get();

                $data['isProposalSent'] = count($proposaldetails) > 0;
            }
            //get bid details
            //get count of total list
            $ProposalNo = SendProposal::where('assignment_id', '=', $assignment_id)->count();
            //user details
            $assignmentdetails['postedBy'] = Assignment::select('users.fname', 'users.lname', 'users.company_name')
                            ->leftJoin('users', function($join) {
                                $join->on('assignment.user_id', '=', 'users.id');
                            })
                            ->where("assignment.id", $assignmentdetails->assignment_id)->get();
            //user details
            //bid send last date count
            $now = Carbon::now()->format('Y-m-d'); // or your date as well
            $end_date = Carbon::parse($assignmentdetails->proposal_end_date);
            $daysleft = $end_date->diffInDays($now);
            //bid send last date count
            $data['Message'] = "Assignment details found!";
            $data['StatusCode'] = 200;
            $data['Results'] = $assignmentdetails;
            $data['Results']['ProposalDetails'] = $proposaldetails;
            $data['Results']['ProposalNo'] = $ProposalNo;
            $data['Results']['openDaysLeft'] = $daysleft;
        }
        return $data;
    }

    private function assignmentTrainingDetail($details) {
        $assignment_id = $details['assignment_id'];
        $user_id = $details['user_id'];
        $prefix = DB::getTablePrefix();
        $assignmentdetails = AssignmentTraining::
                        join('assignment', 'assignment.id', '=', 'assignment_request_training.assignment_id')
                        ->select("assignment_request_training.*", 'assignment.user_id', 'assignment.approval_status', 'assignment.close_job', DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment_request_training.assignment_id ) as jobBudget"), DB::raw("(SELECT SUM(estimated_effort) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment_request_training.assignment_id ) as jobEffort"))
                        ->where('assignment_request_training.assignment_id', $assignment_id)->first();
        if (count($assignmentdetails) == 0) {
            $data['StatusCode'] = 9999;
            $data['Message'] = "No details found.";
        } else {
            //get city name
            if ($assignmentdetails->mode == 1) {
                $locationname = DB::table('assignment_location')
                                ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                                ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                                ->join('states', 'states.id', '=', 'assignment_location.state_id')
                                ->where('assignment_location.assignment_id', $assignment_id)->get();
                $assignmentdetails['locationname'] = $locationname;
            } else {
                $assignmentdetails['locationname'] = array();
            }
            //get assignment creator & assignment approval info
            $assignmentdetails['postedBy'] = Assignment::select('users.fname', 'users.lname', 'users.id', 'users.email', 'assignment.approval_status', 'assignment.user_id')
                            ->leftJoin('users', function($join) {
                                $join->on('assignment.user_id', '=', 'users.id');
                            })
                            ->where("assignment.id", $assignmentdetails->assignment_id)->get();
            //get bid lists by bid sender & assignment creator
            if ($assignmentdetails->user_id == $user_id) { //get bid list by assignment creator id
                $proposaldetails = DB::table('send_proposal')
                                ->select('users.fname', 'users.lname', 'users.profile_img', 'users.id AS userId', 'users.gender', 'send_proposal.*')
                                ->join('users', 'send_proposal.user_id', '=', 'users.id')
                                ->where('send_proposal.assignment_id', $assignment_id)->get();
            } else if ($user_id) { //get bid list by bid sender id
                $proposaldetails = DB::table('send_proposal')
                                ->select('users.fname', 'users.lname', 'users.profile_img', 'users.id AS userId', 'users.gender', 'send_proposal.*')
                                ->join('users', 'send_proposal.user_id', '=', 'users.id')->where('send_proposal.assignment_id', $assignment_id)
                                ->where('send_proposal.approval_status', '!=', 3)->where('send_proposal.user_id', $user_id)->get();
                $assignmentdetails['isProposalSent'] = count($proposaldetails) > 0;
            } else {
                $proposaldetails = array();
            }
            //get count of total list
            $ProposalNo = SendProposal::where('assignment_id', '=', $assignment_id)->count();
            //bid send last date count
            $now = Carbon::now()->format('Y-m-d'); // or your date as well
            $end_date = Carbon::parse($assignmentdetails->proposal_end_date);
            $daysleft = $end_date->diffInDays($now);

            $data['Message'] = "Assignment details found!";
            $data['StatusCode'] = 200;
            $data['Results'] = $assignmentdetails;
            $data['Results']['ProposalDetails'] = $proposaldetails;
            $data['Results']['ProposalNo'] = $ProposalNo;
            $data['Results']['openDaysLeft'] = $daysleft;
        }
        return $data;
    }

    private function assignmentCertificationDetail($details) {
        $assignment_id = $details['assignment_id'];
        $user_id = $details['user_id'];
        $prefix = DB::getTablePrefix();
        $assignmentdetails = AssignmentCertification::select('assignment.user_id', 'assignment_request_certification.*')
                        ->join('assignment', 'assignment.id', '=', 'assignment_request_certification.assignment_id')
                        ->where('assignment_request_certification.assignment_id', $assignment_id)->first();
        if (count($assignmentdetails) == 0) {
            $data['StatusCode'] = 9999;
            $data['Message'] = "No details found.";
        } else {
            //get location names
            $citynames = array();
            @$locationIds = explode(",", $assignmentdetails->location);
            $cityNameArray = DB::table('cities')
                            ->select('countries.name as countryName', 'states.name as stateName', 'cities.name as cityName')
                            ->join('countries', 'countries.id', '=', 'cities.country_id')
                            ->join('states', 'states.id', '=', 'cities.region_id')
                            ->whereIn('cities.id', @$locationIds)->get();

            foreach ($cityNameArray as $key => $val) {
                $citynames[] = $val->cityName . ", " . $val->stateName . ", " . $val->countryName;
            }
            $assignmentdetails['locationName'] = $citynames;
            //get location names
            //send proposal by standard & location
            $assignmentdetails['codeAvailability'] = DB::table('users_isic_code')->where('user_id', $user_id)->count();
            $assignmentdetails['codeSearch'] = DB::table('users_isic_code')->where('user_id', $user_id)
                            ->where('code_sector', $assignmentdetails->sic_code)->count();
            $assignmentdetails['currentAddressSearch'] = DB::table('users_contact_address')
                            ->whereIn('city', @$locationIds)->where('status_current', 1)
                            ->where('user_id', $user_id)->count();
            //send proposal by standard & location
            //get proposal details
            if ($assignmentdetails->user_id == $user_id) {
                $proposaldetails = DB::table('send_proposal')
                        ->select('users.fname', 'users.lname', 'users.profile_img', 'users.id AS userId', 'users.gender', 'send_proposal.*')
                        ->join('users', 'send_proposal.user_id', '=', 'users.id')
                        ->where('send_proposal.assignment_id', $assignment_id)
                        ->where('send_proposal.approval_status', '=', 0)
                        ->get();
            } else if ($user_id) {
                $proposaldetails = DB::table('send_proposal')
                        ->select('users.fname', 'users.lname', 'users.profile_img', 'users.id AS userId', 'users.gender', 'send_proposal.*')
                        ->join('users', 'send_proposal.user_id', '=', 'users.id')
                        ->where('send_proposal.assignment_id', $assignment_id)
                        ->where('send_proposal.user_id', $user_id)
                        ->where('send_proposal.approval_status', '=', 0)
                        ->get();
                $data['isProposalSent'] = count($proposaldetails) > 0;
            }
            //get proposal details
            //user details
            $assignmentdetails['postedBy'] = Assignment::select('users.fname', 'users.lname', 'users.company_name')
                            ->leftJoin('users', function($join) {
                                $join->on('assignment.user_id', '=', 'users.id');
                            })
                            ->where("assignment.id", $assignmentdetails->assignment_id)->get();
            //user details
            //awarded details
            $assignmentdetails['awardedTo'] = Assignment::select('users.fname', 'users.lname')
                            ->leftJoin('users', function($join) {
                                $join->on('assignment.awarded_to', '=', 'users.id');
                            })
                            ->where("assignment.id", $assignmentdetails->assignment_id)->get();
            //awarded details

            $data['Message'] = "Assignment details found!";
            $data['StatusCode'] = 200;
            $data['Results'] = $assignmentdetails;
            $data['Results']['ProposalDetails'] = $proposaldetails;
            $data['Results']['ProposalNo'] = count($proposaldetails);
        }
        return $data;
    }

    public function assignmentDetails(Request $request) {
        $reqdata = $request->all();
        $details['user_id'] = $reqdata['UserId'];
        $details['assignment_id'] = $reqdata['assignmentId'];
        $details['flag'] = $reqdata['assignmentType'];
        if ($details['flag'] && $details['assignment_id']) {
            if ($details['flag'] == 1) {
                $data = $this->assignmentAuditDetail($details);
            } else if ($details['flag'] == 2) {
                $data = $this->assignmentTrainingDetail($details);
            } else if ($details['flag'] == 3) {
                $data = $this->assignmentCertificationDetail($details);
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function checkRespondBid(Request $request) {
        $reqdata = $request->all();
        $assignment_id = $reqdata['assignmentid'];
        $user_id = $reqdata['userid'];
        if ($assignment_id && $user_id) {
            $getDates = AssignmentAudit::select('start_date', 'end_date')->where('assignment_id', $assignment_id)->first();
            $getAcceptedBid = SendProposal::select('send_proposal.user_id', 'assignment_request_audit.assignment_id')
                    ->join('assignment_request_audit', 'assignment_request_audit.assignment_id', '=', 'send_proposal.assignment_id')
                    ->where('send_proposal.user_id', $user_id)->where('send_proposal.approval_status', '!=', 2)
                    ->where('send_proposal.endjob_status', '=', 0)
                    ->where('assignment_request_audit.start_date', '>=', $getDates->start_date)
                    ->where('assignment_request_audit.end_date', '<=', $getDates->end_date)
                    ->get();
            $getBidCount = count($getAcceptedBid);
            if ($getBidCount > 0) {
                $data['checkFlag'] = 1;
                $data['Message'] = "You have bids/jobs in this assignment time span.";
                $data['StatusCode'] = 200;
            } else {
                $data['checkFlag'] = 0;
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function sendProposal(Request $request) {
        $reqdata = $request->all();
        $ins_proposal = $reqdata['proposal'];
        $proposal_id = @$reqdata['proposal_id'];

        //check credit
        $taskPoints = $this->SubscriptionController->getTaskPoint("Send Proposal");
        $creditPoints = $this->SubscriptionController->getUserCreditPoint($ins_proposal['user_id']);
        if ($creditPoints < $taskPoints && empty($proposal_id)) {
            $data['StatusCode'] = 9999;
            $data['Message'] = "You have not sufficient credit points available!";
            echo json_encode($data);
            exit();
        } else {
            if ($proposal_id) {
                SendProposal::where('id', $proposal_id)->update($ins_proposal);
                $data['Message'] = "Bid updated successfully!";
                $data['StatusCode'] = 200;
                //TODO: send emil in update
            } else {
                //in rejection case overlap existing bid
                $bid_exists = SendProposal::select('id')->where('user_id', $ins_proposal['user_id'])->where('approval_status', '=', 2)
                                ->where('assignment_id', $ins_proposal['assignment_id'])->first();
                if (count($bid_exists) > 0) {
                    SendProposal::where('user_id', $ins_proposal['user_id'])->where('id', $bid_exists->id)->delete();
                }
                //in rejection case overlap existing bid
                $ins_proposal['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $ins_proposal['protocol'] = $_SERVER['REMOTE_ADDR'];

                //update transaction
                $debitPoints = $this->SubscriptionController->updateUserCreditPoint("", $ins_proposal['user_id'], $taskPoints, "");
                if ($debitPoints) {
                    $updateTransaction = $this->SubscriptionController->updateUserTransaction($ins_proposal['user_id'], "Send Proposal", Carbon::now()->format('Y-m-d'), "", $taskPoints);
                }

                $res = SendProposal::create($ins_proposal);
                $id = $res->id;
                if ($id) {
                    //send mail to seeker
                    $seekerDetail = DB::table('users')->select('email', 'fname', 'lname', 'fcm_device_token')
                                    ->where('id', $ins_proposal['assignment_user_id'])->first();
                    $assignment_type = Assignment::select('assignment_type')->where('id', $ins_proposal['assignment_id'])->first();
                    $mail['fname'] = $seekerDetail->fname;
                    $mail['lname'] = $seekerDetail->lname;
                    $mail['email'] = $seekerDetail->email;
                    $mail['ref_id'] = $reqdata['ref_id'];
                    $mail['assignment_title'] = $reqdata['assignment_name'];
                    $mail['assignment_id'] = $ins_proposal['assignment_id'];
                    $mail['bid_price'] = $ins_proposal['estimated_budget'];
                    $mail['bid_effort'] = $ins_proposal['estimated_effort'];
                    $mail['subject'] = "New Response To Your Assignment " . $reqdata['ref_id'];
                    $mail['assignment_type'] = $assignment_type->assignment_type;
                    $this->proposalEmailSeeker($mail);
                    
                    //notification send to seeker
                    $notification['assignment_id'] = $ins_proposal['assignment_id']; //Proposal ID
                    $notification['action'] = $assignment_type->assignment_type = 1 ? "audit_send_proposal" : "training_send_proposal"; //Proposal ID
                    $notification['assignment_title'] = $reqdata['assignment_name'];
                    $notification['bid_price'] = $ins_proposal['estimated_budget'];
                    $notification['bid_effort'] = $ins_proposal['estimated_effort'];
                    $body = json_encode($notification);
                    $msg = array(
                        'header' => "New Response To Your Assignment " . $reqdata['ref_id'],
                        'text' => "We have received a new response to your assignment" . $reqdata['ref_id']
                    );
                    $message = json_encode($msg);
                    $fcmdevice_token = $seekerDetail->fcm_device_token;
                    $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);
                    
                    $data['Message'] = "Bid inserted successfully!";
                    $data['StatusCode'] = 200;
                } else {
                    $data['Message'] = "Bad request.";
                    $data['StatusCode'] = 9999;
                }
            }
        }
        echo json_encode($data);
        exit();
    }

    public function proposalEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $messageBody = view('email.proposalSendEmail', $data);
        $a = Mail::send('email.proposalSendEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function deleteProposal(Request $request) {
        $reqdata = $request->all();
        $proposal_id = $reqdata['proposal_id'];
//        $user_type = $reqdata['UserType'];
        $user_id = $reqdata['UserId'];
        if ($proposal_id && $user_id) {
            $proposalDetail = SendProposal::where('id', $proposal_id)->where('user_id', $user_id)->first();
            if ($proposalDetail) {
                SendProposal::where('id', $proposal_id)->where('user_id', $user_id)->delete();
                //send mail to provider
                $mail['fname'] = $reqdata['Fname'];
                $mail['lname'] = $reqdata['Lname'];
                $mail['email'] = $reqdata['Email'];
                $mail['ref_id'] = $reqdata['ref_id'];
                $mail['subject'] = "ISOMart : Bid Deleted!";
                $this->proposalDeleteEmailProvider($mail);
                //send mail to provider
                $data['Message'] = "Bid deleted successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Bid deletion failure!";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function proposalDeleteEmailProvider($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $messageBody = view('email.deleteProposalEmail', $data);
        $a = Mail::send('email.deleteProposalEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function acceptProposal(Request $request) {
        $reqdata = $request->all();
        $proposal_id = $reqdata['proposal_id'];
        $proposal_userid = $reqdata['proposal_userid'];
        $assignment_id = $reqdata['assignment_id'];
        $assignment_type = $reqdata['assignment_type'];
        $ref_id = $reqdata['ref_id'];
        if ($proposal_id && $proposal_userid) {
            $isjobmoveonline = false;
            $assignedLocation = false; //get assigned location
            //get location ID from bid id
            $resbidinfo = SendProposal::select("assignment_location_id", "user_id")->where("id", $proposal_id)->first();
            $difflocation = $resbidinfo->assignment_location_id;
            if (!empty($difflocation)) { //if location available for assignment
                $arraylocation = explode(",", $difflocation);
                $bidlocationcount = count($arraylocation);
                //get all location id of assignment
                $resAL = AssignmentLocation::select("id", "whomtoassign_id")->where("assignment_id", $assignment_id)->pluck("whomtoassign_id", "id");
                $isjobmove = false; //check whether location is assigned or not
                $count = 0; //remaining location ids which are not assigned

                foreach ($resAL as $key => $val) {
                    if (!$val) {   //if assigned id is null in assignment_location
                        $count++;
                    }
                    if (in_array($key, $arraylocation) && ($val)) {  //get assigned location id
                        $assignedLocation = $key;
                    }
                }
                AssignmentLocation::whereIn('id', $arraylocation)->update(array('whomtoassign_id' => $resbidinfo->user_id));
                //update assignment status to job if all location assigned
                if ($count == $bidlocationcount) {
                    $isjobmove = true;
                    Assignment::where("id", $assignment_id)->update(array("approval_status" => 1));
                }
            } else { //move to job for online training mode
                $isjobmoveonline = true;
                Assignment::where("id", $assignment_id)->update(array("approval_status" => 1));
            }

            if ($assignedLocation) {
                $assignedcityname = AssignmentLocation::select('city_name')->where('id', $assignedLocation)->first();
                $data['Message'] = "Already accepted for location " . $assignedcityname->city_name . ". Kindly reject the bid & inform bidder to bid again.";
                $data['StatusCode'] = 9999;
            } else {
                //get bidder details
                $details = DB::table('users')->where('id', $proposal_userid)->first();
                SendProposal::where("id", $proposal_id)->update(array('approval_status' => 1));
                //send mail to provider/bidder
                $mail['fname'] = $details->fname;
                $mail['lname'] = $details->lname;
                $mail['email'] = $details->email;
                $mail['ref_id'] = $ref_id;
                $mail['assignment_id'] = $assignment_id;
                $mail['subject'] = "Update on your bid " . $ref_id;
                $this->proposalAccpetEmailProvider($mail);
                //notification send to provider
                $notification['assignment_id'] = $assignment_id; //Proposal ID
                $notification['action'] = $assignment_type = 1 ? "audit_accept_proposal" : "training_accept_proposal"; //Proposal ID
                $body = json_encode($notification);
                $msg = array(
                    'header' => "Update on your bid " . $ref_id,
                    'text' => "Your bid against assignment : <strong>$ref_id</strong> has been accepted by the owner."
                );
                $message = json_encode($msg);
                $fcmdevice_token = $details->fcm_device_token;
                $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);
                //send mail to provider
                $data['Message'] = "Bid accepted successfully!";
                $data['StatusCode'] = 200;
            }
        } else {
            $data['Message'] = "Bad Request!";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }
    
    public function proposalAccpetEmailProvider($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $messageBody = view('email.proposalAcceptEmail', $data);
        $a = Mail::send('email.proposalAcceptEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function rejectProposal(Request $request) {
        $reqdata = $request->all();
        $proposal_id = $reqdata['proposal_id'];
        $proposal_userid = $reqdata['proposal_userid'];
        $assignment_id = $reqdata['assignment_id'];
        $assignment_type = $reqdata['assignment_type'];
        $ref_id = $reqdata['ref_id'];
//        $user_type = $reqdata['UserType'];
        if ($proposal_id && $proposal_userid) {
            $details = DB::table('users')->where('id', $proposal_userid)->first();
            $id = SendProposal::where("id", $proposal_id)->update(array('approval_status' => 2));
            //send mail to provider
            $mail['fname'] = $details->fname;
            $mail['lname'] = $details->lname;
            $mail['email'] = $details->email;
            $mail['ref_id'] = $ref_id;
            $mail['assignment_type'] = $assignment_type;
            $mail['assignment_id'] = $assignment_id;
            $mail['subject'] = "Update on your bid " . $ref_id;
            $this->proposalRejectEmailProvider($mail);

            //notification send to provider
            $notification['assignment_id'] = $assignment_id; //Assignment ID
            $notification['action'] = $assignment_type = 1 ? "audit_reject_proposal" : "training_reject_proposal"; //Proposal ID
            $body = json_encode($notification);
            $msg = array(
                'header' => "Update on your bid $ref_id",
                'text' => "Your bid against assignment : <strong>$ref_id</strong> has been rejected by the owner."
            );
            $message = json_encode($msg);
            $fcmdevice_token = $details->fcm_device_token;
            $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);
            $data['Message'] = "Bid rejected successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function proposalRejectEmailProvider($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $messageBody = view('email.proposalRejectEmail', $data);
        $a = Mail::send('email.proposalRejectEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function startJob(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $assignment_id = $reqdata['AssignmentId'];
        if ($user_id && $assignment_id) {
            $update_id = Assignment::where('id', $assignment_id)->where('user_id', $user_id)
                    ->update(array(
                "approval_status" => 1,
                "start_job_at" => Carbon::now()->format("Y-m-d H:i:s")
            ));
            if ($update_id) {
                //send mail to seeker
                $mail['fname'] = $reqdata['Fname'];
                $mail['lname'] = $reqdata['Lname'];
                $mail['email'] = $reqdata['Email'];
                $mail['ref_id'] = $reqdata['RefId'];
                $mail['subject'] = "ISOMart : New Job!";
                $mail['proposaltype'] = 1;
                $this->startJobEmailSeeker($mail);
                //send mail to seeker
                $data['Message'] = "Job started successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in job start!";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad Request!";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function startJobEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $proposaltype = $data['proposaltype'];
        $messageBody = view('email.startJobEmail', $data);
        $a = Mail::send('email.startJobEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function alljobslistCertification(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $usertype = $reqdata['Usertype'];
        $acceptedjob = array();
        $awardedjob = array();
        if ($user_id && $usertype) {
            //get job of CB
            if ($usertype == 6) { //CB
                $acceptedjob = DB::table('assignment')
                        ->join('assignment_request_audit', 'assignment.id', '=', 'assignment_request_audit.assignment_id')
                        ->select('assignment_request_audit.*')
                        ->where('assignment.approval_status', '=', 1)
                        ->where('assignment.assignment_type', '=', 1)
                        ->where('assignment.user_id', $user_id)
                        ->orderBy('id', 'DESC')
                        ->get();
                $awardedjob = DB::table('assignment')
                        ->join('assignment_request_certification', 'assignment.id', '=', 'assignment_request_certification.assignment_id')
                        ->select('assignment_request_certification.*')
                        ->where('assignment.approval_status', '=', 1)
                        ->where('assignment.assignment_type', '=', 3)
                        ->where('assignment.awarded_to', $user_id)
                        ->orderBy('id', 'DESC')
                        ->get();
            } else if ($usertype == 1 || $usertype == 5) { //seeker
                $acceptedjob = DB::table('assignment')
                        ->join('assignment_request_certification', 'assignment.id', '=', 'assignment_request_certification.assignment_id')
                        ->select('assignment_request_certification.*')
                        ->where('assignment.approval_status', '=', 1)
                        ->where('assignment.user_id', $user_id)
                        ->orderBy('id', 'DESC')
                        ->get();
            } else if ($usertype == 8) { //Auditor
                $awardedjob = DB::table('assignment')
                        ->join('assignment_request_audit', 'assignment.id', '=', 'assignment_request_audit.assignment_id')
                        ->select('assignment_request_audit.*')
                        ->where('assignment.approval_status', '=', 1)
                        ->where('assignment.awarded_to', $user_id)
                        ->orderBy('id', 'DESC')
                        ->get();
            }
            if (count($awardedjob) == 0 && count($acceptedjob) == 0) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
                echo json_encode($data);
                exit();
            } else {
                $data['Message'] = "Job list found!";
                $data['StatusCode'] = 200;
                $data['Results']['AwardedJobList'] = $awardedjob;
                $data['Results']['AcceptedJobList'] = $acceptedjob;
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }

        echo json_encode($data);
        exit();
    }

    public function allProposalList(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $proposalList = $proposalLisTraining = array();
        if ($user_id) {
            $proposalList = DB::table('assignment_request_audit')
                    ->select('send_proposal.estimated_effort', 'send_proposal.estimated_budget', 'assignment_request_audit.ref_id', 'assignment_request_audit.*', 'assignment.approval_status')
                    ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment_request_audit.assignment_id')
                    ->join('assignment', 'assignment.id', '=', 'assignment_request_audit.assignment_id')
                    ->where('send_proposal.user_id', $user_id)
                    ->where('send_proposal.approval_status', '!=', 1)->where('assignment.approval_status', '=', 0)
                    ->get();
            $proposalLisTraining = DB::table('assignment_request_training')
                    ->select('send_proposal.estimated_effort', 'send_proposal.estimated_budget', 'assignment_request_training.ref_id', 'assignment_request_training.*', 'assignment.approval_status')
                    ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment_request_training.assignment_id')
                    ->join('assignment', 'assignment.id', '=', 'assignment_request_training.assignment_id')
                    ->where('send_proposal.user_id', $user_id)
                    ->where('send_proposal.approval_status', '!=', 1)->where('assignment.approval_status', '=', 0)
                    ->get();
//            if (count($proposalList) == 0) {
            if (count($proposalList) == 0 && count($proposalLisTraining) == 0) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
                echo json_encode($data);
                exit();
            } else {
                $data['Message'] = "Proposal list found!";
                $data['StatusCode'] = 200;
                $data['Results']['proposalLists'] = $proposalList;
                $data['Results']['proposalListTraining'] = $proposalLisTraining;
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function myJobsList(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $usertype = $reqdata['UserType'];
        $prefix = DB::getTablePrefix();
        $val = 1;
        if ($user_id && $usertype) {
            if ($usertype == 6) {
                $myjobslist = DB::table('assignment')
                                ->join('assignment_request_audit as ar', 'ar.assignment_id', '=', 'assignment.id')
                                ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                                ->select('assignment.start_job_at', 'assignment.close_job', 'ar.*', DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT COUNT(id) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobAcceptedBidCount")
                                )
                                ->where(function($q) use ($val) {
                                    $q->where('assignment.approval_status', $val)
                                    ->orWhere('send_proposal.approval_status', $val);
                                })
                                ->where('assignment.user_id', $user_id)
                                ->orderBy('assignment.id', 'DESC')->groupBy('assignment.id')->get();
            } else if ($usertype == 5 || $usertype == 1) {
                $myjobslist = DB::table('assignment')
                                ->join('assignment_request_training as ar', 'ar.assignment_id', '=', 'assignment.id')
                                ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                                ->select('assignment.start_job_at', 'assignment.close_job', 'ar.*', 'send_proposal.id as proposal_id', DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT COUNT(id) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobAcceptedBidCount")
                                )
                                ->where(function($q) use ($val) {
                                    $q->where('assignment.approval_status', $val)
                                    ->orWhere('send_proposal.approval_status', $val);
                                })
                                ->where('assignment.user_id', $user_id)
                                ->orderBy('assignment.id', 'DESC')->groupBy('assignment.id')->get();
            }

            if (count($myjobslist) > 0) {
                $data['Message'] = "Job list found!";
                $data['StatusCode'] = 200;
                $data['Results']['myjoblist'] = $myjobslist;
            } else {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function assignedJobsList(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $assignjobslist = $assignjobslisttraining = array();
        $prefix = DB::getTablePrefix();
        if ($user_id) {
            $assignjobslist = DB::table('assignment')
                            ->join('assignment_request_audit as ar', 'ar.assignment_id', '=', 'assignment.id')
                            ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                            ->select('assignment.start_job_at', 'ar.*', 'send_proposal.id as proposal_id', 'send_proposal.*')
                            ->where('send_proposal.approval_status', 1)->where('send_proposal.user_id', $user_id)
                            ->orderBy('assignment.id', 'DESC')->get();
            $assignjobslisttraining = DB::table('assignment')
                            ->join('assignment_request_training as ar', 'ar.assignment_id', '=', 'assignment.id')
                            ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                            ->select('assignment.start_job_at', 'ar.*', 'ar.start_date as assignment_startdate', 'ar.end_date as assignment_enddate', 'send_proposal.id as proposal_id', 'send_proposal.*')
                            ->where('send_proposal.approval_status', 1)
                            ->where('send_proposal.user_id', $user_id)
                            ->orderBy('assignment.id', 'DESC')->get();
            if (count($assignjobslist) == 0 && count($assignjobslisttraining) == 0) {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
            } else {
                $data['Message'] = "Job list found!";
                $data['StatusCode'] = 200;
                $data['Results']['assignedjoblist'] = $assignjobslist;
                $data['Results']['assignedjoblisttraining'] = $assignjobslisttraining;
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function assignedJobDetail(Request $request) {
        $reqdata = $request->all();
        $assignment_id = $reqdata['assignment_id'];
        $user_id = $reqdata['user_id'];
        if ($assignment_id && $user_id) {
            $assignmentType = Assignment::select('assignment_type')->where('id', $assignment_id)->first();
            //get assigned job details
            if ($assignmentType->assignment_type == 1) { //get assigned job details audit
                $assignmentdetails = DB::table('send_proposal')
                                ->join('users', 'send_proposal.assignment_user_id', '=', 'users.id')
                                ->join('assignment_request_audit', 'assignment_request_audit.assignment_id', '=', 'send_proposal.assignment_id')
                                ->select('send_proposal.*', 'send_proposal.id as proposal_id', 'users.id as userId', 'users.fcm_device_token', 'users.fname', 'users.lname', 'users.gender', 'users.profile_img', 'assignment_request_audit.*')
                                ->where('send_proposal.assignment_id', $assignment_id)->where('send_proposal.user_id', $user_id)->first();
            } else if ($assignmentType->assignment_type == 2) { //get assigned job details training
                $assignmentdetails = DB::table('send_proposal')
                                ->join('users', 'send_proposal.assignment_user_id', '=', 'users.id')
                                ->join('assignment_request_training', 'assignment_request_training.assignment_id', '=', 'send_proposal.assignment_id')
                                ->select('send_proposal.*', 'send_proposal.id as proposal_id', 'users.id as userId', 'users.fcm_device_token', 'users.fname', 'users.lname', 'users.gender', 'users.profile_img', 'assignment_request_training.*')
                                ->where('send_proposal.assignment_id', $assignment_id)->where('send_proposal.user_id', $user_id)->first();
            }

            //get city name
            @$locationIds = explode(",", @$assignmentdetails->assignment_location_id);
            $locationname = DB::table('assignment_location')
                            ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                            ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                            ->join('states', 'states.id', '=', 'assignment_location.state_id')
                            ->whereIn('assignment_location.id', @$locationIds)->get();
            //get city name
            //get CB address
            $addressCb = DB::table('users_contact_address')
                            ->select('countries.name as countryName', 'states.name as stateName', 'users_contact_address.city_name')
                            ->join('countries', 'countries.id', '=', 'users_contact_address.country_id')
                            ->join('states', 'states.id', '=', 'users_contact_address.state_id')
                            ->where('users_contact_address.user_id', @$assignmentdetails->userId)->orderBy('users_contact_address.id', 'DESC')->first();
            //get CB address
            //get job track list
            $jobtracklist = AssignmentTracking::where('sender_id', $user_id)->where('proposal_id', @$assignmentdetails->proposal_id)
                            ->orderBy('id', 'DESC')->get();
            //get job track list
            $queryname = DB::table('query_master')->where('status', 1)->pluck("query", "id");
            if ($assignmentdetails) {
                $data['Message'] = "Job details found!";
                $data['StatusCode'] = 200;
                $data['Results']['assignmentDetails'] = $assignmentdetails;
                $data['Results']['locationName'] = $locationname;
                $data['Results']['addressCB'] = $addressCb;
                $data['Results']['jobTrackLists'] = $jobtracklist;
                $data['Results']['assignmentType'] = $assignmentType->assignment_type;

                //get own rating view
                $ownsendrating = Rating::with('RatingAnswer')
                        ->where('recive_id', @$assignmentdetails->userId)
                        ->where('sender_id', @$user_id)
                        ->where('reference_id', @$assignmentdetails->ref_id)
                        ->first();
                if (count(@$ownsendrating) > 0) {
                    $data['Results']['ratingStatus'] = 1;
                    if (@$ownsendrating->message == '') {
                        $data['Results']['ownsendrating']['message'] = " N/A";
                    } else {
                        $data['Results']['ownsendrating']['message'] = @$ownsendrating->message;
                    }
                    foreach (@$ownsendrating['RatingAnswer'] as $k => $v) {
                        $data['Results']['ownsendrating']['ratingvalue'][$k]['query'] = @$queryname[$v->query_id];
                        $data['Results']['ownsendrating']['ratingvalue'][$k]['value'] = @$v->answer;
                    }
                } else {
                    $data['Results']['ratingStatus'] = 0;
                    $data['Results']['ownsendrating'] = array();
                }
                //end
                //get own receive message
                $rate = Rating::with('RatingAnswer')
                        ->where('sender_id', @$assignmentdetails->userId)
                        ->where('recive_id', @$user_id)
                        ->where('reference_id', @$assignmentdetails->ref_id)
                        ->first();
                if (count(@$rate) > 0) {
                    if (@$rate->message == '') {
                        $data['Results']['ratingme']['message'] = " N/A";
                    } else {
                        $data['Results']['ratingme']['message'] = @$rate->message;
                    }
                    foreach (@$rate['RatingAnswer'] as $k => $v) {
                        $data['Results']['ratingme']['ratingvalue'][$k]['query'] = @$queryname[$v->query_id];
                        $data['Results']['ratingme']['ratingvalue'][$k]['value'] = @$v->answer;
                    }
                } else {
                    $data['Results']['ratingme'] = array();
                }
                //end
            } else {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No details found.";
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function addJobTrack(Request $request) {
        $reqdata = $request->all();
        $ins_jobtrack = $reqdata['jobtrack'];
        if ($ins_jobtrack) {
            $ins_jobtrack['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
            $ins_jobtrack['protocol'] = $_SERVER['REMOTE_ADDR'];
            $ins_id = AssignmentTracking::insertGetId($ins_jobtrack);
            //upload jb track document
            $job_document = @$ins_jobtrack['document'];
            if ($job_document) {
                if (file_exists(public_path('job_document/tempdocument') . "/" . $job_document)) {
                    $source = public_path('job_document/tempdocument') . "/" . $val;
                    $destination = public_path() . '/job_document/' . $ins_jobtrack['assignment_id'] . "/" . $val;
                    @rename($source, $destination);
                    @unlink($source);
                    $id = AssignmentTracking::where('id', $ins_id)->update(array("document" => $job_document));
                }
            }

            //notification send to provider
            $notification['assignment_id'] = $ins_jobtrack['assignment_id']; //Proposal ID
            $notification['action'] = $reqdata['assignment_type'] = 1 ? "audit_job_track" : "traning_job_track"; //notification text
            $notification['new_status'] = $ins_jobtrack['message'];
            $notification['updated_by'] = $reqdata['fname'] . " " . $reqdata['lname'];
            $body = json_encode($notification);
            $msg = array(
                'header' => 'Job ' . $reqdata['ref_id'] . 'Status Update',
                'text' => "Status has been changed or updated for job " . $reqdata['ref_id']
            );
            $message = json_encode($msg);
            $fcmdevice_token = $reqdata['device_token'];
            $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);

            if ($ins_id) {
                $data['StatusCode'] = 200;
                $data['Message'] = "Track report saved Successfully!";
            } else {
                $data['StatusCode'] = 9999;
                $data['Message'] = "Failure in save!";
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function myJobDetail(Request $request) {
        $reqdata = $request->all();
        $assignment_id = $reqdata['assignment_id'];
        $user_id = $reqdata['user_id'];
        $user_type = $reqdata['user_type'];
        $val = 1;
        if ($user_id && $assignment_id) {
            if ($user_type == 6) {
                //get assignment detail
                $assignmentdetail = DB::table('assignment_request_audit')
                        ->join('assignment', 'assignment_request_audit.assignment_id', '=', 'assignment.id')
                        ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                        ->select('assignment_request_audit.*', 'assignment.assignment_type')
                        ->where('assignment_request_audit.assignment_id', $assignment_id)
                        ->where('assignment.user_id', $user_id)
                        ->where(function($q) use ($val) {
                            $q->where('assignment.approval_status', $val)
                            ->orWhere('send_proposal.approval_status', $val);
                        })
                        ->first();
            } else if ($user_type == 5 || $user_type == 1) {
                $assignmentdetail = DB::table('assignment_request_training')
                        ->join('assignment', 'assignment_request_training.assignment_id', '=', 'assignment.id')
                        ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                        ->select('assignment_request_training.*', 'assignment.assignment_type', 'send_proposal.approval_status')
                        ->where('assignment_request_training.assignment_id', $assignment_id)
                        ->where('assignment.user_id', $user_id)
                        ->where(function($q) use ($val) {
                            $q->where('assignment.approval_status', $val)
                            ->orWhere('send_proposal.approval_status', $val);
                        })
                        ->first();
            }

            //get assignment location detail
            $locationname = DB::table('assignment_location')
                            ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                            ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                            ->join('states', 'states.id', '=', 'assignment_location.state_id')
                            ->where('assignment_location.assignment_id', $assignment_id)->get();
            //get assignment location detail
            //get auditor list
            $auditorlists = DB::table('send_proposal')
                    ->join('users', 'users.id', '=', 'send_proposal.user_id')
                    ->select('users.fname', 'users.lname', 'users.email', 'users.fcm_device_token', 'send_proposal.*')
                    ->where('send_proposal.assignment_id', $assignment_id)
                    ->where('send_proposal.assignment_user_id', $user_id)
                    ->where('send_proposal.approval_status', 1)
//                     ->where(function($q) use ($val) {
//                    $q->where('send_proposal.approval_status', $val)
//                    ->orWhere('send_proposal.canceljob_status', $val);
//                })
                    ->get();
            //get auditor list
//            print_r($auditorlists);
//            exit();
            //get job track lists
            $jobtracklists = DB::table('assignment_tracking')
                            ->join('users', 'assignment_tracking.sender_id', '=', 'users.id')
                            ->select('users.fname', 'users.lname', 'assignment_tracking.*')
                            ->where('assignment_tracking.assignment_id', $assignment_id)->get();
            //get job track lists
            $queryname = DB::table('query_master')->where('status', 1)->pluck("query", "id");

            if ($assignmentdetail) {
                $data['Message'] = "Job details found!";
                $data['StatusCode'] = 200;
                $data['Results']['assignmentDetails'] = $assignmentdetail;
                $data['Results']['locationName'] = $locationname;
                $data['Results']['auditorLists'] = $auditorlists;
                $data['Results']['jobTrackLists'] = $jobtracklists;

                //get rating status
                foreach (@$auditorlists as $k => $v) {
                    $rating_status[$v->user_id] = Rating::where('recive_id', @$v->user_id)
                            ->where('sender_id', $user_id)
                            ->where('reference_id', @$assignmentdetail->ref_id)
                            ->get();
                    if (count($rating_status[$v->user_id]) > 0) {
                        $checkrating_status[$v->user_id] = 1;
                    } else {
                        $checkrating_status[$v->user_id] = 0;
                    }
                    foreach (@$rating_status[@$v->user_id] as $key => $val) {
                        if ($val->message == '') {
                            $data['Results']['rating'][@$v->user_id]['message'] = " N/A";
                        } else {
                            $data['Results']['rating'][@$v->user_id]['message'] = $val->message;
                        }
                        foreach (@$rating_status[@$v->user_id][$key]['RatingAnswer'] as $a => $b) {
                            $data['Results']['rating'][@$v->user_id]['ratingvalue'][$a]['query'] = @$queryname[$b->query_id];
                            $data['Results']['rating'][@$v->user_id]['ratingvalue'][$a]['value'] = @$b->answer;
                        }
                    }
                }

                //get rating status
                $data['Results']['ratingStatus'] = @$checkrating_status;

                //get own receive message
                $rate = Rating::with('RatingAnswer')
                        ->where('recive_id', $user_id)
                        ->where('reference_id', @$assignmentdetail->ref_id)
                        ->get();
                $fname = DB::table('users')->pluck("fname", "id");
                $lname = DB::table('users')->pluck("lname", "id");
                if (count(@$rate) > 0) {
                    foreach ($rate as $key => $val) {
                        if ($val->message == '') {
                            $data['Results']['ratingme'][$key]['message'] = " N/A";
                        } else {
                            $data['Results']['ratingme'][$key]['message'] = $val->message;
                        }
                        $data['Results']['ratingme'][$key]['name'] = $fname[$val->sender_id] . " " . $lname[$val->sender_id];
                        foreach ($rate[$key]['RatingAnswer'] as $k => $v) {
                            $data['Results']['ratingme'][$key]['ratingvalue'][$k]['query'] = @$queryname[$v->query_id];
                            $data['Results']['ratingme'][$key]['ratingvalue'][$k]['value'] = @$v->answer;
                        }
                    }
                } else {
                    $data['Results']['ratingme'] = array();
                }
                //print "<pre>";print_r($data['Results']['ratingme']);exit;
                //end
            } else {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No details found.";
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function myCalender(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        if ($user_id) {
            $assignmentlist = DB::table('send_proposal')
                            ->join('assignment_request_audit', 'send_proposal.assignment_id', '=', 'assignment_request_audit.assignment_id')
                            ->select('assignment_request_audit.assignment_name', 'assignment_request_audit.ref_id', 'assignment_request_audit.assignment_id', 'assignment_request_audit.start_date as assignment_startdate', 'assignment_request_audit.end_date as assignment_enddate', 'send_proposal.id as proposal_id')
                            ->where('send_proposal.approval_status', 1)->where("send_proposal.user_id", $user_id)->get();
            if (count($assignmentlist) > 0) {
                $data['Message'] = "Assignment list found!";
                $data['StatusCode'] = 200;
                $data['Results']['assignmentLists'] = $assignmentlist;
            } else {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function publicProfileView(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $usertype = config('selecttype.usertypelist');
        $generaldetails = "";
        $ratingreview = array();
        $isiccodedetails = array();
        $contactdetails = "";
        $averagerating = 0;
        $gettotalaverage = 0;
        $valStatus = 1;
        if ($user_id) {
            $userdetails = DB::table('users')->where('id', $user_id)->first();
            $userType = $usertype[$userdetails->usertype];
            $contactdetails = DB::table('users_contact_details')->where('user_id', $user_id)->first();
            $addressdetails = DB::table('users_contact_address')
                            ->select('countries.name as countryName', 'states.name as stateName', 'users_contact_address.*')
                            ->join('countries', 'countries.id', '=', 'users_contact_address.country_id')
                            ->join('states', 'states.id', '=', 'users_contact_address.state_id')
                            ->where('users_contact_address.user_id', $user_id)
                            ->where('users_contact_address.available_status', $valStatus)->first();
            $generaldetails = DB::table('users_general_info')->where('user_id', $user_id)->first();
            $cpddetails = CpdHours::where('user_id', $user_id)->limit(3)->get();
            $isiccodedetails = DB::table('users_isic_code')
                            ->join('international_standards_section', 'users_isic_code.code_sector', '=', 'international_standards_section.id')
                            ->select('users_isic_code.*', 'international_standards_section.name as codeName', 'international_standards_section.type', 'international_standards_section.code')
                            ->where('users_isic_code.user_id', $user_id)->limit(3)->get();
            $ratingreview = Rating::with('RatingAnswer')
                            ->join('users', 'rating.sender_id', '=', 'users.id')
                            ->join('assignment_request_audit as ar', 'ar.ref_id', '=', 'rating.reference_id')
                            ->join('international_standards_section as is', 'is.id', '=', 'ar.sic_code')
                            ->leftjoin('assignment_location as al', 'al.assignment_id', '=', 'ar.assignment_id')
                            ->select('ar.assignment_name', 'ar.sic_code', 'ar.start_date', 'ar.end_date', 'ar.description', 'users.fname', 'users.lname', 'rating.*', 'is.code', 'is.name', 'is.type', DB::raw("(GROUP_CONCAT(city_name SEPARATOR ', ')) as `cities`"))
                            ->groupBy('al.assignment_id')->where('recive_id', $user_id)->get();

            if ($userdetails->usertype == 6) {
                $jobCount = DB::table('assignment')
                                ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                                ->where(function($q) use ($valStatus) {
                                    $q->where('assignment.approval_status', $valStatus)
                                    ->orWhere('send_proposal.approval_status', $valStatus);
                                })
                                ->where('assignment.user_id', $user_id)->groupBy('assignment.id')->count();
            } else {
                $jobCount = SendProposal::where('user_id', $user_id)->where('approval_status', 1)->count();
            }
            $queryname = DB::table('query_master')->pluck("query", "id");
            //avreage rating
            foreach ($ratingreview as $k => $v) {
                $sumrating = 0;
                foreach ($ratingreview[$k]['RatingAnswer'] as $key => $val) {
                    $sumrating += $val->answer;
                }
                $averagerating += $sumrating / count($queryname);
            }
            if ($averagerating > 0) {
                $gettotalaverage = round($averagerating / count($ratingreview));
            }
            //avreage rating


            $data['Message'] = "User Details info found!";
            $data['StatusCode'] = 200;
            $data['Results']['userDetails'] = $userdetails;
            $data['Results']['userType'] = $userType;
            $data['Results']['contactDetails'] = $contactdetails;
            $data['Results']['generealDetails'] = $generaldetails;
            $data['Results']['isiscodeDetails'] = $isiccodedetails;
            $data['Results']['addressDetails'] = $addressdetails;
            $data['Results']['cpdDetails'] = $cpddetails;
            $data['Results']['ratingReview'] = $ratingreview;
            $data['Results']['queryName'] = $queryname;
            $data['Results']['getTotalAverage'] = $gettotalaverage;
            $data['Results']['jobCount'] = $jobCount;
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function shareAssignment(Request $request) {
        $reqdata = $request->all();
        $data['assignment_id'] = $reqdata['assignment_id'];
        $data['ref_id'] = $reqdata['ref_id'];
        $data['fname'] = $reqdata['fname'];
        $data['lname'] = $reqdata['lname'];
        $toemail = $reqdata['share_email'];
        if ($data['assignment_id']) {
            $subject = "Share Assignment";
            $messageBody = view('email.shareAssignmentEmail', $data);
            $a = Mail::send('email.shareAssignmentEmail', $data, function($message) use ($toemail, $subject) {
                        $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                        $message->to($toemail)->subject($subject);
                    });
            $data['Message'] = "Assignment shared successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function endJobReview(Request $request) {
        $reqdata = $request->all();
        $id_proposal = $reqdata['proposal_id'];
        $id_auditor = $reqdata['auditor_id'];
        $device_token = $reqdata['fcm_device_token'];
        $assignment_type = $reqdata['assignment_type'];
        $assignment_id = $reqdata['assignment_id'];
        $comment = $reqdata['job_comment'];
        $countendjob = 0;
        if ($assignment_id) {
            //update end job status for bid
            $id_update = SendProposal::where('id', $id_proposal)->where('user_id', $id_auditor)
                    ->update(array(
                "endjob_comment" => $comment,
                "endjob_status" => 1
            ));
            //update end job status for bid
            $resbidinfo = SendProposal::where("approval_status", 1)->where("assignment_id", $assignment_id)->get();
            foreach ($resbidinfo as $k => $v) {
                if ($v->endjob_status == 1) {
                    $countendjob++; //get how many bids/jobs ends
                }
            }
            if ($countendjob == count($resbidinfo)) { //set assignment as closed if all bids/jobs ends
                Assignment::where("id", $assignment_id)->update(array("close_job" => 1));
            }
            //send mail to auditor
            $mail['assignment_id'] = $assignment_id;
            $mail['fname'] = $reqdata['auditor_fname'];
            $mail['lname'] = $reqdata['auditor_lname'];
            $mail['email'] = $reqdata['auditor_email'];
            $mail['ref_id'] = $reqdata['assignment_refid'];
            $mail['subject'] = "Job " . $reqdata['assignment_refid'] . " Ended";
            $mail['jobtype'] = 2;
            $this->jobEndEmailProvider($mail);

            //notification send to bidder
            $notification['assignment_id'] = $assignment_id; //Assignment ID
            $notification['ref_id'] = $reqdata['assignment_refid']; //Assignment ID
            $notification['action'] = $assignment_type = 1 ? "audit_end_job" : "training_end_job"; //Proposal ID
            $body = json_encode($notification);
            $msg = array(
                'header' => 'Job ' . $reqdata['assignment_refid'] . ' Ended',
                'text' => "Job " . $reqdata['assignment_refid'] . " has ended."
            );
            $message = json_encode($msg);
            $fcmdevice_token = $device_token;
            $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);

            if (@$id_update) {
                $data['Message'] = "Job ended Successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in ending job!";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function jobEndEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $jobtype = $data['jobtype'];
        $messageBody = view('email.jobEndEmail', $data);
        $a = Mail::send('email.jobEndEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function jobEndEmailProvider($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $jobtype = $data['jobtype'];
        $messageBody = view('email.jobEndEmail', $data);
        $a = Mail::send('email.jobEndEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function queryList() {
        $data = array();
        $queryList = DB::table('query_master')
                ->where('status', 1)
                ->pluck("query", "id");

        if ($queryList) {
            $array_id = array();
            $array_name = array();
            foreach ($queryList as $key => $val) {
                array_push($array_id, $array_query = array("id" => $key, "name" => $val));
            }
            $data['result']['querylist'] = $array_id;
            $data['Message'] = "Message listed successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function messageDetails(Request $request) {
        $reqdata = $request->all();
        $sender_id = @$reqdata['user_id'];
        //DB::enableQueryLog(); 
        $messagelist = DB::table('message')
                ->where("flag", 1)
                ->where(function($q) use ($sender_id) {
                    $q->where('sender_id', $sender_id)
                    ->orWhere('recive_id', $sender_id);
                })
                ->groupBy('sender_id', 'recive_id')
                ->groupBy('reference_id')
                ->orderBy('message_id', 'DESC')
                ->get();

//        print "<pre>";
//        print_r(DB::getQueryLog());exit;

        if (count(@$messagelist) > 0) {
            foreach (@$messagelist as $key => $val) {
                // notice list
                $data['result']['messagelist'][$key]['message_id'] = $val->message_id;
                $data['result']['messagelist'][$key]['sender_id'] = $val->sender_id;
                $data['result']['messagelist'][$key]['recive_id'] = $val->recive_id;
                $data['result']['messagelist'][$key]['reference_id'] = $val->reference_id;
                $data['result']['messagelist'][$key]['proposal_id'] = $val->proposal_id;
                $data['result']['messagelist'][$key]['message'] = $val->message;
                $data['result']['messagelist'][$key]['time'] = Carbon::parse(@$val->add_date)->format('h:i A');
            }
            $data['Message'] = "Message listed successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }

        echo json_encode($data);
        exit();
    }

    public function sendMessage(Request $request) {
        $reqdata = $request->all();
        $sender_id = @$reqdata['user_id'];
        $recive_id = @$reqdata['recive_id'];
        $reference_id = @$reqdata['reference_id'];
        $proposal_id = @$reqdata['proposal_id'];
        //DB::enableQueryLog(); 
        $messagelist = DB::table('message')
                ->where("reference_id", $reference_id)
                ->where(function($q) use ($recive_id) {
                    $q->where('sender_id', $recive_id)
                    ->orWhere('recive_id', $recive_id);
                })
                ->where(function($q) use ($sender_id) {
                    $q->where('sender_id', $sender_id)
                    ->orWhere('recive_id', $sender_id);
                })
                ->orderBy('message_id', 'ASC')
                ->get();
        //get sender name
        $getUserName = DB::table('users')->select('fname', 'lname', 'fcm_device_token')->where('id', $recive_id)->first();
        //get sender name
//        print "<pre>";
//        print_r(DB::getQueryLog());exit;

        if (count(@$messagelist) > 0) {
            foreach (@$messagelist as $key => $val) {
                // notice list
                $data['result']['messagelist'][$key]['message_id'] = $val->message_id;
                $data['result']['messagelist'][$key]['sender_id'] = $val->sender_id;
                $data['result']['messagelist'][$key]['recive_id'] = $val->recive_id;
                $data['result']['messagelist'][$key]['reference_id'] = $val->reference_id;
                $data['result']['messagelist'][$key]['proposal_id'] = $val->proposal_id;
                $data['result']['messagelist'][$key]['message'] = $val->message;
                $data['result']['messagelist'][$key]['time'] = Carbon::parse(@$val->add_date)->format('h:i A');
                $data['result']['getUserName'] = $getUserName;
                if (@$val->sender_id == @$sender_id) {
                    $data['result']['messagelist'][$key]['position'] = 2;
                } else if (@$val->sender_id == @$recive_id) {
                    $data['result']['messagelist'][$key]['position'] = 1;
                }
            }
            $data['Message'] = "Message listed successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }

        echo json_encode($data);
        exit();
    }

    public function insertMessage(Request $request) {
        $reqdata = $request->all();

        $ins_data = array();
        $ins_data['recive_id'] = $reqdata['recive_id'];
        $ins_data['reference_id'] = @$reqdata['reference_id'];
        $ins_data['proposal_id'] = @$reqdata['proposal_id'];
        $ins_data['message'] = $reqdata['message'];
        $ins_data['sender_id'] = $reqdata['user_id'];
        $ins_data['add_date'] = date("Y-m-d H:i:s");
        $ins_data['protocol'] = $_SERVER['REMOTE_ADDR'];
        $ins_data['flag'] = 1;
        Message::where('sender_id', $ins_data['sender_id'])
                ->where('recive_id', $ins_data['recive_id'])
                ->where('reference_id', $ins_data['reference_id'])
                ->update(array("flag" => 0));
        Message::where('sender_id', $ins_data['recive_id'])
                ->where('recive_id', $ins_data['sender_id'])
                ->where('reference_id', $ins_data['reference_id'])
                ->update(array("flag" => 0));
        $id = Message::create($ins_data);
        //notification send to receiver
        $device_token = DB::table('users')->select('fcm_device_token')->where('id', $ins_data['recive_id'])->first();
        $senderName = DB::table('users')->select('fname', 'lname')->where('id', $ins_data['sender_id'])->first();
        $notification['reference_id'] = $ins_data['reference_id']; //Ref ID
        $notification['sender_id'] = $ins_data['sender_id']; //Sender ID
        $notification['sender_name'] = $senderName->fname . " " . $senderName->lname; //Sender name
        $notification['message'] = $ins_data['message']; //Message
        $notification['recive_id'] = $ins_data['recive_id']; //Receive ID
        $notification['action'] = "new_message"; //Action
        $body = json_encode($notification);
        $msg = array(
            'header' => 'You have a new Message',
            'text' => "You have a new Message from ISOMart."
        );
        $message = json_encode($msg);
        $fcmdevice_token = $device_token->fcm_device_token;
        $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);
        if ($id) {
            $data['Message'] = "Inserted successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function insertRating(Request $request) {
        $reqdata = $request->all();

        $ins_data = array();
        $data = array();
        $ins_data['recive_id'] = $reqdata['recive_id'];
        $ins_data['reference_id'] = @$reqdata['reference_id'];
        $ins_data['message'] = $reqdata['review'];
        $ins_data['bid_id'] = $reqdata['bid_id'];
        $ins_data['assignment_id'] = $reqdata['assignment_id'];
        $ratingval = $reqdata['ratingval'];
//        print "<pre>";
//        print_r($ratingval);exit;
        $ins_data['sender_id'] = $reqdata['sender_id'];
        $ins_data['add_date'] = date("Y-m-d");
        $ins_data['protocol'] = $_SERVER['REMOTE_ADDR'];

        $id = DB::table('rating')->insertGetId($ins_data);
        foreach ($ratingval as $key => $val) {
            foreach ($val as $k => $v) {
                $rat_data[$key]['rating_id'] = $id;
                $rat_data[$key]['query_id'] = $k;
                $rat_data[$key]['answer'] = $v;
                $id_insert = DB::table('rating_answer')->insertGetId($rat_data[$key]);
            }
        }
        if ($id) {
            //notification send to receiver
            $receiver_info = DB::table('users')->select("fname", "lname", "email", "usertype", "fcm_device_token")->where("id", $reqdata['recive_id'])->first();
            $notification['assignment_id'] = $reqdata['assignment_id']; //Assignment ID
            $notification['recive_id'] = $reqdata['recive_id']; //Receiver ID
            $notification['reference_id'] = $reqdata['reference_id']; //Ref ID
            if (@$reqdata['assignment_type'] == 1) {
                $action = "audit_rating";
            } else {
                $action = "traning_rating";
            }
            $notification['action'] = $action; //Proposal ID
            $body = json_encode($notification);
            $msg = array(
                'header' => 'You have received a new rating',
                'text' => 'You have received a new rating from ISOMart'
            );
            $message = json_encode($msg);
            $fcmdevice_token = $receiver_info->fcm_device_token;
            $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);
            
            //mail send to receiver
            if ($receiver_info->usertype == 8) {
                $detail_url = asset('assignedjobDetails/' . $reqdata['assignment_id']);
            } else {
                $detail_url = asset('myjobDetails/' . $reqdata['assignment_id']);
            }
            $mail['fname'] = $receiver_info->fname;
            $mail['lname'] = $receiver_info->lname;
            $mail['email'] = $receiver_info->email;
            $mail['email_name'] = $receiver_info->fname . " " . $receiver_info->lname;
            $mail['subject'] = "You have received a new rating";
            $mail['msgbody'] = "<p>Hello " . $mail['email_name'] . "</p>"
                    . "<p>You have received a new rating.</p>"
                    . "<p>Please click <a href='$detail_url'>here</a> to view the details.</p>"
                    . "Thanks<br/>ISOMart Team";
            $this->messageEmailReceiver($mail);
            
            $data['Message'] = "Inserted successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function messageEmailReceiver($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $email_name = $data['email_name'];
        $subject = $data['subject'];
        $msgbody = $data['msgbody'];
        Mail::send([], [], function($message) use ($toemail, $toname, $subject, $email_name, $msgbody) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                    $message->setBody($msgbody, 'text/html');
                });
    }
    
    public function cancelJob(Request $request) {
        $reqdata = $request->all();
        $bid_id = $reqdata['bidId'];
        $ref_id = $reqdata['refId'];
        $assignmentType = $reqdata['assignmentType'];
        $getbiddetails = SendProposal::where("id", $bid_id)->first();
        $userdetails = DB::table('users')->select('fname', 'lname', 'email', 'fcm_device_token')->where("id", $getbiddetails->user_id)->first();
        $arraylocation = explode(",", $getbiddetails->assignment_location_id);
        if ($getbiddetails) {
            //cancel status in send proposal
            SendProposal::where("id", $bid_id)
                    ->update(array(
                        'canceljob_status' => 1,
                        'approval_status' => 3
            ));

            //available location for bid
            foreach ($arraylocation as $k => $v) {
                AssignmentLocation::where('id', $v)->update(array(
                    'whomtoassign_id' => null
                ));
            }

            //update job status for assignment
            Assignment::where('id', $getbiddetails->assignment_id)->update(array(
                'approval_status' => 0
            ));
            //send mail to auditor
            $mail['assignment_id'] = $getbiddetails->assignment_id;
            $mail['fname'] = $userdetails->fname;
            $mail['lname'] = $userdetails->lname;
            $mail['email'] = $userdetails->email;
            $mail['ref_id'] = $ref_id;
            $mail['subject'] = "Job " . $ref_id . " Cancelled";
            $mail['jobtype'] = 2;
            $this->jobCancelEmailProvider($mail);

            //notification send to bidder
            $notification['assignment_id'] = $getbiddetails->assignment_id; //Message ID
            $notification['action'] = $assignmentType = 1 ? "audit_cancel_job" : "training_cancel_job"; //Proposal ID
            $body = json_encode($notification);
            $msg = array(
                'header' => "Job " . $ref_id . " Cancelled",
                'text' => "Job " . $ref_id . " has been cancelled."
            );
            $message = json_encode($msg);
            $fcmdevice_token = $userdetails->fcm_device_token;
            $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);

            $data['Message'] = "Job cancelled successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['Message'] = "Failure in cancel!";
            $data['StatusCode'] = 9999;
        }
        echo json_encode($data);
        exit();
    }

    public function jobCancelEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $jobtype = $data['jobtype'];
        $messageBody = view('email.jobCancelEmail', $data);
        $a = Mail::send('email.jobCancelEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function jobCancelEmailProvider($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $jobtype = $data['jobtype'];
        $messageBody = view('email.jobCancelEmail', $data);
        $a = Mail::send('email.jobCancelEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function cancelledjoblist(Request $request) {
        $reqdata = $request->all();
        $userid = $reqdata['userid'];
        $val = 1;
        if ($userid) {
            //get job of auditor
            $jobslist = DB::table('assignment')
                            ->join('assignment_request_audit as ar', 'ar.assignment_id', '=', 'assignment.id')
                            ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                            ->select('assignment.start_job_at', 'ar.*', 'ar.start_date as assignment_startdate', 'ar.end_date as assignment_enddate', 'send_proposal.id as proposal_id', 'send_proposal.*')
                            ->where('send_proposal.approval_status', 3)->where('send_proposal.canceljob_status', 1)
                            ->where('send_proposal.user_id', $userid)
                            ->orderBy('assignment.id', 'DESC')->get();
            if (count($jobslist) > 0) {
                $data['StatusCode'] = 200;
                $data['Message'] = "Job list found!";
                $data['Results']['jobList'] = $jobslist;
            } else {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found!";
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request!";
        }
        echo json_encode($data);
        exit();
    }

    public function cancelledjobDetails(Request $request) {
        $reqdata = $request->all();
        $assignment_id = $reqdata['assignment_id'];
        //get assigned job details
        $jobslist = DB::table('send_proposal')
                        ->join('users', 'send_proposal.assignment_user_id', '=', 'users.id')
                        ->join('assignment_request_audit', 'assignment_request_audit.assignment_id', '=', 'send_proposal.assignment_id')
                        ->select('send_proposal.*', 'send_proposal.id as proposal_id', 'users.id as userId', 'users.fname', 'users.lname', 'users.gender', 'users.profile_img', 'assignment_request_audit.*')
                        ->where('send_proposal.assignment_id', $assignment_id)->where('send_proposal.assignment_id', $assignment_id)
                        ->where('send_proposal.canceljob_status', 1)->first();
        //get assigned job details
        //get city name
        @$locationIds = explode(",", @$jobslist->assignment_location_id);
        $locationname = DB::table('assignment_location')
                        ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                        ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                        ->join('states', 'states.id', '=', 'assignment_location.state_id')
                        ->whereIn('assignment_location.id', @$locationIds)->get();
        //get city name
        //get CB address
        $addressCb = DB::table('users_contact_address')
                        ->select('countries.name as countryName', 'states.name as stateName', 'users_contact_address.city_name')
                        ->join('countries', 'countries.id', '=', 'users_contact_address.country_id')
                        ->join('states', 'states.id', '=', 'users_contact_address.state_id')
                        ->where('users_contact_address.user_id', @$jobslist->userId)->orderBy('users_contact_address.id', 'DESC')->first();
        //get CB address
        //get job track list
        $jobtracklist = AssignmentTracking::where('sender_id', @Auth::user()->id)->where('proposal_id', @$jobslist->proposal_id)
                        ->orderBy('id', 'DESC')->get();
        //get job track list
        if (count($jobslist) > 0) {
            $data['StatusCode'] = 200;
            $data['Message'] = "Job details found!";
            $data['Results']['jobList'] = $jobslist;
            $data['Results']['locationList'] = $locationname;
            $data['Results']['addressCB'] = $addressCb;
            $data['Results']['jobTrackList'] = $jobtracklist;
        } else {
            $data['StatusCode'] = 9999;
            $data['Message'] = "No details found!";
        }
        echo json_encode($data);
        exit();
    }

    public function addcpdHours(Request $request) {
        $reqdata = $request->all();
        $cpd_ins['user_id'] = $reqdata['userID'];
        $update_id = @$reqdata['updateID'];
        $cpd_ins['cpd_title'] = $reqdata['cpdTitle'];
        $cpd_ins['cpd_description'] = $reqdata['cpdDescription'];
        $cpd_ins['cpd_date'] = Carbon::parse($reqdata['cpdDate'])->format('Y-m-d');
        $cpd_links = @$reqdata['cpdlinks'];
        if ($cpd_links) {
            $cpd_ins['cpd_links'] = implode(',', $cpd_links);
        } else {
            $cpd_ins['cpd_links'] = null;
        }
        if (empty($cpd_ins['user_id'])) {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
            echo json_encode($data);
            exit();
        } else {
            if ($update_id) {
                $uid = CpdHours::where("id", $update_id)->update($cpd_ins);
                if ($uid) {
                    $data['Message'] = "CPD Hours update successfully!";
                    $data['StatusCode'] = 200;
                } else {
                    $data['Message'] = "Failure in CPD Hours update.";
                    $data['StatusCode'] = 9999;
                }
            } else {
                $cpd_ins['status'] = 1;
                $cpd_ins['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $cpd_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
                $ins_id = CpdHours::create($cpd_ins);
                $id_insert = $ins_id->id;
                if (@$id_insert) {
                    $data['Message'] = "CPD Hours saved Successfully!";
                    $data['StatusCode'] = 200;
                } else {
                    $data['StatusCode'] = 9999;
                    $data['Message'] = "Failure in save.";
                }
            }
        }
        echo json_encode($data);
        exit();
    }

    public function getCpdHours(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['userID'];
        if ($user_id) {
            $getcpddetails = CpdHours::where('user_id', $user_id)->orderBy('id', 'DESC')->get();
            if (count($getcpddetails) > 0) {
                $data['Message'] = "CPD Hours list found!";
                $data['StatusCode'] = 200;
                $data['Results'] = $getcpddetails;
            } else {
                $data['StatusCode'] = 9999;
                $data['Message'] = "No list found.";
            }
        } else {
            $data['Message'] = "Bad request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function deleteCpdHours(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['UserId'];
        $id_delete = $reqdata['DeleteId'];
        if ($user_id && $id_delete) {
            $cpdDetail = CpdHours::where('id', $id_delete)->where('user_id', $user_id)->first();
            if ($cpdDetail) {
                CpdHours::where('id', $id_delete)->delete();
                $data['Message'] = "CPD Hours deleted successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['Message'] = "Failure in CPD Hours delete.";
                $data['StatusCode'] = 9999;
            }
        } else {
            $data['Message'] = "Bad Request.";
            $data['StatusCode'] = 400;
        }
        echo json_encode($data);
        exit();
    }

    public function addTrainingPost() {
        $data['result']['trainingtypelist'] = Config::get('selecttype.trainingtypelist');
        $data['result']['standardlist'] = Config::get('standards.standardlist');
        $data['result']['trainingmode'] = Config::get('selecttype.trainingmode');
        $data['result']['assignmentstatus'] = Config::get('selecttype.assignmentstatus');
        $data['StatusCode'] = 200;
        $data['Message'] = "Listing Successfully.";
        echo json_encode($data);
        exit();
    }

    public function createTrainingPost(Request $request) {
        $reqdata = $request->all();

        $ins_data = array();
        $training_post_id = @$reqdata['training_post_id'];
        $ins_data['user_id'] = @$reqdata['user_id'];
        $ins_data['traning_name'] = @$reqdata['traning_name'];
        $ins_data['batch_no'] = $reqdata['batch_no'];
        $ins_data['start_date'] = Carbon::parse($reqdata['start_date'])->format('Y-m-d');
        $ins_data['end_date'] = Carbon::parse($reqdata['end_date'])->format('Y-m-d');
        $ins_data['standard'] = $reqdata['standard'];
        $ins_data['training_type'] = $reqdata['training_type'];
        $ins_data['session_details'] = $reqdata['session_details'];
        $ins_data['mode'] = $reqdata['mode'];
        $ins_data['fee_range'] = $reqdata['fee_range'];
        $ins_data['status'] = $reqdata['status'];
        $ins_data['add_date'] = date("Y-m-d H:i:s");
        $ins_data['protocol'] = $_SERVER['REMOTE_ADDR'];

        $audit_location = @$reqdata['advlocation'];
        $location = array();

        //check credit points
        $taskPoints = $this->SubscriptionController->getTaskPoint("Training Advertisement");
        $creditPoints = $this->SubscriptionController->getUserCreditPoint($ins_data['user_id']);
        if ($creditPoints < $taskPoints && empty($training_post_id)) {
            $data['StatusCode'] = 9999;
            $data['Message'] = "You have not sufficient credit points available!";
            echo json_encode($data);
            exit();
        } else {
            if (@$training_post_id) {
                $checkpresent = TrainingPost::where('training_post_id', @$training_post_id)->first();
                if (count(@$checkpresent) == 0) {
                    $data['StatusCode'] = 400;
                    $data['Message'] = "Bad request.";
                    echo json_encode($data);
                    exit();
                }
                //insert to location
                if (count($audit_location) > 0) {
                    foreach ($audit_location as $key => $val) {
                        if ($val['loc_id']) {
                            
                        } else {
                            $location['country_id'] = $val['country_id'];
                            $location['state_id'] = $val['state_id'];
                            $location['city_name'] = $val['city_name'];
                            $location['city_id'] = $val['city_id'];
                            $location['training_post_id'] = $training_post_id;
                            $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                            $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                            $locid = TrainingPostLocation::insertGetId($location);
                        }
                    }
                }
                $id = TrainingPost::where('training_post_id', $training_post_id)->update($ins_data);
            } else {

                //update transaction
                $debitPoints = $this->SubscriptionController->updateUserCreditPoint("", $ins_data['user_id'], $taskPoints, "");
                if ($debitPoints) {
                    $updateTransaction = $this->SubscriptionController->updateUserTransaction($ins_data['user_id'], "Training Advertisement", Carbon::now()->format('Y-m-d'), "", $taskPoints);
                }

                $training_post_id = TrainingPost::insertGetId($ins_data);
                //insert location
                if (count($audit_location) > 0) {
                    foreach ($audit_location as $key => $val) {
                        $location['country_id'] = $val['country_id'];
                        $location['state_id'] = $val['state_id'];
                        $location['city_name'] = $val['city_name'];
                        $location['city_id'] = $val['city_id'];
                        $location['training_post_id'] = $training_post_id;
                        $location['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                        $location['protocol'] = $_SERVER['REMOTE_ADDR'];
                        TrainingPostLocation::insertGetId($location);
                    }
                }
                //insert location

                $ref_id = "TA" . date("Y") . date("m") . date("d") . $training_post_id;
                TrainingPost::where('training_post_id', $training_post_id)->update(array(
                    "ref_id" => $ref_id
                ));
            }
            //for image
            $path = public_path('training_image');
            $image = @$reqdata['training_image'];
            if (@$image) {
                $img = $this->base64_to_jpeg($image);
                $nameimage = str_replace(" ", "_", @$reqdata['traning_name']);
                $imagename = $training_post_id . "_" . @$nameimage . '.jpg';
                $file = $path . "/" . $imagename;
                if (file_exists($file)) {
                    @unlink($file);
                }
                file_put_contents($file, $img);
                $img_data['image'] = $imagename;
                TrainingPost::where('training_post_id', $training_post_id)->update($img_data);
            }
            //end
            if ($training_post_id) {
                $data['Message'] = "Training post saved successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['StatusCode'] = 400;
                $data['Message'] = "Bad request.";
            }
        }
        echo json_encode($data);
        exit();
    }

    public function trainingPostList(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['user_id'];
        $data = array();
        $trainingpostlist = TrainingPost::where('user_id', $user_id)
                        ->orderBy('training_post_id', 'DESC')->get();
        $trainingtypelist = Config::get('selecttype.trainingtypelist');
        $trainingmode = Config::get('selecttype.trainingmode');
        $status = Config::get('selecttype.assignmentstatus');
        $standard = Config::get('standards.standardlist');

        if (@$user_id) {
            if (count(@$trainingpostlist) > 0) {
                foreach (@$trainingpostlist as $key => $val) {
                    $locationname = DB::table('trainingadv_location')
                                    ->select('countries.name as countryName', 'states.name as stateName', 'trainingadv_location.*')
                                    ->join('countries', 'countries.id', '=', 'trainingadv_location.country_id')
                                    ->join('states', 'states.id', '=', 'trainingadv_location.state_id')
                                    ->where('trainingadv_location.training_post_id', $val->training_post_id)->get();

                    $data['result']['trainingpostlist'][$key]['training_post_id'] = $val->training_post_id;
                    $data['result']['trainingpostlist'][$key]['reference_id'] = @$val->ref_id;
                    $data['result']['trainingpostlist'][$key]['traning_name'] = @$val->traning_name;
                    $data['result']['trainingpostlist'][$key]['start_date'] = Carbon::parse(@$val->start_date)->format(config('assignment.dateformat'));
                    $data['result']['trainingpostlist'][$key]['end_date'] = Carbon::parse(@$val->end_date)->format(config('assignment.dateformat'));
                    $data['result']['trainingpostlist'][$key]['training_type'] = @$trainingtypelist[@$val->training_type];
                    $data['result']['trainingpostlist'][$key]['training_type_id'] = @$val->training_type;
                    $data['result']['trainingpostlist'][$key]['mode'] = @$trainingmode[@$val->mode];
                    $data['result']['trainingpostlist'][$key]['mode_id'] = @$val->mode;

                    if (@$val->status == 1) {
                        $data['result']['trainingpostlist'][$key]['status'] = "Active";
                        $data['result']['trainingpostlist'][$key]['status_id'] = @$val->status;
                    } else if (@$val->status == 2) {
                        $data['result']['trainingpostlist'][$key]['status'] = "Inactive";
                        $data['result']['trainingpostlist'][$key]['status_id'] = @$val->status;
                    }
                    $data['result']['trainingpostlist'][$key]['user_id'] = @$val->user_id;
                    $data['result']['trainingpostlist'][$key]['batch_no'] = @$val->batch_no;
                    $data['result']['trainingpostlist'][$key]['standard'] = @$standard[@$val->standard];
                    $data['result']['trainingpostlist'][$key]['standard_id'] = @$val->standard;
                    $data['result']['trainingpostlist'][$key]['session_details'] = @$val->session_details;
                    $data['result']['trainingpostlist'][$key]['fee_range'] = @$val->fee_range;
                    $data['result']['trainingpostlist'][$key]['image'] = @$val->image;
                    $data['result']['trainingpostlist'][$key]['path'] = url('training_image') . "/";
                    if (count(@$locationname) > 0) {
                        foreach (@$locationname as $k => $v) {
                            $data['result']['trainingpostlist'][$key]['location'][$k]['city'] = @$v->city_name;
                            $data['result']['trainingpostlist'][$key]['location'][$k]['city_id'] = @$v->city_id;
                            $data['result']['trainingpostlist'][$key]['location'][$k]['state'] = @$v->stateName;
                            $data['result']['trainingpostlist'][$key]['location'][$k]['state_id'] = @$v->state_id;
                            $data['result']['trainingpostlist'][$key]['location'][$k]['country'] = @$v->countryName;
                            $data['result']['trainingpostlist'][$key]['location'][$k]['country_id'] = @$v->country_id;
                        }
                    } else {
                        $data['result']['trainingpostlist'][$key]['location'] = "";
                    }
                }
                $data['Message'] = "Training post listed successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['StatusCode'] = 200;
                $data['Message'] = "No List Found.";
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function deleteTrainingPost(Request $request) {
        $reqdata = $request->all();
        $training_post_id = $reqdata['training_post_id'];
        $data = array();
        if ($training_post_id) {
            TrainingPost::where('training_post_id', $training_post_id)->delete();
            $data['Message'] = "Training post deleted successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function latesttrainingAdvertisementList(Request $request) {
        $reqdata = $request->all();
        $data = array();
        $trainingpostlist = TrainingPost::where('status', '=', 1)
                        ->orderBy('training_post_id', 'DESC')->get();
        $trainingtypelist = Config::get('selecttype.trainingtypelist');
        $trainingmode = Config::get('selecttype.trainingmode');
        $status = Config::get('selecttype.assignmentstatus');
        $standard = Config::get('standards.standardlist');


        if (count(@$trainingpostlist) > 0) {
            foreach (@$trainingpostlist as $key => $val) {
                $locationname = DB::table('trainingadv_location')
                                ->select('countries.name as countryName', 'states.name as stateName', 'trainingadv_location.*')
                                ->join('countries', 'countries.id', '=', 'trainingadv_location.country_id')
                                ->join('states', 'states.id', '=', 'trainingadv_location.state_id')
                                ->where('trainingadv_location.training_post_id', $val->training_post_id)->get();

                $data['result']['trainingpostlist'][$key]['training_post_id'] = $val->training_post_id;
                $data['result']['trainingpostlist'][$key]['reference_id'] = @$val->ref_id;
                $data['result']['trainingpostlist'][$key]['traning_name'] = @$val->traning_name;
                $data['result']['trainingpostlist'][$key]['start_date'] = Carbon::parse(@$val->start_date)->format(config('assignment.dateformat'));
                $data['result']['trainingpostlist'][$key]['end_date'] = Carbon::parse(@$val->end_date)->format(config('assignment.dateformat'));
                $data['result']['trainingpostlist'][$key]['training_type'] = @$trainingtypelist[@$val->training_type];
                $data['result']['trainingpostlist'][$key]['training_type_id'] = @$val->training_type;
                $data['result']['trainingpostlist'][$key]['mode'] = @$trainingmode[@$val->mode];
                $data['result']['trainingpostlist'][$key]['mode_id'] = @$val->mode;

                if (@$val->status == 1) {
                    $data['result']['trainingpostlist'][$key]['status'] = "Active";
                    $data['result']['trainingpostlist'][$key]['status_id'] = @$val->status;
                } else if (@$val->status == 2) {
                    $data['result']['trainingpostlist'][$key]['status'] = "Inactive";
                    $data['result']['trainingpostlist'][$key]['status_id'] = @$val->status;
                }
                $data['result']['trainingpostlist'][$key]['user_id'] = @$val->user_id;
                $data['result']['trainingpostlist'][$key]['batch_no'] = @$val->batch_no;
                $data['result']['trainingpostlist'][$key]['standard'] = @$standard[@$val->standard];
                $data['result']['trainingpostlist'][$key]['standard_id'] = @$val->standard;
                $data['result']['trainingpostlist'][$key]['session_details'] = @$val->session_details;
                $data['result']['trainingpostlist'][$key]['fee_range'] = @$val->fee_range;
                $data['result']['trainingpostlist'][$key]['image'] = @$val->image;
                $data['result']['trainingpostlist'][$key]['path'] = url('training_image') . "/";
                if (count(@$locationname) > 0) {
                    foreach (@$locationname as $k => $v) {
                        $data['result']['trainingpostlist'][$key]['location'][$k]['city'] = @$v->city_name;
                        $data['result']['trainingpostlist'][$key]['location'][$k]['city_id'] = @$v->city_id;
                        $data['result']['trainingpostlist'][$key]['location'][$k]['state'] = @$v->stateName;
                        $data['result']['trainingpostlist'][$key]['location'][$k]['state_id'] = @$v->state_id;
                        $data['result']['trainingpostlist'][$key]['location'][$k]['country'] = @$v->countryName;
                        $data['result']['trainingpostlist'][$key]['location'][$k]['country_id'] = @$v->country_id;
                    }
                } else {
                    $data['result']['trainingpostlist'][$key]['location'] = "";
                }
            }
            $data['Message'] = "Training post listed successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 200;
            $data['Message'] = "No List Found.";
        }

        echo json_encode($data);
        exit();
    }

    public function createQuery(Request $request) {
        $reqdata = $request->all();
//        print "<pre>";
//        print_r($reqdata);exit;
        $ins_data = array();
        $query_id = @$reqdata['query_id'];
        $query['startDate'] = Carbon::parse($reqdata['startDate'])->format('Y-m-d');
        $query['endDate'] = Carbon::parse($reqdata['endDate'])->format('Y-m-d');
        $query['user_id'] = @$reqdata['user_id'];
        $query['usertype'] = @$reqdata['user_type'];
        $query['title'] = @$reqdata['title'];
        $query['codetype'] = @$reqdata['codetype'];
        $query['sic_code'] = @$reqdata['sic_code'];

        //check credit points
        $taskPoints = $this->SubscriptionController->getTaskPoint("Query Posting");
        $creditPoints = $this->SubscriptionController->getUserCreditPoint($query['user_id']);
        if ($creditPoints < $taskPoints && empty($query_id)) {
            $data['StatusCode'] = 9999;
            $data['Message'] = "You have not sufficient credit points available!";
            echo json_encode($data);
            exit();
        } else {
            $cityId = DB::table('cities')->select("id")
                            ->where('name', @$reqdata['city_name'])
                            ->where('country_id', @$reqdata['country'])
                            ->where('region_id', @$reqdata['states'])->first();
            if ($cityId) {
                $query['city'] = $cityId->id;
            }
            $query['city_name'] = @$reqdata['city_name'];
            $query['states'] = @$reqdata['states'];
            $query['country'] = @$reqdata['country'];
            //location info

            $query['status'] = @$reqdata['status'];
            $query['effort_mandays'] = @$reqdata['effort_mandays'];
            $query['lookingfor'] = @$reqdata['lookingfor'];
            $query['description'] = @$reqdata['description'];
            if (@$reqdata['lookingfor'] == 1) {
                $query['travelinfo'] = @$reqdata['travelinfo'];
                $query['budgetinfo'] = @$reqdata['budgetinfo'];
            } else if (@$reqdata['lookingfor'] == 2) {
                $query['perday_charge'] = @$reqdata['perday_charge'];
                $query['radious'] = @$reqdata['radious'];
            }

            if (@$query_id) {
                $id = DB::table('query')->where('query_id', $query_id)->update($query);
            } else {
                $query['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $query['protocol'] = $_SERVER['REMOTE_ADDR'];
                $debitPoints = $this->SubscriptionController->updateUserCreditPoint("", $query['user_id'], $taskPoints, "");
                if ($debitPoints) {
                    $updateTransaction = $this->SubscriptionController->updateUserTransaction($query['user_id'], "Query Posting", Carbon::now()->format('Y-m-d'), "", $taskPoints);
                }
                $id = DB::table('query')->insertGetId($query);
            }
            if ($id && $query['status'] == 1) {
            //store email notification
                    $detail_url = asset('queryPostDetail/' . $id);
                    $msgbody = "<p>A new query has been posted related to your industry sector.</p>"
                            . "<p>To view or respond to this query, please click <a href='$detail_url'>here</a>.</p>"
                            . "Thanks,<br />ISOMart Team";
                    $providerList = DB::table('users')
                                    ->join('users_isic_code as ic', 'ic.user_id', '=', 'users.id')
                                    ->select('users.email', 'users.fname', 'users.lname', 'users.fcm_device_token')
                                    ->where('ic.codetype', $query['codetype'])
                                    ->whereIn('users.usertype', array(5, 6))->get();
                    $fcm_token = array();
                    foreach ($providerList as $k => $v) {
                        $fcm_token[] = $v->fcm_device_token;
                        $ins_email['email_type'] = 4; //3->New query
                        $ins_email['email_to'] = $v->email;
                        $ins_email['email_name'] = $v->fname . " " . $v->lname;
                        $ins_email['email_subject'] = "New Query Posted";
                        $ins_email['email_message'] = "<p>Hello " . $ins_email['email_name'] . "</p>" . $msgbody;
                        $ins_email['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
                        $ins_email['protocol'] = $_SERVER['REMOTE_ADDR'];
                        EmailNotification::insertGetId($ins_email);
                    }
                    //send app notification
                    $id_query = $id;
                    $this->NotificationController->newQueryNotification($id_query, $fcm_token);    
            }
            
            if ($id) {
                $data['Message'] = "Query post successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['StatusCode'] = 400;
                $data['Message'] = "Bad request.";
            }
        }
        echo json_encode($data);
        exit();
    }

    public function quertList(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['user_id'];
        $data = array();
        $querypostlist = DB::table('query')->where('user_id', $user_id)
                        ->orderBy('query_id', 'DESC')->get();
        $codeDetail = DB::table('international_standards_section')->pluck("name", "id");
        $codetype = Config::get('selecttype.codetype');
        $status = Config::get('selecttype.assignmentstatus');
        $country = DB::table('countries')->pluck("name", "id");
        $state = DB::table('states')->pluck("name", "id");

        if (@$user_id) {
            if (count(@$querypostlist) > 0) {
                foreach (@$querypostlist as $key => $val) {
                    $data['result']['querypostlist'][$key]['query_id'] = $val->query_id;
                    $data['result']['querypostlist'][$key]['userid'] = @$val->user_id;
                    $data['result']['querypostlist'][$key]['usertype'] = @$val->usertype;
                    $data['result']['querypostlist'][$key]['title'] = @$val->title;
                    $data['result']['querypostlist'][$key]['codetype'] = @$codetype[@$val->codetype];
                    $data['result']['querypostlist'][$key]['codetype_id'] = @$val->codetype;
                    $data['result']['querypostlist'][$key]['sic_code'] = @$codeDetail[@$val->sic_code];
                    $data['result']['querypostlist'][$key]['sic_code_id'] = @$val->sic_code;
                    $data['result']['querypostlist'][$key]['startDate'] = Carbon::parse(@$val->startDate)->format(config('assignment.dateformat'));
                    $data['result']['querypostlist'][$key]['endDate'] = Carbon::parse(@$val->endDate)->format(config('assignment.dateformat'));
                    $data['result']['querypostlist'][$key]['country'] = @$country[@$val->country];
                    $data['result']['querypostlist'][$key]['country_id'] = @$val->country;
                    $data['result']['querypostlist'][$key]['state'] = @$state[@$val->states];
                    $data['result']['querypostlist'][$key]['state_id'] = @$val->states;
                    $data['result']['querypostlist'][$key]['city'] = @$val->city_name;
                    $data['result']['querypostlist'][$key]['city_id'] = @$val->city;

                    if (@$val->status == 1) {
                        $data['result']['querypostlist'][$key]['status'] = "Active";
                        $data['result']['querypostlist'][$key]['status_id'] = @$val->status;
                    } else if (@$val->status == 2) {
                        $data['result']['querypostlist'][$key]['status'] = "Inactive";
                        $data['result']['querypostlist'][$key]['status_id'] = @$val->status;
                    }
                    if (@$val->lookingfor == 1) {
                        $data['result']['querypostlist'][$key]['lookingforName'] = "Seeker";
                        $data['result']['querypostlist'][$key]['lookingfor'] = @$val->lookingfor;
                        $data['result']['querypostlist'][$key]['travelinfo'] = @$val->travelinfo;
                        $data['result']['querypostlist'][$key]['budgetinfo'] = @$val->budgetinfo;
                    } else if (@$val->lookingfor == 2) {
                        $data['result']['querypostlist'][$key]['lookingforName'] = "Provider";
                        $data['result']['querypostlist'][$key]['lookingfor'] = @$val->lookingfor;
                        $data['result']['querypostlist'][$key]['perday_charge'] = @$val->perday_charge;
                        $data['result']['querypostlist'][$key]['radious'] = @$val->radious;
                    }
                    $data['result']['querypostlist'][$key]['effort_mandays'] = @$val->effort_mandays;
                    $data['result']['querypostlist'][$key]['description'] = @$val->description;
                }
                $data['Message'] = "Query post listed successfully!";
                $data['StatusCode'] = 200;
            } else {
                $data['StatusCode'] = 200;
                $data['Message'] = "No List Found.";
            }
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function deleteQueryPost(Request $request) {
        $reqdata = $request->all();
        $query_id = $reqdata['query_id'];
        $data = array();
        if ($query_id) {
            DB::table('query')->where('query_id', $query_id)->delete();
            $data['Message'] = "Query post deleted successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    public function latestQueryList(Request $request) {
        $reqdata = $request->all();
        $data = array();
        $querypostinfo = DB::table('query')->where('status', '=', 1)
                        ->orderBy('startDate', 'DESC')->get();
//        $data['querypostinfo'] = $querypostinfo;
        //$userdetails = DB::table('users')->where("id", @$querypostinfo->user_id)->first();
        $codeDetail = DB::table('international_standards_section')->pluck("name", "id");
        $codetype = Config::get('selecttype.codetype');
        $status = Config::get('selecttype.assignmentstatus');
        $country = DB::table('countries')->pluck("name", "id");
        $state = DB::table('states')->pluck("name", "id");


        if (count(@$querypostinfo) > 0) {
            foreach (@$querypostinfo as $key => $val) {
                $data['result']['querypostlist'][$key]['query_id'] = $val->query_id;
                $data['result']['querypostlist'][$key]['userid'] = @$val->user_id;
                $data['result']['querypostlist'][$key]['usertype'] = @$val->usertype;
                $data['result']['querypostlist'][$key]['title'] = @$val->title;
                $data['result']['querypostlist'][$key]['codetype'] = @$codetype[@$val->codetype];
                $data['result']['querypostlist'][$key]['codetype_id'] = @$val->codetype;
                $data['result']['querypostlist'][$key]['sic_code'] = @$codeDetail[@$val->sic_code];
                $data['result']['querypostlist'][$key]['sic_code_id'] = @$val->sic_code;
                $data['result']['querypostlist'][$key]['startDate'] = Carbon::parse(@$val->startDate)->format(config('assignment.dateformat'));
                $data['result']['querypostlist'][$key]['endDate'] = Carbon::parse(@$val->endDate)->format(config('assignment.dateformat'));
                $data['result']['querypostlist'][$key]['country_name'] = @$country[@$val->country];
                $data['result']['querypostlist'][$key]['country_id'] = @$val->country;
                $data['result']['querypostlist'][$key]['state_name'] = @$state[@$val->states];
                $data['result']['querypostlist'][$key]['state_id'] = @$val->states;
                $data['result']['querypostlist'][$key]['city_name'] = @$val->city_name;
                $data['result']['querypostlist'][$key]['city_id'] = @$val->city;

                if (@$val->status == 1) {
                    $data['result']['querypostlist'][$key]['status'] = "Active";
                    $data['result']['querypostlist'][$key]['status_id'] = @$val->status;
                } else if (@$val->status == 2) {
                    $data['result']['querypostlist'][$key]['status'] = "Inactive";
                    $data['result']['querypostlist'][$key]['status_id'] = @$val->status;
                }
                if (@$val->lookingfor == 1) {
                    $data['result']['querypostlist'][$key]['lookingforName'] = "Seeker";
                    $data['result']['querypostlist'][$key]['lookingfor'] = @$val->lookingfor;
                    $data['result']['querypostlist'][$key]['travelinfo'] = @$val->travelinfo;
                    $data['result']['querypostlist'][$key]['budgetinfo'] = @$val->budgetinfo;
                } else if (@$val->lookingfor == 2) {
                    $data['result']['querypostlist'][$key]['lookingforName'] = "Provider";
                    $data['result']['querypostlist'][$key]['lookingfor'] = @$val->lookingfor;
                    $data['result']['querypostlist'][$key]['perday_charge'] = @$val->perday_charge;
                    $data['result']['querypostlist'][$key]['radious'] = @$val->radious;
                }
                $data['result']['querypostlist'][$key]['effort_mandays'] = @$val->effort_mandays;
                $data['result']['querypostlist'][$key]['description'] = @$val->description;
            }
            $data['Message'] = "Query post listed successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 200;
            $data['Message'] = "No List Found.";
        }

        echo json_encode($data);
        exit();
    }

    public function queryPostDetail(Request $request) {
        $reqdata = $request->all();
        $query_id = $reqdata['query_id'];
        $data = array();

        $querypostdetail = DB::table('query')->where('query_id', $query_id)->first();

        $userdetails = DB::table('users')->where("id", @$querypostdetail->user_id)->first();
        $codeDetail = DB::table('international_standards_section')->pluck("name", "id");
        $codetype = Config::get('selecttype.codetype');
        $status = Config::get('selecttype.assignmentstatus');
        $country = DB::table('countries')->pluck("name", "id");
        $state = DB::table('states')->pluck("name", "id");

        if (count(@$querypostdetail) > 0) {

            $data['result']['query_id'] = @$querypostdetail->query_id;
            $data['result']['userid'] = @$querypostdetail->user_id;
            $data['result']['usertype'] = @$querypostdetail->usertype;
            $data['result']['title'] = @$querypostdetail->title;
            $data['result']['codetype'] = @$codetype[@$querypostdetail->codetype];
            $data['result']['codetype_id'] = @$querypostdetail->codetype;
            $data['result']['sic_code'] = @$codeDetail[@$querypostdetail->sic_code];
            $data['result']['sic_code_id'] = @$querypostdetail->sic_code;
            $data['result']['startDate'] = Carbon::parse(@$querypostdetail->startDate)->format(config('assignment.dateformat'));
            $data['result']['endDate'] = Carbon::parse(@$querypostdetail->endDate)->format(config('assignment.dateformat'));
            $data['result']['country'] = @$country[@$querypostdetail->country];
            $data['result']['country_id'] = @$querypostdetail->country;
            $data['result']['state'] = @$state[@$querypostdetail->states];
            $data['result']['state_id'] = @$querypostdetail->states;
            $data['result']['city'] = @$querypostdetail->city_name;
            $data['result']['city_id'] = @$querypostdetail->city;

            if (@$querypostdetail->status == 1) {
                $data['result']['status'] = "Active";
                $data['result']['status_id'] = @$querypostdetail->status;
            } else if (@$querypostdetail->status == 2) {
                $data['result']['status'] = "Inactive";
                $data['result']['status_id'] = @$querypostdetail->status;
            }
            if (@$querypostdetail->lookingfor == 1) {
                $data['result']['lookingforName'] = "Seeker";
                $data['result']['lookingfor'] = @$querypostdetail->lookingfor;
                $data['result']['travelinfo'] = @$querypostdetail->travelinfo;
                $data['result']['budgetinfo'] = @$querypostdetail->budgetinfo;
            } else if (@$querypostdetail->lookingfor == 2) {
                $data['result']['lookingforName'] = "Provider";
                $data['result']['lookingfor'] = @$querypostdetail->lookingfor;
                $data['result']['perday_charge'] = @$querypostdetail->perday_charge;
                $data['result']['radious'] = @$querypostdetail->radious;
            }
            $data['result']['effort_mandays'] = @$querypostdetail->effort_mandays;
            $data['result']['description'] = @$querypostdetail->description;


            $data['Message'] = "Query post listed successfully!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 200;
            $data['Message'] = "No Details Found.";
        }

        echo json_encode($data);
        exit();
    }

    public function getSubscriptionPackages() {
        $data = array();
        $packageList = SubscriptionPackage::where('status', 1)->orderBy('id', 'DESC')->get();

        if ($packageList) {
            $data['Message'] = "Package list found!";
            $data['StatusCode'] = 200;
            $data['Results']['packageLists'] = $packageList;
            $data['Results']['packageType'] = config('subscription.packagelist');
            $data['Results']['packageDuration'] = config('subscription.packageduration');
        } else {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        }
        echo json_encode($data);
        exit();
    }

    /* subscription & Payment */

    public function purchaseSubscription(Request $request) {
        $reqdata = $request->all();
        $actionFlag = 1;
        $narration = "On subscription purchase";
        $duration = $reqdata['packageDuration'];
        $user_id = $reqdata['user_id'];
        $package_id = $reqdata['package_id'];
        $package_price = $reqdata['package_price'];
        $credit_points = $reqdata['credits_points'];
        $package_duration = $reqdata['package_duration'];
        $purchase_date = Carbon::now()->format("Y-m-d");
        $expiry_date = Carbon::now()->addMonths($duration);
        $res_subscription = $this->SubscriptionController->subscriptionPurchase($user_id, $package_id, $package_price, $credit_points, $package_duration, $purchase_date, $expiry_date);
        if ($res_subscription) {
            $res_creditpoint = $this->SubscriptionController->updateUserCreditPoint($actionFlag, $user_id, "", $credit_points);
        }
        if ($res_creditpoint) {
            $res_transaction = $this->SubscriptionController->updateUserTransaction($user_id, $narration, $purchase_date, $credit_points, "");
        }
        if ($res_subscription && $res_creditpoint && $res_transaction) {
            $packagename = SubscriptionPackage::select('package_name')->where('id', $package_id)->first();
            $creditInfo = UserCreditPoints::where('user_id', $user_id)->first();
            //send mail to buyer
            $mail['package_price'] = $package_price;
            $mail['credits_points'] = $credit_points;
            $mail['totalcredits_points'] = $creditInfo->credit_points;
            $mail['package_name'] = $packagename->package_name;
            $mail['expiry_date'] = $creditInfo->expiry_date;
            $mail['email'] = $reqdata['email'];
            $mail['fname'] = $reqdata['fname'];
            $mail['lname'] = $reqdata['lname'];
            $mail['subject'] = "ISOMart : Subscription Package Purchase";
            $this->purchaseEmailBuyer($mail);

            $data['Message'] = "Successfully updated your subscription package!";
            $data['StatusCode'] = 200;
        } else {
            $data['StatusCode'] = 200;
            $data['Message'] = "Failure in insert.";
        }
        echo json_encode($data);
        exit();
    }

    public function purchaseEmailBuyer($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $package_price = $data['package_price'];
        $credits_points = $data['credits_points'];
        $package_name = $data['package_name'];
        $expiry_date = $data['expiry_date'];
        $expiry_date = $data['expiry_date'];
        $subject = $data['subject'];
        $totalcredits_points = $data['totalcredits_points'];
        $messageBody = view('email.packagePurchaseEmail', $data);
        $a = Mail::send('email.packagePurchaseEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function mySubscription(Request $request) {
        $reqdata = $request->all();
        $user_id = $reqdata['user_id'];
        $prefix = DB::getTablePrefix();
        if (!$user_id) {
            $data['StatusCode'] = 400;
            $data['Message'] = "Bad request.";
        } else {
            $currentpackagename = UserPackagePurchase::
                            select('purchase_invoice', DB::raw("(SELECT package_name FROM {$prefix}subscription_package as sp WHERE sp.id = {$prefix}user_package_purchase.package_id ) as packageName"))
                            ->where('user_package_purchase.user_id', $user_id)
                            ->orderBy('user_package_purchase.id', 'DESC')->first();
            $subscription_info = DB::table('users')->select('credit_points', 'expiry_date')->where('id', $user_id)->first();
            $transaction_list = UserCreditPointsTransaction::where('user_id', $user_id)
                            ->orderBy('id', 'DESC')->get();
            if (count($transaction_list) > 0) {
                $data['Message'] = "Information found!";
                $data['StatusCode'] = 200;
                $data['Results']['CurrentPackgeName'] = $currentpackagename->packageName;
                if ($currentpackagename->purchase_invoice) {
                    $data['Results']['InvoiceUrl'] = asset('subscription/' . $currentpackagename->purchase_invoice);
                }
                $data['Results']['SubscriptionInfo'] = $subscription_info;
                $data['Results']['TransactionLists'] = $transaction_list;
            } else {
                $data['Message'] = "No information found!";
                $data['StatusCode'] = 9999;
            }
        }
        echo json_encode($data);
        exit();
    }

    /* subscription & Payment */
}
