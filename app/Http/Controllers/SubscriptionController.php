<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use Mail;
use Config;
use URL;
use Hash;
use Carbon\Carbon;
use PDF;
use Illuminate\Support\Facades\File;
use App\UserPurchaseTransaction;
use App\UserPackagePurchase;
use App\UserCreditPointsTransaction;
use App\SubscriptionPackage;
use App\TaskCredit;
/** All Paypal Details class * */
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use App\PaypalPayment;

class SubscriptionController extends Controller {

    public function __construct() {
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'], $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function subscriptionPackage() {
        return view('pages.subscription.subscriptionpackage');
    }

    //used for update credit point & purachse package (only package purchase only credit point updated)
    public function subscriptionPurchase($user_id = "", $package_id = "", $package_price = "", $credit_points = "", $package_duration = "", $purchase_date = "", $expiry_date = "", $transaction_id = "", $payment_id = "") {
        //insert to purchase
        $purchase_ins['user_id'] = $user_id;
        $purchase_ins['package_id'] = $package_id;
        $purchase_ins['package_price'] = $package_price;
        $purchase_ins['credit_points'] = $credit_points;
        $purchase_ins['package_duration'] = $package_duration;
        $purchase_ins['purchase_date'] = $purchase_date;
        $purchase_ins['expiry_date'] = $expiry_date;
        $purchase_ins['transaction_id'] = $transaction_id;
        $purchase_ins['payment_id'] = $payment_id;
        $purchase_ins['payment_mode'] = 3;
        $purchase_ins['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
        $purchase_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
        $id_purchase = UserPackagePurchase::insertGetId($purchase_ins);
        if ($id_purchase) {
            $result = 1;
        } else {
            $result = 0;
        }
        return $result;
    }

    public function updateUserCreditPoint($actionFlag = "", $user_id = "", $debit_point = "", $credit_points = "") {
        //check if record exist
        $getCreditRecord = DB::table('users')->select('credit_points')->where('id', $user_id)->first();
        $prefix = DB::getTablePrefix();
        if ($actionFlag) { //on subscription purchase
            $maxExpiry = UserPackagePurchase::select(DB::raw("MAX(expiry_date) AS maxExpiry"))
                            ->where('user_id', $user_id)->groupBy('user_id')->first();
            $expiry_date = $maxExpiry->maxExpiry;

            if ($getCreditRecord->credit_points > 0) {
                $upd['credit_points'] = $getCreditRecord->credit_points + $credit_points;
                $upd['expiry_date'] = $expiry_date;
            } else {
                $upd['credit_points'] = $credit_points;
                $upd['expiry_date'] = $expiry_date;
            }
            $upd['email_creditpoint_flag'] = 0;
            $upd['email_packagedate_flag'] = 0;
            $update_point = DB::table('users')->where("id", $user_id)->update($upd);
        } else { //on transaction
            $update_point = DB::table('users')->where("id", $user_id)
                    ->update(
                    array(
                        "credit_points" => $getCreditRecord->credit_points - $debit_point
                    )
            );
        }
        if (@$update_point) {
            $result = 1;
        } else {
            $result = 0;
        }
        return $result;
    }

    public function updateUserTransaction($user_id = "", $narration = "", $activity_date = "", $credit_points = "", $debit_point = "") {
        $transact_ins['user_id'] = $user_id;
        $transact_ins['narration'] = $narration;
        $transact_ins['activity_date'] = $activity_date;
        $transact_ins['credit_point'] = $credit_points;
        $transact_ins['debit_point'] = $debit_point;
        $transact_ins['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
        $transact_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
        $id_transact = UserCreditPointsTransaction::insertGetId($transact_ins);
        if ($id_transact) {
            $result = 1;
        } else {
            $result = 0;
        }
        return $result;
    }

    public function getUserCreditPoint($user_id = "") {
        $getCreditpoint = DB::table('users')->select('credit_points')->where('id', $user_id)->first();
        if ($getCreditpoint) {
            return $getCreditpoint->credit_points;
        } else {
            return 0;
        }
    }

    public function getTaskPoint($task_name = "") {
        $getTaskpoint = TaskCredit::select('task_point')->where('task_name', "LIKE", "%$task_name%")->first();
        if ($getTaskpoint) {
            return $getTaskpoint->task_point;
        } else {
            return 0;
        }
    }

    public function checkFreePackage($user_id = "") {
        $getExistFree = UserPackagePurchase::select('package_price', 'id')->where('user_id', $user_id)
                        ->where('package_price', 0)->first();
        if ($getExistFree) {
            return $getExistFree->id;
        }
    }

    public function insertPackage(Request $request) {
        $reqdata = $request->all();
        $checkFreePackage = $this->checkFreePackage(@Auth::user()->id);
        if ($reqdata['package_price'] == 0) {
            if ($checkFreePackage) {
                \Session::flash('error_message', "You have already subscribed free package!");
                return redirect("/mySubscription");
                exit();
            }
        }
        $packageduration = config('subscription.packageduration');
        $package_duration = array_search($reqdata['package_duration'], $packageduration); //get duration key from config array
        $durationDigit = substr($reqdata['package_duration'], 0, strpos($reqdata['package_duration'], ' '));
//        $order_refid = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
        $order_refid = @Auth::user()->id . $reqdata['package_id'] . Carbon::now()->format('Ymdhis');

        //insert into purchase transaction
        $transaction_ins['user_id'] = @Auth::user()->id;
        $transaction_ins['package_id'] = $reqdata['package_id'];
        $transaction_ins['package_price'] = $reqdata['package_price'];
        $transaction_ins['credit_points'] = $reqdata['credits_points'];
        $transaction_ins['package_duration'] = $package_duration;
        $transaction_ins['purchase_date'] = Carbon::now()->format("Y-m-d H:i:s");
        $transaction_ins['expiry_date'] = Carbon::now()->addMonths($durationDigit);
        $transaction_ins['order_refid'] = $order_refid;
        $transaction_ins['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
        $transaction_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
        $id_purchase = UserPurchaseTransaction::insertGetId($transaction_ins); //id of user_purchase_transaction
        //check if subscribed free package
        if ($reqdata['package_price'] > 0) {
            return $this->payWithpaypal($order_refid, $id_purchase, $reqdata['package_price']);
        } else {
            $purchase_info = UserPurchaseTransaction::where('id', $id_purchase)->first();
            $upd_transaction['payment_status'] = 1;
            $update_transaction = UserPurchaseTransaction::where("id", $id_purchase)->update($upd_transaction);
            
            //insert into package purchase
            $this->subscriptionPurchase($purchase_info->user_id, $purchase_info->package_id, 
                    $purchase_info->package_price, $purchase_info->credit_points, 
                    $purchase_info->package_duration, $purchase_info->purchase_date, $purchase_info->expiry_date, $purchase_info->id);

            //update credit point in user
            $actionFlag = 1;
            $this->updateUserCreditPoint($actionFlag, $purchase_info->user_id, "", $purchase_info->credit_points);

            //insert into creditpoint transaction
            $narration = "On subscription purchase";
            $res_transaction = $this->updateUserTransaction($purchase_info->user_id, $narration, $purchase_info->purchase_date, $purchase_info->credit_points, "");

            //send mail to buyer
            $userInfo = DB::table('users')->select('fname', 'lname', 'email', 'countrycode', 'phone', 'credit_points', 'expiry_date')->where('id', $purchase_info->user_id)->first();
            $mail['email'] = $userInfo->email;
            $mail['fname'] = $userInfo->fname;
            $mail['lname'] = $userInfo->lname;
            $mail['credits_points'] = $purchase_info->credit_points;
            $mail['totalcredits_points'] = $userInfo->credit_points;
            $mail['expiry_date'] = $userInfo->expiry_date;
            $mail['subject'] = "ISOMart Purchase Confirmation";
            $this->purchaseEmailBuyer($mail);
            
            if ($res_transaction) {
                \Session::flash('success_message', "Package subscribed Successfully!");
                return redirect("/mySubscription");
            } else {
                \Session::flash('error_message', "Failure in subscription!");
                return redirect("/mySubscription");
            }
        }
    }

    public function payWithpaypalGet($user_id, $package_id, $package_price, $credit_points, $package_duration, $durationDigit) {
        $order_refid = $user_id . $package_id . Carbon::now()->format('Ymdhis');

        //insert into purchase transaction
        $transaction_ins['user_id'] = $user_id;
        $transaction_ins['package_id'] = $package_id;
        $transaction_ins['package_price'] = $package_price;
        $transaction_ins['credit_points'] = $credit_points;
        $transaction_ins['package_duration'] = $package_duration;
        $transaction_ins['purchase_date'] = Carbon::now()->format("Y-m-d H:i:s");
        $transaction_ins['expiry_date'] = Carbon::now()->addMonths($durationDigit);
        $transaction_ins['order_refid'] = $order_refid;
        $transaction_ins['add_date'] = Carbon::now()->format("Y-m-d H:i:s");
        $transaction_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
        $id_purchase = UserPurchaseTransaction::insertGetId($transaction_ins);
        return $this->payWithpaypal($order_refid, $id_purchase, $package_price);
    }

    /* payment integration */

    public function payWithpaypal($order_refid, $id_purchase, $package_price) {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName('Item 1') /** item name * */
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($package_price);/** unit price * */
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $amount->setCurrency('USD')
                ->setTotal($package_price);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('status')) /** Specify return URL * */
                ->setCancelUrl(URL::to('status'));
        $payment = new Payment();
        $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; * */
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::to('/testPayment');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/testPayment');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session * */
        Session::put('paypal_payment_id', $payment->getId());
        Session::put('order_refid', $order_refid);
        Session::put('purchase_id', $id_purchase); //parimary id from user_purchase_transaction
        if (isset($redirect_url)) {
            /** redirect to paypal * */
            return Redirect::away($redirect_url);
        }
        \Session::put('error', 'Unknown error occurred');
        return Redirect::to('/');
    }

    public function getPaymentStatus() {
        /** Get the payment ID before session clear * */
        $payment_id = Session::get('paypal_payment_id');
        $order_refid = Session::get('order_refid');
        $purchase_id = Session::get('purchase_id');
        /** clear the session payment ID * */
        Session::forget('paypal_payment_id');
        Session::forget('order_refid');
        Session::forget('purchase_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::to('/mySubscription');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /*         * Execute the payment * */
        $result = $payment->execute($execution, $this->_api_context);

        /* update payment info in purchase transaction */
        $upd_transaction['order_refid'] = $order_refid;
        $upd_transaction['payment_id'] = $payment_id;
        $upd_transaction['payer_id'] = Input::get('PayerID');
        if ($result->getState() == 'approved') {
            $upd_transaction['payment_status'] = 1;
        } else {
            $upd_transaction['payment_status'] = 2;
        }
        $update_transaction = UserPurchaseTransaction::where("id", $purchase_id)->update($upd_transaction);

        $purchase_info = UserPurchaseTransaction::where('id', $purchase_id)->first();
        if ($result->getState() == 'approved') {
            $prefix = DB::getTablePrefix();

            //insert into package purchase
            $this->subscriptionPurchase($purchase_info->user_id, $purchase_info->package_id, $purchase_info->package_price, $purchase_info->credit_points, $purchase_info->package_duration, $purchase_info->purchase_date, $purchase_info->expiry_date, $purchase_info->id, $purchase_info->payment_id);

            //update credit point in user
            $actionFlag = 1;
            $this->updateUserCreditPoint($actionFlag, $purchase_info->user_id, "", $purchase_info->credit_points);

            //insert into creditpoint transaction
            $narration = "On subscription purchase";
            $res_transaction = $this->updateUserTransaction($purchase_info->user_id, $narration, $purchase_info->purchase_date, $purchase_info->credit_points, "");

            //create & save purchase invoice
            $userInfo = DB::table('users')->select('fname', 'lname', 'email', 'countrycode', 'phone', 'credit_points', 'expiry_date')->where('id', $purchase_info->user_id)->first();
            $userAddressInfo = DB::table('users_contact_address')
                            ->select('countries.name as countryName', 'states.name as stateName', 'users_contact_address.addressline1', 'users_contact_address.city_name')
                            ->join('countries', 'countries.id', '=', 'users_contact_address.country_id')
                            ->join('states', 'states.id', '=', 'users_contact_address.state_id')
                            ->where('users_contact_address.user_id', $purchase_info->user_id)
                            ->where('users_contact_address.primary_status', 1)->first();

            $package_name = SubscriptionPackage::select('package_name')->where('id', $purchase_info->package_id)->first();
            $invoice['userInfo'] = $userInfo;
            $invoice['userAddressInfo'] = $userAddressInfo;
            $invoice['packageName'] = $package_name;
            $invoice['purchaseInfo'] = $purchase_info;
            $pdf = PDF::loadView('pdf.packagepurchaseinvoice', $invoice);

            $invoiceid = date("d") . substr($purchase_info->user_id, 0, 3) . substr($purchase_info->id, 0, 3);
            $pdf->save("subscription/invoice_$invoiceid.pdf");
            UserPackagePurchase::where("transaction_id", $purchase_info->id)
                    ->update(array("purchase_invoice" => "invoice_$invoiceid.pdf"));
            //send mail to buyer
            $mail['email'] = $userInfo->email;
            $mail['fname'] = $userInfo->fname;
            $mail['lname'] = $userInfo->lname;
            $mail['credits_points'] = $purchase_info->credit_points;
            $mail['totalcredits_points'] = $userInfo->credit_points;
            $mail['expiry_date'] = $userInfo->expiry_date;
            $mail['filepathinvoice'] = public_path('subscription') . "/invoice_$invoiceid.pdf";
            $mail['subject'] = "ISOMart Purchase Confirmation";
            $this->purchaseEmailBuyer($mail);

            \Session::flash('success_message', "Package subscribed Successfully!");
            return redirect("/mySubscription");
        }
        Session::flash('error_message', "Failure in subscription!");
        return redirect("/mySubscription");
    }

    /* payment integration */

    public function purchaseEmailBuyer($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $subject = $data['subject'];
        $filepathinvoice = @$data['filepathinvoice'];
        $messageBody = view('email.packagePurchaseEmail', $data);
        $a = Mail::send('email.packagePurchaseEmail', $data, function($message) use ($toemail, $toname, $subject, $filepathinvoice) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                    if ($filepathinvoice) {
                        $message->attach($filepathinvoice);
                    }
                });
    }

    public function mySubscription() {
        $data = array();
        $prefix = DB::getTablePrefix();
        $currentpackagename = UserPackagePurchase::
                        select('purchase_invoice', DB::raw("(SELECT package_name FROM {$prefix}subscription_package as sp WHERE sp.id = {$prefix}user_package_purchase.package_id ) as packageName"))
                        ->where('user_package_purchase.user_id', @Auth::user()->id)
                        ->orderBy('user_package_purchase.id', 'DESC')->first();

        $subscription_info = DB::table('users')->select('credit_points', 'expiry_date')->where('id', @Auth::user()->id)->first();
        $data['transaction_list'] = UserCreditPointsTransaction::where('user_id', @Auth::user()->id)
                        ->orderBy('id', 'DESC')->paginate(10);
        $data['selectmenu'] = "mysubscription";
        $data['subscription_info'] = $subscription_info;
        $data['currentpackagename'] = $currentpackagename;
        return view('pages.subscription.mysubscription', $data);
    }

    public function subscriptionPackages() {
        $data = array();
        $data['package_list'] = SubscriptionPackage::where('status', 1)->orderBy('package_price', 'ASC')->paginate(3);
        $data['selectmenu'] = "subscriptionpackages";
        return view('pages.subscription.subscriptionPackages', $data);
    }

}
