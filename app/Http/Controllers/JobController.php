<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Session;
use Mail;
use Config;
use Illuminate\Support\Facades\File;
use App\AssignmentAudit;
use App\AssignmentTraining;
use App\AssignmentReport;
use App\AssignmentCertification;
use App\Assignment;
use App\SendProposal;
use App\AssignmentLocation;
use App\AssignmentTracking;
use App\Rating;
use App\RatingAnswer;
use Carbon\Carbon;
use App\Http\Controllers\NotificationController;

class JobController extends UserController {
    private $NotificationController;
    public function __construct() {
        $this->NotificationController = new NotificationController();
        $action = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);
    }

    public function myjobslist() {
        UserController::validuser();
        $data['jobslist'] = array();
        $prefix = DB::getTablePrefix();
        //get job of CB
        $arraysearchappendurl = array();
        $val = 1;
        if (@Auth::user()->usertype == 6) {
        $jobslist = DB::table('assignment')
                        ->join('assignment_request_audit as ar', 'ar.assignment_id', '=', 'assignment.id')
                        ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                        ->select('assignment.start_job_at', 'assignment.close_job', 'ar.*', 'send_proposal.id as proposal_id', DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT COUNT(id) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobAcceptedBidCount")
                        )
                        ->where(function($q) use ($val) {
                            $q->where('assignment.approval_status', $val)
                            ->orWhere('send_proposal.approval_status', $val);
                        })
                        ->where('assignment.user_id', @Auth::user()->id)
                        ->orderBy('assignment.id', 'DESC')->groupBy('assignment.id')->paginate(6);
        $jobslist->appends($arraysearchappendurl)->links();
        $data['jobslist'] = $jobslist;
        } else if (@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) {
            $jobslist = DB::table('assignment')
                        ->join('assignment_request_training as ar', 'ar.assignment_id', '=', 'assignment.id')
                        ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                        ->select('assignment.start_job_at', 'assignment.close_job', 'ar.*', 'send_proposal.id as proposal_id', DB::raw("(SELECT SUM(estimated_budget) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobBudget"), DB::raw("(SELECT COUNT(id) FROM {$prefix}send_proposal as s WHERE s.approval_status = 1 and s.assignment_id = {$prefix}assignment.id ) as jobAcceptedBidCount")
                        )
                        ->where(function($q) use ($val) {
                            $q->where('assignment.approval_status', $val)
                            ->orWhere('send_proposal.approval_status', $val);
                        })
                        ->where('assignment.user_id', @Auth::user()->id)
                        ->orderBy('assignment.id', 'DESC')->groupBy('assignment.id')->paginate(6);
        $jobslist->appends($arraysearchappendurl)->links();
        $data['jobslist'] = $jobslist;
        }
        $data['selectmenu'] = "myjoblist";
        return view('pages.job.myjobslist', $data);
    }

    public function assignedjoblist() {
        UserController::validuser();
        $data['jobslist'] = array();
        $prefix = DB::getTablePrefix();
        $val = 1;
        //get job of auditor
        $arraysearchappendurl = array();
        $jobslist = DB::table('assignment')
                        ->join('assignment_request_audit as ar', 'ar.assignment_id', '=', 'assignment.id')
                        ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                        ->select('assignment.start_job_at', 'ar.*', 'ar.start_date as assignment_startdate', 'ar.end_date as assignment_enddate', 'send_proposal.id as proposal_id', 'send_proposal.*')
                        ->where('send_proposal.approval_status', 1)
                ->where('send_proposal.user_id', @Auth::user()->id)
                        ->orderBy('assignment.id', 'DESC')->paginate(6);
        $jobslist->appends($arraysearchappendurl)->links();
        $data['jobslist'] = $jobslist;
        $data['selectmenu'] = "assignedjobslist";
        return view('pages.job.assignedjoblist', $data);
    }
    
    public function assignedjoblisttraining() {
        UserController::validuser();
        $data['jobslist'] = array();
        $prefix = DB::getTablePrefix();
        $val = 1;
        //get job of auditor
        $arraysearchappendurl = array();
        $jobslist = DB::table('assignment')
                        ->join('assignment_request_training as ar', 'ar.assignment_id', '=', 'assignment.id')
                        ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                        ->select('assignment.start_job_at', 'ar.*', 'ar.start_date as assignment_startdate', 'ar.end_date as assignment_enddate', 'send_proposal.id as proposal_id', 'send_proposal.*')
                        ->where('send_proposal.approval_status', 1)
                ->where('send_proposal.user_id', @Auth::user()->id)
                        ->orderBy('assignment.id', 'DESC')->paginate(6);
        $jobslist->appends($arraysearchappendurl)->links();
        $data['jobslist'] = $jobslist;
        return view('pages.job.assignedjoblisttraining', $data);
    }
    
    public function cancelledjoblist() {
        UserController::validuser();
        $data['jobslist'] = array();
        $prefix = DB::getTablePrefix();
        $val = 1;
        //get job of auditor
        $arraysearchappendurl = array();
        $jobslist = DB::table('assignment')
                        ->join('assignment_request_audit as ar', 'ar.assignment_id', '=', 'assignment.id')
                        ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                        ->select('assignment.start_job_at', 'ar.*', 'ar.start_date as assignment_startdate', 'ar.end_date as assignment_enddate', 'send_proposal.id as proposal_id', 'send_proposal.*')
                        ->where('send_proposal.approval_status', 3)->where('send_proposal.canceljob_status', 1)
                ->where('send_proposal.user_id', @Auth::user()->id)
                        ->orderBy('assignment.id', 'DESC')->paginate(6);
        $jobslist->appends($arraysearchappendurl)->links();
        $data['jobslist'] = $jobslist;
        $data['selectmenu'] = "cancelledjobslist";
        return view('pages.job.cancelledjoblist', $data);
    }
    
    public function cancelledjoblisttraining() {
        UserController::validuser();
        $data['jobslist'] = array();
        $prefix = DB::getTablePrefix();
        $val = 1;
        //get job of auditor
        $arraysearchappendurl = array();
        $jobslist = DB::table('assignment')
                        ->join('assignment_request_training as at', 'at.assignment_id', '=', 'assignment.id')
                        ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                        ->select('assignment.start_job_at', 'at.*', 'at.start_date as assignment_startdate', 'at.end_date as assignment_enddate', 'send_proposal.id as proposal_id', 'send_proposal.*')
                        ->where('send_proposal.approval_status', 3)->where('send_proposal.canceljob_status', 1)
                ->where('send_proposal.user_id', @Auth::user()->id)
                        ->orderBy('assignment.id', 'DESC')->paginate(6);
        $jobslist->appends($arraysearchappendurl)->links();
        $data['jobslist'] = $jobslist;
        return view('pages.job.cancelledjoblisttraining', $data);
    }

    public function myjobDetails($assignment_id) {
        UserController::validuser();
        $data['assignmentdetail'] = array();
        $data['auditorlists'] = array();
        $data['jobtracklists'] = array();
        //get assignment detail
        $val = 1;
        // DB::enableQueryLog(); 
        if (@Auth::user()->usertype == 6) {
        $assignmentdetail = DB::table('assignment_request_audit')
                ->join('assignment', 'assignment_request_audit.assignment_id', '=', 'assignment.id')
                ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                ->select('assignment_request_audit.*', 'assignment.assignment_type', 'send_proposal.approval_status')
                ->where('assignment_request_audit.assignment_id', $assignment_id)
                ->where('assignment.user_id', @Auth::user()->id)
                ->where(function($q) use ($val) {
                    $q->where('assignment.approval_status', $val)
                    ->orWhere('send_proposal.approval_status', $val);
                })
                ->first();
        } else if (@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) {
            $assignmentdetail = DB::table('assignment_request_training')
                ->join('assignment', 'assignment_request_training.assignment_id', '=', 'assignment.id')
                ->join('send_proposal', 'send_proposal.assignment_id', '=', 'assignment.id')
                ->select('assignment_request_training.*', 'assignment.assignment_type', 'send_proposal.approval_status')
                ->where('assignment_request_training.assignment_id', $assignment_id)
                ->where('assignment.user_id', @Auth::user()->id)
                ->where(function($q) use ($val) {
                    $q->where('assignment.approval_status', $val)
                    ->orWhere('send_proposal.approval_status', $val);
                })
                ->first();
        }
//        $query = DB::getQueryLog();
//        print "<pre>";
//        print_r($query);
//        exit;
        $data['assignmentdetail'] = $assignmentdetail;
        //get assignment detail
        //get assignment location detail
        $locationname = DB::table('assignment_location')
                        ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                        ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                        ->join('states', 'states.id', '=', 'assignment_location.state_id')
                        ->where('assignment_location.assignment_id', $assignment_id)->get();
//                        ->where('assignment_location.whomtoassign_id', "!=", "")
        $data['locationname'] = $locationname;
        //get assignment location detail
        //get auditor list
        $auditorlists = DB::table('send_proposal')
                        ->join('users', 'users.id', '=', 'send_proposal.user_id')
                        ->select('users.fname', 'users.lname', 'users.email', 'users.fcm_device_token', 'users.id as userId', 'send_proposal.*')
                        ->where('send_proposal.assignment_id', $assignment_id)
                        ->where('send_proposal.assignment_user_id', @Auth::user()->id)
                ->where(function($q) use ($val) {
                    $q->where('send_proposal.approval_status', $val)
                    ->orWhere('send_proposal.canceljob_status', $val);
                })
//                        ->where('send_proposal.approval_status', 1)
                ->get();
        $data['auditorlists'] = $auditorlists;
        //get auditor list
        //get job track lists
        $jobtracklists = DB::table('assignment_tracking')
                        ->join('users', 'assignment_tracking.sender_id', '=', 'users.id')
                        ->select('users.fname', 'users.lname', 'assignment_tracking.*')
                        ->where('assignment_tracking.assignment_id', $assignment_id)->get();
        $data['jobtracklists'] = $jobtracklists;
        //get job track lists
        //get query list
        $data['querylists'] = DB::table('query_master')->where('status', 1)->get();
        //DB::enableQueryLog(); 
        $rate = Rating::with('RatingAnswer')
                ->where('recive_id', @Auth::user()->id)
                ->where('reference_id', @$assignmentdetail->ref_id)
                ->get();
        $data['rate'] = $rate;
        $data['queryname'] = DB::table('query_master')->pluck("query", "id");
        $data['fname'] = DB::table('users')->pluck("fname", "id");
        $data['lname'] = DB::table('users')->pluck("lname", "id");

        foreach (@$auditorlists as $k => $v) {
            $data['ownsendrating'][$v->user_id] = Rating::with('RatingAnswer')
                    ->where('recive_id', @$v->user_id)
                    ->where('sender_id', @Auth::user()->id)
                    ->where('reference_id', @$assignmentdetail->ref_id)
                    ->get();
        }
        $data['rating_enable'] = Session::get('rating_enable', 0);
//        $sessionValue = Session::get('rating_enable');
//        if ($sessionValue) {
//            $data['rating_enable'] = $sessionValue;
//        } else {
//            $data['rating_enable'] = Session::get('rating_enable', 0);
//        }
        
//        print "<pre>";
//        print_r($data['ownsendrating']);exit;
        return view('pages.job.myjobdetails', $data);
    }
    
    public function assignedjobDetails($assignment_id) {
        UserController::validuser();
        $data['jobslist'] = array();
        $data['jobtracklist'] = array();
        //get assigned job details
        $assignmentType = Assignment::select('assignment_type')->where('id', $assignment_id)->first();
        if($assignmentType->assignment_type == 1) { //get assigned job details audit
        $jobslist = DB::table('send_proposal')
                        ->join('users', 'send_proposal.assignment_user_id', '=', 'users.id')
                        ->join('assignment_request_audit', 'assignment_request_audit.assignment_id', '=', 'send_proposal.assignment_id')
                        ->select('send_proposal.*', 'send_proposal.id as proposal_id', 'users.id as userId', 'users.fcm_device_token', 'users.fname', 'users.lname', 'users.gender', 'users.profile_img', 'assignment_request_audit.*')
                        ->where('send_proposal.approval_status', 1)->where('send_proposal.assignment_id', $assignment_id)->where('send_proposal.user_id', @Auth::user()->id)->first();
        } else if ($assignmentType->assignment_type == 2) { //get assigned job details training
            $jobslist = DB::table('send_proposal')
                        ->join('users', 'send_proposal.assignment_user_id', '=', 'users.id')
                        ->join('assignment_request_training', 'assignment_request_training.assignment_id', '=', 'send_proposal.assignment_id')
                        ->select('send_proposal.*', 'send_proposal.id as proposal_id', 'users.id as userId', 'users.fcm_device_token', 'users.fname', 'users.lname', 'users.gender', 'users.profile_img', 'assignment_request_training.*')
                        ->where('send_proposal.assignment_id', $assignment_id)->where('send_proposal.user_id', @Auth::user()->id)->first();
        }
        $data['assignment_type'] = $assignmentType->assignment_type;
        //get city name
        @$locationIds = explode(",", @$jobslist->assignment_location_id);
        $locationname = DB::table('assignment_location')
                        ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                        ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                        ->join('states', 'states.id', '=', 'assignment_location.state_id')
                        ->whereIn('assignment_location.id', @$locationIds)->get();
        $data['locationname'] = $locationname;
        //get city name
        //get CB address
        $addressCb = DB::table('users_contact_address')
                        ->select('countries.name as countryName', 'states.name as stateName', 'users_contact_address.city_name')
                        ->join('countries', 'countries.id', '=', 'users_contact_address.country_id')
                        ->join('states', 'states.id', '=', 'users_contact_address.state_id')
                        ->where('users_contact_address.user_id', @$jobslist->userId)->orderBy('users_contact_address.id', 'DESC')->first();
        $data['addressCb'] = $addressCb;
        //get CB address
        //get job track list
        $jobtracklist = AssignmentTracking::where('sender_id', @Auth::user()->id)->where('proposal_id', @$jobslist->proposal_id)
                        ->orderBy('id', 'DESC')->get();
        //get job track list
        $data['querylists'] = DB::table('query_master')->where('status', 1)->get();
        $rate = Rating::with('RatingAnswer')
                ->where('sender_id', @$jobslist->userId)
                ->where('recive_id', @Auth::user()->id)
                ->where('reference_id', @$jobslist->ref_id)
                ->first();
        $data['rate'] = $rate;

        $data['queryname'] = DB::table('query_master')->pluck("query", "id");

        $data['ownsendrating'] = Rating::with('RatingAnswer')
                ->where('recive_id', @$jobslist->userId)
                ->where('sender_id', @Auth::user()->id)
                ->where('reference_id', @$jobslist->ref_id)
                ->first();


        $data['jobslist'] = $jobslist;
        $data['jobtracklist'] = $jobtracklist;
        return view('pages.job.assignedjobDetails', $data);
    }
    
    public function cancelledjobDetails($assignment_id) {
        UserController::validuser();
        $data['jobslist'] = array();
        $data['jobtracklist'] = array();
        //get assigned job details
        $assignmentType = Assignment::select('assignment_type')->where('id', $assignment_id)->first();
        if($assignmentType->assignment_type == 1) { //get assigned job details audit
        $jobslist = DB::table('send_proposal')
                        ->join('users', 'send_proposal.assignment_user_id', '=', 'users.id')
                        ->join('assignment_request_audit', 'assignment_request_audit.assignment_id', '=', 'send_proposal.assignment_id')
                        ->select('send_proposal.*', 'send_proposal.id as proposal_id', 'users.id as userId', 'users.fname', 'users.lname', 'users.gender', 'users.profile_img', 'assignment_request_audit.*')
                        ->where('send_proposal.assignment_id', $assignment_id)->where('send_proposal.assignment_id', $assignment_id)
                ->where('send_proposal.canceljob_status', 1)->first();
        } else if ($assignmentType->assignment_type == 2) { //get assigned job details training
            $jobslist = DB::table('send_proposal')
                        ->join('users', 'send_proposal.assignment_user_id', '=', 'users.id')
                        ->join('assignment_request_training', 'assignment_request_training.assignment_id', '=', 'send_proposal.assignment_id')
                        ->select('send_proposal.*', 'send_proposal.id as proposal_id', 'users.id as userId', 'users.fname', 'users.lname', 'users.gender', 'users.profile_img', 'assignment_request_training.*')
                        ->where('send_proposal.assignment_id', $assignment_id)->where('send_proposal.assignment_id', $assignment_id)
                ->where('send_proposal.canceljob_status', 1)->first();
        }
        //get assigned job details
        //get city name
        @$locationIds = explode(",", @$jobslist->assignment_location_id);
        $locationname = DB::table('assignment_location')
                        ->select('countries.name as countryName', 'states.name as stateName', 'assignment_location.*')
                        ->join('countries', 'countries.id', '=', 'assignment_location.country_id')
                        ->join('states', 'states.id', '=', 'assignment_location.state_id')
                        ->whereIn('assignment_location.id', @$locationIds)->get();
        $data['locationname'] = $locationname;
        //get city name
        //get CB address
        $addressCb = DB::table('users_contact_address')
                        ->select('countries.name as countryName', 'states.name as stateName', 'users_contact_address.city_name')
                        ->join('countries', 'countries.id', '=', 'users_contact_address.country_id')
                        ->join('states', 'states.id', '=', 'users_contact_address.state_id')
                        ->where('users_contact_address.user_id', @$jobslist->userId)->orderBy('users_contact_address.id', 'DESC')->first();
        $data['addressCb'] = $addressCb;
        //get CB address
        //get job track list
        $jobtracklist = AssignmentTracking::where('sender_id', @Auth::user()->id)->where('proposal_id', @$jobslist->proposal_id)
                        ->orderBy('id', 'DESC')->get();
        //get job track list
        
        $data['jobslist'] = $jobslist;
        $data['jobtracklist'] = $jobtracklist;
        return view('pages.job.cancelledjobDetails', $data);
    }

    public function downloadDocumentJob($assignment_id, $document) {
        $this->validuser();
        if ($assignment_id) {
            $record_exist = AssignmentTracking::where('assignment_id', $assignment_id)->where('document', $document)->first();
            if ($record_exist) {
                $path = public_path() . '/job_document/' . $assignment_id . "/" . $document;
                return response()->download($path);
            } else {
                print "Error";
            }
        } else {
            print "Error";
        }
        exit();
    }

    public function addJobTrack(Request $request) {
        UserController::validuser();
        $reqdata = $request->all();
        $id_update = @$reqdata['user_id'];
        $tracking_ins = $reqdata['tracking'];
        $tracking_ins['add_date'] = Carbon::now()->format('Y-m-d H:i:s');
        $tracking_ins['protocol'] = $_SERVER['REMOTE_ADDR'];
        $ins_id = AssignmentTracking::create($tracking_ins);
        $id_insert = $ins_id->id;
        //create directory for each assignment one time
        $pathdocument = public_path() . '/job_document/';
//        File::makeDirectory($pathdocument, $mode = 0777, true, true);
        if ($dh = opendir($pathdocument)) {
            if (readdir($dh) !== true) {
                $path = public_path() . '/job_document/' . $tracking_ins['assignment_id'];
                File::makeDirectory($path, $mode = 0777, true, true);
            }
        }

//        $path = public_path() . '/job_document/' . $tracking_ins['assignment_id'];
//        if (empty($path)) {
//            File::makeDirectory($path, $mode = 0777, true, true);
//        }
        //create directory for each assignment one time
        $document = $request->file('trackDocuemnt');
        if ($document) {
            $getNumeric1 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
            $getNumeric2 = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
            $namefile = $document->getClientOriginalName();
            $extension = pathinfo($namefile, PATHINFO_EXTENSION);
            @$name = $getNumeric1 . $tracking_ins['sender_id'] . $getNumeric2 . "." . $extension;
            $cp = $document->move(public_path() . '/job_document/' . $tracking_ins['assignment_id'] . "/", $name);
            if ($cp) {
                $id = AssignmentTracking::where('id', $id_insert)->update(array("document" => $name));
            }
        }
        //notification send to bidder
        $notification['assignment_id'] = $tracking_ins['assignment_id']; //Assignment ID
        if (@$reqdata['assignment_type'] == 1) {
            $action = "audit_job_track";
        } else {
            $action = "traning_job_track";
        }
        $notification['action'] = $action; //Proposal ID
        $notification['new_status'] = $tracking_ins['message'];
        $notification['updated_by'] = @Auth::user()->fname . " " . @Auth::user()->lname;
        $body = json_encode($notification);
        $msg = array(
            'header' => 'Job ' . @$reqdata['ref_id'] . 'Status Update',
            'text' => "Status has been changed or updated for job " . @$reqdata['ref_id']
        );
        $message = json_encode($msg);
        $fcmdevice_token = $reqdata['device_token'];
        $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);
//        exit();
        if (@$id_insert) {
            Session::flash('success_message', "Track report saved Successfully!");
            return redirect("/assignedjobDetails/" . $tracking_ins['assignment_id']);
        } else {
            Session::flash('serror_message', "Failure in save!");
            return redirect("/assignedjobDetails/" . $tracking_ins['assignment_id']);
        }
    }

    public function endJobReview(Request $request) {
        UserController::validuser();
        $reqdata = $request->all();
        $id_proposal = $reqdata['proposal_id'];
        $id_auditor = $reqdata['auditor_id'];
        $assignment_id = $reqdata['assignment_id'];
        $comment = $reqdata['job_comment'];
        $countendjob = 0;
        //update end job status for bid
        $id_update = SendProposal::where('id', $id_proposal)->where('user_id', $id_auditor)
                ->update(array(
            "endjob_comment" => $comment,
            "endjob_status" => 1
        ));
        //update end job status for bid
        $resbidinfo = SendProposal::select('*')->where("approval_status", 1)->where("assignment_id", $assignment_id)->get();

        foreach ($resbidinfo as $k => $v) {
            if ($v->endjob_status == 1) {
                $countendjob++; //get how many bids/jobs ends
            }
        }
        if ($countendjob == count($resbidinfo)) { //set assignment as closed if all bids/jobs ends
            Assignment::where("id", $assignment_id)->update(array("close_job" => 1));
        }
        //send mail to CB
//        $mail['fname'] = @Auth::user()->fname;
//        $mail['lname'] = @Auth::user()->lname;
//        $mail['email'] = @Auth::user()->email;
//        $mail['ref_id'] = $reqdata['assignment_refid'];
//        $mail['subject'] = "ISOMart : Job Ended!";
//        $mail['jobtype'] = 1;
//        $this->jobEndEmailSeeker($mail);
        //send mail to CB
        //send mail to auditor
        $mail['assignment_id'] = $assignment_id;
        $mail['fname'] = $reqdata['auditor_fname'];
        $mail['lname'] = $reqdata['auditor_lname'];
        $mail['email'] = $reqdata['auditor_email'];
        $mail['ref_id'] = $reqdata['assignment_refid'];
        $mail['subject'] = "Job " .$reqdata['assignment_refid']. " Ended";
        $mail['jobtype'] = 2;
        $this->jobEndEmailProvider($mail);
        
         //notification send to bidder
        $notification['assignment_id'] = $assignment_id; //Message ID
        $notification['ref_id'] = $reqdata['assignment_refid'];
        $notification['action'] = $reqdata['assignment_type'] = 1 ? "audit_end_job" : "training_end_job"; //Proposal ID
        $body = json_encode($notification);
        $msg = array(
            'header' => 'Job ' .$reqdata['assignment_refid']. ' Ended',
            'text' => "Job " . $reqdata['assignment_refid'] . " has ended."
        );
        $message = json_encode($msg);
        $fcmdevice_token = $reqdata['device_token'];
        $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);
        
        if (@$id_update) {
            Session::put('rating_enable', $id_proposal);
            Session::flash('success_message', "Job ended Successfully!");
            return redirect("/myjobDetails/" . $assignment_id);
        } else {
            Session::flash('serror_message', "Failure in ending job!");
            return redirect("/myjobDetails/" . $assignment_id);
        }
    }

    public function jobEndEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $jobtype = $data['jobtype'];
        $messageBody = view('email.jobEndEmail', $data);
        $a = Mail::send('email.jobEndEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function jobEndEmailProvider($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $jobtype = $data['jobtype'];
        $assignment_id = $data['assignment_id'];
        $messageBody = view('email.jobEndEmail', $data);
        $a = Mail::send('email.jobEndEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function cancelJob(Request $request) {
        $reqdata = $request->all();
        $bid_id = $reqdata['bidId'];
        $ref_id = $reqdata['refId'];
        $assignmentType = $reqdata['assignmentType'];
        $getbiddetails = SendProposal::where("id", $bid_id)->first();
        $userdetails = DB::table('users')->select('fname', 'lname', 'email', 'fcm_device_token')->where("id", $getbiddetails->user_id)->first();
        $arraylocation = explode(",", $getbiddetails->assignment_location_id);
        if ($getbiddetails) {
            //cancel status in send proposal
            SendProposal::where("id", $bid_id)
                    ->update(array(
                        'canceljob_status' => 1,
                        'approval_status' => 3
            ));
            
            if (!empty($arraylocation)) {
            //available location for bid
            foreach ($arraylocation as $k => $v) {
                AssignmentLocation::where('id', $v)->update(array(
                    'whomtoassign_id' => null
                ));
            }
            }

            //update job status for assignment
            Assignment::where('id', $getbiddetails->assignment_id)->update(array(
                'approval_status' => 0
            ));

            //mail for cancelled job 
            //send mail to CB
//            $mail['fname'] = @Auth::user()->fname;
//            $mail['lname'] = @Auth::user()->lname;
//            $mail['email'] = @Auth::user()->email;
//            $mail['ref_id'] = $ref_id;
//            $mail['subject'] = "ISOMart : Job Cancelled!";
//            $mail['jobtype'] = 1;
//            $this->jobCancelEmailSeeker($mail);

            //send mail to auditor
            $mail['assignment_id'] = $getbiddetails->assignment_id;
            $mail['fname'] = $userdetails->fname;
            $mail['lname'] = $userdetails->lname;
            $mail['email'] = $userdetails->email;
            $mail['ref_id'] = $ref_id;
            $mail['subject'] = "Job " . $ref_id . " Cancelled";
            $mail['jobtype'] = 2;
            $this->jobCancelEmailProvider($mail);
            //notification send to bidder
            $notification['assignment_id'] = $getbiddetails->assignment_id; //Message ID
            $notification['action'] = $assignmentType = 1 ? "audit_cancel_job" : "training_cancel_job"; //Proposal ID
            $body = json_encode($notification);
            $msg = array(
                'header' => "Job " . $ref_id . " Cancelled",
                'text' => "Job " . $ref_id ." has been cancelled."
            );
            $message = json_encode($msg);
            $fcmdevice_token = $userdetails->fcm_device_token;
            $this->NotificationController->sendNotification($message, $fcmdevice_token, $body);

            Session::put('rating_enable', $bid_id);
            print "success";
        } else {
            print "failure";
        }
        exit;
    }

    public function jobCancelEmailSeeker($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $jobtype = $data['jobtype'];
        $messageBody = view('email.jobCancelEmail', $data);
        $a = Mail::send('email.jobCancelEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

    public function jobCancelEmailProvider($data) {
        $toemail = $data['email'];
        $toname = $data['fname'] . " " . $data['lname'];
        $ref_id = $data['ref_id'];
        $subject = $data['subject'];
        $jobtype = $data['jobtype'];
        $messageBody = view('email.jobCancelEmail', $data);
        $a = Mail::send('email.jobCancelEmail', $data, function($message) use ($toemail, $toname, $subject) {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->to($toemail, $toname)->subject($subject);
                });
    }

}
