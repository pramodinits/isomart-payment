<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class TaskCredit extends Model
{
    protected $fillable = [
        'id',
        'task_name',
        'task_point',
        'add_date',
        'status',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'task_credit';
}
