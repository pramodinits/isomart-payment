<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class EmailNotification extends Model
{
    protected $fillable = [
        'id',
        'email_type',
        'email_to',
        'email_name',
        'email_subject',
        'email_cc',
        'email_attachment',
        'email_message',
        'add_date',
        'delivery_date',
        'email_flag',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'email_notification';
}
