<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class AssignmentLocation extends Model {

    protected $fillable = [
        'assignment_id',
        'country_id',
        'state_id',
        'add_date',
        'protocol',
        'status',
        'city_id',
        'city_name',
        'effort'
    ];
    public $timestamps = false;
    protected $table = 'assignment_location';
    

}
