<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class SendProposal extends Model {

    protected $fillable = [
        'assignment_id',
        'user_id',
        'assignment_user_id',
        'assignment_location_id',
        'estimated_effort',
        'estimated_budget',
        'query_ans',
        'description',
        'start_date',
        'end_date',
        'approval_status',
        'endjob_comment',
        'endjob_status',
        'canceljob_status',
        'add_date',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'send_proposal';

}
