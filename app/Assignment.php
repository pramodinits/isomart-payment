<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model {

    protected $fillable = [
        'user_id',
        'assignment_type',
        'parent_assignment_id',
        'add_date',
        'protocol',
        'status',
        'approval_status',
        'start_job_at',
        'awarded_to'
    ];
    public $timestamps = false;
    protected $table = 'assignment';

    public function AssignmentAudit() {
        return $this->hasOne('App\AssignmentAudit');
    }
    
    public function AssignmentTraining() {
        return $this->hasOne('App\AssignmentTraining');
    }
    
    public function AssignmentCertification() {
        return $this->hasMany('App\AssignmentCertification');
    }
    public function AssignmentLocation() {
        return $this->hasMany('App\AssignmentLocation');
    }

//    public function assignment() {
//        return $this->hasMany('App\AssignmentAudit');
//    }

}
