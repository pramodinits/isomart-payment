<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class UserDocument extends Model {

    protected $fillable = [
        'user_id',
        'code_id',
        'name',
        'flag',
        'status',
        'add_date',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'users_document';

//    public function IsicCode() {
//        return $this->hasOne('App\IsicCode');
//    }
    
}
