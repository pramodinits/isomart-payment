<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class AssignmentTracking extends Model {

    protected $fillable = [
        'assignment_id',
        'assignment_location_id',
        'proposal_id',
        'sender_id',
        'message',
        'document',
        'status',
        'add_date',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'assignment_tracking';

}
