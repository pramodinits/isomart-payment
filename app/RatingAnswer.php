<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class RatingAnswer extends Model
{
    protected $fillable = [
        'id',
        'rating_id',
        'query_id',
        'answer'
    ];
    public $timestamps = false;
    protected $table = 'rating_answer';
    
}
