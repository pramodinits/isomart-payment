<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'message_id',
        'reference_id',
        'proposal_id',
        'sender_id',
        'recive_id',
        'message',
        'add_date',
        'protocol',
        'flag'
    ];
    public $timestamps = false;
    protected $table = 'message';
}
