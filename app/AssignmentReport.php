<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class AssignmentReport extends Model
{
    protected $fillable = [
        'user_id',
        'description',
        'assignment_type',
        'status',
        'add_date',
        'protocol'
    ];
    public $timestamps = false;
    protected $table = 'assignment_report';
}
