<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class AssignmentAudit extends Model
{
    protected $fillable = [
        'ref_id',
        'assignment_id',
        'assignment_name',
        'sic_code',
        'auditor_name',
        'location',
        'effort_tentative',
        'budget_usd',
        'start_date',
        'end_date',
        'proposal_end_date',
        'local_travel_allowance',
        'local_stay_allowance',
        'air_fare',
        'assignment_status',
        'description',
        'query_first',
        'query_second',
        'query_second',
        'admin_approval',
        'add_date',
        'protocol',
//        'approval_status',
//        'awarded_to'
    ];
    public $timestamps = false;
    protected $table = 'assignment_request_audit';
    
    public function Assignment()
    {
        return $this->belongsTo('App\Assignment');
    }
    
    public function AssignmentLocation() {
        return $this->hasMany('App\AssignmentLocation');
    }
    
//    public function assignmentaudit() {
//        return $this->hasOne('App\Assignment');
//    }
}
