<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();
Route::get('/admin', ['middleware' => 'guest', function() {
        return view('vendor.adminlte.login');
    }]);
Route::get('/', 'UserController@index');

Route::get('/signin/{phone?}/{assignment_id?}', 'UserController@signin');
Route::post('/signin', 'UserController@login');

Route::get('/signup/{phone?}', 'UserController@signup');
Route::post('/signup', 'UserController@registration');
Route::get('/verifyotp', 'UserController@verifyotp');
Route::post('/verifyotp', 'UserController@submitverifyotp');

Route::get('/forgotpassword', 'UserController@forgotpassword');
Route::post('/checkphone', 'UserController@checkphone');
Route::post('/setPasswordOtp', 'UserController@setPasswordOtp');
Route::post('/sendOTPtoUser', 'UserController@sendOTPtoUser');

Route::get('/profile', 'UserController@profile');
Route::post('/getStateList', 'UserController@getStateList');
Route::post('/getCityList', 'UserController@getCityList');
Route::post('/getLocationInfo', 'UserController@getLocationInfo');
Route::post('/getStandards', 'UserController@getStandards');
Route::post('/updateprofile', 'UserController@updateprofile');
Route::post('/updateProfileGeneral', 'UserController@updateProfileGeneral');
Route::post('/updateProfileContact', 'UserController@updateProfileContact');
Route::post('/addIsicCode', 'UserController@addIsicCode');
Route::post('/getIsicCode', 'UserController@getIsicCode');
Route::post('/deletedocIsic', 'UserController@deletedocIsic')->middleware('auth');
Route::post('/addStandard', 'UserController@addStandard');
Route::get('/downloaddocument/{id?}/{uid?}', 'UserController@downloaddocument')->middleware('auth');
Route::post('/deletedocument', 'UserController@deletedocument')->middleware('auth');
Route::post('/addAddress', 'UserController@addAddress');
Route::get('/setCurrentAddress/{id?}', 'UserController@setCurrentAddress');
Route::post('/addIndustryCode', 'UserController@addIndustryCode');
Route::post('/getAddress', 'UserController@getAddress');
Route::post('/checkAvailableDate', 'UserController@checkAvailableDate')->middleware('auth');
Route::post('/deleteAddress', 'UserController@deleteAddress')->middleware('auth');
Route::post('/deleteSicCode', 'UserController@deleteSicCode')->middleware('auth');
Route::post('/deleteStandard', 'UserController@deleteStandard')->middleware('auth');
Route::post('/deleteIsicCode', 'UserController@deleteIsicCode')->middleware('auth');
Route::post('/logout', 'UserController@logout');
Route::get('/logout', 'UserController@logout');
Route::get('/dashboard', 'UserController@dashboard');
Route::get('/publicProfileView/{user_id}', 'UserController@publicProfileView');
Route::get('/cpdHours', 'UserController@cpdHours');
Route::post('/addcpdHours', 'UserController@addcpdHours')->middleware('auth');
Route::post('/getCPD', 'UserController@getCPD')->middleware('auth');
Route::post('/deleteCPD', 'UserController@deleteCPD')->middleware('auth');

Route::get('/searchContent', 'AssignmentController@searchContent');
Route::get('/assignmentlist', 'AssignmentController@assignmentlist');
Route::get('/requestassignment/{assignment_id?}', 'AssignmentController@requestassignment');
Route::post('/requestassignment', 'AssignmentController@assignmentrequest')->middleware('auth');
Route::post('/requestassignmenttraining', 'AssignmentController@requestassignmenttraining')->middleware('auth');
Route::get('/allassignment', 'AssignmentController@allassignment');
Route::get('/assignmentAuditDetail/{assignment_id}', 'AssignmentController@assignmentAuditDetail');
Route::get('/assignmentTrainingDetail/{assignment_id}', 'AssignmentController@assignmentTrainingDetail');
Route::get('/assignmentCertificationDetail/{assignment_id}', 'AssignmentController@assignmentCertificationDetail');
Route::post('/checkRespondBid', 'AssignmentController@checkRespondBid')->middleware('auth');
Route::post('/sendproposal', 'AssignmentController@sendproposal')->middleware('auth');
Route::get('/allproposal', 'AssignmentController@allproposal');
Route::post('/rejectProposal', 'AssignmentController@rejectProposal')->middleware('auth');
Route::post('/acceptProposal', 'AssignmentController@acceptProposal')->middleware('auth');
Route::post('/deleteProposal', 'AssignmentController@deleteProposal')->middleware('auth');
Route::post('/deleteAssignment', 'AssignmentController@deleteAssignment')->middleware('auth');
Route::post('/deleteLocation', 'AssignmentController@deleteLocation')->middleware('auth');
Route::get('/alljobslist', 'AssignmentController@alljobslist');
Route::post('/requestassignmentcertification', 'AssignmentController@requestassignmentcertification')->middleware('auth');
Route::post('/startJob', 'AssignmentController@startJob')->middleware('auth');
Route::post('/shareAssignment', 'AssignmentController@shareAssignment')->middleware('auth');

Route::get('/testUpdate', 'AssignmentController@testUpdate');
Route::get('/testemail', 'AssignmentController@testemail');
Route::get('/myCalender', 'CalenderController@myCalender')->middleware('auth');

/*Job routes*/
Route::get('/myjobslist', 'JobController@myjobslist');
Route::get('/cancelledjoblist', 'JobController@cancelledjoblist');
Route::get('/cancelledjoblisttraining', 'JobController@cancelledjoblisttraining');
Route::get('/myjobDetails/{assignment_id?}', 'JobController@myjobDetails');
Route::get('/cancelledjobDetails/{assignment_id?}', 'JobController@cancelledjobDetails');
Route::get('/assignedjobDetails/{assignment_id?}', 'JobController@assignedjobDetails');

Route::get('/assignedjoblist', 'JobController@assignedjoblist');
Route::get('/assignedjoblisttraining', 'JobController@assignedjoblisttraining');
Route::post('/addJobTrack', 'JobController@addJobTrack')->middleware('auth');
Route::get('/downloadDocumentJob/{assignment_id?}/{document?}', 'JobController@downloadDocumentJob')->middleware('auth');
Route::post('/endJobReview', 'JobController@endJobReview')->middleware('auth');
Route::post('/cancelJob', 'JobController@cancelJob')->middleware('auth');
/*Job routes*/

/*rating & message, training advertiseent post*/
Route::get('/messageDetails/{receive_id?}/{bidid?}/{refid?}', 'MessageController@messageDetails')->middleware('auth');
Route::get('/sendMessage/{reciveid?}/{bidid?}/{refid?}', 'MessageController@sendMessage')->middleware('auth');
Route::post('/sendMessage', 'MessageController@insertMessage')->middleware('auth');
Route::post('/insertRating', 'MessageController@insertRating')->middleware('auth');
Route::get('/addTrainingPost', 'MessageController@addTrainingPost')->middleware('auth');
Route::get('/addTrainingPost/{id?}', 'MessageController@addTrainingPost')->middleware('auth');
Route::post('/addTrainingPost', 'MessageController@insertTrainingPost')->middleware('auth');
Route::get('/trainingPost', 'MessageController@trainingPost')->middleware('auth');
Route::post('/deleteTrainingPost', 'MessageController@deleteTrainingPost')->middleware('auth');
Route::post('/deletePostLocation', 'MessageController@deletePostLocation')->middleware('auth');
Route::get('/trainingPostList', 'MessageController@trainingPostList');
Route::get('/trainingPostDetail/{id?}', 'MessageController@trainingPostDetail');
/*rating & message*/

/*query*/
Route::get('/addQuery', 'MessageController@addQuery')->middleware('auth');
Route::post('/addQuery', 'MessageController@insertQuery')->middleware('auth');
Route::get('/myQuery', 'MessageController@myQuery')->middleware('auth');
Route::get('/addQuery/{id?}', 'MessageController@addQuery')->middleware('auth');
Route::post('/deleteQueryPost', 'MessageController@deleteQueryPost')->middleware('auth');
Route::get('/queryPostList', 'MessageController@queryPostList');
Route::get('/queryPostDetail/{id?}', 'MessageController@queryPostDetail');
/*query*/

/*static pages*/
Route::get('/aboutus', 'UserController@aboutus');
Route::get('/contactus', 'UserController@contactus');
Route::get('/support', 'UserController@support');
Route::get('/faq', 'UserController@faq');
Route::get('/termcondition', 'UserController@termcondition');
/*static pages*/

/*subscription & Payment*/
Route::get('/subscriptionPackage', 'SubscriptionController@subscriptionPackage');
Route::get('/subscriptionPurchase', 'SubscriptionController@subscriptionPurchase');
Route::get('/updateUserCreditPoint', 'SubscriptionController@updateUserCreditPoint');
Route::get('/updateUserTransaction', 'SubscriptionController@updateUserTransaction');
Route::get('/getUserCreditPoint', 'SubscriptionController@getUserCreditPoint');
Route::get('/getTaskPoint', 'SubscriptionController@getTaskPoint');
Route::post('/insertPackage', 'SubscriptionController@insertPackage')->middleware('auth');
Route::get('/mySubscription', 'SubscriptionController@mySubscription');
Route::get('/subscriptionPackages', 'SubscriptionController@subscriptionPackages');
/*subscription & Payment*/

/*cron email notification*/
Route::get('/cronEditAssignment', 'CronJobController@cronEditAssignment');
Route::get('/cronAddAssignment', 'CronJobController@cronAddAssignment');
Route::get('/cronDeleteAssignment', 'CronJobController@cronDeleteAssignment');
Route::get('/cronExpireCreditPoint', 'CronJobController@cronExpireCreditPoint');
Route::get('/cronExpirePackageDuration', 'CronJobController@cronExpirePackageDuration');
Route::get('/cronNewQuery', 'CronJobController@cronNewQuery');
/*cron email notification*/

/*paypal payment test*/
//payment form
Route::get('/testPayment', 'PaymentController@index');
// route for processing payment
Route::post('paypal', 'PaymentController@payWithpaypal');
Route::get('paypalAndroid/{user_id?}/{package_id?}/{package_price?}/{credit_points?}/{package_duration?}/{durationDigit?}', 
        'SubscriptionController@payWithpaypalGet');
// route for check status of the payment
Route::get('status', 'SubscriptionController@getPaymentStatus');
/*paypal payment test*/

Route::get('/usertype', function() {
    if (Auth::user()->is_admin) {
        return redirect('admin/home');
    } else {
        return redirect('/');
    }
})->middleware('auth');

Route::get('/home', function() {
    if (Auth::user()->is_admin) {
        return redirect('admin/home');
    } else {
        return redirect('/');
    }
})->middleware('auth');

Route::group(array('prefix' => 'admin'), function() {
    Route::get('/', ['middleware' => 'guest', function() {
            return view('vendor.adminlte.login');
        }]);
    Route::post('/logout', 'AdminController@logout');
    Route::get('/logout', 'AdminController@logout');
    Route::get('/home', 'AdminController@index')->middleware('auth');
    Route::get('/userListing', 'AdminController@userListing')->middleware('auth');
    Route::get('/userDetail/{user_id?}', 'AdminController@userDetail')->middleware('auth');
    Route::post('/updateUserStatus', 'AdminController@updateUserStatus')->middleware('auth');
    Route::get('/assignmentListing', 'AdminController@assignmentListing')->middleware('auth');
    Route::get('/trainingPostListing', 'AdminController@trainingPostListing')->middleware('auth');
    Route::get('/auditDetail/{assignment_id?}', 'AdminController@auditDetail')->middleware('auth');
    Route::get('/trainingDetail/{assignment_id?}', 'AdminController@trainingDetail')->middleware('auth');
    Route::get('/advertisementShape', 'AdminController@advertisementShape')->middleware('auth');
    Route::post('/insertShape', 'AdminController@insertShape')->middleware('auth');
    Route::post('/deleteShape', 'AdminController@deleteShape')->middleware('auth');
    Route::get('/advertisementShape/{id?}', 'AdminController@advertisementShape')->middleware('auth');
    
    Route::get('/advertisementLocation', 'AdminController@advertisementLocation')->middleware('auth');
    Route::post('/deleteLocation', 'AdminController@deleteLocation')->middleware('auth');
    Route::post('/insertLocation', 'AdminController@insertLocation')->middleware('auth');
    Route::get('/advertisementLocation/{id?}', 'AdminController@advertisementLocation')->middleware('auth');
    
    Route::get('/advertisementListing', 'AdminController@advertisementListing')->middleware('auth');
    Route::get('/addAdvertisement', 'AdminController@addAdvertisement')->middleware('auth');
    Route::post('/insertAdvertisement', 'AdminController@insertAdvertisement')->middleware('auth');
    Route::post('/deleteAdvertisement', 'AdminController@deleteAdvertisement')->middleware('auth');
    Route::get('/addAdvertisement/{id?}', 'AdminController@addAdvertisement')->middleware('auth');
    Route::post('/changeNote', 'AdminController@changeNote')->middleware('auth');
    Route::get('/queryListing', 'AdminController@queryListing')->middleware('auth');
    
    //subscription package
    Route::get('/taskcreditListing', 'AdminController@taskcreditListing')->middleware('auth');
    Route::get('/addTaskcredit', 'AdminController@addTaskcredit')->middleware('auth');
    Route::post('/insertCreditTask', 'AdminController@insertCreditTask')->middleware('auth');
    Route::get('/addPackage/{id?}', 'AdminController@addPackage')->middleware('auth');
    Route::post('/insertPackage', 'AdminController@insertPackage')->middleware('auth');
    Route::post('/deletePackage', 'AdminController@deletePackage')->middleware('auth');
});

/* Email Routes */
Route::get('/providerRequestEmail', 'NotificationController@providerRequestEmail');
Route::get('/trainingRequestEmail', 'NotificationController@trainingRequestEmail');
/* Email Routes */

/* API Routes */
Route::group(array('prefix' => 'api'), function() {
    Route::get('/getUsertype', 'ApiController@getUsertype');
    Route::post('/checkMobileNumber', 'ApiController@checkMobileNumber');
    Route::post('/login', 'ApiController@login');
    Route::post('/requestOTP', 'ApiController@requestOTP');
    Route::post('/verifyOTP', 'ApiController@verifyOTP');
    Route::post('/signup', 'ApiController@signup');
    Route::post('/setPassword', 'ApiController@setPassword');
    Route::post('/updateinfo', 'ApiController@updateinfo');
    Route::post('/updateProfileImage', 'ApiController@updateProfileImage');
    Route::post('/addAddress', 'ApiController@addAddress');
    Route::post('/getAddress', 'ApiController@getAddress');
    Route::post('/setCurrentAddress', 'ApiController@setCurrentAddress');
    Route::post('/deleteAddress', 'ApiController@deleteAddress');
    Route::post('/contactDetails', 'ApiController@contactDetails');
    Route::post('/addIsicCode', 'ApiController@addIsicCode');
    Route::post('/getIsicCode', 'ApiController@getIsicCode');
    Route::post('/deleteIsicCode', 'ApiController@deleteIsicCode');
    Route::post('/addStandards', 'ApiController@addStandards');
    Route::post('/getStandards', 'ApiController@getStandards');
    Route::post('/uploadDocument', 'ApiController@uploadDocument');
    Route::post('/generalDetails', 'ApiController@generalDetails');
    Route::get('/test', 'ApiController@test');
    Route::post('/testDocument', 'ApiController@testDocument');
    Route::post('/requestAssignmentTraining', 'ApiController@requestAssignmentTraining');
    Route::post('/requestAssignmentAudit', 'ApiController@requestAssignmentAudit');
    Route::post('/deleteLocation', 'ApiController@deleteLocation');
    Route::post('/myAssignmentList', 'ApiController@myAssignmentList');
    Route::post('/allAssignmentList', 'ApiController@allAssignmentList');
    Route::post('/assignmentDetails', 'ApiController@assignmentDetails');
    Route::post('/getLocationList', 'ApiController@getLocationList');
    Route::get('/getStandardsSection', 'ApiController@getStandardsSection');
    Route::get('/getIsoStandards', 'ApiController@getIsoStandards');
    Route::post('/assignmentCertification', 'ApiController@assignmentCertification');
    Route::post('/deleteAssignment', 'ApiController@deleteAssignment');
    Route::post('/checkRespondBid', 'ApiController@checkRespondBid');
    Route::post('/sendProposal', 'ApiController@sendProposal');
    Route::post('/deleteProposal', 'ApiController@deleteProposal');
    Route::post('/acceptProposal', 'ApiController@acceptProposal');
    Route::post('/rejectProposal', 'ApiController@rejectProposal');
    Route::post('/startJob', 'ApiController@startJob');
    Route::post('/alljobslistCertification', 'ApiController@alljobslistCertification');
    Route::post('/allProposalList', 'ApiController@allProposalList');
    Route::post('/myJobsList', 'ApiController@myJobsList');
    Route::post('/assignedJobsList', 'ApiController@assignedJobsList');
    Route::post('/assignedJobDetail', 'ApiController@assignedJobDetail');
    Route::post('/addJobTrack', 'ApiController@addJobTrack');
    Route::post('/myJobDetail', 'ApiController@myJobDetail');
    Route::post('/myCalender', 'ApiController@myCalender');
    Route::post('/publicProfileView', 'ApiController@publicProfileView');
    Route::post('/shareAssignment', 'ApiController@shareAssignment');
    Route::post('/endJobReview', 'ApiController@endJobReview');
    Route::post('/cancelJob', 'ApiController@cancelJob');
    Route::post('/cancelledjoblist', 'ApiController@cancelledjoblist');
    Route::post('/cancelledjobDetails', 'ApiController@cancelledjobDetails');
    Route::post('/addcpdHours', 'ApiController@addcpdHours');
    Route::post('/getCpdHours', 'ApiController@getCpdHours');
    Route::post('/deleteCpdHours', 'ApiController@deleteCpdHours');
	
	Route::get('/queryList', 'ApiController@queryList');
    Route::post('/messageDetails', 'ApiController@messageDetails');
    Route::post('/insertRating', 'ApiController@insertRating');
    Route::post('/sendMessage', 'ApiController@sendMessage');
    Route::post('/insertMessage', 'ApiController@insertMessage');
	
	Route::post('/createTrainingPost', 'ApiController@createTrainingPost');
    Route::post('/trainingPostList', 'ApiController@trainingPostList');
    Route::get('/addTrainingPost', 'ApiController@addTrainingPost');
    Route::post('/deleteTrainingPost', 'ApiController@deleteTrainingPost');
    Route::get('/latesttrainingAdvertisementList', 'ApiController@latesttrainingAdvertisementList');

    /*query*/
    Route::post('/createQuery', 'ApiController@createQuery');
    Route::post('/quertList', 'ApiController@quertList');
    Route::post('/deleteQueryPost', 'ApiController@deleteQueryPost');
    Route::get('/latestQueryList', 'ApiController@latestQueryList');
    Route::post('/queryPostDetail', 'ApiController@queryPostDetail');
    /*query*/
    
    /*notification*/
    Route::post('/testPrivate', 'ApiController@testPrivate');
    Route::get('/testNotification', 'ApiController@testNotification');
    /*notification*/
    
    /*subscription packages*/
    Route::get('/getSubscriptionPackages', 'ApiController@getSubscriptionPackages');
    Route::post('/purchaseSubscription', 'ApiController@purchaseSubscription');
    Route::post('/mySubscription', 'ApiController@mySubscription');
    /*subscription packages*/
     
     
});
//
/*API Routes*/




//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');
