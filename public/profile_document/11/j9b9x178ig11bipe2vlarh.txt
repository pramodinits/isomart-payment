<div class="xx_small">
                                            <label class="control-label formtext">
                                                {{@$trainingCertificate[0]->name}}
                                            </label>
                                            @if(@$trainingCertificate[0]->id)
                                            <label class="control-label formtext floatright">
                                                <a href="{{ asset('downloaddocument')}}/{{MD5(@$trainingCertificate[0]->id)}}" >
                                                    <i class="fa fa-download" data-toggle="tooltip" title="{{@$trainingCertificate[0]->name}} download" data-original-title="Download document"></i>
                                                </a>
                                            </label>
                                            @endif
                                        </div>


<div class="xx_small">
                                            @if(@$regCertificate)
                                            @foreach(@$regCertificate as $key=>$val)
                                            <label class="control-label formtext">
                                                {{@$val->name}}
                                            </label>
                                            @if(@$val->id)
                                            <label class="control-label formtext floatright">
                                                <a href="{{ asset('downloaddocument')}}/{{MD5(@$val->id)}}" >
                                                    <i class="fa fa-download" data-toggle="tooltip" title="{{@$val->name}} download" data-original-title="Download document"></i>
                                                </a>
                                            </label>
                                            <label class="control-label formtext floatright">
                                                <a href="javascript:void(0);" onclick="deletedoc('{{$val->id}}')" >
                                                    <i class="fa fa-remove text-danger" aria-hidden="true" title="Delete"></i>
                                                </a>
                                            </label>
                                            @endif
                                            @endforeach
                                            @endif
                                        </div>



<div class="xx_small">
                                            @if(@$auditLog)
                                            @foreach(@$auditLog as $key=>$val)
                                            <label class="control-label formtext">
                                                {{@$val->name}}
                                            </label>
                                            @if(@$val->id)
                                            <label class="control-label formtext floatright">
                                                <a href="{{ asset('downloaddocument')}}/{{MD5(@$val->id)}}" >
                                                    <i class="fa fa-download" title="Download document"></i>
                                                </a>
                                            </label>
                                            <label class="control-label formtext floatright">
                                                <a href="javascript:void(0);" onclick="deletedoc('{{$val->id}}')" >
                                                    <i class="fa fa-remove text-danger" aria-hidden="true" title="Delete"></i>
                                                </a>
                                            </label>
                                            @endif
                                            @endforeach
                                            @endif
                                        </div>



<div class="xx_small">
                                            <label class="control-label formtext">
                                                {{$val->address}}
                                            </label>
                                            <label class="control-label formtext floatright">
                                                <a href="javascript:void(0);" onclick="editAddress('{{$val->id}}','{{$val->address}}')">
                                                    <i class="fa fa-edit" title="Edit Address"></i>
                                                </a>
                                            </label>
                                            <label class="control-label formtext floatright">
                                                <a href="javascript:void(0);" onclick="deleteAddress('{{$val->id}}')">
                                                    <i class="fa fa-remove text-danger" aria-hidden="true" title="Delete Address"></i>
                                                </a>
                                            </label>
                                        </div>