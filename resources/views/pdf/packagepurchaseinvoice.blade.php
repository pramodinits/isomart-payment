<!DOCTYPE html>
<html lang="en">
    <title>Good Receive Note </title>
    <link href="{{ public_path('invoicecss/bootstrap.min.css') }}" rel="stylesheet" media="all" />
    <link href="{{ public_path('invoicecss/style.css') }}" rel="stylesheet" media="all" />

    <!------ Include the above in your HEAD tag ---------->
    <style>
        .invoice-title h2, .invoice-title h3 {
            display: inline-block;
        }

        .table > tbody > tr > .no-line {
            border-top: none;
        }

        .table > thead > tr > .no-line {
            border-bottom: none;
        }

        .table > tbody > tr > .thick-line {
            border-top: 2px solid;
        }
        body table{
            font-size: 12px !important;
        }
    </style>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="invoice-title" style="visibility:hidden;">
                        <img src="{{ public_path('invoicecss/logo.png') }}">
                        <div class="invoice-title-text">
                            <div class="invoice-title-text_border" style="padding: 3px 80px !important;" > GRN </div>
                        </div>
                    </div>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 63%">
                                <address style="font-size: 10px;">
                                   <b>ISOMart </b>
                                   <br/>
                                    Plot: 597-836, Dubai Investment Park -II,<br>
                                    Dubai, United Arab Emirates.<br>
                                    P.O Box: 242241<br>
                                    Contact No: + 971 4 885 3930 <br/>
                                    Fax. : + 971 4 885 4917 <br/>
                                    Email: info@isomart.com <br/>
                                </address>
                            </td>
                            <td style="width: 37%;vertical-align: top;">
                                <table style="width: 100%" >

                                    <tr>
                                        <td style="width: 50%" >Invoice No</td>
                                        <td style="width: 5%">:</td>
                                        <td style="width: 45%" >{{@$purchaseInfo->order_refid}}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 50%">Date</td>
                                        <td style="width: 5%" >:</td>
                                        <td style="width: 45%">
                                            {{Carbon\Carbon::parse(@$purchaseInfo->purchase_date)->format('d-M-Y')}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 50%;">TRN</td>
                                        <td style="width: 5%;">:</td>
                                        <td style="width: 45%;">100305858100003</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <hr style="margin: 0px;">

                    <table style="width: 100%;font-size: 11px;">
                        <tr>
                            <td style="width: 63%;vertical-align: top;">
                                <table>
                                    <tr>
                                        <td><strong>Name </strong></td>
                                        <td>: </td>
                                        <td>{{@$userInfo->fname . " " . @$userInfo->lname}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Email </strong></td>
                                        <td>: </td>
                                        <td>{{@$userInfo->email}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Contact No </strong></td>
                                        <td>: </td>
                                        <td>{{"+" . @$userInfo->countrycode ."-". @$userInfo->phone}}</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;" ><strong>Address </strong></td>
                                        <td>: </td>
                                        <td> {{@$userAddressInfo->addressline1}}, {{@$userAddressInfo->city_name}}, {{@$userAddressInfo->stateName}}, {{@$userAddressInfo->countryName}}  </td>
                                    </tr>
                                </table>    

                            </td>
                            <td style="width: 37%;vertical-align: top;">

                            </td>
                        </tr>
                    </table>

                    <br/>


                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong> Order Summary </strong></h3>
                        </div>
                        <div class="">
                            <div class="" style="width: 692px;">
                                <table class="table table-condensed" style="border-bottom: 1px solid #ccc;">
                                    <thead>
                                        <tr>
                                            <td >#</td>
                                            <td class="text-center" ><strong>Item</strong></td>
                                            <td class="text-center" ><strong>Price</strong></td>
                                            <td class="text-center"><strong>Quantity </strong></td>
                                            <td class="text-center" ><strong>Tax</strong></td>
                                            <td class="text-center" ><strong>Total</strong></td>

                                        </tr>
                                    </thead>
                                    <tbody style="height: 500px;">
                                        <tr>
                                            <td class="text-center" >1</td>
                                            <td>{{@$packageName->package_name . " Subscription"}}</td>
                                            <td class="text-center" >{{config('assignment.price') . " ". @$purchaseInfo->package_price}}</td>
                                            <td class="text-center" >1</td>
                                            <td class="text-center">0</td>
                                            <td class="text-center">{{config('assignment.price') . " ". @$purchaseInfo->package_price}}</td>
                                        </tr>

                                        <?php
                                        for ($j = 0; $j < 3; $j++) {
                                            ?>
                                            <tr>

                                                <td class="no-line"></td>
                                                <td class="no-line"></td>
                                                <td class="no-line"></td>
                                                <td class="no-line"></td>
                                                <td class="no-line"></td>
                                                <td class="no-line"></td>

                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>

                                <table class="table table-condensed summary">
                                    <tbody style="height: 500px;">
                                        <tr>
                                            <td class="no-line" colspan="4" style="width:65%;"></td>
                                            <td class="no-line text-left"><strong>Total Price:</strong></td>
                                            <td class="no-line text-right">{{config('assignment.price') . " ". @$purchaseInfo->package_price}}</td>
                                        </tr>

                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive" style="border:none !important;">
                <div class="table-responsive" style="border: none !important;">
                    <div style="font-size:11px;padding-left: 15px;">
                        This invoice was created electronically and is valid without a signature and seal.    
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>