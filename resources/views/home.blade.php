@extends('adminlte::master')

@section('title', 'AdminLTE')

@section('adminlte_css')

@yield('css')
@stop

@section('body')
@include('layouts.header')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--home content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="row">
                                <div class="col-md-12 col-xl-4">
                                    <div class="card counter-card-1">
                                        <div class="card-block-big">
                                            <div class="row">
                                                <div class="col-md-5 col-xl-5">
                                                    <h3>{{@$usercount}}</h3>
                                                    <p>{{trans('adminlabel.dashboard.totaluser')}}</p>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped progress-xs progress-bar-pink" role="progressbar" style="width: 20%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7 col-xl-7">
                                                    <h3>{{@$latestcount}}</h3>
                                                    <p>{{trans('adminlabel.dashboard.latestuser')}}</p>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped progress-xs progress-bar-pink" role="progressbar" style="width: 20%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                
                                            </div>
                                            <i class="icofont icofont-comment"></i> </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card counter-card-2">
                                        <div class="card-block-big">
                                            <div class="row">
                                                <div class="col-md-6 col-xl-6">
                                                    <h3>{{@$auditcount}}</h3>
                                                    <p>{{trans('adminlabel.dashboard.audittodal')}}</p>
                                                    <div class="progress ">
                                                        <div class="progress-bar progress-bar-striped progress-xs progress-bar-success" role="progressbar" style="width: 20%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-xl-6">
                                                    <h3>{{@$activeAuditCount}}</h3>
                                                    <p>{{trans('adminlabel.dashboard.auditactive')}}</p>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped progress-xs progress-bar-success" role="progressbar" style="width: 40%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <i class="icofont icofont-coffee-mug"></i> </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card counter-card-3">
                                        <div class="card-block-big">
                                            <div class="row">
                                                <div class="col-md-6 col-xl-6">
                                                    <h3>{{@$trainingcount}}</h3>
                                                    <p>{{trans('adminlabel.dashboard.trainingtodal')}}</p>
                                                    <div class="progress ">
                                                        <div class="progress-bar progress-bar-striped progress-xs progress-bar-default" role="progressbar" style="width: 20%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-xl-6">
                                                    <h3>{{@$activeTrainingCount}}</h3>
                                                    <p>{{trans('adminlabel.dashboard.trainingactive')}}</p>
                                                    <div class="progress ">
                                                        <div class="progress-bar progress-bar-striped progress-xs progress-bar-default" role="progressbar" style="width: 20%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <i class="icofont icofont-upload"></i> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
<!--                                    <img src="{{asset('images')}}/graph1.png" class="" width="100%" />-->
                                    <canvas id="myChart" width="400" height="100" aria-label="Hello ARIA World" role="img"></canvas>
                                </div>
                                
<!--                                <div class="col-md-6 col-xl-4">
                                    <div class="card table-card">
                                        <div class="row-table">
                                            <div class="col-sm-6 card-block-big br">
                                                <div class="row">
                                                    <div class="col-sm-4"> <i class="fa fa-eye text-success"></i> </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>10k</h5>
                                                        <span>Visitors</span> </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 card-block-big">
                                                <div class="row">
                                                    <div class="col-sm-4"> <i class="fa fa-music text-danger"></i> </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>100%</h5>
                                                        <span>Volume</span> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-table">
                                            <div class="col-sm-6 card-block-big br">
                                                <div class="row">
                                                    <div class="col-sm-4"> <i class="fa fa-file text-info"></i> </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>2000 +</h5>
                                                        <span>Files</span> </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 card-block-big">
                                                <div class="row">
                                                    <div class="col-sm-4"> <i class="fa fa-envelope-open text-warning"></i> </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>120</h5>
                                                        <span>Mails</span> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 col-xl-4">
                                    <div class="card table-card">
                                        <div class="row-table">
                                            <div class="col-sm-6 card-block-big br">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div id="barchart" style="height:40px;width:40px;"></div>
                                                    </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>1000</h5>
                                                        <span>Shares</span> </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 card-block-big">
                                                <div class="row ">
                                                    <div class="col-sm-4"> <i class="fa fa-newspaper-o text-primary"></i> </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>600</h5>
                                                        <span>Network</span> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-table">
                                            <div class="col-sm-6 card-block-big br">
                                                <div class="row ">
                                                    <div class="col-sm-4">
                                                        <div id="barchart2" style="height:40px;width:40px;"></div>
                                                    </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>350</h5>
                                                        <span>Returns</span> </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 card-block-big">
                                                <div class="row ">
                                                    <div class="col-sm-4"> <i class="fa fa-address-book text-primary"></i> </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>100%</h5>
                                                        <span>Connections</span> </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 card-block-big">
                                                <div class="row ">
                                                    <div class="col-sm-4"> <i class="fa fa accordion-msg text-primary"></i> </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>100%</h5>
                                                        <span>Connections</span> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card table-card">
                                        <div class="row-table">
                                            <div class="col-sm-6 card-block-big br">
                                                <div class="row">
                                                    <div class="col-sm-4"> <i class="fa fa-eye text-success"></i> </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>10k</h5>
                                                        <span>Visitors</span> </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 card-block-big">
                                                <div class="row">
                                                    <div class="col-sm-4"> <i class="fa fa-music text-danger"></i> </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>100%</h5>
                                                        <span>Volume</span> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-table">
                                            <div class="col-sm-6 card-block-big br">
                                                <div class="row">
                                                    <div class="col-sm-4"> <i class="fa fa-file text-info"></i> </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>2000 +</h5>
                                                        <span>Files</span> </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 card-block-big">
                                                <div class="row">
                                                    <div class="col-sm-4"> <i class="fa fa-envelope-open text-warning"></i> </div>
                                                    <div class="col-sm-8 text-center">
                                                        <h5>120</h5>
                                                        <span>Mails</span> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xl-4">
                                    <div class="card table-card widget-primary-card">
                                        <div class="row-table">
                                            <div class="col-sm-3 card-block-big"> <i class="icofont icofont-star"></i> </div>
                                            <div class="col-sm-9">
                                                <h4>4000 +</h4>
                                                <h6>Ratings Received</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card table-card widget-success-card">
                                        <div class="row-table">
                                            <div class="col-sm-3 card-block-big"> <i class="fa fa-envira"></i> </div>
                                            <div class="col-sm-9">
                                                <h4>17</h4>
                                                <h6>Achievements</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xl-5">
                                    <div class="card table-card group-widget">
                                        <div class="row-table">
                                            <div class="col-sm-4 bg-primary card-block-big"> <i class="fa fa-video-camera"></i>
                                                <p>1,586</p>
                                            </div>
                                            <div class="col-sm-4 bg-dark-primary card-block-big"> <i class="fa fa-video-camera"></i>
                                                <p>1,743</p>
                                            </div>
                                            <div class="col-sm-4 bg-darkest-primary card-block-big"> <i class="fa fa-mail-forward"></i>
                                                <p>1,096</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-3">
                                    <div class="card group-widget">
                                        <div class="card-block-big text-center">
                                            <h1 class="text-muted">150</h1>
                                            <h6 class="text-muted m-t-10">Counters</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card group-widget">
                                        <div class="card-block-big bg-info text-center">
                                            <h1>2065</h1>
                                            <h6 class="m-t-10">Counters</h6>
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>
                    <!--home content--->

                </div>
            </div>
        </div>
    </div>
</div>
<?php

  //$datestring = implode(',', $datestring);
  $userstring = implode(',', $userstring);
  $assignmentstring = implode(',', $assignmentstring);
?>
<script>
    var pausecontent = new Array();
    <?php foreach($datestring as $key => $val){ ?>
        pausecontent.push('<?php echo $val; ?>');
    <?php } ?>
var ctx = document.getElementById('myChart').getContext('2d');

var data = {
    labels: pausecontent,
    datasets: [
        {
            label: "New user",
            backgroundColor: "#04b362",
            data: [<?=$userstring?>]
        },
        {
            label: "New Assignment",
            backgroundColor: "#d5a829",
            data: [<?=$assignmentstring?>]
        }

    ]
};

var myBarChart = new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
        barValueSpacing: 20,
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                }
            }]
        }
    }
});

</script>
@stop
