@include('includes.header')
<style>
    .bubble p {
    border-radius: 3px !important;
    color: #646464 !important;
    font-size: 14px !important;
    margin: 0 !important;
    padding: 5px 10px 5px 12px !important;
}
.msg_history {
    overflow-x: hidden;
}
.msg_history {
    margin: 0 auto;
    overflow:hidden;
}

.msg_history:hover {
    overflow-y:scroll;
}
::-webkit-scrollbar {
    display: none;
}
::-moz-scrollbar {
    display: none;
}

</style>
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                   
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container mt-4">
	<h5>Message : #{{@$ref_id}}</h5>
       
        <div class="row">
<div class="col-12">
<div class="messaging">
      <div class="inbox_msg">
        <div class="mesgs mb-3">
            <div class="msg_history" id="msg-text">
<!--          <div class="msg_history" id="msg-text">-->
            @if(count(@$messagelist) > 0) <!---count proposal list--->
            @foreach(@$messagelist as $key=>$val)
            @if(@$val->sender_id == @$senderid)
            <div class="bubble2">
                <p>
                    <strong>You</strong><br>
                    {{@$val->message}}</p>
            </div>
            <div class="clearfix"></div>
            <div class="bubble_time pull-right">{{ Carbon\Carbon::parse(@$val->add_date)->format('h:i A') }} </div>
            <div class="clearfix"></div>
            @elseif(@$val->sender_id == @$reciveid)
            <div class="bubble">
                  <p>
                      <strong>{{@$getUserName->fname . " " . @$getUserName->lname}}</strong><br>
                      {{@$val->message}}
                  </p>
            </div>
            <div class="clearfix"></div>
     <div class="bubble_time pull-left"> {{ Carbon\Carbon::parse(@$val->add_date)->format('h:i A') }}</div>
             <div class="clearfix"></div>
            @endif
            @endforeach
            @elseif(@$getEndJob->endjob_status == 1)
            <div style="text-align:center;padding: 20px;">No previous messages.</div>
			@else
            <div style="text-align:center;padding: 20px;">Write your first message</div>
            @endif
          </div>
            
          @if(@$getEndJob->endjob_status == 0)
          <div class="type_msg">
            <div class="input_msg_write">
                <form name="msgform" id="msgform" method="post">
                {!! csrf_field() !!}
                <input type="hidden" name="recive_id" id="recive_id" value="{{@$reciveid}}">
                <input type="hidden" name="reference_id" id="reference_id" value="{{@$ref_id}}">
                <input type="hidden" name="proposal_id" id="proposal_id" value="{{@$proposal_id}}">
                <input type="text" class="write_msg" placeholder="Type a message" name="message" id="message"/>
                <button id="submit" class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                </form> 
            </div>
          </div>
           @endif 
        </div>
      </div> 
      
    </div></div>
    </div>
</div>
            
            
            
<!--            <div class="container">
                <div class="card agreementform">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <div id="successmsg"></div>
                            
                            <div class="row">
                                <div class="col-md-4 mb30" id="msg-text">
                                    <div class="card card-body text-center">
                                        @if(count(@$messagelist) > 0) -count proposal list-
                                        @foreach(@$messagelist as $key=>$val)
                                        <div class="col-12" >
                                            @if(@$val->sender_id == @$senderid)
                                            <div style="float:right;"> 
                                            @elseif(@$val->sender_id == @$reciveid)
                                            <div style="float:left;"> 
                                            @endif
                                                {{@$val->message}}
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif
                                       
                                        
                                    </div>
                                </div>
                                
                            </div>
                              
                                
                            
                            <form name="msgform" id="msgform" method="post">
                                {!! csrf_field() !!}
                                <input type="hidden" name="recive_id" id="recive_id" value="{{@$reciveid}}">
                                <input type="hidden" name="reference_id" id="reference_id" value="{{@$ref_id}}">

                                <div class="row">
                                    
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <label class="mt-2">{{trans('messagelabel.message')}}</label>
                                        <div class="form-group">
                                            <textarea class="form-control" name="message" id="message" ></textarea>
                                        </div>
                                        <div>
                                    <button id="submit" type="button" class="btn btn-primary text-center btn-md pull-right mt-2">{{trans('formlabel.postassignmenttraining.rowaction')}}</button>
                                </div>
                                    </div>
                                </div>
                                
                            </form> 


                        </div>
                    </div>
                </div>

              

            </div>
        </div>-->
    </div>
</div>
</div>
<script>
    $(document).ready(function () {
        $(".msg_history").animate({ scrollTop: $(document).height() }, "slow");
  return false;
      });
      
      //to disable enter key
      $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
      //to disable enter key
</script>
    
<script>
    $(document).ready(function () {
        $("#submit").click(function () {
           
            //alert(1211);
            var recive_id = $("#recive_id").val();
            var reference_id = $("#reference_id").val();
            var proposal_id = $("#proposal_id").val();
            var message = $("#message").val();
            var _token = "<?php echo csrf_token(); ?>";
            
            var url = "{{url('/sendMessage')}}";
            if(message == '')
            {
                alert("Please enter message.");
                return false;
            } else {
            $.post(url, {"_token": _token, "recive_id": recive_id, "reference_id": reference_id,
                "message": message, "proposal_id": proposal_id}, function (res) {
                //alert(res);
                if (res === 'insert')
                {
                    $("#message").val("");
                    $("#successmsg").delay(4000).show().fadeOut('slow').html("<div class='alert alert-mini alert-success margin-bottom-30'> Successfully Inserted</div>");
                    
                    var ajaxurl ="{{url('/sendMessage')}}/"+recive_id+"/"+proposal_id+"/"+reference_id+"?ajax=1";
                    //alert(ajaxurl);
                     $.get(ajaxurl, function (data) {
                        $("#msg-text").html(data);
                       
                    });
                    
                }
                else if (res === 'error')
                {
                    $("#successmsg").delay(4000).show().fadeOut('slow').html("<div class='alert alert-mini alert-danger margin-bottom-30'> Error In inserted.</div>");
                }
                
               
            });
        }
         
        });
    });
</script>
<!--<script src="{{ asset('/')}}js/simplebar.js" type="text/javascript"></script>-->
@include('includes.footer')