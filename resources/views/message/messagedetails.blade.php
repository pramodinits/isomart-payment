@include('includes.header')


<div class="container">
    <div class="main-content">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="details mt-4 border-right">
                            <h5 class="profile-no">
                                <!--- assignment status for send bid--->
                                <small class="text-success">
                                    
                                </small>
                                <!--- assignment status for send bid--->
                            </h5>
                            <div>
                            </div>
                            <div><span class="text-primary"><b></b></span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---send, edit & delete proposal button-->
            <div class="col-md-6 col-sm-6">
                <div class="btnsection mt-2 pull-right">
                    <h6 class="text-center"></h6>
                    <div class="buget-price"></div>
                    
                </div>
            </div>
            <!---send, edit & delete proposal button-->
        </div>
    </div>
</div>
<!---proposal details-->
<div class="container">
    <h5>{{trans('messagelabel.message')}}</h5>
    @if(count(@$messagelist) > 0) <!---count proposal list--->
            @foreach(@$messagelist as $key=>$val)
            <?php
            if(@$senderid == @$val->recive_id)
            {
                $receiveid = @$val->sender_id;
            } else {
                $receiveid = @$val->recive_id;
            }
            ?>
        <div class="card mb-2" >
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="messagebox">
                    <a href="{{ url("/sendMessage/".@$receiveid)."/".@$val->proposal_id."/".@$val->reference_id }}">
                    <h6 class="text-dark m-2"><strong>{{"#".@$val->reference_id}}</strong></h6>
                    <div class=" m-2">{{@$val->message}}</div>
                    <div class="m-2">{{ Carbon\Carbon::parse(@$val->add_date)->format('d-M-Y h:i A') }}</div>
                    </a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    @else
    <div class="card mb-2 p-3">
        <div class="col-md-12">
            <div class="text-secondary text-center" style="text-align: center;">
                No message found.
            </div>
        </div>
    </div>
    @endif
<!--<a href="{{ url("/sendMessage/".@$reciveid."/".@$ref_id) }}">
        <button type="button" class="btn btn-orange btn-sm btn-xs mb3"><i class="fa fa-envelope"></i> Send</button>
        </a>-->
    
</div>
<!---proposal details-->





<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>

@include('includes.footer')
