@if(count(@$messagelist) > 0) <!---count proposal list--->
            @foreach(@$messagelist as $key=>$val)
            @if(@$val->sender_id == @$senderid)
            <div class="bubble2">
                <p>
                    <strong>You</strong><br>
                    {{@$val->message}}
                </p>
            </div>
            <div class="clearfix"></div>
            <div class="bubble_time pull-right">{{ Carbon\Carbon::parse(@$val->add_date)->format('h:i A') }} </div>
            <div class="clearfix"></div>
            @elseif(@$val->sender_id == @$reciveid)
            <div class="bubble">
                  <p>
                      <strong>{{@$getUserName->fname . " " . @$getUserName->lname}}</strong><br>
                      {{@$val->message}}
                  </p>
            </div>
            <div class="clearfix"></div>
     <div class="bubble_time pull-left"> {{ Carbon\Carbon::parse(@$val->add_date)->format('h:i A') }}</div>
             <div class="clearfix"></div> 
            @endif
            @endforeach
            @endif
            
            
<script>
    $(document).ready(function () {
        $(".msg_history").animate({ scrollTop: $(document).height() }, "slow");
  return false;
      });
</script>