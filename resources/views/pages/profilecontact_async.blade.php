<?php
//print "<pre>";
//print_r($addressDetails);
//exit();
$contactStatus = $contactstatuscolor;
if (count(@$addressDetails) > 0 &&
        @$contactDetails->landline &&
        @$contactDetails->alternate_number &&
        @$contactDetails->skype &&
        @$contactDetails->instagram &&
        @$contactDetails->twitter &&
        @$contactDetails->whatsapp
) {
    $contactStatus = "#5CB85C";
}
?>
<style>
    .addressstatus{
        color: <?= $contactStatus; ?> !important;
    }
    .tiles-inner {
        height: 230px;
        box-shadow: 0 2px 1px 0 rgba(0,0,0,.16);
    }
    .spacing-small {
        margin-bottom: 10px;
        max-height: 115px;
    }
    .a-nostyle {
        margin: 0;
        color: #111;
        padding-left: 0 !important;
    }
    
    .a-nostyle li {
        list-style: none;
        font-size: 13px;
    }
    .a-list-item {
        color: #111;
    }
    .edit-address-link {
        position: absolute;
        bottom: 20px;
        left: 22px;
    }
    .address-action li{
        position: relative;
        display:inline-block;
        list-style-type: none;
    }
    ul.address-action {
        position: relative;
        top: 20%;
        padding-left: 0;
    }
    #loading-image {
        position: absolute;
        z-index: 1;
        right: 6px;
        top: 7px;
    }
    #dateColumn {
        display: none;
    }
</style>
<div class="col-md-12 col-sm-12 col-12" id="address-left">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-12">
            <form id="profile_address" name="profile_address" action="{{asset('checkAvailableDate')}}" method="post" onsubmit="return AsyncUpload.submitForm(this, addressformvalidation, calbackFun);">
                {!! csrf_field() !!}
                <input type="hidden" name="user_id" id="user_id" value="{{Auth::user()->id}}">
                <input type="hidden" name="id_update" id="id_update" value="">
                <input type="hidden" name="alertmsg" id="alertmsg" value="">
                <input type="hidden" name="availableMatch" id="availableMatch" value="">
                <label>{{trans('formlabel.profilecontact.country')}}</label>
                <div class="form-group">
                    <select class="form-control" name="address[country_id]" id="countries" onchange="getLocationInfo('countries', 'states');">
                        <option value="">{{trans('formlabel.profilecontact.addresscountry')}}</option>
                        @foreach(@$countryList as $k=>$v)
                        <option value="{{@$v->id}}">{{@$v->name}}</option>
                        @endforeach
                    </select>
                </div>
                <label>{{trans('formlabel.profilecontact.state')}}</label>
                <div class="form-group">
                    <select class="form-control" name="address[state_id]" required id="states" onchange="getLocationInfo('states', 'cities');">
                        <option value="">{{trans('formlabel.profilecontact.addressstate')}}</option>
                    </select>
                </div>
                <label>{{trans('formlabel.profilecontact.city')}}</label>
                <div class="form-group">
                    <img id="loading-image" src="{{asset('images')}}/ajax-loader.gif" alt="Loading..." />
                    <input type="text" style="position: relative;" placeholder="{{trans('formlabel.profilecontact.addresscity')}}" class="form-control" required id="city" name="address[city_name]" value="">
                </div>
                <label>{{trans('formlabel.profilecontact.address')}}</label>
                <div class="form-group">
                    <input type="text" placeholder="{{trans('formlabel.profilecontact.addressline1')}}" required class="form-control" id="address1" name="address[addressline1]" value="" data-bv-notempty-message="Please enter an address.">
                    <br>
                    <input type="text" placeholder="{{trans('formlabel.profilecontact.addressline2')}}" class="form-control" id="address2" name="address[addressline2]" value="">
                </div>
                <label>{{trans('formlabel.profilecontact.landmark')}}</label>
                <div class="form-group">
                    <input type="text" class="form-control" id="landmark" name="address[landmark]" value="">
                </div>
                <label>{{trans('formlabel.profilecontact.zipcode')}}</label>
                <div class="form-group">
                    <input type="text" required class="form-control" id="zipcode" name="address[zipcode]" value="" data-bv-notempty-message="Please enter a ZIP or postal code.">
                </div>
                @if(count(@$addressDetails) > 0)
                <div class="input-field">
                    <input type="checkbox" style="display: inline;" class="form-control" name="address[primary_status]" id="primaryAddress" value="1">
                    <span class="mr-2">&nbsp;
                        {{trans('formlabel.profilecontact.primarystatus')}}
                    </span>
                    <input type="checkbox" style="display: inline;" class="form-control" name="address[available_status]" id="availableAddress" value="1">
                    <span>&nbsp;
                        {{trans('formlabel.profilecontact.availablestatus')}}
                    </span><br/>
                </div>
                @endif
                <div id="dateColumn">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 form-group">
                        <label>{{trans('formlabel.profilecontact.availablefromdate')}}</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="available_fromdate" id="available_fromdate" readonly />
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 form-group">
                        <label>{{trans('formlabel.profilecontact.availabletodate')}}</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="available_todate" id="available_todate" readonly />
                        </div>
                    </div>
                    </div>
                </div>
                <div class="mt-1 mb-2">
                    <button type="submit" id="saveAddress" class="btn btn-primary text-center btn-md pull-right">{{trans('formlabel.profilecontact.saveaddress')}}</button>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
        <div class="col-md-6 col-sm-6 col-12">
            <form id="profile_contact" name="profile_contact" action="{{ asset('updateProfileContact')}}" method="post">
                {!! csrf_field() !!}
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <input type="hidden" name="id_update" value="{{@$contactDetails->id}}">
                <label>{{trans('formlabel.profilecontact.landline')}}</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="landline" name="profile[landline]" value="{{@$contactDetails->landline}}">
                </div>
                <label>{{trans('formlabel.profilecontact.alternateno')}}</label>
                <div class="form-group">
                    <input type="text" class="form-control" required id="alternateNumber" data-bv-notempty-message="Please enter an alternate no." name="profile[alternate_number]" value="{{@$contactDetails->alternate_number}}">
                </div>
                <label>{{trans('formlabel.profilecontact.skype')}}</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="skype" name="profile[skype]" value="{{@$contactDetails->skype}}">
                </div>
                <label>{{trans('formlabel.profilecontact.instagram')}}</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="instagram" name="profile[instagram]" value="{{@$contactDetails->instagram}}">
                </div>
                <label>{{trans('formlabel.profilecontact.twitter')}}</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="twitter" name="profile[twitter]" value="{{@$contactDetails->twitter}}">
                </div>
                <label>{{trans('formlabel.profilecontact.whatsapp')}}</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="whatsapp" name="profile[whatsapp]" value="{{@$contactDetails->whatsapp}}">
                </div>
                <div class="mt-3">
                    <button type="submit" class="btn btn-primary text-center btn-md pull-right">{{trans('formlabel.profilecontact.savecontact')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container mt-3">
    <div class="row">
        <div class="col-md-12 text-left mb-3">
            <hr>
            <h4>{{trans('formlabel.profilecontact.savedaddress')}}</h4>
        </div>
        @if(count(@$addressDetails) > 0)
        @foreach(@$addressDetails as $key=>$val)
        <div class="col-md-4 col-sm-4 col-xs-12 addresstiles mb-5">
            <div class="card card-body tiles-inner">
                <div class="spacing-small">
                    <ul class="a-unordered-list a-nostyle a-vertical">
                        <li>
                            <span class="a-list-item">
                                <span id="address-ui-widgets-AddressLineOne">{{$val->addressline1}}</span>
                            </span>
                        </li>
                        <li>
                            <span class="a-list-item">
                                <span id="address-ui-widgets-AddressLineTwo">
                                    {{$val->addressline2}}
                                </span>
                            </span>
                        </li>
                        @if($val->landmark)
                        <li>
                            <span class="a-list-item">
                                <span id="address-ui-widgets-AddressLineTwo">
                                    {{$val->landmark}}
                                </span>
                            </span>
                        </li>
                        @endif
                        <li>
                            <span class="a-list-item">
                                <span id="address-ui-widgets-CityStatePostalCode">
                                    {{$val->city_name .", ". @$stateNameArray[$val->state_id] ." ". $val->zipcode}}
                                </span>
                            </span>
                        </li>
                        <li>
                            <span class="a-list-item">
                                <span id="address-ui-widgets-Country">{{$countryName[$val->country_id]}}</span>
                            </span>
                        </li>
                        @if ($val->available_fromdate && $val->available_todate)
                        <li>
                            <span class="a-list-item">
                                <span id="address-ui-widgets-Country">
                                    {{"Available from ". Carbon\Carbon::parse(@$val->available_fromdate)->format(config('assignment.dateformat'))." to " . Carbon\Carbon::parse(@$val->available_todate)->format(config('assignment.dateformat'))}}
                                </span>
                            </span>
                        </li>
                        @endif
                    </ul>
                </div>
                <ul class="address-action">
                    <li>
                        <a href="javascript:void(0);" onclick="editAddress('{{$val->id}}')">
                            Edit
                        </a>&nbsp; | &nbsp;
                    </li>
                    <li>
                        <a href="javascript:void(0);" onclick="deleteAddress('{{$val->id}}')">Delete</a>&nbsp; 
                    </li>
                    <li>
                        @if($val->available_status == 1)
                        | &nbsp;<span class="text-success">{{trans('formlabel.profilecontact.availableactive')}}</span>
                        @endif
                        @if($val->primary_status == 1)
                         | <span class="text-success">{{trans('formlabel.profilecontact.primaryactive')}}</span>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
        @endforeach
        @else
        <div class="col-md-12 p-2">
            <div class="text-secondary text-center" style="text-align: center;">
                No data found.
            </div>
        </div>
        @endif
    </div>
</div>

<!--datepicker-->
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>
<script>
    $("#availableAddress").click(function () {
    if($('#availableAddress').is(":checked")) {
        $("#dateColumn").show();
    } else {
        $("#dateColumn").hide();
    }
    });
    
    $(function () {
    $('#available_fromdate').datepicker({
    changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            minDate: 0,
            onSelect: function (selected) {
            $("#available_todate").datepicker("option", "minDate", selected)
            $('#available_fromdate').parents('div.form-group').addClass('has-success');
            $('#available_fromdate').parents('div.form-group').removeClass('has-error');
            $("#saveAddress").attr("disabled", false);
            }
    });
            $('#available_todate').datepicker({
    changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            minDate: 0,
            onSelect: function (selected) {
               $('#available_todate').parents('div.form-group').addClass('has-success');
            $('#available_todate').parents('div.form-group').removeClass('has-error');
            $("#saveAddress").attr("disabled", false);
            }
    });
    });
</script>    
<!--datepicker-->
<script src="{{ asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/ajaxupload.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js')}}"></script>
<script>
//get location list
                            function getLocationInfo(id, tbl) {
                            if (id == "countries") {
                            $("#states").removeAttr("selected");
                                    $('#states').val("");
                                    $('#city').val("");
                            }
                            else if (id == "states") {
                            $('#loading-image').show();
                                    $('#city').val("");
                            }
                            var selected_val = $('select#' + id + ' option:selected').val();
                                    var url = "{{url('/getLocationInfo')}}";
                                    $.post(url, {"selected_val": selected_val, "id": id, "tbl": tbl, "_token": '<?= csrf_token(); ?>'}, function (res) {
                                    if (tbl == 'cities'){
                                    availableTags = JSON.parse(res);
                                            $('#loading-image').hide();
                                            $("#city").autocomplete({
                                    source: availableTags
                                    });
                                    } else{
                                    $("#" + tbl).html(res);
                                            var total = $('select#' + tbl + ' option').length;
                                            $("#" + tbl + "_cnt").html(" <b>(" + (total - 1) + ")</b>");
                                    }
                                    });
                            }
//get location list

function addressformvalidation() {
    var validator = $("#chk_frm").validate({
        rules: {
            "address[country_id]": {
            required: true
            },
            "address[state_id]": {
            required: true
            },
            "address[city_name]": {
            required: true
            }
        },
         messages: {
             "address[country_id]": {
                required: "Please enter a country or region."
                },
             "address[state_id]": {
                required: "Please enter a state, region, or province."
                },
             "address[city_name]": {
                required: "Please enter a city name."
                }
         }
    });
    var x = validator.form();
            if (x) {
    $("#saveAddress").prop("disabled", true);
            return true;
    } else {
    $("#saveAddress").prop('disabled', false);
            return false;
    }
}

function calbackFun(res) {
    alert(res);
        }
        

$(document).ready(function () {
$('#loading-image').hide();
});

                            
                            $('#profile_contact').bootstrapValidator({
                    feedbackIcons: {
                    valid: 'fa fa-check',
                            invalid: 'fa fa-remove',
                            validating: 'glyphicon glyphicon-refresh'
                    }
                    })
                            .on('success.form.bv', function (e) {
                            $('#custom_loader').hide();
                                    return true;
                            });
                            function deleteAddress(id) {
                            var del = confirm('Are you sure to remove this?');
                                    if (del) {
                                        
                            var url = "{{url('/deleteAddress')}}";
                                    $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                        if (data == "Success") {
                                            alert("Address deleted successfully");
                                             setTimeout(function () {
                                                 location.reload();
                                    }, 2000);
                                        } else {
                                            return false;
                                        }
//                                    window.location.href = "{{asset('profile')}}";
                                    });
                            } else {
                            return false;
                            }
                            }

                    function editAddress(id) {
                    if (id) {
                    $.ajax({
                    type: "POST",
                            url: "{{url('/getAddress')}}",
                            dataType: 'json',
                            data: {'id': id, '_token': "<?= csrf_token(); ?>"},
                            cache: false,
                            success: function (res) {
                            $("#id_update").val(res.id);
                                    $("#countries").val(res.country_id);
                                    $("#countries").trigger("onchange");
                                    $("#states").removeAttr("selected");
                                    setTimeout(function(){
                                    $("#states option[value='" + res.state_id + "']").attr("selected", "selected");
                                            $("#states").trigger("onchange");
                                    }, 800);
                                    $("#address1").val(res.addressline1);
                                    $("#address2").val(res.addressline2);
                                    $("#landmark").val(res.landmark);
                                    $("#zipcode").val(res.zipcode);
                                    $("#available_fromdate").val(res.available_fromdate);
                                    $("#available_todate").val(res.available_todate);
                                    $("#city").val(res.city_name);
                                    setTimeout(function(){
                                    $("#city").val(res.city_name);
                                    }, 900);
                                    if (res.primary_status == 1) {
                            $("#primaryAddress").prop("checked", true);
                            }
                            if (res.available_status == 1) {
                            $("#availableAddress").prop("checked", true);
                            }
                            }
                    });
                    }
                    }
</script>