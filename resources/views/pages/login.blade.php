@include('includes.header')
<link rel="stylesheet" href="{{ asset('intltel/css/intlTelInput.css')}}">

<link rel="stylesheet" href="{{ asset('intltel/css/demo.css')}}">

<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="login-wrapper col-md-5 col-sm-12">
                    <div class="text-center text-dark">
                        <h4><strong>Login</strong></h4>
                         <br>
                    </div>
                    <form id="login_form" name="login_form" action="{{ asset('signin')}}" method="post"> 
                        {!! csrf_field() !!}
                        @if(@$assignment_id)
                        <input type="hidden" name="assignment_id" value="{{@$assignment_id}}"/>
                        @endif
                        <div class="input-field form-group">
                            <label for="Phone" class="active">{{trans('formlabel.login.phone')}}*</label>
                            @if(@$phone)
                            <input type="text" class="form-control" id="Phone" name="Phone" value="{{@$phone}}" onkeypress="return isNumberKey(event)">
                            @else
                            <input type="text" class="form-control" id="Phone" name="Phone" onkeypress="return isNumberKey(event)">
                            @endif
                            <input type="hidden" value="+91" name="countrycode" id="countrycode" />
                            <i style="display: none; color: #b24d4b;" class="form-control-feedback fa fa-remove" data-bv-icon-for="phone-check"></i>
                            <div class="help-block has-error small" style="color: #b24d4b;"></div>
                        </div>
                        <div class="input-field form-group">
                            <label for="Password" class="">{{trans('formlabel.login.password')}}*</label>
                            <input type="password" class="form-control" id="Password" name="Password" required>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block btn-lg">{{trans('formlabel.login.loginbutton')}}</button>
                        <div class="form-group col-md-12 mt-2">
                            <span class="col-md-6 text-left">
                                <a href="{{asset('signup')}}" class="forgot-password">{{trans('formlabel.login.createaccount')}}</a>
                            </span>
                            <span class="col-md-6 float-right text-right">
                                <a href="{{asset('forgotpassword')}}" class="forgot-password">{{trans('formlabel.login.forgotpassword')}}</a>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
//    function setUserID(myValue) {
//        $('#countrycode').val(myValue)
//                .trigger('change');
//    }
//
//    $("input[type=hidden] #countrycode").bind("change", function () {
//        console.log($(this).val());
//    });
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    $(document).ready(function () {
        var phone = $("#Phone").val();
        if (phone) {
//            $("#Phone").val().replace(/[^\d]/g, '');
            checkPhone();
            $('label[for="Phone"]').addClass("has-value");
        }
    });
    var typingTimer; //timer identifier
    var doneTypingInterval = 1000; //time in ms, 1 second for example
    var input_phone = $('#Phone');
    input_phone.on('keyup', function () {
        $("#Phone").val().replace(/[^\d]/g, '');
        if (input_phone.val().length >= 8) {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(checkPhone, doneTypingInterval);
        } else {
            $("i[data-bv-icon-for=Phone]").show();
            $("div.help-block").hide();
            return false;
        }
    });
    function checkPhone() {
        var phone = $("#Phone").val();
        var countrycode = $("#countrycode").val();
        var _token = "<?php echo csrf_token(); ?>";
        $("#custom_loader").show();
        if (phone) {
            $.ajax({
                method: "POST",
                url: "{{url('/checkphone')}}",
                data: {
                    "phone": phone,
                    "countrycode": countrycode,
                    "_token": _token
                },
                cache: false,
                success: function (res) {
                    if (res === "success") {
                        $("#custom_loader").hide();
                        $("#custom_loader_alert_box").hide();
                        $("div.help-block").hide();
                        $("i[data-bv-icon-for=Phone]").show();
                        $("i[data-bv-icon-for=phone-check]").hide();
                    } else {
                        $("#custom_loader_alert_box").hide();
                        $("#custom_loader").hide();
                        $("i[data-bv-icon-for=Phone]").hide();
                        $("i[data-bv-icon-for=phone-check]").show();
                        $("div.help-block").html("Mobile number is not registered. Kindly <a href='{{asset('signup')}}/" + phone + "'>Register</a>").show();
                    }
                }
            });
        }
    }

    $(document).ready(function () {

        $(".input-field :input").focus(function () {
            $("label[for='" + this.name + "']").addClass("active");
        }).blur(function () {
            $("label").removeClass("active");
        });
        $('#login_form').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "Phone": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        },
                        stringLength: {
                            min: 8,
                            message: 'Please enter valid phone number.'
                        }
                    }
                },
                "Password": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                }
            }
        })
                .on('error.form.bv', function (e) {
                    var getPhone = $("#Phone").val();
                    if (getPhone) {
                        var number = getPhone.replace(/[^\d]/g, '');
                        $("#Phone").val(number);
                    }
//                    if ($('#email') && $('#email').val().length === 0 && $('#email').parents('.form-group').hasClass('has-success')) {
//                        $('#email').parents('div.form-group').removeClass('has-success');
//                        $('#email').parents('.form-group').find('.form-control-feedback[data-bv-icon-for="email"]').hide();
//                    }
                })

                .on('success.form.bv', function (e) {
                    $("#Phone").val().replace(/[^\d]/g, '');
                    $('#custom_loader').show();
                    return true;
                });
    });
        </script>
<script src="{{ asset('intltel/js/intlTelInput.js')}}"></script>
<script>
    $("#Phone").intlTelInput({
        // allowDropdown: false,
        // autoHideDialCode: false,
        // autoPlaceholder: "off",
        // dropdownContainer: "body",
        // excludeCountries: ["us"],
        // formatOnDisplay: false,
        // geoIpLookup: function(callback) {
        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //     var countryCode = (resp && resp.country) ? resp.country : "";
        //     callback(countryCode);
        //   });
        // },
        // hiddenInput: "full_number",
        initialCountry: "in",
        // nationalMode: false,
        //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        separateDialCode: true,
        utilsScript: "{{ asset('intltel/js/utils.js')}}"
    });
    // on blur: validate    
    $('.country').click(function () {
        $('#countrycode').val($(this).children('.dial-code').html());
//        checkPhone();
    });
    $('.country').click(function () {
//        console.log($('li.highlight').attr('data-dial-code'));
        if ($("#Phone").val().length >= 8) {
        checkPhone();
    }
    });
</script>
