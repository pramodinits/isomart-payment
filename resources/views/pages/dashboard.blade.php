@include('includes.header')
@php
$packagetype = config('subscription.packagelist');
$packageduration = config('subscription.packageduration');
@endphp
<br>    
<style>
    #packageModal {
        width: 50%;
        margin: 0 auto;
    }
    #packageModal button {
        width: 15%;
    }
</style>
<div class="clearfix"></div>
<div class="container pt90 pb30">
    <h5 class="text-left text-secondary font-weight-normal">Latest Assignments : 
        <small class="pull-right">
            <a href="{{asset('assignmentlist')}}" class="text-danger">
                <button type="button" class="btn btn-dark btn-sm">
                    More <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                </button>
            </a>
        </small>
    </h5>
</div>
<div class="clearfix"></div>
<div class="container">
    <div class="">
        <div class="row">
            <div class="col-lg-12 mb40">
                <div id="tabAssign">
                    <ul class="nav nav-tabs tabs-default mb30" role="tablist bg-light">
                        <li class="nav-item " role="presentation" style="display: none;"><a class="nav-link  custom-tabs" href="#certification" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Certification</a></li>
                        <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs active show" href="#audit" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Audit</a></li>
                        <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs " href="#training" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Training</a></li>
                    </ul>

                    <!-- Tab panes -->
                    
                    <div class="tab-content card p-3">
                        <div role="tabpanel" class="tab-pane fade" id="certification">
                            <div class="container">
                                <div class="row">
                                    @if(count($assignmentcertification) > 0)
                                    @foreach($assignmentcertification as $k=>$v)
                                    <div class="col-md-4 mb10">
                                        <div class="col-12 card text-center">
                                            <h4 class="card-title">
                                                <a href="{{ asset('assignmentCertificationDetail/'.$v->assignment_id)}}">{{substr(ucfirst($v->assignment_name), 0, 20) . "..."}}</a>
                                            </h4>
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="col-3"></div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-3"></div>
                                            </div>
                                            <hr/>
                                            <p class="card-text text-left">{{substr($v->description, 0, 40) . "..."}}<br>
                                                Ref ID : <a href="{{ asset('assignmentCertificationDetail/'.$v->assignment_id)}}">{{"#".$v->ref_id}}</a><br>
                                                Duration : {{$v->effort_tentative . " days"}} <br>
                                                <?php
                                                $names = "";
                                                $cityArray = explode(",", $v->location);
                                                foreach ($cityArray as $key => $val) {
                                                    $names .= @$cityNameArray[$val] . ", ";
                                                }
                                                ?>
                                                Location : {{rtrim($names, ", ")}}<br>
                                                <strong>{{$v->add_date}}</strong>
                                            </p>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else 
                                    <div class="col-md-12">
                                        <div class="text-secondary text-center" style="text-align: center;">
                                            No assignments found.
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                  
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade active show" id="audit">
                            <div class="">
                                <div class="row">
                                    @if(count($assignmentaudit) > 0)
                                    @foreach(@$assignmentaudit as $k=>$v)
                                    <div class="col-md-4 mb30">
                                         <div class="card-header bg-premiry text-white text-center">
                                             <h5 class="mb-0">
                                                <a href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}">
                                                    @if(strlen($v->assignment_name) > 20)
                                                    {{substr(ucfirst($v->assignment_name), 0, 20) . "..."}}
                                                    @else
                                                    {{ucfirst($v->assignment_name)}}
                                                    @endif
                                                </a>
                                             </h5>
                                            </div>
                                        <div class="card">                                              
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="mx-auto">
                                                    <?php
                                                    /*days left*/
                                                    $now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
                                                    $end_date = Carbon\Carbon::parse($v->proposal_end_date);
                                                    $now_date = Carbon\Carbon::parse($now);
                                                    $daysleft = $now_date->diffInDays($end_date, false);
                                                    /*days left*/
                                                    /* budget counted with approved bid */
                                                    $assignmentBudget = $v->budget_usd - $v->jobBudget;
                                                    $assignmentEffort = $v->effort_tentative - $v->jobEffort;
                                                    /* budget counted with approved bid */
                                                    ?>
                                                    <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $assignmentBudget}}</span>
                                                    {{"(" . trans('formlabel.assignmentinfoaudit.efforts') .  $assignmentEffort . config('assignment.effortdays') . ")" }}
                                                    @if($v->approval_status == 1)
                                                    <span class="text-success">{{trans('formlabel.auditdetail.awarded')}}</span>
                                                    @elseif(@$daysleft < 0)
                                                    <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                    @elseif(@$daysleft == 0)
                                                    <span class="text-success">{{trans('formlabel.auditdetail.openfortoday')}}</span>
                                                    @else
                                                    <span class="text-success">Open / {{@$daysleft}} days left</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="col-md-3"></div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-md-3"></div>
                                            </div>
                                            <hr/>
                                            <div class="col-12 card-text text-left">
                                                {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                                                {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}
                                                <div class="headings">
                                                <?php
                                                $citynames = "";
                                                foreach (@$v->AssignmentLocation as $key => $val) {
                                                    if(!$val->whomtoassign_id) {
                                                    $citynames .= $val->city_name . ", ";
                                                    } elseif($v->approval_status == 1) {
                                                        $citynames .= $val->city_name . ", ";
                                                    }
                                                }
                                                ?>
                                                {{trans('formlabel.assignmentinfoaudit.location')}} {{rtrim($citynames, ", ")}}</div>
                                                <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else 
                                    <div class="col-md-12">
                                        <div class="text-secondary text-center" style="text-align: center;">
                                            No assignments found.
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="training">
                          
                            <div class="">                                  
                                <div class="row">
                                    @if(count($assignmenttraining) > 0)
                                    @foreach($assignmenttraining as $k=>$v)
                                    <div class="col-md-4 mb30">
                                        <div class="card-header bg-premiry text-white text-center">
                                           <h5 class="mb-0">
                                                <a href="{{ asset('assignmentTrainingDetail/'.$v->assignment_id)}}">{{ucfirst($v->batch_no)}}</a>
                                            </h5>
                                        </div>
                                        
                                        <div class="col-12 card text-center">
                                            
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="mx-auto">
                                                    <?php
                                                    /*days left*/
                                                    $now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
                                                    $end_date = Carbon\Carbon::parse($v->proposal_end_date);
                                                    $now_date = Carbon\Carbon::parse($now);
                                                    $daysleft = $now_date->diffInDays($end_date, false);
                                                    /*days left*/
                                                    /* budget counted with approved bid */
                                                    $assignmentBudget = $v->fee_range - $v->jobBudget;
                                                    $assignmentEffort = $v->effort_tentative - $v->jobEffort;
                                                    /* budget counted with approved bid */
                                                    ?>
                                                    <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $assignmentBudget}}</span>
                                                    {{"(" . trans('formlabel.assignmentinfoaudit.efforts') .  $assignmentEffort . config('assignment.effortdays') . ")" }}
                                                    @if($v->approval_status == 1)
                                                    <span class="text-success">{{trans('formlabel.auditdetail.awarded')}}</span>
                                                    @elseif(@$daysleft < 0)
                                                    <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                    @elseif(@$daysleft == 0)
                                                    <span class="text-success">{{trans('formlabel.auditdetail.openfortoday')}}</span>
                                                    @else
                                                    <span class="text-success">Open / {{@$daysleft}} days left</span>
                                                    @endif
                                                </div>
                                            </div>
                                            @if($v->mode == 1)
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="col-3"></div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-3"></div>
                                            </div>
                                            @endif
                                            <hr/>
                                            <p class="card-text text-left">
                                                {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('assignmentTrainingDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                                                {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                                                @if($v->mode == 1)
                                                <?php
                                                $citynames = "";
                                                foreach (@$v->AssignmentLocation as $key => $val) {
                                                    if(!$val->whomtoassign_id) {
                                                    $citynames .= $val->city_name . ", ";
                                                    } elseif($v->approval_status == 1) {
                                                        $citynames .= $val->city_name . ", ";
                                                    }
                                                }
                                                ?>
                                                {{trans('formlabel.assignmentinfotraining.mode')}} {{config('assignment.modef2f')." (" . rtrim($citynames, ", ") . ")"}}<br>
                                                @else
                                                {{trans('formlabel.assignmentinfotraining.mode')}} {{config('assignment.modeonline')}}<br>
                                                @endif
                                                <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                                            </p>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else 
                                    <div class="col-md-12">
                                        <div class="text-secondary text-center" style="text-align: center;">
                                            No assignments found.
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            
                            </div>
                        </div>
                    </div>
                    <!-- Tab panes -->

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if(@$advtinfo && @$advtinfo[1])
            <img src="{{asset('training_image')}}/{{@$advtinfo[1]}}" class="img-fluid pt-2 pb-2">
            @else
            <img src="images/add_img.jpg" class="img-fluid pt-2 pb-2">
            @endif
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="container pt30 pb30">
    <h5 class="text-left text-secondary font-weight-normal">Latest Training Advertisement : 
        <small class="pull-right">
            <a href="{{asset('trainingPostList')}}" class="text-danger">
                <button type="button" class="btn btn-dark btn-sm">
                    More <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                </button>
            </a>
        </small>
    </h5>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 mb40">
            <div class="row">
              @if(count(@$trainingpostinfo) > 0)
              @foreach(@$trainingpostinfo as $k=>$v)
              <div class="col-md-4 mb30">
                  <div class="card-header bg-premiry text-white text-center">
                   <h5 class="mb-0">
                          <a href="{{ asset('trainingPostDetail/'.$v->training_post_id)}}">
                              @if(strlen($v->traning_name) > 20)
                              {{substr(ucfirst($v->traning_name), 0, 20) . "..."}}
                              @else
                              {{ucfirst($v->traning_name)}}
                              @endif
                          </a>
                      </h5>
                  </div>
                  <div class="col-12 card text-center">
                     
                      <div class="d-flex flex-row mx-auto col-12">
                          <h4>
                              {{trans('messagelabel.training.feerange')}} : <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $v->fee_range}}</span>
                          </h4>
                      </div>
                      
                      <hr/>
                      <p class="card-text text-left">
                          {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('trainingPostDetail/'.$v->training_post_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                          {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                          {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                          <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                      </p>
                  </div>
              </div>
              @endforeach
              @else 
              <div class="col-md-12">
                  <div class="text-secondary text-center" style="text-align: center;">
                      No assignments found.
                  </div>
              </div>
              @endif
          </div>  
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            @if(@$advtinfo && @$advtinfo[2])
            <img src="{{asset('training_image')}}/{{@$advtinfo[2]}}" class="img-fluid pt-2 pb-2">
            @else
            <img src="images/add_img.jpg" class="img-fluid pt-2 pb-2">
            @endif
        </div>
    </div>
</div>

<div class="container pt30 pb30">
    <h5 class="text-left text-secondary font-weight-normal ">{{trans('bladelable.home.latestqueries')}} 
        <small class="pull-right "> <a href="{{asset('queryPostList')}}" class="text-danger"><button type="button" class="btn btn-dark btn-sm">
                    {{trans('bladelable.home.morelinks')}} <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                </button></a></small>
    </h5>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="row">
                <div class="col-md-3 col-3">
                    @if(@$advtinfo && @$advtinfo[4])
                    <img src="{{asset('training_image')}}/{{@$advtinfo[4]}}" class="img-fluid pt-2 pb-2">
                    @else
                    <img src="images/iso.jpg" class="img-fluid">
                    @endif
                </div>
                <div class="col-md-9 col-9">
                    <div class="addsection_grey bg-gray">
                        <a href="{{ asset('queryPostDetail/'.@$querypostinfo['0']->query_id)}}">
                        <h5 class="sidebar-title pl-3">{{@$querypostinfo['0']->title}}</h5>
                        </a>
                        <p class="pl-3 mb-0">{{substr(@$querypostinfo['0']->description, 0, 50)."..."}} <br>
                            Effort in man days : {{@$querypostinfo['0']->effort_mandays}} days <br>
                            Location : {{@$querypostinfo['0']->city_name}},{{@$state[@$querypostinfo['0']->states]}},{{@$country[@$querypostinfo['0']->country]}}<br>
                           
                        </p>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6 col-xs-12">
            <div class="row">

                <div class="col-md-9 col-9">
                    <div class="addsection_grey bg-gray ">
                        
                        <a href="{{ asset('queryPostDetail/'.@$querypostinfo['1']->query_id)}}">
                        <h5 class="sidebar-title pl-3">{{@$querypostinfo['1']->title}}</h5>
                        </a>
                        <p class="pl-3 mb-0">{{substr(@$querypostinfo['1']->description, 0, 50)."..."}} <br>
                            Effort in man days : {{@$querypostinfo['1']->effort_mandays}} days <br>
                            Location : {{@$querypostinfo['1']->city_name}},{{@$state[@$querypostinfo['1']->states]}},{{@$country[@$querypostinfo['0']->country]}}<br>
                            
                        </p>

                    </div>
                </div>
                <div class="col-md-3 col-3">
                    @if(@$advtinfo && @$advtinfo[5])
                    <img src="{{asset('training_image')}}/{{@$advtinfo[5]}}" class="img-fluid pt-2 pb-2">
                    @else
                    <img src="images/iso.jpg" class="img-fluid">
                    @endif

                </div>
            </div>

        </div>
        <div class="col-12">

            @if(@$advtinfo && @$advtinfo[3])
            <img src="{{asset('training_image')}}/{{@$advtinfo[3]}}" class="img-fluid pt-2 pb-2">
            @else
            <img src="images/add_img.jpg" class="img-fluid pt-2 pb-2">
            @endif
        </div>
    </div>

</div>

<!--subscription packages-->
<div class="container pt30">
    <h4 class="text-left text-secondary font-weight-normal pb30">
        {{trans('bladelable.home.packageheader')}}
    </h4>
</div>

<div class="container">
    <div class="card p-4">
                    <div class="row">
                            @if(count(@$package_list) > 0)
                            @foreach(@$package_list as $key => $val)
                            <div class="col-lg-4 col-md-4">
                                <div class="card text-center p-4">
                                <div class="plan-header ">
                                    <h4>{{$val->package_name}}</h4>
                                    <h1 class="text-info">{{config('assignment.price') . " ". $val->package_price}}</h1>
                                </div>
                                    
                                    <a data-toggle="modal" data-target="#packageModal" 
                                       data-packageid="{{$val->id}}"
                                       data-packagename="{{$val->package_name}}" 
                                       data-packageprice="{{$val->package_price}}"
                                       data-packageduration="{{$packageduration[$val->package_duration]}}"
                                       data-creditpoints="{{$val->credit_points}}"
                                       onclick="buyPackage(this)" style="cursor: pointer;">
                                        <button type="button" class="btn btn-primary btn-lg btn-block mb-3">
                                            @if ($val->package_price > 0)
                                            {{trans('bladelable.home.buypackage')}}
                                            @else
                                            {{trans('bladelable.home.subscribepackage')}}
                                            @endif
                                        </button>
                                    </a>
                                    
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check-circle-o"></i> {{$packageduration[$val->package_duration]}}</li>
                                    <li><i class="fa fa-check-circle-o"></i> {{$val->credit_points . " " . config('assignment.creditpoint')}}</li>
                                </ul>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="col-md-12">
                                <div class="text-secondary text-center" style="text-align: center;">
                                    {{trans('bladelable.home.nopackages')}}
                                </div>
                            </div>
                            @endif  
                            
                            <!---package info-->
                            <div class="modal fade" id="packageModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <h4 id="modal-packagetitle">
                                                </h4>
                                            <hr>
                                            <form id="subscriptionPackage" name="subscriptionPackage" action="{{ asset('insertPackage')}}" method="post">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="package_id" value="">
                                                <input type="hidden" name="credits_points" value="">
                                                <input type="hidden" name="package_duration" value="">
                                                <input type="hidden" name="package_price" value="">
                                                
                                                <div class="col-lg-4 col-md-4 text-center" style="margin: 0 auto;">
                                                    <div class="card text-center p-4">
                                                        <div class="plan-header">
                                                            <h4 id="package-name"></h4>
                                                            <h1 class="text-info">{{config('assignment.price') . " "}}<span id="package-price"></span></h1>
                                                        </div>
                                                        <hr>
                                                        <ul class="list-unstyled">
                                                            <li><i class="fa fa-check-circle-o"></i> <span id="package-duration"></span></li>
                                                            <li><i class="fa fa-check-circle-o"></i> <span id="package-credits"></span>{{" " . config('assignment.creditpoint')}}</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                
                                                <button type="submit" id="submitPackage" class="p-2 btn btn-primary text-center pull-right">
                                                    
                                                </button>
                                                <button class="p-2 btn btn-primary text-center pull-left" id="cancelBuy" type="button">
                                                    {{trans('formlabel.subscriptionmodal.cancalpackage')}}
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!---package info-->
        </div>
    </div>
</div>
<!--subscription packages-->
<script>
    $(document).ready(function () {
        var packageModal = JSON.parse(localStorage.getItem("buy_package"));
        if (packageModal) {
            $('#packageModal').modal('show');
            $("#package-name").text(packageModal.package_name);
            $("#package-price").text(packageModal.package_price);
            $("#package-duration").text(packageModal.package_duration);
            $("#package-credits").text(packageModal.credit_points);
            if (packageModal.package_price > 0) {
                $("#submitPackage").attr('onclick', 'buySubscription()');
                $("#modal-packagetitle").html("{{trans('formlabel.subscriptionmodal.buymessage')}}");
                $("#submitPackage").html("{{trans('formlabel.subscriptionmodal.submitpackage')}}");
            } else {
                $("#modal-packagetitle").html("{{trans('formlabel.subscriptionmodal.subscribemessage')}}");
                $("#submitPackage").html("{{trans('formlabel.subscriptionmodal.subscribepackage')}}");
            }
            
            $("input[name='package_id']").val(packageModal.id);
            $("input[name='credits_points']").val(packageModal.credit_points);
            $("input[name='package_duration']").val(packageModal.package_duration);
            $("input[name='package_price']").val(packageModal.package_price);
        }
//        alert(valuePackage.id + " " + valuePackage.package_name + " " + valuePackage.package_price + " "+valuePackage.package_duration + " "+valuePackage.credit_points);
    });
    
    //cancel package buy
    $("#cancelBuy").click(function(){
    $('#packageModal').modal('hide');
  localStorage.removeItem("buy_package");
//  location.reload();
}); 

function buyPackage(e) {
    var packageid = $(e).attr('data-packageid');
    var packageprice = $(e).attr('data-packageprice');
    var packageduration = $(e).attr('data-packageduration');
    var creditpoints = $(e).attr('data-creditpoints');
    var packagename = $(e).attr('data-packagename');
    
    $("#package-name").text(packagename);
    $("#package-price").text(packageprice);
    $("#package-duration").text(packageduration);
    $("#package-credits").text(creditpoints);
    if (packageprice > 0) {
        $("#submitPackage").attr('onclick', 'buySubscription()');
        $("#modal-packagetitle").html("{{trans('formlabel.subscriptionmodal.buymessage')}}");
        $("#submitPackage").html("{{trans('formlabel.subscriptionmodal.submitpackage')}}");
    } else {
        $("#modal-packagetitle").html("{{trans('formlabel.subscriptionmodal.subscribemessage')}}");
        $("#submitPackage").html("{{trans('formlabel.subscriptionmodal.subscribepackage')}}");
    }
    
    $("input[name='package_id']").val(packageid);
    $("input[name='package_price").val(packageprice);
    $("input[name='package_duration']").val(packageduration);
    $("input[name='credits_points']").val(creditpoints);
}

$("#subscriptionPackage").submit(function() {
    var del = confirm('Are you sure to subscribe this?');
    if (del) {
        return true;
    } else {
        return false;   
    }
});

    history.pushState(null, document.title, location.href);
    window.addEventListener('popstate', function (event)
    {
        history.pushState(null, document.title, location.href);
    });
</script>
@include('includes.footer')

