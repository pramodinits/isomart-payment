@include('includes/header')
<link rel="stylesheet" href="{{ asset('intltel/css/intlTelInput.css')}}">

<link rel="stylesheet" href="{{ asset('intltel/css/demo.css')}}">
<style>
    .genderSelect p {
        margin-bottom: 0;
    }
    .signup-wrapper div.has-error label[for="gender"],
    .signup-wrapper div.has-error label[for="cgender"],
    .signup-wrapper div.has-success label[for="gender"],
    .signup-wrapper div.has-success label[for="cgender"] {
        transform: translateY(0%);
    }
    .signup-wrapper input {
        margin: 0 0 5px !important;
    }
    .signup-wrapper input[type="radio"] {
        cursor: pointer;
    }
</style>
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="signup-wrapper col-md-6 col-sm-12">
                    <div class="text-center text-dark">
                        <h4><strong>Signup</strong></h4>
                        <br>
                    </div>
                    <form id="signup_form" name="signup_form" action="{{ asset('signup')}}" method="post"> 
                        {!! csrf_field() !!}
                        <div class="input-field form-group">
                            <select class="form-control" id="Usertype" name="Usertype">
                                <option value="">{{trans('formlabel.signup.usertype')}}</option>
                                <?php $i = 0; ?>
                                @foreach($usertype as $k=>$v)
                                <option value="{{@$k}}">{{@$v}}</option>
                                <?php $i++; ?>
                                @endforeach
                            </select>
                        </div>
                        <div id="basic-info">
                            <div class="input-field form-group">
                                <label for="Phone" class="active">{{trans('formlabel.signup.phone')}}*</label>
                                @if(@$phone)
                                <input type="tel" class="form-control" id="Phone" name="Phone" value="{{@$phone}}" onkeypress="return isNumberKey(event)">
                                @else
                                <input type="tel" class="form-control" id="Phone" name="Phone" onkeypress="return isNumberKey(event)">
                                @endif
                                <input type="hidden" value="+91" name="countrycode" id="countrycode" />
                                <i style="display: none; color: #b24d4b;" class="form-control-feedback fa fa-remove" data-bv-icon-for="phone-check"></i>
                                <div class="help-block has-error small" style="color: #b24d4b;"></div>
                            </div>
                            <div class="input-field form-group">
                                <label for="fName" class="">{{trans('formlabel.signup.firstname')}}*</label>
                                <input type="text" class="form-control" id="fName" name="fName">
                            </div>
                            <div class="input-field form-group">
                                <label for="lName" class="">{{trans('formlabel.signup.lastname')}}*</label>
                                <input type="text" class="form-control" id="lName" name="lName">
                            </div>
                            <div class="input-field form-group">
                                <label for="email" class="">{{trans('formlabel.signup.email')}}*</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                            <div class="form-group col s12 genderSelect">
                                <label for="gender">{{trans('formlabel.signup.gender')}}*</label>
                                <p>
                                    <input name="cgender" id="gender" value="1" type="radio">
                                    <label for="cgender">{{trans('formlabel.signup.gendermale')}}</label>&nbsp;&nbsp;
                                    <input name="cgender" id="gender" value="2" type="radio">
                                    <label for="cgender">{{trans('formlabel.signup.genderfemale')}}</label>
                                </p>
                            </div>
                            <div class="input-field form-group" id="show-organization">
                                <label for="Cname" class="">{{trans('formlabel.signup.company')}}*</label>
                                <input type="text" class="form-control" id="Cname" name="Cname">
                            </div>
                            <div class="input-field form-group">
                                <label for="Password" class="">{{trans('formlabel.signup.password')}}*</label>
                                <input type="password" class="form-control" id="Password" name="Password" required>
                            </div>
                            <div class="input-field form-group">
                                <label for="Confirm_password" class="">{{trans('formlabel.signup.confirmpassword')}}*</label>
                                <input type="password" class="form-control" id="Confirm_password" name="Confirm_password" required>
                            </div>
                            <div class="input-field form-group">
                                <input type="checkbox" class="form-control" name="Term" id="Term"><span>&nbsp;&nbsp;
                                    {{trans('formlabel.signup.termpolicy')}}<a href="#">{{trans('formlabel.signup.termpolicyurl')}}</a>
                                </span><br/>
                            </div>
                        </div>

                        <button type="submit" id="sbmt" class="btn btn-primary btn-block btn-lg">{{trans('formlabel.signup.signupbutton')}}</button>
                        <input type="reset" style="display: none;" name="resetform" id="resetform" value="Reset" class="btn btn-block btn-primary btn-lg" style="display: none;">
                        <div class="form-group col-md-12 mt-2">
                            <span class="col-md-6 text-left">
                                <a href="{{asset('signin')}}" class="forgot-password">{{trans('formlabel.signup.loginlink')}}</a>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="{{ asset('js/ajaxupload.js')}}"></script>
<script>
                                    function isNumberKey(evt) {
                                        var charCode = (evt.which) ? evt.which : event.keyCode
                                        if (charCode > 31 && (charCode < 48 || charCode > 57))
                                            return false;
                                        return true;
                                    }

                                    $(document).ready(function () {
                                        var phone = $("#Phone").val();
                                        if (phone) {
                                            checkPhone();
                                            $('label[for="Phone"]').addClass("has-value");
                                        }

                                    });

                                    var typingTimer;                //timer identifier
                                    var doneTypingInterval = 1000;  //time in ms, 1 second for example
                                    var input_phone = $('#Phone');

                                    input_phone.on('keyup', function () {
                                        $("#Phone").val().replace(/[^\d]/g, '');
                                        if (input_phone.val().length >= 8) {
                                            clearTimeout(typingTimer);
                                            typingTimer = setTimeout(checkPhone, doneTypingInterval);
                                        } else {
                                            $("div.help-block").hide();
                                            return false;
                                        }
                                    });
                                    function checkPhone() {
                                        var phone = $("#Phone").val();
                                        var countrycode = $("#countrycode").val();
                                        var _token = "<?php echo csrf_token(); ?>";
//    $("#custom_loader").show();
                                        if (phone) {
                                            $.ajax({
                                                method: "POST",
                                                url: "{{url('/checkphone')}}",
                                                data: {
                                                    "phone": phone,
                                                    "countrycode": countrycode,
                                                    "_token": _token
                                                },
                                                cache: false,
                                                success: function (res) {
                                                    if (res === "success") {
                                                        $("#custom_loader_alert_box").hide();
                                                        $("i[data-bv-icon-for=Phone]").hide();
                                                        $("i[data-bv-icon-for=phone-check]").show();
                                                        $("div.help-block").html("Mobile number is already registered. Kindly <a href='{{asset('signin/')}}/" + phone + "/assignment_id'>Login</a>").show();
                                                    } else {
                                                        $("div.help-block").hide();
                                                        $("i[data-bv-icon-for=Phone]").show();
                                                        $("i[data-bv-icon-for=phone-check]").hide();
                                                    }
                                                }
                                            });
                                        }
                                    }


                                    $("#Usertype").change(function () {
                                        var selectVal = $(this).val();
                                        if (selectVal !== "") {
                                            $("#basic-info").show();
                                            if (selectVal >= 5) {
                                                $("div#show-organization").show();
                                            } else {
                                                $("div#show-organization").hide();
                                            }
                                        } else {
                                            $("#basic-info").hide();
                                        }
                                    });

                                    $(".input-field :input").focus(function () {
                                        $("label[for='" + this.name + "']").addClass("active");
                                    }).blur(function () {
                                        $("label").removeClass("active");
                                    });


                                    $(document).ready(function () {
                                        $('#signup_form').bootstrapValidator({
                                            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                                            feedbackIcons: {
                                                valid: 'fa fa-check',
                                                invalid: 'fa fa-remove',
                                                validating: 'glyphicon glyphicon-refresh'
                                            },
                                            fields: {
                                                "fName": {
                                                    validators: {
                                                        notEmpty: {
                                                            message: "This field is required."
                                                        },
                                                        stringLength: {
                                                            min: 3,
                                                            max: 20,
                                                            message: 'The first name must be within 3 to 20 characters.'
                                                        }
                                                    }
                                                },
                                                "lName": {
                                                    validators: {
                                                        notEmpty: {
                                                            message: "This field is required."
                                                        },
                                                        stringLength: {
                                                            min: 3,
                                                            max: 20,
                                                            message: 'The last name must be within 3 to 20 characters'
                                                        }
                                                    }
                                                },
                                                "email": {
                                                    validators: {
                                                        notEmpty: {
                                                            message: "This field is required."
                                                        }
                                                    }
                                                },
                                                "cgender": {
                                                    validators: {
                                                        notEmpty: {
                                                            message: "This field is required."
                                                        }
                                                    }
                                                },
                                                "Phone": {
                                                    validators: {
                                                        notEmpty: {
                                                            message: "This field is required."
                                                        },
                                                        stringLength: {
                                                            min: 8,
                                                            message: 'Please enter valid phone number.'
                                                        }
                                                    }
                                                },
                                                "Cname": {
                                                    validators: {
                                                        notEmpty: {
                                                            message: "This field is required."
                                                        }
                                                    }
                                                },
                                                "Usertype": {
                                                    validators: {
                                                        notEmpty: {
                                                            message: "This field is required."
                                                        }
                                                    }
                                                },
                                                "Password": {
                                                    validators: {
                                                        notEmpty: {
                                                            message: "This field is required."
                                                        },
                                                        stringLength: {
                                                            min: 6,
                                                            max: 20,
                                                            message: 'The password must be within 6 to 20 characters.'
                                                        }
                                                    }
                                                },
                                                "Confirm_password": {
                                                    validators: {
                                                        notEmpty: {
                                                            message: "This field is required."
                                                        },
                                                        identical: {
                                                            field: 'Password',
                                                            message: 'The password and confirm password are not same'
                                                        }
                                                    }
                                                },
                                                "Term": {
                                                    validators: {
                                                        notEmpty: {
                                                            message: "This field is required."
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                                .on('error.form.bv', function (e) {
                                                    var getPhone = $("#Phone").val();
                                                    if (getPhone) {
                                                        var number = getPhone.replace(/[^\d]/g, '');
                                                        $("#Phone").val(number);
                                                    }
//                    if ($('#email') && $('#email').val().length === 0 && $('#email').parents('.form-group').hasClass('has-success')) {
//                        $('#email').parents('div.form-group').removeClass('has-success');
//                        $('#email').parents('.form-group').find('.form-control-feedback[data-bv-icon-for="email"]').hide();
//                    }
                                                })
                                                .on('success.form.bv', function (e) {
                                                    $("#Phone").val().replace(/[^\d]/g, '');
                                                    var setPassword = $("#Password").val();
                                                    var resetPassword = $("#Confirm_password").val();
                                                    if (setPassword !== resetPassword) {
                                                        $("i[data-bv-icon-for=Confirm_password]").addClass("fa fa-remove").css('color', '#b24d4b');
                                                        $("small[data-bv-validator=identical]").show();
                                                        $('.help-block').css('color', '#b24d4b');
                                                        return false;
                                                    } else {
                                                        $("i[data-bv-icon-for=Confirm_password]").addClass("fa fa-check").css('color', '#3c763d');
                                                        return true;
                                                    }
                                                    //$('#custom_loader').show();
                                                    return true;
                                                });
//                
                                    });
//$('#signup_form').bootstrapValidator('enableFieldValidators', 'email', 'notEmpty', false);

</script>
<script src="{{ asset('intltel/js/intlTelInput.js')}}"></script>
<script>
                                    $("#Phone").intlTelInput({
                                        // allowDropdown: false,
                                        // autoHideDialCode: false,
                                        // autoPlaceholder: "off",
                                        // dropdownContainer: "body",
                                        // excludeCountries: ["us"],
                                        // formatOnDisplay: false,
                                        // geoIpLookup: function(callback) {
                                        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                                        //     var countryCode = (resp && resp.country) ? resp.country : "";
                                        //     callback(countryCode);
                                        //   });
                                        // },
                                        // hiddenInput: "full_number",
                                        initialCountry: "in",
//    autoFormat: true,
                                        // nationalMode: false,
                                        //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                                        // placeholderNumberType: "MOBILE",
                                        // preferredCountries: ['cn', 'jp'],
                                        separateDialCode: true,
                                        utilsScript: "{{ asset('intltel/js/utils.js')}}"
                                    });
// on blur: validate    
                                    $('.country').click(function () {
                                        $('#countrycode').val($(this).children('.dial-code').html());
                                    });
                                    $('.country').click(function () {
                                        if ($("#Phone").val().length >= 8) {
                                            checkPhone();
                                        }
                                    });
</script>