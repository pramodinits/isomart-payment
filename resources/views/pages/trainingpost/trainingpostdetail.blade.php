@include('includes.header')

<style>
    .tooltip {
        position: relative;
        display: inline-block;
        opacity: 1;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        width: 100%;
        background-color: black;
        color: #fff;
        text-align: left;
        border-radius: 6px;
        padding: 5px;

        /* Position the tooltip */
        position: absolute;
        top: 90%;
        left: 0%;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }
    #queryTooltip {
        width: 100% !important;
        top: 90%;
        right: 0%;
    }
    #queryTool .btn-sm {
        padding: 5% 9% !important;
    }
    #queryToolAdmin .btn-sm {
        padding: 2% 2.5% !important;
    }
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        padding: 2px 5px;
        border-radius: 5px;
    }
    .genderSelect p {
        margin-bottom: 0;
    }
</style>



<div class="container">
    <div class="main-content">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="details  border-right">
                            <h5 class="profile-no">{{config('assignment.refid') . @$trainingpostdetail->ref_id}} 
                                
                            </h5>
                            <div>{{@$trainingpostdetail->traning_name}} 
                                
                            </div>
                            <div>{{trans('formlabel.auditdetail.postedby')}}<span class="text-primary"><b>{{ucfirst(@$userdetails->fname) . " " . ucfirst(@$userdetails->lname)}}</b></span> </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!---send, edit & delete proposal button-->
            <div class="col-md-6 col-sm-6">
                
                <div class="btnsection mt-2 pull-right ml-5">
                    <h6 class="text-center">{{trans('formlabel.auditdetail.budget')}}</h6>
                    <div class="buget-price">{{config('assignment.price') . " ". @$trainingpostdetail->fee_range}}</div>
                    
                </div>
                @if(@$trainingpostdetail->image)
                <div class="btnsection mt-2 pull-right">
                    <img src="{{asset('training_image')}}/{{@$trainingpostdetail->image}}" height="80" width="80" />
                </div>
                @endif
            </div>
            <!---send, edit & delete proposal button-->
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container">
    <!--assignment details-->
    <hr>
    <h5>{{trans('messagelabel.training.detail')}}</h5>
    <div class="row">
        <div class="col-md-12 mb30">
            <div class="card card-body">
                <p class="text-grey" style="font-size:1.3rem;">{{@$trainingpostdetail->session_details}}</p>
                @if(@$trainingpostdetail->mode == 2)
                <h6 class="text-dark">{{trans('messagelabel.training.training_mode')}} : {{@$trainingmode[@$trainingpostdetail->mode]}}</h6>
                @else
                <h6 class="text-dark">{{trans('messagelabel.training.training_mode')}} : {{@$trainingmode[@$trainingpostdetail->mode]}}</h6>
                <h6 class="text-dark">{{trans('messagelabel.training.traininglocation')}} : 
                    
                    @foreach(@$locationname as $key=>$val)
                    <span class="locationCity">
                        <i class="fa fa-map-marker pr-1 text-danger" aria-hidden="true"></i>
                        {{$val->city_name . ", " . $val->stateName . ", " . $val->countryName}}
                        
                    </span>&nbsp;
                    @endforeach
                </h6>
                
                @endif
                <hr>
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="inforamation-section">
                            <div>{{trans('messagelabel.training.batchno')}} : <span class="text-dark">{{@$trainingpostdetail->batch_no}}</span> </div>
                            <div>{{trans('messagelabel.training.standard')}} : <span class="text-dark">{{@$standardlist[@$trainingpostdetail->standard]}}</span> </div>
                            <div>{{trans('messagelabel.training.training_type')}} : <span class="text-dark">{{@$trainingtypelist[@$trainingpostdetail->training_type]}}</span> </div>
                            
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="inforamation-section">
                            <div>{{trans('formlabel.auditdetail.startdate')}}<span class="text-dark">{{Carbon\Carbon::parse(@$trainingpostdetail->start_date)->format(config('assignment.dateformat'))}}</span> </div>
                            <div>{{trans('formlabel.auditdetail.endadte')}}<span class="text-dark">{{Carbon\Carbon::parse(@$trainingpostdetail->end_date)->format(config('assignment.dateformat'))}}</span> </div>
                            <div>{{trans('messagelabel.training.status')}} : <span class="text-dark">{{@$status[@$trainingpostdetail->status]}}</span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!--assignment details-->
</div>








<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>

@include('includes.footer')
