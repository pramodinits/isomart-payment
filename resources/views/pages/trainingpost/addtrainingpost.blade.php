@include('includes.header')

<style>
    #loading-image {
        position: absolute;
        z-index: 1;
        right: -5px;
        top: 7px;
    }
    div#locationCity span {
        padding: 2px 5px !important;
    }
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        border-radius: 5px;
    }
    .fa-times {
        cursor: pointer;
    }
    .form-group i.form-control-feedback {
        display: none !important;
    }
    .form-group small[data-bv-for="audit_effort[]"],
    .form-group small[data-bv-for="country"],
    .form-group small[data-bv-for="states"]
    {
        color: #b24d4b !important;
    }
    i#addLocation {
        font-size: 1rem;
        cursor: pointer;
        position: absolute;
        right: 90%;
        top: 20%;
    }
</style>

<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="pr-5">
                                <h4>@if(@$traininginfo->training_post_id) Edit @else Add @endif {{trans('messagelabel.training.trainingpost')}}</h4>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="card agreementform">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <div id="auditrequest">
                                <form name="trainingpost" id="trainingpost" method="post" action="{{ asset('addTrainingPost') }}" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="user_id" id="user_id" value="{{@Auth::user()->id}}">
                                    <input type="hidden" name="id_update" id="id_update" value="{{@$traininginfo->training_post_id}}">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-12">
                                            <label>{{trans('messagelabel.training.trainingname')}}</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="training[traning_name]" value="{{@$traininginfo->traning_name}}">
                                            </div>
                                            <label>{{trans('formlabel.postassignmenttraining.batchno')}}</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="training[batch_no]" value="{{@$traininginfo->batch_no}}">
                                            </div>
                                            <label class="mt-2">{{trans('formlabel.postassignmenttraining.startdate')}}</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="startDate" id="startDate" onkeydown="return false" value="{{@$traininginfo->start_date}}">
                                            </div>
                                            <label class="mt-2">{{trans('formlabel.postassignmenttraining.enddate')}}</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="endDate" id="endDate" onkeydown="return false" value="{{@$traininginfo->end_date}}">
                                            </div>
                                            
                                            <label class="mt-2">{{trans('formlabel.postassignmenttraining.standard')}}</label>
                                            <div class="form-group">
                                                <select class="form-control" name="training[standard]" id="isoCode" value="{{@$traininginfo->standard}}">
                                                    <option value="">{{trans('formlabel.postassignmenttraining.standardoption')}}</option>
                                                    @foreach(@$standardlist as $k=>$v)
                                                    <option value="{{@$k}}" @if(@$k == @$traininginfo->standard) selected @endif>{{@$v}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <label class="mt-2">{{trans('formlabel.postassignmenttraining.trainingtype')}}</label>
                                            <div class="form-group">
                                                <select class="form-control" name="training[training_type]">
                                                    <option value="">Select Training type</option>
                                                    @foreach(@$trainingtypelist as $k=>$v)
                                                    <option value="{{@$k}}" @if(@$k == @$traininginfo->training_type) selected @endif>{{@$v}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-12">
                                            <label class="mt-2">{{trans('messagelabel.training.trainingimage')}}</label>
                                            <div class="form-group">
                                                <input type="file" class="form-control" name="img" <?= @$traininginfo->image ? "" : "required"; ?> accept="image/*">
                                                @if(@$traininginfo->image)
                                                <br/>
                                                <input type="hidden" name='photo_hid' value="{{@$traininginfo->image}}" class="form-control" />
                                                <img src="{{asset('training_image')}}/{{@$traininginfo->image}}" height="50" width="50" />
                                                @endif
                                            </div>
                                            <label class="mt-2">{{trans('formlabel.postassignmenttraining.sessiondetails')}}</label>
                                            <div class="form-group">
                                                <textarea class="form-control" name="training[session_details]" id="briefInfo" rows="3" cols="25">{{@$traininginfo->session_details}}</textarea>
                                            </div>
                                            <label class="mt-2">{{trans('formlabel.postassignmenttraining.mode')}}</label>
                                            <div class="form-group">
                                                <select class="form-control" id="trainingMode" name="training[mode]" onchange="setLocation(this.value);">
                                                    <option value="">Select Mode</option>
                                                    @foreach(@$trainingmode as $k=>$v)
                                                    <option value="{{@$k}}"@if(@$k == @$traininginfo->mode) selected @endif>{{@$v}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div id="setloc" @if(@$traininginfo->training_post_id && @$traininginfo->mode ==1)style="display:block;" @else style="display:none;" @endif>
                                            <label class="mt-2">{{trans('formlabel.postassignmenttraining.location')}}</label>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <select class="form-control" name="country" id="countries" onchange="getLocationInfo('countries', 'states');">
                                                            <option value="">Select Country</option>
                                                            @foreach(@$countryList as $k=>$v)
                                                            <option value="{{@$v->id}}">{{@$v->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <select class="form-control" name="states" id="states" onchange="getLocationInfo('states', 'cities');">
                                                            <option value="">Select State</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <img id="loading-image" src="{{asset('images')}}/ajax-loader.gif" alt="Loading..." />
                                                        <input type="text" style="position: relative;" placeholder="{{trans('formlabel.profilecontact.addresscity')}}" class="form-control" id="city" name="city[]" value="" data-bv-notempty-message="Please enter a city name">
                                                    </div> 
                                                    <div class="form-group col-md-6">
                                                        <i id="addLocation" class="fa fa-plus-circle text-success" aria-hidden="true"></i>
                                                    </div> 
                                                </div>
                                                <div id="reloadDiv">
                                                    <div class="form-group" id="locationCity">
                                                        @if(@$traininglocation)
                                                        @foreach(@$traininglocation as $k => $v)
                                                        <span class="locationCity">
                                                            {{$v->city_name . ", " . $v->stateName . ", " . $v->countryName . "&nbsp;"}}
                                                            <a href="javascript:void(0);" onclick="deletePostLocation('{{$v->location_id}}')"><i class="fa fa-times"></i></a>
                                                        </span>&nbsp;
                                                        @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <label class="mt-2">{{trans('formlabel.postassignmenttraining.feerange')}}</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="training[fee_range]" onkeypress="return isNumberKey(event)" value="{{@$traininginfo->fee_range}}">
                                            </div>
                                            <label class="mt-2">{{trans('formlabel.postassignmenttraining.status')}}</label>
                                            <div class="form-group">
                                                <select class="form-control" name="training[status]">
                                                    <option value="">Select Status</option>
                                                    @foreach(@$status as $k=>$v)
                                                    <option value="{{@$k}}"@if(@$k == @$traininginfo->status) selected @endif>{{@$v}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary text-center btn-md pull-right mt-2">{{trans('formlabel.postassignmenttraining.rowaction')}}</button>
                                    </div>
                                </form>

                                
                            </div>
                            <div class="clearfix"></div>
                            <br/>
                            <div class="contact-address col-md-12">
                            <div class="d-flex text-white">
                                <div class="p-1 bg-warning col-2 pl-0 border-right">{{trans('messagelabel.training.ref_id')}}</div>
                                <div class="p-1 bg-warning col-2 pl-0 border-right">{{trans('messagelabel.training.traning_name')}}</div>
                                <div class="p-1 bg-warning col-1 border-right">{{trans('messagelabel.training.start_date')}}</div>
                                <div class="p-1 bg-warning col-1 border-right">{{trans('messagelabel.training.end_date')}}</div>
                                <div class="p-1 bg-warning col-3 border-right">{{trans('messagelabel.training.training_type')}}</div>
                                <div class="p-1 bg-warning col-2 border-right">{{trans('messagelabel.training.training_mode')}}</div>
                                <div class="p-0 bg-warning col-1 text-center border-right">{{trans('messagelabel.training.rowaction')}}</div> 
                            </div>
                            @if(count(@$traininglist)>0)
                            <?php
                            $i = 1;
                            ?>
                            @foreach(@$traininglist as $key=>$val)
                            <div class="d-flex text-secondary contactdetails" id="{{@$val->training_post_id}}">
                                <div class="p-1 col-2 pl-0"><a href="{{ asset('trainingPostDetail/'.@$val->training_post_id)}}">{{@$val->ref_id}}</a></div>
                                <div class="p-1 col-2 pl-0"><a href="{{ asset('trainingPostDetail/'.@$val->training_post_id)}}">{{@$val->traning_name}}</a></div>
                                <div class="p-1 col-1 pl-0">{{Carbon\Carbon::parse(@$val->start_date)->format(config('assignment.dateformat'))}}</div>
                                <div class="p-1 col-1 pl-0">{{Carbon\Carbon::parse(@$val->end_date)->format(config('assignment.dateformat'))}}</div>
                                <div class="p-1 col-3 pl-0">{{@$trainingtypelist[@$val->training_type]}}</div>
                                <div class="p-1 col-2 pl-0">{{@$trainingmode[@$val->mode]}}</div>
                                <div class="p-0 col-1 text-center">
                                    <a href="{{ asset('trainingPostDetail/'.@$val->training_post_id)}}">
                                        <i class="fa fa-eye text-success" title="View Training Post"></i>
                                    </a>&nbsp;&nbsp;
                                    <a href="{{ asset('addTrainingPost/'.@$val->training_post_id)}}">
                                        <i class="fa fa-edit text-success" title="Edit Training Post"></i>
                                    </a>&nbsp;&nbsp;
                                    <a href="javascript:void(0);" onclick="deleteTrainingPost('{{$val->training_post_id}}')">
                                        <i class="fa fa-remove text-danger" aria-hidden="true" title="Delete Training Post"></i>
                                    </a>
                                </div>
                            </div>
                            <?php $i++; ?>
                            @endforeach
                            @else 
                            <div class="d-flex contactdetails text-secondary">
                                <div class="col-12 text-center">No data found.</div>
                            </div>
                            @endif
                        </div>
                        </div>
                    </div>
                </div>

                

            </div>
        </div>
    </div>
</div>

<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
                        $('#loading-image').hide();
                    });
     
     //datepicker
                    $(function () {
                        $('#startDate').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            minDate: 0,
                            dateFormat: 'dd-mm-yy',
                            onSelect: function (selected) {
                                $('#trainingpost').bootstrapValidator('revalidateField', 'startDate');
                                $("#endDate").datepicker("setDate", null);
                                $("#endDate").datepicker("option", "minDate", selected)
                            }
                        });
                        $('#endDate').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            minDate: 0,
                            dateFormat: 'dd-mm-yy',
                            onSelect: function (selected) {
                                $('#trainingpost').bootstrapValidator('revalidateField', 'endDate');
                            }
                        });
                    });
//datepicker
     
    //get location list
                    function getLocationInfo(id, tbl) {
                        if (id == "states") {
                            $('#loading-image').show();
                            $('#city').val("");
                        } else {
                            $('#city').val("");
                        }
                        var selected_val = $('select#' + id + ' option:selected').val();
                        var url = "{{url('/getLocationInfo')}}";
                        $.post(url, {"selected_val": selected_val, "id": id, "tbl": tbl, "_token": '<?= csrf_token(); ?>'}, function (res) {
                            if (tbl == 'cities') {
                                availableTags = JSON.parse(res);
                                $('#loading-image').hide();
                                $("#city").autocomplete({
                                    source: availableTags
                                });
                            } else {
                                $("#" + tbl).html(res);
                                var total = $('select#' + tbl + ' option').length;
                                $("#" + tbl + "_cnt").html(" <b>(" + (total - 1) + ")</b>");
                            }
                        });
                    }
//get location list


//add multiple effort & location
                    $("#addLocation").click(function () {
                        var city = $("#city").val();
                        var stateid = $("#states").val();
                        var countryid = $("#countries").val();
                        var statename = $("#states option:selected").text();
                        var countryname = $("#countries option:selected").text();

                        if (city !== "" && countryid !== "" && stateid !== "") {
                            $('#countries, #states').prop('selectedIndex',0);
                            $('#city').val('');
                            $('#countries, #states, #city').parents('div.form-group').removeClass('has-error');
                            $("#locationCity").show();
                            var res = '<input type="hidden" name=cityname[] value="' + city + '" >\n\
                <input type="hidden" name=stateid[] value="' + stateid + '" >\n\
<input type="hidden" name=countryid[] value="' + countryid + '" >\n\
' + city + ', ' + statename + ', ' + countryname + '<span class="removeLocation"><i class="fa fa-times"></i></span>';
                            $('<span class="locationCity">' + res + '</span>&nbsp;').appendTo("#locationCity");
                            $(".removeLocation").click(function () {
                                $(this).parent().remove();
                            });
                        } else {
                            if(countryid == "") {
//                                $('#countries').bootstrapValidator('revalidateField', 'country');
                                $('#countries').parents('div.form-group').addClass('has-error');
                            } else if (stateid == "") {
                                $('#states').parents('div.form-group').addClass('has-error');
                            } else if (city == "") {
                                $('#city').parents('div.form-group').addClass('has-error');
                            }
                        }
                    });
//add multiple effort & location

//delete loation info
function deletePostLocation(location_id) {
        if (location_id) {
var url = "{{url('/deletePostLocation')}}";
        $.post(url, {'location_id': location_id, '_token': "<?= csrf_token(); ?>"}, function (data) {
        if(data == "Success"){
            $("#reloadDiv").load(" #locationCity");
            return true;
        } else {
            return false;
        }
        });
} else {
return false;
}
}
//delete loation info
    </script>
<script>
    function deleteTrainingPost(id) {
        var del = confirm('Are you sure to remove this?');
            if (del) {
                $("#"+ id).fadeOut(400, function() {
                $("#"+ id).remove();
            });
            var url = "{{url('/deleteTrainingPost')}}";
            $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
            //location.reload();
            });
        } else {
            return false;
        }
    }
    function setLocation(val)
    {
        if(val == 1)
        {
            $('#setloc').show();
        } else {
            $('#location').val(""); 
            $('#setloc').hide(); 
        }
    }
</script>
<script>
    $('#trainingpost').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "training[traning_name]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "training[batch_no]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "startDate": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "endDate": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "training[training_type]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "training[session_details]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "training[mode]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "training[standard]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "training[location]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "training[fee_range]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "training[status]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            }
        }
    })

            .on('success.form.bv', function (e) {
                if ($("#trainingMode").val() == 1) {
                    if ($('.locationCity').text() == "") {
                            alert("Please add location.");
                        $("button.btn-primary").removeAttr("disabled");
                            return false;
                            } else {
                                return true;
                            }
                }
                
//                $('#confirmModal').show();
//                $("#content-form").html($("#assignment-request").serialize());
                return true;
            });
            
            
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
@include('includes.footer')