@include('includes.header')
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h3 class="text-left text-secondary font-weight-normal p-4">Training Advertisement</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
        <div class="row">
            <div class="col-lg-12 mb40">
                <div class="row">
                  @if(count(@$trainingpostinfo) > 0)
                  @foreach(@$trainingpostinfo as $k=>$v)
                  <div class="col-md-4 mb30">
                      <div class="card card-body text-center">
                          <h4 class="card-title">
                              <a href="{{ asset('trainingPostDetail/'.$v->training_post_id)}}">
                                  @if(strlen($v->traning_name) > 20)
                                  {{substr(ucfirst($v->traning_name), 0, 20) . "..."}}
                                  @else
                                  {{ucfirst($v->traning_name)}}
                                  @endif
                              </a>
                          </h4>
                          <div class="d-flex flex-row mx-auto col-12">
                              <div>
                                  {{trans('messagelabel.training.feerange')}} : <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $v->fee_range}}</span>
                              </div>
                          </div>

                          <hr/>
                          <p class="card-text text-left">
                              {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('trainingPostDetail/'.$v->training_post_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                              {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                              {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                              <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                          </p>
                      </div>
                  </div>
                  @endforeach
                  @else 
                  <div class="col-md-12">
                      <div class="text-secondary text-center" style="text-align: center;">
                          No assignments found.
                      </div>
                  </div>
                  @endif
              </div>  
            </div>
        </div>
    
    
</div>
        </div>
    </div>
</div>


@include('includes.footer')