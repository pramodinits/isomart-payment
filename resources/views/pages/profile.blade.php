@include('includes.header')
<style>
    .tab .nav-tabs li .active {
        background:#33b6cb !important;
    }
    .tab .nav-tabs li:nth-last-child(2) .active::after
    {
        border-left: none !important;
    }
    .tab .nav-tabs li:nth-last-child(2) .active::before {
        border-left: none !important;
    }
    .tab .nav-tabs li .active::before {
        border-left: 15px solid #33b6cb !important;
    }
    .tab .nav-tabs li .active::after{
        border-left: 17px solid #33b6cb !important;
    }
    .tab .nav-tabs li a {
        font-weight: bold;
/*        font-size: 15px;*/
        letter-spacing: 1px;
    }
    a.active {
        color: #fff !important;
    }
</style>
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <a class="edit-icons" data-toggle="modal" data-target="#imageModal"><i class="fa fa-edit text-light"></i></a>
                                    @if(Auth::user()->profile_img)
                                    <img src="{{asset('profile_image')}}/{{@Auth::user()->profile_img}}" class="rounded img-fluid img-profile" />
                                    @else
                                    <img src="{{asset('images')}}/{{ Auth::user()->gender == 1 ? 'profile_default.jpg' : 'profile_female.jpg' }}" class="rounded img-fluid img-profile">
                                    @endif
                                </div>
                                <div class="col-md-8">
                                    <div class="details mt-4 border-right">
                                        <h5>{{@Auth::user()->fname ." ". @Auth::user()->lname }}
                                            <small>
                                                <a data-toggle="modal" data-target="#nameModal" style="cursor: pointer;">
                                                    <i class="fa fa-edit pl-2 font16 text-secondary"></i>
                                                </a>
                                            </small>
                                        </h5>
                                        <?php
                                        $usertype = config('selecttype.usertypelist');
                                        ?>
                                        <i class="fa fa-mobile icons pr-4" title="Phone"></i>{{"+" . @Auth::user()->countrycode ."-". @Auth::user()->phone}}<br>
                                        <i class="fa fa-envelope  pr-3" title="Email"></i>{{@Auth::user()->email}}<br>
                                        <i class="fa fa-user icons pr-3" title="User Role"></i>{{$usertype[@Auth::user()->usertype]}}
                                        <p class="font-weight-light font-italic small"><i class="fa fa-calendar  pr-3"></i> Last update on {{date('d-m-Y', strtotime(@Auth::user()->updated_at))}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="btnsection mt-5 pull-right">
                                <a href="{{ asset('publicProfileView/'.@Auth::user()->id)}}">
                                    <button type="button" class="btn btn-dark">{{trans('formlabel.profile.publicview')}}</button>
                                </a>
                                <!--<button type="button" class="btn btn-orange">{{trans('formlabel.profile.portfolio')}}</button>-->
                                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#pwdModal">{{trans('formlabel.profile.resetpassword')}}</button>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <div class="list-group list-group-horizontal">
                                    <a href="#" class="list-group-item border-0 ">
                                        <small><i class="fa fa-square text-dark pr-2"></i></small>{{trans('formlabel.profile.nodata')}}
                                    </a>
                                    <a href="#" class="list-group-item  border-0">
                                        <small><i class="fa fa-square text-orange pr-2"></i></small>{{trans('formlabel.profile.incompletedata')}}
                                    </a>
                                    <a href="#" class="list-group-item border-0">
                                        <small><i class="fa fa-square text-success pr-2"></i></small>{{trans('formlabel.profile.completedata')}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--attribute tab-->
                <div class="clearfix"></div>
              
                    <div class="row">

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tab" role="tabpanel">                                         
                                        <ul class="nav nav-tabs tabs-default" role="tablist">
                                            <li role="presentation">
                                                <a class="generalstatus {{ @$profile_activetab == 1 ? 'show active' : '' }}" href="#home" aria-controls="home" role="tab" data-toggle="tab">{{trans('formlabel.profile.generaltab')}}</a>
                                            </li>
                                            <li role="presentation">
                                                <a class="addressstatus {{ @$profile_activetab == 2 ? 'show active' : '' }}" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">{{trans('formlabel.profile.contacttab')}}</a>
                                            </li>
                                            <li role="presentation">
                                                <a class="sicstatus {{ @$profile_activetab == 3 ? 'show active' : '' }}" href="#sic_code" aria-controls="home" role="tab" data-toggle="tab">{{trans('formlabel.profile.codetab')}}</a>
                                            </li>
                                            <li role="presentation">
                                                <a class="standardstatus {{ @$profile_activetab == 4 ? 'show active' : '' }}" href="#standards" aria-controls="profile" role="tab" data-toggle="tab">{{trans('formlabel.profile.standardtab')}}</a>
                                            </li>
                                            <li role="presentation" style="display: none;">
                                                <a class="industrystatus {{ @$profile_activetab == 5 ? 'show active' : '' }}" href="#industry" aria-controls="home" role="tab" data-toggle="tab">{{trans('formlabel.profile.industrytab')}}</a>
                                            </li>
                                        </ul>
                                        
                                        
 <div class="card">
                                        <!--tab content-->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade {{ @$profile_activetab == 1 ? 'in show active' : '' }}" id="home">
                                                @include('pages.profilegeneral')
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade {{ @$profile_activetab == 2 ? 'in show active' : '' }}" id="profile">
                                                @include('pages.profilecontact')
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade {{ @$profile_activetab == 3 ? 'in show active' : '' }}" id="sic_code">
                                                @include('pages.profileisiccode')
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade {{ @$profile_activetab == 4 ? 'in show active' : '' }}" id="standards">
                                                @include('pages.profilestandard')
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade {{ @$profile_activetab == 5 ? 'in show active' : '' }}" id="industry" style="display: none;">
                                                @include('pages.profileindustry')
                                            </div>
                                        </div>
                                        <!--tab content-->
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!--attribute tab-->

                <!-- Modal -->
                <div class="modal fade" id="nameModal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title text-left">{{trans('formlabel.profile.nametitle')}}</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form id="profile_form" name="profile_form" action="{{ asset('updateprofile')}}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="form-group input-field">
                                        <input type="text" class="form-control" placeholder="{{trans('formlabel.profile.profilefirstname')}}" id="fName" name="fName" value="{{@Auth::user()->fname}}">
                                    </div>
                                    <div class="form-group input-field">
                                        <input type="text" class="form-control" placeholder="{{trans('formlabel.profile.profilelastname')}}" id="lName" name="lName" value="{{@Auth::user()->lname}}">
                                    </div>
                                    <input type="hidden" name="update" value="profilename">
                                    <button type="submit" class="btn btn-primary text-center">{{trans('formlabel.profile.namesubmitbutton')}}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="pwdModal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title text-left">{{trans('formlabel.profile.passwordtitle')}}</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form id="profile_form" name="profile_form" action="{{ asset('updateprofile')}}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="form-group input-field">
                                        <input type="password" class="form-control" placeholder="{{trans('formlabel.profile.currentpassword')}}*" id="oldPassword" name="oldPassword">
                                    </div>
                                    <div class="input-field form-group">
                                        <input type="password" placeholder="{{trans('formlabel.profile.newpassword')}}*" class="form-control" id="newPassword" name="newPassword" required>
                                    </div>
                                    <div class="input-field form-group">
                                        <input type="password" placeholder="{{trans('formlabel.profile.confirmnewpassword')}}*" class="form-control" id="confirmPassword" name="confirmPassword" required>
                                    </div>
                                    <input type="hidden" name="update" value="profilepassword">
                                    <button type="submit" class="btn btn-primary text-center">{{trans('formlabel.profile.passwordsubmitbutton')}}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="imageModal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title text-left">{{trans('formlabel.profile.imagetitle')}}</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form id="profile_form" name="profile_form" action="{{ asset('updateprofile')}}" method="post" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="hidden_img" value="{{@Auth::user()->profile_img}}" class="form-control"/>
                                    <div class="form-group input-field">
                                        <div class="">
                                            <input type="file" class="form-control" id="profileImg" name="profileImg" required accept="image/*">
                                        </div>
                                    </div>
                                    <input type="hidden" name="update" value="profileimg">
                                    <button type="submit" class="btn btn-primary text-center">{{trans('formlabel.profile.imagesubmitbutton')}}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Modal--->

                <div class="login-wrapper col-md-5 col-sm-12" style="display: none;">
                    test profile<br>
                    <div class="float-left">
                        <a href="{{ asset('dashboard')}}" >Skip Now <i class="fa fa-fw fa-check"></i>
                        </a>
                    </div>
                    <div class="float-right">
                        <a href="{{ asset('logout')}}" >Logout <i class="fa fa-fw fa-power-off"></i>
                        </a>
                        <form id="logout-form" action="{{ asset('logout')}}" method="POST" style="display: none;">
                            {!! csrf_field() !!}
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
   $(document).ready(function() {
       $('#pwdModal').find("input").val('');
   });
   
    $('form').each(function () {
        $(this).bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "profileImg": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "fName": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        },
                        stringLength: {
                            min: 3,
                            max: 50,
                            message: 'The full name must be less than 50 characters'
                        }
                    }
                },
                "lName": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        },
                        stringLength: {
                            min: 3,
                            max: 50,
                            message: 'The full name must be less than 50 characters'
                        }
                    }
                },
                "oldPassword": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "newPassword": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        },
                        different: {
                            field: 'oldPassword',
                            message: 'Old password & new password should not be same'
                        }
                    }
                },
                "confirmPassword": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        },
                        identical: {
                            field: 'newPassword',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                }
            }
        }).on('success.form.bv', function (e) {
            var setPassword = $("#newPassword").val();
            var resetPassword = $("#confirmPassword").val();
            if (setPassword !== resetPassword) {
                $("i[data-bv-icon-for=confirmPassword]").addClass("fa fa-remove").css('color', '#b24d4b');
                $("small[data-bv-validator=identical]").show();
                $('.help-block').css('color', '#b24d4b');
                return false;
            } else {
                $("i[data-bv-icon-for=confirmPassword]").addClass("fa fa-check").css('color', '#3c763d');
                return true;
            }
//                    $('#custom_loader').show();
            return true;
        });
    });

</script>
<script>
    history.pushState(null, document.title, location.href);
    window.addEventListener('popstate', function (event)
    {
        history.pushState(null, document.title, location.href);
    });
</script>
@include('includes.footer')

