@include('includes.header')



<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="pr-5">
                                <h4> {{trans('messagelabel.query.myquery')}}</h4>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class=" agreementform ">
                    <div class="row">
                        <div class="col-sm-12 col-12">
                            
                            <div class="contact-address ">
                            <div class="d-flex text-white">
                                <div class="p-1 bg-warning col-2 pl-3 border-right">{{trans('messagelabel.query.title')}}</div>
                                <div class="p-1 bg-warning col-2 pl-3 border-right">{{trans('messagelabel.query.type')}}</div>
                                <div class="p-1 bg-warning col-3 pl-3 border-right">{{trans('messagelabel.query.section/sector')}}</div>
                                <div class="p-1 bg-warning col-2 pl-3 border-right">{{trans('messagelabel.training.start_date')}}</div>
                                <div class="p-1 bg-warning col-2 pl-3 border-right">{{trans('messagelabel.training.end_date')}}</div>
                                <div class="p-0 bg-warning col-1 pl-3 border-right">{{trans('messagelabel.training.rowaction')}}</div> 
                            </div>
                            @if(count(@$querylist)>0)
                            <?php
                            $i = 1;
                            ?>
                            @foreach(@$querylist as $key=>$val)
                            <div class="d-flex text-secondary contactdetails" id="{{@$val->query_id}}">
                                <div class="p-1 col-2 pl-0">{{@$val->title}}</div>
                                <div class="p-1 col-2 pl-0">{{@$codetype[@$val->codetype]}}</div>
                                <div class="p-1 col-3 pl-0">{{@$codeDetail[@$val->sic_code]}}</div>
                                <div class="p-1 col-2 pl-0">{{Carbon\Carbon::parse(@$val->startDate)->format(config('assignment.dateformat'))}}</div>
                                <div class="p-1 col-2 pl-0">{{Carbon\Carbon::parse(@$val->endDate)->format(config('assignment.dateformat'))}}</div>
                                <div class="p-0 col-1 text-center">
                                    <a href="{{ asset('queryPostDetail/'.@$val->query_id)}}">
                                        <i class="fa fa-eye text-success" title="View Query Post"></i>
                                    </a>&nbsp;&nbsp;
                                    <a href="{{ asset('addQuery/'.@$val->query_id)}}">
                                        <i class="fa fa-edit text-success" title="Edit Query Post"></i>
                                    </a>&nbsp;&nbsp;
                                    <a href="javascript:void(0);" onclick="deleteQueryPost('{{$val->query_id}}')">
                                        <i class="fa fa-remove text-danger" aria-hidden="true" title="Delete Query Post"></i>
                                    </a>

                                </div>
                            </div>
                            <?php $i++; ?>
                            @endforeach
                            @else 
                            <div class="d-flex contactdetails text-secondary">
                                <div class="col-12 text-center">No data found.</div>
                            </div>
                            @endif
                        </div>
                        </div>
                    </div>
                </div>

                

            </div>
        </div>
    </div>
</div>
<script>
    function deleteQueryPost(id) {
        var del = confirm('Are you sure to remove this?');
            if (del) {
                $("#"+ id).fadeOut(400, function() {
                $("#"+ id).remove();
            });
            var url = "{{url('/deleteQueryPost')}}";
            $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
            //location.reload();
            });
        } else {
            return false;
        }
    }
</script>

@include('includes.footer')