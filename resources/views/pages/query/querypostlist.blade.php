@include('includes.header')
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h3 class="text-left text-secondary font-weight-normal p-4">Queries List</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
        <div class="row">
            <div class="col-lg-12 mb40">
                <div class="row">
                  @if(count(@$querypostinfo) > 0)
                  @foreach(@$querypostinfo as $k=>$v)
                  <div class="col-md-4 mb20">
                    <div class="addsection_grey">
                        <a href="{{ asset('queryPostDetail/'.$v->query_id)}}">
                        <h5 class="sidebar-title pl-3">{{@$v->title}}</h5>
                        </a>
                        <hr/>
                        <p class="pl-3 mb-0">@if(@$v->description){{substr(@$v->description, 0, 40)."..."}} @endif <br>
                            Effort in man days : {{@$v->effort_mandays}} days <br>
                            Location : {{@$v->city_name}},{{@$state[@$v->states]}},{{@$country[@$v->country]}}<br>
                        </p>
                        
                    </div>
                  </div>
                  <div style="clear:both"></div>
                  @endforeach
                  @else 
                  <div class="col-md-12">
                      <div class="text-secondary text-center" style="text-align: center;">
                          No Query found.
                      </div>
                  </div>
                  @endif
              </div>  
            </div>
        </div>
    
    
</div>
        </div>
    </div>
</div>


@include('includes.footer')