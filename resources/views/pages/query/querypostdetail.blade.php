@include('includes.header')
<style>
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        padding: 2px 5px;
        border-radius: 5px;
    }
    
</style>
<div class="container">
    <div class="main-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="details mt-4 border-right">
                    <h5 class="profile-no">{{@$querypostdetail->title}}</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container">
    <hr>
    <h5>{{trans('messagelabel.querydetail.querydetails')}}</h5>
    <div class="row">
        <div class="col-md-12 mb30">
            <div class="card card-body">
                <p class="text-dark">
                    {{@$querypostdetail->description}}
                </p>
                <h6 class="text-dark">Query Location</h6>
                <address>
                    <span class="locationCity">
                        <i class="fa fa-map-marker pr-1 text-danger" aria-hidden="true"></i>
                        {{@$querypostdetail->city_name . ", " . @$state[@$querypostdetail->states] . ", " . @$country[@$querypostdetail->country]}}
                    </span>
                    <div class="clearfix"></div>
                </address>
                <hr>
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="inforamation-section">
                            <div><strong>{{trans('messagelabel.querydetail.querycode')}}</strong>
                                <span class="text-dark">{{@$codetype[@$querypostdetail->codetype]}} - {{@$codeDetail[@$querypostdetail->sic_code]}}</span> 
                            </div>
                            <div><strong>{{trans('messagelabel.querydetail.queryeffort')}}</strong>
                                <span class="text-dark">{{@$querypostdetail->effort_mandays}}</span>
                            </div>
                            <div><strong>{{trans('messagelabel.querydetail.querydays')}}</strong>
                                <span class="text-dark">{{Carbon\Carbon::parse(@$querypostdetail->startDate)->format(config('assignment.dateformat'))
                                            . " to " . Carbon\Carbon::parse(@$querypostdetail->endDate)->format(config('assignment.dateformat'))}}</span> 
                            </div>
                            <div><strong>{{trans('messagelabel.querydetail.queryookingfor')}}</strong>
                                <span class="text-dark">{{@$querypostdetail->lookingfor == 1 ? "Seeker" : "Provider" }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="inforamation-section">
                            @if(@$querypostdetail->lookingfor == 1)
                            <div><strong>{{trans('messagelabel.querydetail.querytravel')}}</strong>
                                <span class="text-dark">{{@$querypostdetail->travelinfo}}</span>
                            </div>
                            <div><strong>{{trans('messagelabel.querydetail.querybudget')}}</strong>
                                <span class="text-dark">{{@$querypostdetail->budgetinfo}}</span>
                            </div>
                            @elseif(@$querypostdetail->lookingfor == 2)
                            <div><strong>{{trans('messagelabel.querydetail.querycharge')}}</strong>
                                <span class="text-dark">{{@$querypostdetail->perday_charge}}</span>
                            </div>
                            <div><strong>{{trans('messagelabel.querydetail.queryradius')}}</strong>
                                <span class="text-dark">{{(@$querypostdetail->radious) ? @$querypostdetail->radious : 'N/A' }}</span>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>

@include('includes.footer')
