@include('includes.header')

<style>
    #loading-image {
        position: absolute;
        z-index: 1;
        right: -5px;
        top: 7px;
    }
    div#locationCity span {
        padding: 2px 5px !important;
    }
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        border-radius: 5px;
    }
    .fa-times {
        cursor: pointer;
    }
    .form-group i.form-control-feedback {
        display: none !important;
    }
    .form-group small[data-bv-for="audit_effort[]"],
    .form-group small[data-bv-for="country"],
    .form-group small[data-bv-for="states"]
    {
        color: #b24d4b !important;
    }
    i#addLocation {
        font-size: 1rem;
        cursor: pointer;
        position: absolute;
        right: 90%;
        top: 20%;
    }
</style>

<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="pr-5">
                                <h4>@if(@$queryinfo->query_id) Edit @else Add  @endif {{trans('messagelabel.query.query')}}</h4>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="card agreementform">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <div id="auditrequest">
                                <form name="querypost" id="querypost" method="post" action="{{ asset('addQuery') }}" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="user_id" id="user_id" value="{{@Auth::user()->id}}">
                                    <input type="hidden" name="user_type" id="user_type" value="{{@Auth::user()->usertype}}">
                                    <input type="hidden" name="id_update" id="id_update" value="{{@$queryinfo->query_id}}">
                                    <input type="hidden" id="select_sic_code" value="{{@$queryinfo->sic_code}}">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-12">
                                            <label>{{trans('messagelabel.query.title')}}</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="title" value="{{@$queryinfo->title}}">
                                            </div>
                                            <div class="form-group" id="codeType">
                                                @foreach(@$codetype as $k=>$v)
                                                @if(@$queryinfo->codetype)
                                                <input type="radio" class="p-1" value="{{$k}}" name="codetype" <?= @$queryinfo->codetype == $k ? 'checked' : "" ?>> {{$v}}&nbsp;
                                                @else
                                                <input type="radio" class="p-1" value="{{$k}}" name="codetype"  <?= $k == 2 ? 'checked' : "" ?>> {{$v}}&nbsp;
                                                @endif
                                                @endforeach
                                            </div>
                                            <label class="mt-2">{{trans('formlabel.postassignmentaudit.siccode')}}</label>
                                            <div class="form-group">
                                                <select class="form-control" required name="sic_code" id="codeNumber">
                                                    <option value="">Select Code</option>
                                                </select>
                                            </div>
                                            <label class="mt-2">{{trans('formlabel.postassignmenttraining.location')}}</label>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <select class="form-control" name="country" id="countries" onchange="getLocationInfo('countries', 'states');">
                                                            <option value="">Select Country</option>
                                                            @foreach(@$countryList as $k=>$v)
                                                            <option value="{{@$v->id}}" @if(@$v->id == @$queryinfo->country) selected @endif>{{@$v->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <select class="form-control" name="states" id="states" onchange="getLocationInfo('states', 'cities');">
                                                            <option value="">Select State</option>
                                                            @if(@$queryinfo->states)
                                                                @foreach(@$states as $key => $val)
                                                                <option value="{{$val->id}}" @if(@$val->id == @$queryinfo->states) selected @endif >{{$val->name}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <img id="loading-image" src="{{asset('images')}}/ajax-loader.gif" alt="Loading..." />
                                                        <input type="text" style="position: relative;" placeholder="{{trans('formlabel.profilecontact.addresscity')}}" class="form-control" id="city" name="city" value="{{@$queryinfo->city_name}}" data-bv-notempty-message="Please enter a city name">
                                                    </div> 
                                                    
                                                </div>
                                                <label class="mt-2">{{trans('formlabel.postassignmentaudit.effort')}}</label>
                                                <div class="form-group">
                                                    <input type="text" id="effort_mandays" class="form-control" name="effort_mandays" value="{{@$queryinfo->effort_mandays}}" onkeypress="return isNumberKey(event)">
                                                </div>
                                                <label class="mt-2">{{trans('messagelabel.query.description')}}</label>
                                                <div class="form-group">
                                                    <textarea class="form-control" name="description" id="description" rows="3" cols="25">{{@$queryinfo->description}}</textarea>
                                                </div>
                                                
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-12">
                                            <label class="mt-2">{{trans('formlabel.postassignmentaudit.startdate')}}</label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="startDate" value="{{@$queryinfo->startDate}}" id="start_date" placeholder="" readonly> 
                                                </div>
                                                <label class="mt-2">{{trans('formlabel.postassignmentaudit.enddate')}}</label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="endDate" value="{{@$queryinfo->endDate}}" id="end_date" placeholder="" readonly>
                                                </div>
                                                <label class="mt-2">{{trans('messagelabel.query.lookingfor')}}</label>
                                                <div class="form-group">
                                                    <select class="form-control" required name="lookingfor" id="lookingfor" onchange="changeval(this.value);" @if(@Auth::user()->usertype != 6) disabled="disabled" @endif>
                                                        <option value="">Select ... </option>
                                                        <option value="1" @if(@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) selected @elseif(@$queryinfo->lookingfor == 1) selected @endif>{{trans('messagelabel.query.lookingforseeker')}}</option>
                                                        <option value="2" @if(@Auth::user()->usertype == 8) selected @elseif(@$queryinfo->lookingfor == 2) selected @endif>{{trans('messagelabel.query.lookingforprovider')}}</option>
                                                    </select>
                                                </div>
                                                <div id="seekerDiv" @if(@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) style="display:block;" @else style="display:none;" @endif>
                                                    <label class="mt-2">{{trans('messagelabel.query.travelinfo')}}</label>
                                                    <div class="form-group">
                                                        <input type="text" id="travelinfo" class="form-control" name="travelinfo" value="{{@$queryinfo->travelinfo}}" >
                                                    </div>
                                                    <label class="mt-2">{{trans('messagelabel.query.budgetinfo')}}</label>
                                                    <div class="form-group">
                                                        <input type="text" id="budgetinfo" class="form-control" name="budgetinfo" value="{{@$queryinfo->budgetinfo}}" >
                                                    </div>
                                                </div>
                                                <div id="providerDiv" @if(@Auth::user()->usertype != 8) style="display:none;" @endif>
                                                    <label class="mt-2">{{trans('messagelabel.query.perdaycharge')}}</label>
                                                    <div class="form-group">
                                                        <input type="text" id="perday_charge" class="form-control" name="perday_charge" value="{{@$queryinfo->perday_charge}}"  onkeypress="return isNumberKey(event)">
                                                    </div>
                                                    <label class="mt-2">{{trans('messagelabel.query.preferradiouslocation')}}</label>
                                                    <div class="form-group">
                                                        <input type="text" id="radious" class="form-control" name="radious" value="{{@$queryinfo->radious}}" >
                                                    </div>
                                                </div>
                                                <label class="mt-2">{{trans('messagelabel.query.status')}}</label>
                                                <div class="form-group">
                                                    <select class="form-control" required name="status" id="status" >
                                                        @foreach(@$status as $k=>$v)
                                                        <option value="{{@$k}}"@if(@$k == @$queryinfo->status) selected @endif>{{@$v}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary text-center btn-md pull-right mt-2">{{trans('formlabel.postassignmenttraining.rowaction')}}</button>
                                    </div>
                                </form>

                                
                            </div>
                            <div class="clearfix"></div>
                            <br/>
                           
                        </div>
                    </div>
                </div>

                

            </div>
        </div>
    </div>
</div>
@if(@$queryinfo->query_id)
<script>
$(document).ready(function () {
        changeval({{@$queryinfo->lookingfor}});
    });
</script>
@endif
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        $('#loading-image').hide();
        $("input[type='radio'][name='codetype']:checked").prop("checked", true).trigger("click");
    });
    $("input[name='codetype']").click(function () {
        var radioValue = $("input[type='radio']:checked").val();
        var select_sic_code = $('#select_sic_code').val();
        var url = "{{url('/getStandards')}}";
        if (radioValue) {
            $('#codeNumber').html("<option value='' >Select Code</option>");
            $.post(url, {"selected_val": radioValue,"select_sic_code":select_sic_code, "_token": '<?= csrf_token(); ?>'}, function (res) {
                $("#codeNumber").html(res);
                var total = $('select#codeNumber option').length;
                $("#codeNumber_cnt").html(" <b>(" + (total - 1) + ")</b>");
            });
        }
    });
    
    //get location list
    function getLocationInfo(id, tbl) {
        if (id == "states") {
            $('#loading-image').show();
            $('#city').val("");
        } else {
            $('#city').val("");
        }
        var selected_val = $('select#' + id + ' option:selected').val();
        var url = "{{url('/getLocationInfo')}}";
        $.post(url, {"selected_val": selected_val, "id": id, "tbl": tbl, "_token": '<?= csrf_token(); ?>'}, function (res) {
            if (tbl == 'cities') {
                availableTags = JSON.parse(res);
                $('#loading-image').hide();
                $("#city").autocomplete({
                    source: availableTags
                });
            } else {
                $("#" + tbl).html(res);
                var total = $('select#' + tbl + ' option').length;
                $("#" + tbl + "_cnt").html(" <b>(" + (total - 1) + ")</b>");
            }
        });
    }
    //get location list
    
    function changeval(val)
    {
        if(val == 1)
        {
            $("#providerDiv").hide();
            $("#seekerDiv").show();
        } 
        else if(val == 2)
        {
            $("#seekerDiv").hide();
            $("#providerDiv").show();
        }
    }
    
    //datepicker
                    $(function () {
                        var enddate = $('#start_date').val();
                        $('#start_date').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            minDate:0,
                            dateFormat: 'dd-mm-yy',
                            onSelect: function (selected) {
                                $('#querypost').bootstrapValidator('revalidateField', 'startDate');
//                                $('#start_date').parents('div.form-group').addClass('has-success');
//                                $('#start_date').parents('div.form-group').removeClass('has-error');
//                                $("small[data-bv-for='startDate").hide();
//                                $("#end_date").datepicker("setDate", null);
                                if(enddate) {
                                    $("#end_date").datepicker("option", "minDate", enddate)
                                } else {
                                    $("#end_date").datepicker("option", "minDate", selected)
                                }
                            }
                        });
                        $('#end_date').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: 'dd-mm-yy',
                            onSelect: function (selected) {
                                $('#querypost').bootstrapValidator('revalidateField', 'endDate');
//                                $('#end_date').parents('div.form-group').addClass('has-success');
//                                $('#end_date').parents('div.form-group').removeClass('has-error');
//                                $("small[data-bv-for='endDate").hide();
                            }
                        });
                    });
//datepicker
</script>

<script>
    $('#querypost').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "title": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "sic_code": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "country": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "states": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "city": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "effort_mandays": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "startDate": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "endDate": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "lookingfor": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "travelinfo": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "budgetinfo": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "description": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "perday_charge": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            }
        }
    })

            .on('success.form.bv', function (e) {
                if ($('#states').val() == "")
            {
                $('#states').parents('div.form-group').addClass('has-error');
                $('#states').parents('div.form-group').removeClass('has-success');
                $("small[data-bv-for='states']").html("This field is required.").show();
//                $("i[data-bv-icon-for='address[state_id]").hide();
                return false;
            } else if ($('#city').val() == "") {
                $('#city').parents('div.form-group').addClass('has-error');
                $('#city').parents('div.form-group').removeClass('has-success');
                $("small[data-bv-for='city']").html("This field is required.").show();
                return false;
            } else if ($('#end_date').val() == "") {
                $('#end_date').parents('div.form-group').removeClass('has-success'); 
                $('#end_date').parents('div.form-group').addClass('has-error');
                $("small[data-bv-for='endDate']").html("This field is required.").show();
                return false;
            }
                document.getElementById('lookingfor').disabled=false;
//                $('#confirmModal').show();
//                $("#content-form").html($("#assignment-request").serialize());
                return true;
            });
            
            
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
@include('includes.footer')