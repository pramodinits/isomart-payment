<?php
$contactStatus = $industrystatuscolor;
if (count($industryDetails) > 0) {
    $contactStatus = "#5CB85C";
}
?>
<style>
    .industrystatus{
        color: <?= $contactStatus; ?> !important;
    }
</style>
<div class="col-md-12 col-sm-12 col-12">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-12">
            <form id="isic-code" name="isic-code" action="{{asset('addIndustryCode')}}" method="post">
                {!! csrf_field() !!}
                <input type="hidden" name="user_id" id="user_id" value="{{Auth::user()->id}}">
                <label>{{trans('formlabel.profileindustry.isiccode')}}</label>
                <div class="form-group">
                    <input type="text" class="form-control" id="isicCode" name="isicCode" required>
                </div>
                <div class="mt-2 mb-2">
                    <button type="submit" class="btn btn-primary text-center btn-md pull-right">{{trans('formlabel.profileindustry.saveindustry')}}</button>
                    <div class="clearfix"></div>
                </div>
            </form>
            <div class="contact-address">
                <div class="d-flex text-white">
                    <div class="p-2 bg-warning col-1 pl-0 border-right">{{trans('formlabel.profileindustry.rowslno')}}</div>
                    <div class="p-2 bg-warning col-7 text-center border-right">{{trans('formlabel.profileindustry.rowisiccode')}}</div>
                    <div class="p-2 bg-warning col-4 text-center border-right">{{trans('formlabel.profileindustry.rowaction')}}</div>
                </div>
                @if(@$industryDetails)
                @php $i = 1; @endphp
                @foreach(@$industryDetails as $key=>$val)
                <div class="d-flex text-secondary contactdetails" id="contact-address">  
                    <div class="p-2 col-1 pl-0">{{ $i }}</div>
                    <div class="p-2 col-7">{{$val->isic_code}}</div>
                    <div class="p-2 col-2 text-center">
                        <a href="javascript:void(0);" onclick="deleteIsicCode('{{$val->id}}')">
                            <i class="fa fa-window-close text-danger"></i>
                        </a>
                    </div>
                </div>
                @php $i++; @endphp
                @endforeach
                @else
                <div class="d-flex contactdetails text-secondary">
                    <div class="col-12 text-center">No data found.</div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

<script>
            $('#isic-code').bootstrapValidator({
    feedbackIcons: {
    valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'glyphicon glyphicon-refresh'
    },
            fields: {
            "isicCode": {
            validators: {
            notEmpty: {
            message: "This field is required."
            }
            }
            }
            }
    })
            .on('success.form.bv', function (e) {
            $('#custom_loader').hide();
                    return true;
            });
            
            function deleteIsicCode(id) {
            var del = confirm('Are you sure to remove this?');
                    if (del) {

            var url = "{{url('/deleteIsicCode')}}";
                    $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                    window.location.href = "{{asset('profile')}}";
                    });
            } else {
            return false;
            }
            }
</script>