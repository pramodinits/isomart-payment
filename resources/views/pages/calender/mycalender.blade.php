@include('includes.header')
<link href="{{ asset('/')}}fullcalendar/fullcalendar.min.css" rel='stylesheet' />
<link href="{{ asset('/')}}fullcalendar/fullcalendar.print.min.css" rel='stylesheet' media='print' />
<script type="text/javascript" src="{{ asset('/')}}fullcalendar/lib/moment.min.js"></script>
<script type="text/javascript" src="{{ asset('/')}}fullcalendar/lib/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('/')}}fullcalendar/fullcalendar.js"></script>
<style>
    table td.fc-year-monthly-td {
        width: 30% !important;
    }
    div.fc-year-monthly-name a {
        padding: 1em;
    }
    table.fc-year-main-table tbody tr td.fc-month-view div.fc-row {
       height: 40px !important; 
    }
    td.fc-prev-month {
        opacity: 0.5 !important;
    }
    td.fc-event-container a {
        margin-top: -6px !important;
    }
</style>
<?php
$event = array();
foreach ($list as $key => $val) {
    $array = array();
    $array['title'] = $val->assignment_name;
    $array['ref_id'] = $val->ref_id;
    $array['start'] = explode(" ", $val->assignment_startdate)[0];
    $array['end'] = explode(" ", $val->assignment_enddate)[0];
    $array['url'] = asset('assignedjobDetails/') . "/" . $val->assignment_id;
    array_push($event, $array);
}
$eventjson = json_encode($event);
?>
<!--send proposal modal--->

<!-- Button trigger modal -->

<!--send proposal modal--->
<script>
        /*
         var inputdata = [
         {
         title: 'All Day Event',
         start: '2018-03-01'
         },
         {
         title: 'Long Event',
         start: '2018-03-07',
         end: '2018-03-10'
         },
         {
         id: 999,
         title: 'Repeating Event',
         start: '2018-03-09T16:00:00'
         },
         {
         id: 999,
         title: 'Repeating Event',
         start: '2018-03-16T16:00:00'
         },
         {
         title: 'Conference',
         start: '2018-03-11',
         end: '2018-03-13'
         },
         {
         title: 'Meeting',
         start: '2018-03-12T10:30:00',
         end: '2018-03-12T12:30:00'
         },
         {
         title: 'Lunch',
         start: '2018-03-12T12:00:00'
         },
         {
         title: 'Meeting',
         start: '2018-03-12T14:30:00'
         },
         {
         title: 'Happy Hour',
         start: '2018-03-12T17:30:00'
         },
         {
         title: 'Dinner',
         start: '2018-03-12T20:00:00'
         },
         {
         title: 'Birthday Party',
         start: '2018-03-13T07:00:00'
         },
         {
         title: 'Click for Google',
         url: 'http://google.com/',
         start: '2018-03-28'
         }
         ];
         */
        var datearray = <?= $eventjson; ?>;
        $(document).ready(function () {

$('#calendar').fullCalendar({
    header: {
      left: 'prev,next',
      center: 'title',
      right: 'year,month'
    },
    timezone: 'Asia/Calcutta',
    bootstrap: false,
    defaultView: 'year',
    yearColumns: 3,
defaultDate: "<?= $defaultDate; ?>",
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        events:datearray,
        eventClick:  function(event, jsEvent, view) {
            $('#eventtitle').html(event.title)
            $('#eventdescription').html(event.description)
//            $('#modalbutton').click()
        },
                select: function(start, end, allDay) {
        calendar.fullCalendar('renderEvent', event, true);
    },
});
});

</script>
<style>

    body {
        margin: 40px 10px;
        padding: 0;
        font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
        font-size: 14px;
    }

    #calendar {
        max-width: 900px;
        margin: 0 auto;
    }

</style>
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="pr-5">
                                <h4 class="p-4">Assignment List</h4>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="btnsection ">
                                <a href="{{ asset('allassignment') }}">
                                    <button type="button" class="btn btn-dark btn-sm" style="display: none;">
                                        My Assignments
                                    </button></a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
           

            <div class="clearfix"></div>
            <div class="container">
                 <button type="button" style="display: none;" id="modalbutton" class="btn btn-primary text-danger" data-toggle="modal" data-target="#exampleModal">
                Modal btn
            </button>

            <!-- Modal -->
            <div class="modal fade col-md-6 center-block" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Event Details</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="eventtitle" class="text-bold text-primary"></div>
                            <div id="eventdescription"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

                <div id='calendar'></div>
            </div>
        </div>
    </div>
</div>
@include('includes.footer')