@include('includes.header')
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">  
                    
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h4 class="text-left text-secondary font-weight-normal pb30">Assignments</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="">
                    <div class="row">
                        <div class="col-lg-12 mb40">
                            <div id="tabAssign">
                                <ul class="nav nav-tabs tabs-default mb30" role="tablist bg-light">
                                    <li class="nav-item" role="presentation" style="display: none;"><a class="nav-link  custom-tabs {{ @$tabActive == 3 ? 'show active' : '' }}" href="#certification" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Certification</a></li>
                                    <li class="nav-item" role="presentation"><a class="nav-link  custom-tabs {{ @$tabActive == 1 ? 'show active' : '' }}" href="#audit" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Audit</a></li>
                                    <li class="nav-item" role="presentation"><a class="nav-link  custom-tabs {{ @$tabActive == 2 ? 'show active' : '' }}" href="#training" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Training</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content card pt-4">
                                    <div role="tabpanel" class="tab-pane fade {{ @$tabActive == 1 ? 'show active' : '' }}" id="audit">
                                        <div class="container">
                                            <div class="row">
                                                @if(count(@$assignmentaudit)> 0)
                                                @foreach(@$assignmentaudit as $k=>$v)
                                                <div class="col-md-4 mb30">
                                                    <div class="card-header bg-premiry text-white text-center">
                                                        <h5 class="mb-0">
                                                            <a href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}">
                                                                @if(strlen($v->assignment_name) > 20)
                                                                {{substr(ucfirst($v->assignment_name), 0, 20) . "..."}}
                                                                @else
                                                                {{ucfirst($v->assignment_name)}}
                                                                @endif
                                                            </a>&nbsp;
                                                        </h5>
                                                    </div>
                                                    <div class="card text-center">
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                        <div class="mx-auto">
                                                            <?php 
                                                            /*days left*/
                                                            $now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
                                                            $end_date = Carbon\Carbon::parse($v->proposal_end_date);
                                                            $now_date = Carbon\Carbon::parse($now);
                                                            $daysleft = $now_date->diffInDays($end_date, false); 
                                                            /*days left*/
                                                            /*budget counted with approved bid*/
                                                            $assignmentBudget = $v->budget_usd - $v->jobBudget;
                                                            $assignmentEffort = $v->effort_tentative - $v->jobEffort;
                                                            /*budget counted with approved bid*/
                                                            ?>
                                                            <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $assignmentBudget}}</span>
                                                            {{" ( " . trans('formlabel.assignmentinfoaudit.efforts') .  $assignmentEffort . config('assignment.effortdays') . ")" }}
                                                            @if($v->approval_status == 1)
                                                            <span class="text-success">{{trans('formlabel.auditdetail.awarded')}}</span>
                                                            @elseif(@$daysleft < 0) 
                                                            <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                            @elseif(@$daysleft == 0)
                                                            <span class="text-success">{{trans('formlabel.auditdetail.openfortoday')}}</span>
                                                            @else
                                                            <span class="text-success">Open / {{@$daysleft}} days left</span>
                                                            @endif
                                                        </div>
                                                        </div>
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                            <div class="col-3"></div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="col-3"></div>
                                                        </div>
                                                        <hr/>
                                                        <div class="col-12 card-text text-left">
                                                {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                                                {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}
                                                <div class="headings">
                                                <?php
                                                $citynames = "";
                                                    foreach (@$v->AssignmentLocation as $key => $val) {
                                                    if(!$val->whomtoassign_id) {
                                                    $citynames .= $val->city_name . ", ";
                                                    } elseif($v->approval_status == 1) {
                                                        $citynames .= $val->city_name . ", ";
                                                    }
                                                }
                                                ?>
                                                {{trans('formlabel.assignmentinfoaudit.location')}} {{rtrim($citynames, ", ")}}<br>
                                                <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                                                </div>
                                            </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No assignments found.
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade {{ @$tabActive == 2 ? 'show active' : '' }}" id="training">
                                        <div class="container">
                                            <div class="row">
                                                @if(count(@$assignmenttraining) > 0)
                                                @foreach(@$assignmenttraining as $k=>$v)
                                                <div class="col-md-4 mb30">
                                                    <div class="card-header bg-premiry text-white text-center">
                                                    <h5 class="mb-0">
                                                            <a href="{{ asset('assignmentTrainingDetail/'.$v->assignment_id)}}">{{ucfirst($v->batch_no)}}</a>
                                                        </h5>
                                                    </div>
                                                    <div class="col-12 card text-center">
                                                        
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                        <div class="mx-auto">
                                                            <?php 
                                                            /*days left*/
                                                            $now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
                                                            $end_date = Carbon\Carbon::parse($v->proposal_end_date);
                                                            $now_date = Carbon\Carbon::parse($now);
                                                            $daysleft = $now_date->diffInDays($end_date, false); 
                                                            /*days left*/
                                                            /*budget counted with approved bid*/
                                                            $assignmentBudget = $v->fee_range - $v->jobBudget;
                                                            $assignmentEffort = $v->effort_tentative - $v->jobEffort;
                                                            /*budget counted with approved bid*/
                                                            ?>
                                                            <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $assignmentBudget}}</span>
                                                            {{" ( " . trans('formlabel.assignmentinfoaudit.efforts') .  $assignmentEffort . config('assignment.effortdays') . ")" }}
                                                            @if($v->approval_status == 1)
                                                            <span class="text-success">{{trans('formlabel.auditdetail.awarded')}}</span>
                                                            @elseif(@$daysleft < 0) 
                                                            <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                            @elseif(@$daysleft == 0)
                                                            <span class="text-success">{{trans('formlabel.auditdetail.openfortoday')}}</span>
                                                            @else
                                                            <span class="text-success">Open / {{@$daysleft}} days left</span>
                                                            @endif
                                                        </div>
                                                        </div>
                                                        @if($v->mode == 1)
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                            <div class="col-3"></div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="col-3"></div>
                                                        </div>
                                                        @endif
                                                        <hr/>
                                                        <p class="card-text text-left">
                                                            {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('assignmentTrainingDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                                                            {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                                                            {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                                                            @if($v->mode == 1)
                                                            <?php
                                                            $citynames = "";
                                                            foreach (@$v->AssignmentLocation as $key => $val) {
                                                                if (!$val->whomtoassign_id) {
                                                                    $citynames .= $val->city_name . ", ";
                                                                } elseif ($v->approval_status == 1) {
                                                                    $citynames .= $val->city_name . ", ";
                                                                }
                                                            }
                                                            ?>
                                                            {{trans('formlabel.assignmentinfotraining.mode')}} {{config('assignment.modef2f')." (" . rtrim($citynames, ", ") . ")"}}<br>
                                                            @else
                                                            {{trans('formlabel.assignmentinfotraining.mode')}} {{config('assignment.modeonline')}}<br>
                                                            @endif
                                                            <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                                                        </p>

                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No assignments found.
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade {{ @$tabActive == 3 ? 'show active' : '' }}" id="certification">
                                        <div class="container">
                                            <div class="row">
                                                @if(count(@$assignmentcertification)> 0)
                                                @foreach(@$assignmentcertification as $k=>$v)
                                                <div class="col-md-4 mb30">
                                                    <div class="card card-body text-center">
                                                        <h4 class="card-title">
                                                            <a href="{{ asset('assignmentCertificationDetail/'.$v->assignment_id)}}">{{substr(ucfirst($v->assignment_name), 0, 20) . "..."}}</a>&nbsp;
                                                            <i style="font-size: 1rem;" class="{{$v->approval_status == "1" ? "text-success fa fa-check-square-o" : "" }}" title="Awarded" aria-hidden="true"></i>
                                                        </h4>
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                            <div class="col-3"></div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="col-3"></div>
                                                        </div>
                                                        <hr/>
                                                        <p class="card-text text-left">{{substr($v->description, 0, 40) . "..."}}<br>
                                                            Ref ID : <a href="{{ asset('assignmentCertificationDetail/'.$v->assignment_id)}}">{{"#".$v->ref_id}}</a><br>
                                                            Duration : {{$v->effort_tentative . " days"}} <br>
                                                            <?php
                                                            $names = "";
                                                            $cityArray = explode(",", $v->location);
                                                            foreach ($cityArray as $key => $val) {
                                                                $names .= @$cityNameArray[$val] . ", ";
                                                            }
                                                            ?>
                                                            Location : {{rtrim($names, ", ")}}<br>
                                                            <strong>{{$v->add_date}}</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No assignments found.
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tab panes -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


@include('includes.footer')