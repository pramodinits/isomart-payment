<script type="text/javascript" src="{{ asset('/')}}js/chosen.jquery.js"></script>
<style>
    div#locationCity span {
        padding: 2px 5px !important;
    }
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        border-radius: 5px;
    }
    .fa-times {
        cursor: pointer;
    }
</style>
<script>
$(function () {
    $('.chosen-select').chosen();
    $('.chosen-select-deselect').chosen({allow_single_deselect: true});
});
</script>
<form name="assignment-certification" id="assignment-certification" method="post" action="{{ asset('requestassignmentcertification') }}">
    {!! csrf_field() !!}
    <input type="hidden" name="user_id" id="user_id" value="{{@Auth::user()->id}}">
    <input type="hidden" name="id_update" id="id_update" value="">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-12">
            <label>{{trans('formlabel.postassignmentaudit.assignmentname')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="certification[assignment_name]">
            </div>
            <div class="form-group">
                <input type="radio" class="p-1" value="1" name="codetype"> ISIC&nbsp;&nbsp;
                <input type="radio" class="p-1" value="2" name="codetype"> NACE&nbsp;&nbsp;
                <input type="radio" class="p-1" value="3" name="codetype"> ANZSIC
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.siccode')}}</label>
            <div class="form-group">
                <select class="form-control" required name="certification[sic_code]" id="codeNumber">
                    <option value="">Select Code</option>
                </select>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.location')}}</label>
            <div class="form-group">
                <select style="display: inline-block;" class="form-control col-md-3" name="country" id="countries" onchange="getState('countries', 'states');">
                    <option value="">Select Country</option>
                    @foreach(@$countryList as $k=>$v)
                    <option value="{{@$v->id}}">{{@$v->name}}</option>
                    @endforeach
                </select>
                <select style="display: inline-block;" class="form-control col-md-4" name="states" id="states" onchange="getCity('states', 'cities');">
                    <option value="">Select State</option>
                </select>
                <select style="display: inline-block;" class="form-control col-md-4" required name="city[]" id="cities">
                    <option value="">Select City</option>
                </select>&nbsp;&nbsp;
                <i id="addLocation" class="fa fa-plus-circle text-success" style="font-size: 1rem; cursor: pointer;" aria-hidden="true"></i>
            </div>
            <div class="form-group" id="locationCity" style="display: none;">

            </div>
            <!--            <div class="form-group">
                            <select data-placeholder="Select City" multiple class="form-control chosen-select" required name="location[]" id="cities">
                                <option value="">Select City</option>
                            </select>
                        </div>-->
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.effort')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="certification[effort_tentative]" onkeypress="return isNumberKey(event)">
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.budget')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="certification[budget_usd]" onkeypress="return isNumberKey(event)">
            </div>
            <!--            <label class="mt-2">{{trans('formlabel.postassignmentaudit.startdate')}}</label>
                        <div class="form-group">
                            <input type="date" class="form-control" name="startDate" id="startDate" placeholder="">
                        </div>
                        <label class="mt-2">{{trans('formlabel.postassignmentaudit.enddate')}}</label>
                        <div class="form-group">
                            <input type="date" class="form-control" name="endDate" id="endDate" placeholder="">
                        </div>-->
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.proposalenddate')}}</label>
            <div class="form-group">
                <input type="date" class="form-control" name="proposalEndDate" id="proposalEndDate" onkeydown="return false">
            </div>
            <div class="form-group" style="display: none;">
                <div class="d-flex">
                    <label class="container-radio pr-2">
                        <input type="radio" value="1" name="certification[local_travel_allowance]">Yes
                        <span class="checkmark"></span> 
                    </label>
                    <label class="container-radio pr-2">
                        <input type="radio" value="2" name="certification[local_travel_allowance]">No
                        <span class="checkmark"></span> 
                    </label>
                    <small class="help-block"></small>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-12">
            <label class="mt-1"  style="display: none;">{{trans('formlabel.postassignmentaudit.stayallowance')}}</label>
            <div class="form-group" style="display: none;">
                <div class="d-flex">
                    <label class="container-radio pr-2">
                        <input type="radio" value="1" name="certification[local_stay_allowance]">Yes
                        <span class="checkmark"></span>
                    </label>
                    <label class="container-radio pr-2">
                        <input type="radio" value="2" name="certification[local_stay_allowance]">No
                        <span class="checkmark"></span> 
                    </label>
                </div>
                <div class="help-block with-errors"></div>
                <i style="display: none;" class="form-control-feedback stay-error fa fa-remove" data-bv-icon-for="audit[local_stay_allowance]"></i>
            </div>
            <label class="mt-1" style="display: none;">{{trans('formlabel.postassignmentaudit.airfare')}}</label>
            <div class="form-group" style="display: none;">
                <div class="d-flex">
                    <label class="container-radio pr-2">
                        <input type="radio" value="1" name="certification[air_fare]">Yes<span class="checkmark"></span> 
                    </label>
                    <label class="container-radio pr-2">
                        <input type="radio" value="2" name="certification[air_fare]">No<span class="checkmark"></span> 
                    </label>
                </div>
            </div>
            <label class="mt-1">{{trans('formlabel.postassignmentaudit.allallowance')}}</label>
            <div class="row">
                <div class="col-md-4">
                    <label class="">{{trans('formlabel.postassignmentaudit.travelallowance')}}</label>
                    <div class="list-group list-group-horizontal">
                        <label class="container-radio pr-2">
                            <input type="radio" value="1" name="certification[local_travel_allowance]">Yes
                            <span class="checkmark"></span> </label>
                        <label class="container-radio pr-2">
                            <input type="radio" value="2" name="certification[local_travel_allowance]">No
                            <span class="checkmark"></span> 
                        </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="">{{trans('formlabel.postassignmentaudit.stayallowance')}}</label>
                    <div class="list-group list-group-horizontal">
                        <label class="container-radio pr-2">
                            <input type="radio" value="1" name="certification[local_stay_allowance]">Yes
                            <span class="checkmark"></span> </label>
                        <label class="container-radio pr-2">
                            <input type="radio" value="2" name="certification[local_stay_allowance]">No
                            <span class="checkmark"></span> </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="">{{trans('formlabel.postassignmentaudit.airfare')}}</label>
                    <div class="list-group list-group-horizontal">
                        <label class="container-radio pr-2">
                            <input type="radio" value="1" name="certification[air_fare]">Yes 
                            <span class="checkmark"></span> </label>
                        <label class="container-radio pr-2">
                            <input type="radio" value="2" name="certification[air_fare]">No
                            <span class="checkmark"></span> </label>
                    </div>
                </div>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.currentstatus')}}</label>
            <div class="form-group">
                <select class="form-control" name="certification[assignment_status]">
                    <option value="">Select Status</option>
                    @foreach(@$status as $k=>$v)
                    <option value="{{@$k}}">{{@$v}}</option>
                    @endforeach
                </select>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.description')}}</label>
            <div class="form-group">
                <textarea class="form-control" name="certification[description]" id="briefInfo" rows="5" cols="25"></textarea>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.query1')}}</label>
            <div class="input-group">
                <textarea class="form-control" name="certification[query_first]" id="briefInfo" rows="5" cols="25"></textarea>
            </div>
            <!--            <label class="mt-2">{{trans('formlabel.postassignmentaudit.query2')}}</label>
                        <div class="input-group">
                            <textarea class="form-control" name="audit[query_second]" id="briefInfo" rows="5" cols="25"></textarea>
                        </div>-->
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-center btn-md pull-right mt-2">{{trans('formlabel.postassignmentaudit.save')}}</button>
    </div>
</form>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
                    $("#addLocation").click(function () {
                        var city = $("#cities").val();
                        var cityname = $("#cities option:selected").text();
                        var statename = $("#states option:selected").text();
                        var countryname = $("#countries option:selected").text();

                        if (cityname !== "" && statename !== "" && countryname !== "") {
                            $('<option value="">Select Country</option>').appendTo("#countries");
                            $("#locationCity").show();
                            var res = '<input type="hidden" name=location[] value="' + city + '" >' + cityname + ', ' + statename + ', ' + countryname + '<span class="removeLocation"><i class="fa fa-times"></i></span>';
                            $('<span class=locationCity>' + res + '</div>').appendTo("#locationCity");
                            $(".removeLocation").click(function () {
                                $(this).parent().remove();
                            });
                        } else {
                            $('#cities').parents('div.form-group').addClass('has-error');
                            $(".help-block").show();
                        }
                    });


//get location list
                    function getState(id, tbl) {
                        $('#states').html("<option value='' >Select State</option>");
                        $('#cities').html("<option value='' >Select City</option>");
                        var selected_val = $('select#' + id + ' option:selected').val();
                        var url = "{{url('/getStateList')}}";
                        $.post(url, {"selected_val": selected_val, "id": id, "tbl": tbl, "_token": '<?= csrf_token(); ?>'}, function (res) {
                            $("#" + tbl).html(res);
                            var total = $('select#' + tbl + ' option').length;
                            $("#" + tbl + "_cnt").html(" <b>(" + (total - 1) + ")</b>");
                        });
                    }
                    function getCity(id, tbl) {
                        $('#cities').html("<option value='' >Select City</option>");
                        var selected_val = $('select#' + id + ' option:selected').val();
                        var url = "{{url('/getCityList')}}";
                        $.post(url, {"selected_val": selected_val, "id": id, "tbl": tbl, "_token": '<?= csrf_token(); ?>'}, function (res) {
                            $("#cities").html(res);
                            var total = $('select#' + tbl + ' option').length;
                            $("#" + tbl + "_cnt").html(" <b>(" + (total - 1) + ")</b>");
//                            $(".chosen-select").trigger("chosen:updated");
                        });
                    }
//get location list

                    $("input[name='codetype']").click(function () {
                        var radioValue = $("input[type='radio']:checked").val();
                        var url = "{{url('/getStandards')}}";
                        if (radioValue) {
                            $('#codeNumber').html("<option value='' >Select Code</option>");
                            $.post(url, {"selected_val": radioValue, "_token": '<?= csrf_token(); ?>'}, function (res) {
                                $("#codeNumber").html(res);
                                var total = $('select#codeNumber option').length;
//                                $("#codeNumber_cnt").html(" <b>(" + (total - 1) + ")</b>");
                            });
                        }
                    });

                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0!
                    var yyyy = today.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd;
                    }
                    if (mm < 10) {
                        mm = '0' + mm;
                    }

                    today = yyyy + '-' + mm + '-' + dd;
                    document.getElementById("proposalEndDate").setAttribute("min", today);

                    function isNumberKey(evt) {
                        var charCode = (evt.which) ? evt.which : event.keyCode
                        if (charCode > 31 && (charCode < 48 || charCode > 57))
                            return false;
                        return true;
                    }

                    $('#assignment-certification').bootstrapValidator({
                        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                        feedbackIcons: {
                            valid: 'fa fa-check',
                            invalid: 'fa fa-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        fields: {
                            "certification[assignment_name]": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "certification[sic_code]": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "certification[auditor_name]": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "certification[location]": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "certification[effort_tentative]": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "certification[budget_usd]": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "startDate": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "endDate": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "proposalEndDate": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "certification[local_travel_allowance]": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "certification[local_stay_allowance]": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "certification[air_fare]": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "certification[assignment_status]": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            },
                            "certification[description]": {
                                validators: {
                                    notEmpty: {
                                        message: "This field is required."
                                    }
                                }
                            }
                        }
                    })

                            .on('success.form.bv', function (e) {
//                $('#confirmModal').show();
//                $("#content-form").html($("#assignment-request").serialize());
                                return true;
                            });
</script>