@include('includes.header')
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="pr-5">
                                <h4 class="p-4">Assignment List</h4>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="btnsection ">
                                <a href="{{ asset('allassignment') }}">
                                    <button type="button" class="btn btn-dark btn-sm" style="display: none;">
                                        My Assignments
                                    </button></a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12 mb40">
                            <div id="tabAssign">
                                <ul class="nav nav-tabs tabs-default mb30" role="tablist bg-light">
                                    <?php if (@Auth::user()->usertype == 6) { ?>
                                        <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs show active" href="#audit" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Audit</a></li>
                                    <?php } else if (@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) { ?>
                                        <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs show active" href="#training" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Training</a></li>
                                    <?php } ?>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    @if (@Auth::user()->usertype == 6)
                                    <div role="tabpanel" class="tab-pane fade show active" id="audit">
                                        <div class="container">
                                            <div class="row">
                                                @if(count(@$assignmentlistaudit) > 0)
                                                @foreach(@$assignmentlistaudit as $k => $v)
                                                <div class="col-md-4 mb30">
                                                    <div class="card card-body text-center">
                                                        <h4 class="card-title">
                                                            <a href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}">
                                                                @if(strlen($v->assignment_name) > 20)
                                                                {{substr(ucfirst($v->assignment_name), 0, 20) . "..."}}
                                                                @else
                                                                {{ucfirst($v->assignment_name)}}
                                                                @endif
                                                            </a>
                                                        </h4>
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                        <div class="mx-auto">
                                                            <?php 
                                                            /*days left*/
                                                            $now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
                                                            $end_date = Carbon\Carbon::parse($v->proposal_end_date);
                                                            $now_date = Carbon\Carbon::parse($now);
                                                            $daysleft = $now_date->diffInDays($end_date, false);
                                                            /*days left*/
                                                            /*budget counted with approved bid*/
                                                            $assignmentBudget = $v->budget_usd - $v->jobBudget;
                                                            $assignmentEffort = $v->effort_tentative - $v->jobEffort;
                                                            /*budget counted with approved bid*/
                                                            ?>
                                                            <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $assignmentBudget}}</span>
                                                            {{" ( " . trans('formlabel.assignmentinfoaudit.efforts') .  $assignmentEffort . config('assignment.effortdays') . ")" }}
                                                            @if($v->assignment_status == 2)
                                                            <span class="text-danger">{{trans('formlabel.auditdetail.draft')}}</span>
                                                            @elseif($v->approval_status == 1)
                                                            <span class="text-success">{{trans('formlabel.auditdetail.awarded')}}</span>
                                                            @elseif(@$daysleft < 0) 
                                                            <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                            @elseif(@$daysleft == 0)
                                                            <span class="text-success">{{trans('formlabel.auditdetail.openfortoday')}}</span>
                                                            @else
                                                            <span class="text-success">Open / {{@$daysleft}} days left</span>
                                                            @endif
                                                        </div>
                                                        </div>
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                            <div class="col-3"></div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="col-3"></div>
                                                        </div>
                                                        <hr/>
                                                        <p class="card-text text-left">
                                                {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                                                {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                                                <?php
                                                $citynames = "";
                                                    foreach (@$v->AssignmentLocation as $key => $val) {
                                                    if(!$val->whomtoassign_id) {
                                                    $citynames .= $val->city_name . ", ";
                                                    }
                                                }
                                                ?>
                                                {{trans('formlabel.assignmentinfoaudit.location')}} {{rtrim($citynames, ", ")}}<br>
                                                <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                                            </p>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No assignments found.
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @elseif (@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1)
                                    <div role="tabpanel" class="tab-pane fade show active" id="training">
                                        <div class="container">
                                            <div class="row">
                                                @if(count(@$assignmentlisttraining) > 0)
                                                @foreach(@$assignmentlisttraining as $k=>$v)
                                                <div class="col-md-4 mb30">
                                                    <div class="card card-body text-center">
                                                        <h4 class="card-title">
                                                            <a href="{{ asset('assignmentTrainingDetail/'.$v->assignment_id)}}">{{ucfirst($v->batch_no)}}</a>
                                                        </h4>
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                            <div class="mx-auto">
                                                                <?php
                                                                /* days left */
                                                                $now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
                                                                $end_date = Carbon\Carbon::parse($v->proposal_end_date);
                                                                $now_date = Carbon\Carbon::parse($now);
                                                                $daysleft = $now_date->diffInDays($end_date, false);
                                                                /* days left */
                                                                /* budget counted with approved bid */
                                                                $assignmentBudget = $v->fee_range - $v->jobBudget;
                                                                $assignmentEffort = $v->effort_tentative - $v->jobEffort;
                                                                /* budget counted with approved bid */
                                                                ?>
                                                                <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $assignmentBudget}}</span>
                                                                {{"(" . trans('formlabel.assignmentinfoaudit.efforts') .  $assignmentEffort . config('assignment.effortdays') . ")" }}
                                                                @if($v->approval_status == 1)
                                                                <span class="text-success">{{trans('formlabel.auditdetail.awarded')}}</span>
                                                                @elseif(@$daysleft < 0)
                                                                <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                                @elseif(@$daysleft == 0)
                                                                <span class="text-success">{{trans('formlabel.auditdetail.openfortoday')}}</span>
                                                                @else
                                                                <span class="text-success">Open / {{@$daysleft}} days left</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        @if($v->mode == 1)
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                            <div class="col-3"></div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="col-3"></div>
                                                        </div>
                                                        @endif
                                                        <hr/>
                                                        <p class="card-text text-left">
                                                            {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('assignmentTrainingDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                                                            {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                                                            {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                                                            @if($v->mode == 1)
                                                            <?php
                                                            $citynames = "";
                                                            foreach (@$v->AssignmentLocation as $key => $val) {
                                                                if (!$val->whomtoassign_id) {
                                                                    $citynames .= $val->city_name . ", ";
                                                                } elseif ($v->approval_status == 1) {
                                                                    $citynames .= $val->city_name . ", ";
                                                                }
                                                            }
                                                            ?>
                                                            {{trans('formlabel.assignmentinfotraining.mode')}} {{config('assignment.modef2f')." (" . rtrim($citynames, ", ") . ")"}}<br>
                                                            @else
                                                            {{trans('formlabel.assignmentinfotraining.mode')}} {{config('assignment.modeonline')}}<br>
                                                            @endif
                                                            <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No assignments found.
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @elseif (@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1)
                                    <div role="tabpanel" class="tab-pane fade {{ @$tabActive == 3 ? 'show active' : '' }}" id="certification">
                                        <div class="container">
                                            <div class="row">
                                                @if(count(@$assignmentcertification) > 0)
                                                @foreach(@$assignmentcertification as $key=>$val)
                                                @foreach($val->assignmentcertification as $k=>$v)
                                                <div class="col-md-4 mb30">
                                                    <div class="card card-body text-center">
                                                        <h4 class="card-title">
                                                            <a href="{{ asset('assignmentCertificationDetail/'.$v->assignment_id)}}">{{substr(ucfirst($v->assignment_name), 0, 20) . "..."}}</a>
                                                        </h4>
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                            <div class="col-3"></div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="col-3"></div>
                                                        </div>
                                                        <hr/>
                                                        <p class="card-text text-left">{{substr($v->description, 0, 40) . "..."}}<br>
                                                            Ref ID : <a href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}">{{"#".$v->ref_id}}</a><br>
                                                            Duration : {{$v->effort_tentative . " days"}}<br>
                                                            <?php
                                                            $names = "";
                                                            $cityArray = explode(",", $v->location);
                                                            foreach ($cityArray as $key => $val) {
                                                                $names .= @$cityNameArray[$val] . ", ";
                                                            }
                                                            ?>
                                                            Location : {{rtrim($names, ", ")}}<br>
                                                            <strong>{{$v->add_date}}</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @endforeach
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No assignments found.
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <!-- Tab panes -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@include('includes.footer')