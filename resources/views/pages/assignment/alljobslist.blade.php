@include('includes.header')
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="pr-5">
                                <h4 class="p-4">Jobs List</h4>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12 mb40">
                            <div id="tabAssign">
                                <ul class="nav nav-tabs tabs-default mb30" role="tablist bg-light">
                                    <?php 
                                    if (@Auth::user()->usertype == 8) { ?>
                                        <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs show active" href="#audit" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Audit</a></li>
                                   <?php } else if (@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) { ?>
                                        <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs show active" href="#certification" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Certification</a></li>
                                  <?php } else if (@Auth::user()->usertype == 6) { ?>
                                      <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs show active" href="#certification" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Certification</a></li>
                                      <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs" href="#audit" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Audit</a></li>
                                 <?php }
                                    ?>
                                    
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade  {{@Auth::user()->usertype == 8 ? "show active" : "" }}" id="audit">
                                        <div class="container">
                                            <div class="row">
                                                @if(count(@$jobslist) > 0)
                                                @foreach(@$jobslist as $k=>$v)
                                                <div class="col-md-4 mb30">
                                                    <div class="card card-body text-center">
                                                        <h4 class="card-title">
                                                            <a href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}">{{ucfirst($v->assignment_name)}}</a>
                                                        </h4>
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                            <div class="col-3"></div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="col-3"></div>
                                                        </div>
                                                        <hr/>
                                                        <p class="card-text text-left">{{substr($v->description, 0, 40) . "..."}}<br>
                                                            Ref ID : <a href="{{ asset('assignmentAuditDetail/'.$v->id)}}">{{"#".$v->ref_id}}</a><br>
                                                            Duration : {{$v->effort_tentative . " days"}}<br>
                                                            Location : {{$v->location}}<br>
                                                            <strong>{{$v->add_date}}</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No jobs found.
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade {{@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1 || @Auth::user()->usertype == 6 ? "show active" : "" }}" id="certification">
                                        <div class="container">
                                            <div class="row">
                                                @if(count(@$certificationjob) > 0)
                                                @foreach(@$certificationjob as $k=>$v)
                                                <div class="col-md-4 mb30">
                                                    <div class="card card-body text-center">
                                                        <h4 class="card-title">
                                                            <a href="{{ asset('assignmentCertificationDetail/'.$v->assignment_id)}}">{{ucfirst($v->assignment_name)}}</a>
                                                        </h4>
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                            <div class="col-3"></div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="col-3"></div>
                                                        </div>
                                                        <hr/>
                                                        <p class="card-text text-left">{{substr($v->description, 0, 40) . "..."}}<br>
                                                            Ref ID : <a href="{{ asset('assignmentCertificationDetail/'.$v->assignment_id)}}">{{"#".$v->ref_id}}</a><br>
                                                            Duration : {{$v->effort_tentative . " days"}}<br>
                                                            Location : {{$v->location}}<br>
                                                            <strong>{{$v->add_date}}</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No jobs found.
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tab panes -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@include('includes.footer')