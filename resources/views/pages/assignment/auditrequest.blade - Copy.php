
<style>
    #loading-image {
        position: absolute;
        z-index: 1;
        right: -5px;
        top: 7px;
    }
    div#locationCity span {
        padding: 2px 5px !important;
    }
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        border-radius: 5px;
    }
    .fa-times {
        cursor: pointer;
    }
    .form-group i.form-control-feedback {
        display: none !important;
    }
    .form-group small[data-bv-for="audit_effort[]"],
    .form-group small[data-bv-for="country"],
    .form-group small[data-bv-for="states"]
    {
        color: #b24d4b !important;
    }
    i#addLocation {
        font-size: 1rem;
        cursor: pointer;
        position: absolute;
        right: -1%;
        top: 14%;
    }
</style>
<form name="{{ @$auditdetails->id ? 'assignmentaudit-edit' : 'assignment-audit' }}" id="{{ @$auditdetails->id ? 'assignmentaudit-edit' : 'assignment-audit' }}" method="post" action="{{ asset('requestassignment') }}">
    {!! csrf_field() !!}
    <input type="hidden" name="user_id" id="user_id" value="{{@Auth::user()->id}}">
    <input type="hidden" name="id_update" id="id_update" value="{{@$auditdetails->id}}">
    <input type="hidden" name="assignment_id" id="id_assignment" value="{{@$auditdetails->assignment_id}}">
    <input type="hidden" id="select_sic_code" value="{{@$auditdetails->sic_code}}">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-12">
            <label>{{trans('formlabel.postassignmentaudit.assignmentname')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="audit[assignment_name]" value="{{@$auditdetails->assignment_name}}">
            </div>
            <div class="form-group" id="codeType">
                @foreach(@$codetype as $k=>$v)
                @php 
                $checkedvar = (($v == @$codeDetail->type) ? 'checked' : ($k == '2') ? 'checked' : ''); 
                @endphp
                <input type="radio" class="p-1" value="{{$k}}" name="codetype" {{$checkedvar}}> {{$v}}&nbsp;
                @endforeach
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.siccode')}}</label>
            <div class="form-group">
                <select class="form-control" required name="audit[sic_code]" id="codeNumber">
                    <option value="">Select Code</option>
                </select>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.effort')}}</label>
            <div class="form-group">
                <input type="text" id="effort_tentative" class="form-control" name="audit[effort_tentative]" value="{{@$auditdetails->effort_tentative}}" onkeypress="return isNumberKey(event)">
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.location')}}</label>
            <div class="row">
                <div class="form-group col-md-6">
                    <select class="form-control" name="country" id="countries" onchange="getLocationInfo('countries', 'states');">
                        <option value="">Select Country</option>
                        @foreach(@$countryList as $k=>$v)
                        <option value="{{@$v->id}}">{{@$v->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <select class="form-control" name="states" id="states" onchange="getLocationInfo('states', 'cities');">
                        <option value="">Select State</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <img id="loading-image" src="{{asset('images')}}/ajax-loader.gif" alt="Loading..." />
                    <input type="text" style="position: relative;" placeholder="{{trans('formlabel.profilecontact.addresscity')}}" class="form-control" id="city" name="city[]" value="" data-bv-notempty-message="Please enter a city name">
                </div>
                <div class="form-group col-md-6">  
                    <input type="text" id="effort" class="form-control" name="audit_effort[]" onkeypress="return isNumberKey(event)" placeholder="Effort in man days" data-bv-notempty-message="Please enter effort">
                    <i id="addLocation" class="fa fa-plus-circle text-success" aria-hidden="true"></i>
                </div>  
            </div>
            <div id="reloadDiv">
                <div class="form-group" id="locationCity">
                @if(@$locationname)
                <?php $sum = 0; ?>
                @foreach(@$locationname as $k => $v)
                <span class="locationCity" data-effort ="{{$v->effort}}">
                    {{$v->city_name . ", " . $v->stateName . ", " . $v->countryName . " (Effort-" . $v->effort . ") &nbsp;"}}
                    <?php $sum+= $v->effort; ?>
                    <a href="javascript:void(0);" onclick="deleteLocation('{{$v->id}}')"><i class="fa fa-times"></i></a>
                </span>&nbsp;
                @endforeach
                @endif
            </div>
            </div>            
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.budget')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="audit[budget_usd]" value="{{@$auditdetails->budget_usd}}" onkeypress="return isNumberKey(event)">
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.proposalenddate')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="proposalEndDate" value="{{(@$auditdetails->proposal_end_date) ? Carbon\Carbon::parse(@$auditdetails->proposal_end_date)->format(config('assignment.dateformat')) : "" }}" id="proposalEndDate" readonly>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-12">
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.startdate')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="startDate" value="{{(@$auditdetails->proposal_end_date) ? Carbon\Carbon::parse(@$auditdetails->start_date)->format(config('assignment.dateformat')) : "" }}" id="start_date" placeholder="" readonly> 
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.enddate')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="endDate" value="{{(@$auditdetails->proposal_end_date) ? Carbon\Carbon::parse(@$auditdetails->end_date)->format(config('assignment.dateformat')) : "" }}" id="end_date" placeholder="" readonly>
            </div>
            <label class="mt-1">{{trans('formlabel.postassignmentaudit.allallowance')}}</label>
            <div class="row">
                <div class="col-md-4 form-group">
                    <label class="">{{trans('formlabel.postassignmentaudit.travelallowance')}}</label>
                    <div class="list-group list-group-horizontal">
                        @foreach(@$allowance as $k => $v)
                        <label class="container-radio pr-2">
                            <input type="radio" value="{{$k}}" <?= (@$auditdetails->local_travel_allowance == $k) ? "checked" : ""; ?> name="audit[local_travel_allowance]">{{$v}}
                            <span class="checkmark"></span> 
                        </label>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4 form-group">
                    <label class="">{{trans('formlabel.postassignmentaudit.stayallowance')}}</label>
                    <div class="list-group list-group-horizontal">
                        @foreach(@$allowance as $k => $v)
                        <label class="container-radio pr-2">
                            <input type="radio" value="{{$k}}" <?= (@$auditdetails->local_stay_allowance == $k) ? "checked" : ""; ?> name="audit[local_stay_allowance]">{{$v}}
                            <span class="checkmark"></span> 
                        </label>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4 form-group">
                    <label class="">{{trans('formlabel.postassignmentaudit.airfare')}}</label>
                    <div class="list-group list-group-horizontal">
                        @foreach(@$allowance as $k => $v)
                        <label class="container-radio pr-2">
                            <input type="radio" value="{{$k}}" <?= (@$auditdetails->air_fare == $k) ? "checked" : ""; ?> name="audit[air_fare]">{{$v}}
                            <span class="checkmark"></span> 
                        </label>
                        @endforeach
                    </div>
                </div>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.currentstatus')}}</label>
            <div class="form-group">
                <select class="form-control" name="audit[assignment_status]">
                    @foreach(@$status as $k=>$v)
                    <option value="{{@$k}}" @if(@$auditdetails->assignment_status == $k) selected=selected @endif>{{@$v}}</option>
                    @endforeach
                </select>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.description')}}</label>
            <div class="form-group">
                <textarea class="form-control" name="audit[description]" id="briefInfo" rows="5" cols="25">{{@$auditdetails->description}}</textarea>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmentaudit.query1')}}</label>
            <div class="input-group">
                <textarea class="form-control" name="audit[query_first]" id="briefInfo" rows="5" cols="25">{{@$auditdetails->query_first}}</textarea>
            </div>
            <!--            <label class="mt-2">{{trans('formlabel.postassignmentaudit.query2')}}</label>
                        <div class="input-group">
                            <textarea class="form-control" name="audit[query_second]" id="briefInfo" rows="5" cols="25"></textarea>
                        </div>-->
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-center btn-md pull-right mt-2">{{trans('formlabel.postassignmentaudit.save')}}</button>
    </div>
</form>

<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>
<script>
   
                    $(document).ready(function () {
                        //trigger click event selected check box
//                         $('#codeType').find(':radio[name=codetype][value="2"]').prop('checked', true).trigger("click");
                        //trigger click event selected check box
                        
                        
                        //change datepicker set value
                        $('#effort_tentative').on('keyup', function () {
                            var effortdays = $("#effort_tentative").val();
                            if(effortdays) {
                                $("#end_date").datepicker("setDate", null);
                                var date = $("#start_date").datepicker("getDate");
                                date.setDate(date.getDate() + parseInt(effortdays,10)); //Set date object adding 3 days.
                                $("#end_date").datepicker("option", "minDate", date);
                                $("#end_date").datepicker("setDate", date); //Set the date of the datepicker with our date object
                            } 
                        });
                        //change datepicker set value
                        
                        
                        $('#loading-image').hide();
                        $("input[type='radio'][name='codetype']:checked").prop("checked", true).trigger("click");
                    });
                    $("input[name='codetype']").click(function () {
                        var radioValue = $("input[type='radio']:checked").val();
                        var select_sic_code = $('#select_sic_code').val();
                        var url = "{{url('/getStandards')}}";
                        if (radioValue) {
                            $('#codeNumber').html("<option value='' >Select Code</option>");
                            $.post(url, {"selected_val": radioValue,"select_sic_code":select_sic_code, "_token": '<?= csrf_token(); ?>'}, function (res) {
                                $("#codeNumber").html(res);
                                var total = $('select#codeNumber option').length;
                                $("#codeNumber_cnt").html(" <b>(" + (total - 1) + ")</b>");
                            });
                        }
                    });
                    //get location list
                    function getLocationInfo(id, tbl) {
                        if (id == "states") {
                            $('#loading-image').show();
                            $('#city').val("");
                            $('#effort').val("");
                        } else {
                            $('#city').val("");
                            $('#effort').val("");
                        }
                        var selected_val = $('select#' + id + ' option:selected').val();
                        var url = "{{url('/getLocationInfo')}}";
                        $.post(url, {"selected_val": selected_val, "id": id, "tbl": tbl, "_token": '<?= csrf_token(); ?>'}, function (res) {
                            if (tbl == 'cities') {
                                availableTags = JSON.parse(res);
                                $('#loading-image').hide();
                                $("#city").autocomplete({
                                    source: availableTags
                                });
                            } else {
                                $("#" + tbl).html(res);
                                var total = $('select#' + tbl + ' option').length;
                                $("#" + tbl + "_cnt").html(" <b>(" + (total - 1) + ")</b>");
                            }
                        });
                    }
//get location list

//delete loation info
function deleteLocation(location_id) {
        if (location_id) {
var url = "{{url('/deleteLocation')}}";
        $.post(url, {'location_id': location_id, '_token': "<?= csrf_token(); ?>"}, function (data) {
        if(data == "Success"){
            $("#reloadDiv").load(" #locationCity");
            return true;
        } else {
            return false;
        }
        });
} else {
return false;
}
}
//delete loation info

//add multiple effort & location
                    $("#addLocation").click(function () {
                        var city = $("#city").val();
                        var effort = $("#effort").val();
                        var stateid = $("#states").val();
                        var countryid = $("#countries").val();
                        var statename = $("#states option:selected").text();
                        var countryname = $("#countries option:selected").text();

                        if (city !== "" && countryid !== "" && stateid !== "") {
                            $('#city').parents('div.form-group').removeClass('has-error');
                            $('<option value="">Select Country</option>').appendTo("#countries");
                            $("#locationCity").show();
                            var res = '<input type="hidden" name=cityname[] value="' + city + '" >\n\
                <input type="hidden" name=stateid[] value="' + stateid + '" >\n\
<input type="hidden" name=countryid[] value="' + countryid + '" >\n\
<input type="hidden" name=effort[] value="' + effort + '" >\n\
' + city + ', ' + statename + ', ' + countryname + " (Effort- " + effort + ")" + '<span class="removeLocation"><i class="fa fa-times"></i></span>';
                            $('<span class="locationCity" data-effort ="'+effort+'">' + res + '</span>&nbsp;').appendTo("#locationCity");
                            $(".removeLocation").click(function () {
                                $(this).parent().remove();
                            });
                        } else {
                            if(countryid == "") {
                                $('#city').parents('div.form-group').addClass('has-error');
                            }
                            $('#city').parents('div.form-group').addClass('has-error');
                            $(".help-block").show();
                        }
                    });
//add multiple effort & location


//datepicker
                    $(function () {
                        $('#proposalEndDate').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: 'dd-mm-yy',
                            minDate: 0,
                            onSelect: function (selected) {
                                $('#proposalEndDate').parents('div.form-group').addClass('has-success');
                                $('#proposalEndDate').parents('div.form-group').removeClass('has-error');
                                $("small[data-bv-for='proposalEndDate").hide();
                                $("#end_date").datepicker("setDate", null);
                                $("#start_date").datepicker("setDate", null);
                                $("#start_date").datepicker("option", "minDate", selected)
                            }
                        });
                        $('#start_date').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: 'dd-mm-yy',
                            onSelect: function (selected) {
                                $('#start_date').parents('div.form-group').addClass('has-success');
                                $('#start_date').parents('div.form-group').removeClass('has-error');
                                $("small[data-bv-for='startDate").hide();
                                var effortdays = $("#effort_tentative").val();
                                if(effortdays) {
                                var date = $(this).datepicker("getDate"); //Get the Date object with actual date
                                date.setDate(date.getDate() + parseInt(effortdays,10)); //Set date object adding 3 days.
                                $("#end_date").datepicker("setDate", date); //Set the date of the datepicker with our date object
                                $("#end_date").datepicker( "option", "minDate", date);
                            } else {
                                alert("Kindly enter effort.");
                            }
                            }
                        });
                        $('#end_date').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: 'dd-mm-yy',
                            onSelect: function (selected) {
                                $('#end_date').parents('div.form-group').addClass('has-success');
                                $('#end_date').parents('div.form-group').removeClass('has-error');
                                $("small[data-bv-for='endDate").hide();
                            }
                        });
                    });
//datepicker
</script>

<script>

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $(document).ready(function () {
        $('#assignment-audit').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "audit[assignment_name]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[sic_code]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[auditor_name]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "country": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "states": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "city[]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit_effort[]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[effort_tentative]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[budget_usd]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "startDate": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "endDate": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "proposalEndDate": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[local_travel_allowance]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[local_stay_allowance]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[air_fare]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[assignment_status]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[description]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                }
            }
        })
                .on('error.form.bv', function (e) {
                    if ($('#codeNumber').val() == "")
                    {
                        $('#codeNumber').parents('div.form-group').addClass('has-error');
                        $('#codeNumber').parents('div.form-group').removeClass('has-success');
                        $("small[data-bv-for='audit[sic_code]").html("This field is required.").show();
                        $("i[data-bv-icon-for='audit[sic_code]").hide();
                        return false;
                    }
                    var dataeffort = 0;
                    $(".locationCity").each(function(){
                        var effort =$(this).attr("data-effort");
                        var eacheffort = parseInt(effort,10);
                        dataeffort = dataeffort+eacheffort;
                    })
                    if($('#effort_tentative').val() != dataeffort ){
                        alert("Total effort mismatched with individual effort.");
                        return false;
                    }
                    if ($('.locationCity').text() == "") {
                        return false;
                    } else {
                        $('#countries, #states, #city, #effort').parents('div.form-group').removeClass('has-feedback has-error');
                        $("small[data-bv-for='country").hide();
                        $("small[data-bv-for='states").hide();
                        $("small[data-bv-for='city[]").hide();
                        $("small[data-bv-for='audit_effort[]").hide();
                        return true;
                    }
                })

                .on('success.form.bv', function (e) {
                    if ($('#codeNumber').val() == "")
                    {
                        $('#codeNumber').parents('div.form-group').addClass('has-error');
                        $('#codeNumber').parents('div.form-group').removeClass('has-success');
                        $("small[data-bv-for='audit[sic_code]").html("This field is required.").show();
                        $("i[data-bv-icon-for='audit[sic_code]").hide();
                        return false;
                    }
                    var values = $("input[name='effort[]']").map(function () {
                        return $(this).val();
                    }).get();
                    var spliteffort = 0;
                    for (var i = 0; i < values.length; i++) {
                        spliteffort += values[i] << 0;
                    }
                    var totalEffort = $("#effort_tentative").val();
                    console.log(totalEffort + "+++++" + spliteffort);
                    if (parseFloat(spliteffort) === parseFloat(totalEffort)) {
                        return true;
                    } else {
                        alert("Total effort mismatched with individual effort.");
                        return false;
                    }
//                $('#confirmModal').show();
//                $("#content-form").html($("#assignment-request").serialize());
                    return true;
                });
    });
</script>

<!---edit assignment validation-->
<script>
$(document).ready(function () {
        $('#assignmentaudit-edit').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "audit[assignment_name]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[sic_code]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[effort_tentative]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[budget_usd]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "startDate": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "endDate": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "proposalEndDate": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[local_travel_allowance]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[local_stay_allowance]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[air_fare]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[assignment_status]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "audit[description]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                }
            }
        })
                .on('error.form.bv', function (e) {
                    if ($('#codeNumber').val() == "")
                    {
                        $('#codeNumber').parents('div.form-group').addClass('has-error');
                        $('#codeNumber').parents('div.form-group').removeClass('has-success');
                        $("small[data-bv-for='audit[sic_code]").html("This field is required.").show();
                        $("i[data-bv-icon-for='audit[sic_code]").hide();
                        return false;
                    }
                    if ($('.locationCity').text() == "") {
                        return false;
                    } 
                })

                .on('success.form.bv', function (e) {
                    var dataeffort = 0;
                    $(".locationCity").each(function(){
                        var effort =$(this).attr("data-effort");
                        var eacheffort = parseInt(effort,10);
                        dataeffort = dataeffort+eacheffort;
                    })
                    if($('#effort_tentative').val() != dataeffort ){
                        alert("Total effort mismatched with individual effort.");
                        return false;
                    }
                    if ($('#codeNumber').val() == "")
                    {
                        $('#codeNumber').parents('div.form-group').addClass('has-error');
                        $('#codeNumber').parents('div.form-group').removeClass('has-success');
                        $("small[data-bv-for='audit[sic_code]").html("This field is required.").show();
                        $("i[data-bv-icon-for='audit[sic_code]").hide();
                        return false;
                    }
//                    var values = $("input[name='effort[]']").map(function () {
//                        return $(this).val();
//                    }).get();
//                    var spliteffort = 0;
//                    for (var i = 0; i < values.length; i++) {
//                        spliteffort += values[i] << 0;
//                    }
//                    var totalEffort = $("#effort_tentative").val();
//                    console.log(totalEffort + "+++++" + spliteffort);
//                    if (parseFloat(spliteffort) === parseFloat(totalEffort)) {
//                        return true;
//                    } else {
//                        alert("Total effort mismatched with individual effort.");
//                        return false;
//                    }
//                $('#confirmModal').show();
//                $("#content-form").html($("#assignment-request").serialize());
                    return true;
                });
    });
</script>
<!---edit assignment validation-->