@include('includes.header')

<?php
//$now = time(); // or your date as well
//$your_date = strtotime(@$auditdetails->proposal_end_date);
//$datediff = $your_date - $now;
//$daysleft = round($datediff / (60 * 60 * 24));

$now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
$end_date = Carbon\Carbon::parse(@$auditdetails->proposal_end_date);
$now_date = Carbon\Carbon::parse($now);
$daysleft = $now_date->diffInDays($end_date, false);
$assignmentBudget = @$auditdetails->budget_usd - @$auditdetails->jobBudget;
?>
<style>
    .tooltip {
        position: relative;
        display: inline-block;
        opacity: 1;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        width: 100%;
        background-color: black;
        color: #fff;
        text-align: left;
        border-radius: 6px;
        padding: 5px;

        /* Position the tooltip */
        position: absolute;
        top: 90%;
        left: 0%;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }
    #queryTooltip {
        width: 100% !important;
        top: 90%;
        right: 0%;
    }
    #queryTool .btn-sm {
        padding: 5% 9% !important;
    }
    #queryToolAdmin .btn-sm {
        padding: 2% 2.5% !important;
    }
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        padding: 2px 5px;
        border-radius: 5px;
    }
    .genderSelect p {
        margin-bottom: 0;
    }
</style>

<!--share modal-->
<div class="modal fade queryModal" id="queryModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Share Assignment</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="shareAssignment" name="shareAssignment" action="{{ asset('shareAssignment')}}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="assignment_id" id="assignment_id" value="{{@$auditdetails->assignment_id}}">
                    <input type="hidden" name="ref_id" id="ref_id" value="{{@$auditdetails->ref_id}}">
                    <label>{{trans('formlabel.shareassignment.enteremail')}}</label>
                    <div class="form-group">
                        <input type="email" class="form-control" id="share_email" name="share_email" required>
                    </div>
                    <button type="submit" class="btn btn-primary text-center">{{trans('formlabel.shareassignment.sharebutton')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!--share modal-->

<div class="container">
    <div class="main-content">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="details mt-4 border-right">
                            <h5 class="profile-no">{{config('assignment.refid') . @$auditdetails->ref_id}} 
                                <!--- assignment status for send bid--->
                                <small class="text-success">
                                    @if(@$auditdetails->assignment_status == 2)
                                    <span class="text-danger">{{trans('formlabel.auditdetail.draft')}}</span>
                                    @elseif(@$userdetails[0]->approval_status == 1)
                                    {{trans('formlabel.auditdetail.awarded')}}
                                    @elseif(@$daysleft < 0) 
                                    <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                    @elseif(@$daysleft == 0)
                                    {{trans('formlabel.auditdetail.openfortoday')}}
                                    @else
                                    Open / {{@$daysleft}} days left
                                    @endif
                                </small>
                                <!--- assignment status for send bid--->
                            </h5>
                            <div>{{@$auditdetails->assignment_name}} 
                                @if(@$userdetails[0]->approval_status == 0)
                                ({{trans('formlabel.auditdetail.proposals')}}{{@$countTotalBid}})
                                @endif
                            </div>
                            <div>{{trans('formlabel.auditdetail.postedby')}}<span class="text-primary"><b>{{ucfirst(@$userdetails[0]->fname) . " " . ucfirst(@$userdetails[0]->lname)}}</b></span> </div>
                            @if (@Auth::user()->id && @$userdetails[0]->approval_status == 0 && @$daysleft >= 0)
                            <div>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#queryModal">
                                    <button type="button" class="btn btn-orange btn-sm btn-xs">
                                        <i style="font-size: 1rem;" class="fa fa-share-alt font16" aria-hidden="true"></i> {{trans('formlabel.shareassignment.shareassignment')}}
                                    </button>
                                </a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!---send, edit & delete proposal button-->
            <div class="col-md-6 col-sm-6">
                <div class="btnsection mt-2 pull-right">
                    <h6 class="text-center">{{trans('formlabel.auditdetail.budget')}}</h6>
                    <div class="buget-price">{{config('assignment.price') . " ". $assignmentBudget}}</div>
                    @if(@Auth::id())
                    @if(@Auth::user()->id == @$userdetails[0]->id && (@$auditdetails->assignment_status == 2 || count(@$proposaldetails) == 0))
                    <div class="btn-group">
                        <a href="{{ asset('requestassignment/'.@$auditdetails->assignment_id)}}">
                            <button type="button" class="btn btn-orange btn-sm btn-xs">{{trans('formlabel.auditdetail.editassignment')}}</button>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="javascript:void(0);" onclick="deleteAssignment('{{@$auditdetails->assignment_id}}')">
                            <button type="button" class="btn btn-orange btn-sm btn-xs">{{trans('formlabel.auditdetail.deleteassignment')}}</button>
                        </a>
                    </div>
                    @elseif(@Auth::user()->id == @$userdetails[0]->id && @$userdetails[0]->approval_status == 0)
                    <div class="btn-group">
                        <a href="javascript:void(0);" onclick="deleteAssignment('{{@$auditdetails->assignment_id}}')">
                            <button type="button" class="btn btn-orange btn-sm btn-xs">{{trans('formlabel.auditdetail.deleteassignment')}}</button>
                        </a>
                    </div>
                    @elseif(@Auth::user()->id == @$proposalEdit->user_id && @$proposalEdit->approval_status == 0 && @$userdetails[0]->approval_status == 0)
                    <div class="btn-group">
                        <a data-toggle="modal" class="pr-2" data-target="#proposalModal" style="cursor: pointer;">
                            <button type="button" class="btn btn-orange btn-sm btn-xs mb3">{{trans('formlabel.auditdetail.editbid')}}</button>
                        </a>
                        <a href="javascript:void(0);" onclick="deleteProposal('{{@$proposalEdit->id}}')">
                            <button type="button" class="btn btn-danger btn-sm btn-xs mb3">{{trans('formlabel.auditdetail.deletebid')}}</button>
                        </a>
                    </div>
                    @elseif(@$daysleft >= 0 && @Auth::user()->usertype == 8 && @$userdetails[0]->approval_status == 0 && (@$proposalEdit->approval_status != 1))
                    <div class="btn-group">
                        <!--<a data-toggle="modal" data-target="#proposalModal" style="cursor: pointer;">-->
                        <a href="javascript:void(0);" onclick="checkRespondBid('{{@$auditdetails->assignment_id}}')" style="cursor: pointer;">
                            <button type="button" class="btn btn-orange btn-sm btn-xs">{{trans('formlabel.auditdetail.respondbid')}}</button>
                        </a>
                    </div>
                    @endif
                    @elseif(@$daysleft < 0)
                    @elseif(@$userdetails[0]->approval_status == 0)
                    <div class="btn-group">
                        <a href="{{asset('signin')}}/phone/{{@$auditdetails->assignment_id}}">
                            <button type="button" class="btn btn-orange btn-sm btn-xs">{{trans('formlabel.auditdetail.respondbid')}}</button>
                        </a>
                    </div>
                    @else
                    @endif
                </div>
            </div>
            <!---send, edit & delete proposal button-->
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container">
    <!--assignment details-->
    <hr>
    <h5>{{trans('formlabel.auditdetail.assignmenttitle')}}</h5>
    <div class="row">
        <div class="col-md-12 mb30">
            <div class="card card-body">
                <p class="text-grey" style="font-size:1.3rem;">{{@$auditdetails->description}}</p>
                <h6 class="text-dark">{{trans('formlabel.auditdetail.assignmentlocation')}}</h6>
                <address>
                    @foreach(@$locationname as $key=>$val)
                    <span class="locationCity">
                        <i class="fa fa-map-marker pr-1 text-danger" aria-hidden="true"></i>
                        {{$val->city_name . ", " . $val->stateName . ", " . $val->countryName . " (Effort- " . $val->effort . " days)"}}
                        @if($val->whomtoassign_id)
                        <span class="text-success">Awarded</span>
                        @endif
                    </span>&nbsp;
                    @endforeach
                </address>
                <hr>
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="inforamation-section">
                            <div>{{trans('formlabel.auditdetail.section')}}<span class="text-dark">{{@$standardDeatil->code ." - (". @$standardDeatil->type ." / ". @$standardDeatil->name . ")"}}</span> </div>
                            <div>{{trans('formlabel.auditdetail.efforts')}}<span class="text-dark">{{@$auditdetails->effort_tentative . config('assignment.effortdays')}}</span> </div>
                            <div>{{trans('formlabel.auditdetail.startdate')}}<span class="text-dark">{{Carbon\Carbon::parse(@$auditdetails->start_date)->format(config('assignment.dateformat'))}}</span> </div>
                            <div>{{trans('formlabel.auditdetail.endadte')}}<span class="text-dark">{{Carbon\Carbon::parse(@$auditdetails->end_date)->format(config('assignment.dateformat'))}}</span> </div>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="inforamation-section">
                            <div>{{trans('formlabel.auditdetail.localtravel')}} 
                                <span class="text-dark">
                                    <i style="font-size: 1rem;" class="{{@$auditdetails->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                </span> 
                            </div>
                            <div>{{trans('formlabel.auditdetail.localstay')}}<span class="text-dark">
                                    <i style="font-size: 1.2rem;" class="{{@$auditdetails->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                </span> </div>
                            <div>{{trans('formlabel.auditdetail.airfare')}}<span class="text-dark">
                                    <i style="font-size: 1.2rem;" class="{{@$auditdetails->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                </span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!--assignment details-->
</div>

@if(@Auth::id() && count(@$proposaldetails) > 0) <!--check user is login or not and proposaldetails available or not-->
<!---proposal details-->
<div class="container">
    <h5>{{trans('formlabel.auditdetail.bidheading')}}({{count(@$proposaldetails)}})</h5>
    @foreach(@$proposaldetails as $key=>$val)
    <div class="card mb-2" style="{{@Auth::user()->id == $val->user_id ? 'border:1px solid green' : '' }}">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-7 col-12">
                @if(@$val->profile_img)
                <img src="{{asset('profile_image')}}/{{@$val->profile_img}}" style="height:146px" width="140" class="img-fluid pull-left mr-4" />
                @else
                <img src="{{asset('images')}}/{{@$val->gender == 1 ? 'profile_default.jpg' : 'profile_female.jpg' }}" style="border: 1px solid rgba(0,0,0,.125);" width="140" class="img-fluid pull-left mr-4">
                @endif
                <h6 class="text-dark m-2">
                    <strong>
                        <a href="{{ asset('publicProfileView/'.@$val->userId)}}">
                            {{ucfirst(@$val->fname) . " " . ucfirst(@$val->lname)}}
                        </a>
                    </strong>
                </h6>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span><br>
                <div class="text-dark tooltip">
                    {{substr(ucfirst(@$val->description), 0, 60) . "..."}}
                    <span class="tooltiptext">{{ucfirst(@$val->description)}}</span>
                </div>
                <div class="small">{{Carbon\Carbon::parse(@$val->add_date)->format(config('assignment.adddateformat'))}}</div>
            </div>

            <div class="col-lg-5 col-md-5 col-sm-4 col-12">
                <!--seeker-->
                @if(@Auth::user()->id == $val->assignment_user_id)
                <div class="d-flex pull-right pt-3 mr-3">
                    <div class="p-2"><strong>{{trans('formlabel.auditdetail.budget')}}</strong> <span class="text-dark">{{config('assignment.price') ." ". @$val->estimated_budget}}</span></div>
                    <div class="p-2"><strong>{{trans('formlabel.auditdetail.time')}}</strong><span class="text-dark">{{@$val->estimated_effort . config('assignment.effortdays')}}</span></div>
                </div>
                <div class="clearfix"></div>
                <div class="text-right mr-3" id="queryToolAdmin">
                    <a href="{{ url("/sendMessage/".@$val->user_id."/".@$val->id."/".@$auditdetails->ref_id) }}">
                    <button type="button" class="btn btn-orange btn-sm btn-xs mb3"><i class="fa fa-envelope"></i> {{trans('formlabel.auditdetail.message')}}</button>
                    </a>
                    @if($val->approval_status == 0 && @$userdetails[0]->approval_status == 0)
                    <a href="javascript:void(0);" onclick="acceptProposal('{{@$val->id}}','{{@$auditdetails->ref_id}}','{{@$auditdetails->assignment_id}}','{{@$userdetails[0]->assignment_type}}')">
                        <button type="button" class="btn btn-success  btn-sm btn-xs mb3"><i class="fa fa-check-circle"></i> {{trans('formlabel.auditdetail.accept')}}</button>
                    </a>
                    <a href="javascript:void(0);" onclick="rejectProposal('{{@$val->id}}', '{{@$userdetails[0]->assignment_type}}')">
                        <button type="button" class="btn btn-danger btn-sm btn-xs mb3"><i class="fa fa-crosshairs"></i> {{trans('formlabel.auditdetail.reject')}}</button>
                    </a>
                    @elseif(@$val->approval_status == 1)
                    <button type="button" class="btn btn-success btn-sm btn-xs mb3"><i class="fa fa-crosshairs"></i> {{trans('formlabel.auditdetail.accepted')}}</button>
                    @elseif(@$val->approval_status == 2)
                    <button type="button" class="btn btn-danger btn-sm btn-xs mb3"><i class="fa fa-crosshairs"></i> {{trans('formlabel.auditdetail.rejected')}}</button>
                    @elseif(@$val->approval_status == 3)
                    <button type="button" class="btn btn-danger btn-sm btn-xs mb3"><i class="fa fa-crosshairs"></i> {{trans('formlabel.auditdetail.cancelled')}}</button>
                    @endif
                    <a data-toggle="modal" data-target="#queryModal{{@$val->id}}" style="cursor: pointer;">
                        <button type="button" class="btn btn-danger btn-xs btn-sm mb3">
                            <i class="fa fa-question-circle"> {{trans('formlabel.auditdetail.moreinfo')}}</i>
                        </button>
                    </a>
                </div>
                @else
                <!--seeker-->
                <!--provider-->
                <!--<div class="d-flex pull-right mt-4 mb-4 p-1">-->
                <div class="d-flex pull-right m-2 p-1">
                    <div class="text-dark p-2 pr-4"><strong>{{trans('formlabel.auditdetail.budget')}}</strong> <span class="text-dark">{{config('assignment.price') ." ". @$val->estimated_budget}}</span>
                        &nbsp;&nbsp;
                        <strong>{{trans('formlabel.auditdetail.time')}}</strong><span class="text-dark"> {{@$val->estimated_effort . config('assignment.effortdays')}}</span>
                        <br>
                        @if(@Auth::user()->id == $val->user_id)
                        <div id="text-right mr-3">
                            <a href="{{ url("/sendMessage/".@$userdetails[0]->user_id."/".@$val->id."/".@$auditdetails->ref_id) }}">
                            <button type="button" class="btn btn-orange btn-sm btn-xs mb3"><i class="fa fa-envelope"></i> {{trans('formlabel.auditdetail.message')}}</button>
                            </a>
                            <a data-toggle="modal" data-target="#queryModal{{@$val->id}}" style="cursor: pointer;">
                                <button type="button" class="btn btn-danger btn-xs btn-sm mb3">
                                    <i class="fa fa-question-circle"> {{trans('formlabel.auditdetail.moreinfo')}}</i> 
                                </button>
                            </a>
                            @if(@$val->approval_status == 1)
                            <button type="button" class="btn btn-success btn-sm btn-xs mb3"><i class="fa fa-crosshairs"></i> {{trans('formlabel.auditdetail.accepted')}}</button>
                            @elseif(@$val->approval_status == 2)
                            <button type="button" class="btn btn-danger btn-sm btn-xs mb3"><i class="fa fa-crosshairs"></i> {{trans('formlabel.auditdetail.rejected')}}</button>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
                <!--provider-->
                @endif
            </div>
        </div>
    </div>

    <!--- proposal modal-->
    <div class="modal fade queryModal" id="queryModal{{@$val->id}}" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center">{{ucfirst(@$val->fname) . " " . ucfirst(@$val->lname)}}</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 border-right">
                            <div class="inforamation-section">
                                <div>{{trans('formlabel.sendproposalaudit.assignmentlocation')}} 
                                    <?php
                                    $cityname = "";
                                    $cityids = explode(",", @$val->assignment_location_id);
                                    foreach (@$cityids as $k => $v) {
                                        foreach (@$locationname as $ke => $va) {
                                            if ($va->id == $v) {
                                                $cityname .= $va->city_name . ", " . $va->stateName . ", " . $va->countryName . " ";
                                            }
                                        }
                                    }
                                    ?>
                                    {{$cityname}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div>{{trans('formlabel.sendproposalaudit.estimatedbudget')}} <span class="text-dark">{{config('assignment.price') ." ". $val->estimated_budget}}</span> </div>
                            <div>{{trans('formlabel.sendproposalaudit.estimatedeffortbid')}} <span class="text-dark">{{$val->estimated_effort . config('assignment.effortdays')}}</span> </div>
                        </div>
                    </div>
                    <br>
                    <div><strong>{{trans('formlabel.sendproposalaudit.description')}}</strong> <span class="text-dark">{{$val->description}}</span> </div>
                    <br>
                    @if($val->query_ans)
                    <strong>{{trans('formlabel.sendproposalaudit.bidquestions')}} {{ucfirst(@$auditdetails->query_first)}}</strong>
                    <br>
                    {{trans('formlabel.sendproposalaudit.bidanswers')}} {{ucfirst($val->query_ans)}}
                    @else
                    No query found.
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--- proposal modal-->
    @endforeach
</div>
<!---proposal details-->
@endif <!--check user is login or not and proposaldetails available or not-->

<!--send bid modal--->
<div class="modal fade" id="proposalModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">{{trans('formlabel.sendproposalaudit.nametitle')}}</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                @if (@$countisicCode == 0)
                <div class="text-danger">
                    <h5>
                        You have not added any ISIC/ANZSIC/NACE code yet. Please add in profile section and send your bid again.
                    </h5>
                </div>
                @elseif(@$codeMapping == 0)
                <div class="text-danger">
                    <h5>
                        You are not eligible to post bid on {{@$standardDeatil->code ." - (". @$standardDeatil->type ." / ". @$standardDeatil->name . ")"}}. Please add in profile section and send your bid again.
                    </h5>
                </div>
                @else
                <form id="sendProposal" name="sendProposal" action="{{ asset('sendproposal')}}" method="post">
                    @if(@$proposalEdit->approval_status == 2)
                    @php @$proposalEdit = array(); @endphp
                    @endif
                    {!! csrf_field() !!}
                    <input type="hidden" name="proposal_id" value="{{@$proposalEdit->id}}">
                    <input type="hidden" name="proposal[assignment_user_id]" value="{{@$userdetails[0]->id}}">
                    <input type="hidden" name="proposal[assignment_id]" value="{{@$auditdetails->assignment_id}}">
                    <input type="hidden" name="ref_id" value="{{@$auditdetails->ref_id}}">
                    <input type="hidden" name="assignment_name" value="{{@$auditdetails->assignment_name}}">
                    <input type="hidden" name="fcm_device_token" value="{{@$userdetails[0]->fcm_device_token}}">
                    <input type="hidden" name="fname" value="{{@$userdetails[0]->fname}}">
                    <input type="hidden" name="lname" value="{{@$userdetails[0]->lname}}">
                    <input type="hidden" name="email" value="{{@$userdetails[0]->email}}">
                    <input type="hidden" name="proposal[estimated_effort]" id="estimated_effort" value="">
                    <input type="hidden" name="assignment_type" value="1">
                    <div>
                        <label><b>{{trans('formlabel.sendproposalaudit.estimatedeffort')}}</b></label>
                        <span>{{@$auditdetails->effort_tentative. " ". config('assignment.effortdays')}}</span>
                    </div>
                    <label>{{trans('formlabel.sendproposalaudit.assignmentlocation')}}</label>
                    <div class="form-group col s12 genderSelect">
                        <?php
                        $locationArray = explode(',', @$proposalEdit->assignment_location_id);
                        ?>
                        <p>
                            @foreach(@$locationname as $k=>$v) 
                            @if(!$v->whomtoassign_id)
                            <input name="locationId[]" class="locationId" data-effort="{{$v->effort}}" <?= (in_array($v->id, $locationArray)) ? "checked" : ""; ?> value="{{$v->id}}" type="checkbox">
                            <label for="locationId[]">
                                {{$v->city_name . ", " . $v->stateName . ", " . $v->countryName . " (Effort-" . $v->effort . " days)"}}
                            </label><br>
                            @endif
                            @endforeach
                        </p>
                    </div>
                    <label>{{trans('formlabel.sendproposalaudit.estimatedbudget')}}</label>
                    <div class="form-group">
                        <input type="number" class="form-control" id="estimatedbudget" name="proposal[estimated_budget]" value="{{@$proposalEdit->estimated_budget}}">
                    </div>
                    @if($auditdetails->query_first)
                    <div id="queryBlock">
                        <label>{{trans('formlabel.sendproposalaudit.bidquestions')}} {{@$auditdetails->query_first}}</label>
                        <div class="form-group">
                            <textarea class="form-control" name="proposal[query_ans]" id="query_ans" rows="5" cols="15">{{@$proposalEdit->query_ans}}</textarea>
                        </div>
                    </div>
                    @endif
                    <label>{{trans('formlabel.sendproposalaudit.description')}}</label>
                    <div class="form-group">
                        <textarea class="form-control" name="proposal[description]" id="description" rows="5" cols="15">{{@$proposalEdit->description}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary text-center">{{trans('formlabel.sendproposalaudit.submitproposal')}}</button>
                </form>
                @endif
            </div>
        </div>
    </div>
</div>
<!--send bid modal--->




<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>
<script>
                                                                $('#shareAssignment').bootstrapValidator({
                                                        fields: {
                                                        "share_email": {
                                                        validators: {
                                                        notEmpty: {
                                                        message: "This field is required."
                                                        }
                                                        }
                                                        }
                                                        }
                                                        })
                                                                .on('success.form.bv', function (e) {
                                                                return true;
                                                                });
                                                                $(document).ready(function () {
                                                        $('.locationId').click(function(){
                                                        sum = 0;
                                                                $(".locationId:checked").each(function(){
                                                        sum += parseInt($(this).attr("data-effort"));
                                                        });
                                                                $("#estimated_effort").val(sum);
                                                        })
                                                                });
                                                                $('#sendProposal').bootstrapValidator({
                                                        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                                                        feedbackIcons: {
                                                        valid: 'fa fa-check',
                                                                invalid: 'fa fa-remove',
                                                                validating: 'glyphicon glyphicon-refresh'
                                                        },
                                                                fields: {
                                                                "locationId[]": {
                                                                validators: {
                                                                notEmpty: {
                                                                message: "This field is required."
                                                                }
                                                                }
                                                                },
                                                                        "proposal[estimated_budget]": {
                                                                        validators: {
                                                                        notEmpty: {
                                                                        message: "This field is required."
                                                                        }
                                                                        }
                                                                        },
                                                                        "proposal[query_ans]": {
                                                                        validators: {
                                                                        notEmpty: {
                                                                        message: "This field is required."
                                                                        }
                                                                        }
                                                                        },
                                                                        "proposal[description]": {
                                                                        validators: {
                                                                        notEmpty: {
                                                                        message: "This field is required."
                                                                        }
                                                                        }
                                                                        }
                                                                }
                                                        }).on('success.form.bv', function (e) {
                                                        return true;
                                                        });
                                                        function checkRespondBid(assignmentid) {
                                                            if(assignmentid) {
                                                                var url = "{{url('/checkRespondBid')}}"; //alert(url)
                                                                        $.post(url, {'assignmentid': assignmentid, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                                                        if(data == "success") {
                                                                            var confirmMsg = confirm('You have bids/jobs in this assignment time span.');
                                                                            if(confirmMsg) {
                                                                                $("#proposalModal").modal('show');
                                                                            } else {
                                                                                return false;
                                                                            }
                                                                        } else {
                                                                            $("#proposalModal").modal('show');
                                                                        }
                                                                        });
                                                            } else {
                                                                return false;
                                                            }
                                                        }
                                                                function rejectProposal(id, assignmentType) {
                                                                var del = confirm('Are you sure to reject this?');
                                                                        if (del) {
                                                                var url = "{{url('/rejectProposal')}}"; //alert(url)
                                                                        $.post(url, {'id': id, 'assignmentType': assignmentType, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                                                        location.reload();
                                                                        });
                                                                } else {
                                                                return false;
                                                                }
                                                                }
                                                        function acceptProposal(id, refId, assignmentId, assignmentType) {
                                                        var del = confirm('Are you sure to accept this?');
                                                                if (del) {
                                                        var url = "{{url('/acceptProposal')}}"; //alert(url)
                                                                $.post(url, {'id': id, 'refId': refId, 'assignmentId': assignmentId, 'assignmentType': assignmentType, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                                                if (data == "Success") {
                                                                location.reload();
                                                                } else if (data == "Error") {
                                                                alert("Error in accept bid!");
                                                                        return false;
                                                                }
                                                                else {
                                                                alert("Already accepted for location " + data + ". Kindly reject the bid & inform bidder to bid again.");
                                                                        return false;
                                                                }
                                                                });
                                                        } else {
                                                        return false;
                                                        }
                                                        }

                                                        function startJob(assignment_id, ref_id) {
                                                        var jobCofirm = confirm('Are you sure to convert your assignment to job?');
                                                                if (jobCofirm) {
                                                        var url = "{{url('/startJob')}}"; //alert(url)
                                                                $.post(url, {'assignment_id': assignment_id, 'ref_id': ref_id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                                                window.location.href = "{{asset('myjobslist')}}";
                                                                });
                                                        } else {
                                                        return false;
                                                        }
                                                        }

                                                        function deleteProposal(id) {
                                                        var del = confirm('Are you sure to delete this?');
                                                                if (del) {
                                                        var url = "{{url('/deleteProposal')}}"; //alert(url)
                                                                $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                                                location.reload();
                                                                });
                                                        } else {
                                                        return false;
                                                        }
                                                        }

                                                        function deleteAssignment(id) {
                                                        var del = confirm('Are you sure to delete this?');
                                                                if (del) {
                                                        var url = "{{url('/deleteAssignment')}}"; //alert(url)
                                                                $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                                                window.location.href = "{{asset('allassignment')}}";
                                                                });
                                                        } else {
                                                        return false;
                                                        }
                                                        }


</script>
@include('includes.footer')
