@include('includes.header')
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <?php
                        if (@Auth::user()->usertype == 6) {
                            ?>
                            <div class="col-md-6 col-sm-12">
                                <div class="pr-5">
                                    <h4>
                                        {{ @$auditdetails->id ? trans('formlabel.postassignment.assignmentauditedit') : trans('formlabel.postassignment.assignmentaudit') }}
                                    </h4>
                                </div>
                            </div>
                        <?php } else if(@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) { ?>
                            <div class="col-md-6 col-sm-12">
                                <div class="pr-5">
                                    <h4>{{trans('formlabel.postassignment.assignmenttraining')}}</h4>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-6 col-sm-12">
                            <div class="btnsection ">
                                <a href="{{ asset('allassignment') }}">
                                    <button type="button" class="btn btn-dark btn-sm">
                                        {{trans('formlabel.postassignment.myassignments')}}
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="card agreementform">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <div id="auditrequest">
                                @if(@Auth::user()->usertype == 6)
                                @include('pages.assignment.auditrequest')
                                @elseif(@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1)
                                @include('pages.assignment.trainingrequest')
                                @endif
                            </div>
                            
                        </div>
                    </div>
                </div>

                <!--confirm modal-->
                <div class="modal fade" id="confirmModal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title text-left">Confirm Assignment Request</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div id="content-form"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--confirm modal-->

            </div>
        </div>
    </div>
</div>
<script>
    $("#dynamic_select").change(function () {
        if ($(this).val() === "1") {
            $("#auditrequest").show();
            $("#trainingrequest").hide();
        } else {
            $("#auditrequest").hide();
            $("#trainingrequest").show();
        }
    });
</script>
@include('includes.footer')