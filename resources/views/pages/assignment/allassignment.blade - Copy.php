@include('includes.header')
<style>
    div.dataTables_wrapper {
        width: 800px;
        margin: 0 auto;
    }
    table, th, td {
        border: 1px solid black;
        padding: 1%;
    }
    table th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #A18C82;
        color: white;
        letter-spacing: 1px;
    }
    table tbody {
        color: #000;
    }
    table tr:nth-child(even){background-color: #f2f2f2;}

    table tr:hover {background-color: #ddd;}
</style>
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="pr-5">
                                <h4>Assignment List</h4>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="btnsection ">
                                <a href="{{ asset('allassignment') }}">
                                    <button type="button" class="btn btn-dark btn-sm" style="display: none;">
                                        My Assignments
                                    </button></a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12 mb40">
                            <div>
                                <ul class="nav nav-tabs tabs-default mb30" role="tablist bg-light">
                                    <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs active show" href="#audit" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Audit</a></li>
                                    <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs " href="#training" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Training</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade show active" id="audit">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-12">
                                                <table id="example" class="display nowrap mt-1" style="width:98%; margin: 0 1%;">
                                                    <thead>
                                                        <tr>
                                                            <th>Assignment Name</th>
                                                            <th>SIC Code</th>
                                                            <th>Auditor </th>
                                                            <th>Location</th>
                                                            <th>Effort(in days)</th>
                                                            <th>Budget($)</th>
                                                            <th>Start date</th>
                                                            <th>End Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if(@$assignmentlistaudit)
                                                        @foreach(@$assignmentlistaudit as $k=>$v)
                                                        <tr>
                                                            <td>{{@$v->assignment_name}}</td>
                                                            <td>{{@$v->sic_code}}</td>
                                                            <td>{{@$v->auditor_name}}</td>
                                                            <td>{{@$v->location}}</td>
                                                            <td>{{@$v->effort_tentative}}</td>
                                                            <td>{{@$v->budget_usd}}</td>
                                                            <td>{{@$v->start_date}}</td>
                                                            <td>{{@$v->end_date}}</td>
                                                        </tr>
                                                        @endforeach
                                                        @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="training">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-12">
                                                <table id="example" class="display nowrap mt-1" style="width:98%; margin: 0 1%;">
                                                    <thead>
                                                        <tr>
                                                            <th>Batch No</th>
                                                            <th>Standard</th>
                                                            <th>Type of Training</th>
                                                            <th>Mode</th>
                                                            <th>Location</th>
                                                            <th>Effort(in days)</th>
                                                            <th>Fee Range($)</th>
                                                            <th>Start date</th>
                                                            <th>End Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $trainingtype = config('selecttype.trainingtypelist');
                                                        $mode = config('selecttype.trainingmode');
                                                        ?>
                                                        @if(@$assignmentlisttraining)
                                                        @foreach(@$assignmentlisttraining as $k=>$v)
                                                        <tr>
                                                            <td>{{@$v->batch_no}}</td>
                                                            <td>{{@$v->standard}}</td>
                                                            <td>{{$trainingtype[@$v->training_type]}}</td>
                                                            <td>{{$mode[@$v->mode]}}</td>
                                                            <td>{{@$v->location}}</td>
                                                            <td>{{@$v->effort_tentative}}</td>
                                                            <td>{{@$v->fee_range}}</td>
                                                            <td>{{@$v->start_date}}</td>
                                                            <td>{{@$v->end_date}}</td>
                                                        </tr>
                                                        @endforeach
                                                        @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tab panes -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "scrollX": true
        });
    });
</script>
@include('includes.footer')