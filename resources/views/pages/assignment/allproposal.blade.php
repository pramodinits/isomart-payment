@include('includes.header')
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="pr-5">
                                <h4 class="p-4">Bid List</h4>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12 mb40">
                            <div id="tabAssign">
                                <ul class="nav nav-tabs tabs-default mb30" role="tablist bg-light">
                                    <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs show active" href="#audit" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Audit</a></li>
                                    <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs" href="#training" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Training</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade show active" id="audit">
                                        <div class="container">
                                            <div class="row">
                                                @if(count(@$proposalListAudit) > 0)
                                                @foreach(@$proposalListAudit as $k=>$v)
                                                <div class="col-md-4 mb30">
                                                    <div class="card card-body text-center">
                                                        <h4 class="card-title">
                                                            <a href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a>&nbsp;
                                                        </h4>
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                        <div class="mx-auto">
                                                            <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $v->estimated_budget}}</span>
                                                            {{" ( " . trans('formlabel.assignmentinfoaudit.efforts') .  $v->estimated_effort . config('assignment.effortdays') . ")" }}
                                                            <?php
                                                            $now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
                                                            $end_date = Carbon\Carbon::parse($v->proposal_end_date);
                                                            $now_date = Carbon\Carbon::parse($now);
                                                            $daysleft = $now_date->diffInDays($end_date, false);
                                                            ?>
                                                            @if($v->approval_status == 1)
                                                            <span class="text-success">{{trans('formlabel.auditdetail.awarded')}}</span>
                                                            @elseif(@$daysleft < 0) 
                                                            <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                            @elseif(@$daysleft == 0)
                                                            <span class="text-success">{{trans('formlabel.auditdetail.openfortoday')}}</span>
                                                            @else
                                                            <span class="text-success">Open / {{@$daysleft}} days left</span>
                                                            @endif
                                                        </div>
                                                        </div>
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                            <div class="col-3"></div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="col-3"></div>
                                                        </div>
                                                        <hr/>
                                                        <p class="card-text text-left">
                                                {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                                                {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                                                <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                                            </p>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No bids found.
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="training">
                                        <div class="container">
                                            <div class="row">
                                                @if(count(@$proposalLisTraining) > 0)
                                                @foreach(@$proposalLisTraining as $k=>$v)
                                                <div class="col-md-4 mb30">
                                                    <div class="card card-body text-center">
                                                        <h4 class="card-title">
                                                            <a href="{{ asset('assignmentTrainingDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a>&nbsp;
                                                        </h4>
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                        <div class="mx-auto">
                                                            <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $v->estimated_budget}}</span>
                                                            {{" ( " . trans('formlabel.assignmentinfoaudit.efforts') .  $v->estimated_effort . config('assignment.effortdays') . ")" }}
                                                            <?php
                                                            $now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
                                                            $end_date = Carbon\Carbon::parse($v->proposal_end_date);
                                                            $now_date = Carbon\Carbon::parse($now);
                                                            $daysleft = $now_date->diffInDays($end_date, false);
                                                            ?>
                                                            @if($v->approval_status == 1)
                                                            <span class="text-success">{{trans('formlabel.auditdetail.awarded')}}</span>
                                                            @elseif(@$daysleft < 0) 
                                                            <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                            @elseif(@$daysleft == 0)
                                                            <span class="text-success">{{trans('formlabel.auditdetail.openfortoday')}}</span>
                                                            @else
                                                            <span class="text-success">Open / {{@$daysleft}} days left</span>
                                                            @endif
                                                        </div>
                                                        </div>
                                                        @if($v->mode == 1)
                                                        <div class="d-flex flex-row mx-auto col-12">
                                                            <div class="col-3"></div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="p-1 col-2">
                                                                <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="col-3"></div>
                                                        </div>
                                                        @endif
                                                        <hr/>
                                                        <p class="card-text text-left">
                                                {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('assignmentTrainingDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                                                {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                                                <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                                            </p>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No bids found.
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tab panes -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@include('includes.footer')