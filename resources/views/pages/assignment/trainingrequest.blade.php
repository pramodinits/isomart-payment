
<style>
    #trainingLocation, #loading-image, #allowance, #online-effort {
        display: none;
    }
    #loading-image {
        position: absolute;
        z-index: 1;
        right: -5px;
        top: 7px;
    }
    div#locationCity span {
        padding: 2px 5px !important;
    }
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        border-radius: 5px;
    }
    .fa-times {
        cursor: pointer;
    }
    .form-group i.form-control-feedback {
        display: none !important;
    }
    .form-group small[data-bv-for="audit_effort[]"],
    .form-group small[data-bv-for="country"],
    .form-group small[data-bv-for="states"]
    {
        color: #b24d4b !important;
    }
    i#addLocation {
        font-size: 1rem;
        cursor: pointer;
        position: absolute;
        right: -1%;
        top: 14%;
    }
</style>
<form name="{{ @$trainingdetails->id ? 'training-edit' : 'training-add' }}" id="{{ @$trainingdetails->id ? 'training-edit' : 'training-add' }}" method="post" action="{{ asset('requestassignmenttraining') }}">
    {!! csrf_field() !!}
    <input type="hidden" name="user_id" id="user_id" value="{{@Auth::user()->id}}">
    <input type="hidden" name="id_update" id="id_update" value="{{@$trainingdetails->id}}">
    <input type="hidden" name="ref_id" id="ref_id" value="{{@$trainingdetails->ref_id}}">
    <input type="hidden" name="assignment_id" id="id_assignment" value="{{@$trainingdetails->assignment_id}}">
    <input type="hidden" name="assignment_type" id="assignment_type" value="2">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-12">
            <label>{{trans('formlabel.postassignmenttraining.batchno')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="training[batch_no]" value="{{@$trainingdetails->batch_no}}">
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.standard')}}</label>
            <div class="form-group">
                <select class="form-control" name="training[standard]" id="isoCode" value="">
                    <option value="">{{trans('formlabel.postassignmenttraining.standardoption')}}</option>
                    @foreach(@$standardlist as $k=>$v)
                    <option value="{{@$k}}" @if(@$trainingdetails->standard == $k) selected=selected @endif>{{@$v}}</option>
                    @endforeach
                </select>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.trainingtype')}}</label>
            <div class="form-group">
                <select class="form-control" name="training[training_type]">
                    <option value="">Select Training type</option>
                    @foreach(@$trainingtypelist as $k=>$v)
                    <option value="{{@$k}}" @if(@$trainingdetails->training_type == $k) selected=selected @endif>{{@$v}}</option>
                    @endforeach
                </select>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.feerange')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="training[fee_range]" onkeypress="return isNumberKey(event)" value="{{@$trainingdetails->fee_range}}">
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.proposalenddate')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="proposalEndDate" id="proposalEndDate" readonly value="{{@$trainingdetails->proposal_end_date}}">
            </div>          
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.startdate')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="startDate" id="start_date" readonly value="{{@$trainingdetails->start_date}}">
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.enddate')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" name="endDate" id="end_date" readonly value="{{@$trainingdetails->end_date}}">
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-12">
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.mode')}}</label>
            <div class="form-group">
                <select class="form-control" name="training[mode]" id="trainingMode">
                    <option value="">Select Mode</option>
                    @foreach(@$trainingmode as $k=>$v)
                    <option value="{{@$k}}" @if(@$trainingdetails->mode == $k) selected=selected @endif>{{@$v}}</option>
                    @endforeach
                </select>
            </div>
            <div id="allowance" @if(@$trainingdetails->mode == 1)style="display:block;" @endif>
            <label class="mt-1">{{trans('formlabel.postassignmenttraining.allallowance')}}</label>
            <div class="row">
                <div class="col-md-4 form-group">
                    <label>{{trans('formlabel.postassignmenttraining.travelallowance')}}</label>
                    <div class="list-group list-group-horizontal">
                        @foreach(@$allowance as $k => $v)
                        <label class="container-radio pr-2">
                            <input type="radio" value="{{$k}}" <?= (@$trainingdetails->local_travel_allowance == $k) ? "checked" : ""; ?> name="training[local_travel_allowance]">{{$v}}
                            <span class="checkmark"></span> 
                        </label>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4 form-group">
                    <label>{{trans('formlabel.postassignmenttraining.stayallowance')}}</label>
                    <div class="list-group list-group-horizontal">
                        @foreach(@$allowance as $k => $v)
                        <label class="container-radio pr-2">
                            <input type="radio" value="{{$k}}" <?= (@$trainingdetails->local_stay_allowance == $k) ? "checked" : ""; ?> name="training[local_stay_allowance]">{{$v}}
                            <span class="checkmark"></span> 
                        </label>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4 form-group">
                    <label>{{trans('formlabel.postassignmenttraining.airfare')}}</label>
                    <div class="list-group list-group-horizontal">
                        @foreach(@$allowance as $k => $v)
                        <label class="container-radio pr-2">
                            <input type="radio" value="{{$k}}" <?= (@$trainingdetails->air_fare == $k) ? "checked" : ""; ?> name="training[air_fare]">{{$v}}
                            <span class="checkmark"></span>
                        </label>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
            <div id="trainingLocation" @if(@$trainingdetails->mode == 1)style="display:block;" @endif>
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.location')}}</label>
            <div class="row">
                <div class="form-group col-md-6">
                    <select class="form-control" name="country" id="countries" onchange="getLocationInfo('countries', 'states');">
                        <option value="">Select Country</option>
                        @foreach(@$countryList as $k=>$v)
                        <option value="{{@$v->id}}">{{@$v->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <select class="form-control" name="states" id="states" onchange="getLocationInfo('states', 'cities');">
                        <option value="">Select State</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <img id="loading-image" src="{{asset('images')}}/ajax-loader.gif" alt="Loading..." />
                    <input type="text" style="position: relative;" placeholder="{{trans('formlabel.profilecontact.addresscity')}}" class="form-control" id="city" name="city[]" value="">
                </div>
                <div class="form-group col-md-6">
                    <input type="text" id="effort" class="form-control" name="training_effort[]" onkeypress="return isNumberKey(event)" placeholder="Effort in man days">
                    <i id="addLocation" class="fa fa-plus-circle text-success" aria-hidden="true"></i>
                </div>
            </div>
            <div id="reloadDiv">
                <div class="form-group" id="locationCity">
                    @if(@$locationname)
                    <?php $sum = 0; ?>
                    @foreach(@$locationname as $k => $v)
                    <span class="locationCity" data-effort ="{{$v->effort}}">
                        {{$v->city_name . ", " . $v->stateName . ", " . $v->countryName . " (Effort-" . $v->effort . ") &nbsp;"}}
                        <?php $sum+= $v->effort; ?>
                        <span class="removeLocation" id="{{$v->id}}"><i class="fa fa-times"></i></span>
                    </span>&nbsp;
                    @endforeach
                    @endif
                </div>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.efforttentative')}}</label>
            <div class="form-group">
                <input type="text" readonly="" class="form-control" id="effort_tentative" value="{{@$trainingdetails->effort_tentative}}" name="training[effort_tentative]">
            </div>
        </div>
            <div id="online-effort" @if(@$trainingdetails->mode == 2)style="display:block;" @endif>
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.efforttentative')}}</label>
            <div class="form-group">
                <input type="text" class="form-control" id="effort_tentative_online" value="{{@$trainingdetails->effort_tentative}}" name="effort_tentative_online" onkeypress="return isNumberKey(event)">
            </div>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.sessiondetails')}}</label>
            <div class="form-group">
                <textarea class="form-control" name="training[session_details]" id="briefInfo" rows="5" cols="20">{{@$trainingdetails->session_details}}</textarea>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.query')}}</label>
            <div class="form-group">
                <textarea class="form-control" name="training[query]" id="query" rows="5" cols="20">{{@$trainingdetails->query}}</textarea>
            </div>
            <label class="mt-2">{{trans('formlabel.postassignmenttraining.status')}}</label>
            <div class="form-group">
                <select class="form-control" name="training[assignment_status]">
                    <option value="">Select Status</option>
                    @foreach(@$status as $k=>$v)
                    <option value="{{@$k}}" @if(@$trainingdetails->assignment_status == $k) selected=selected @endif>{{@$v}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-center btn-md pull-right mt-2">{{trans('formlabel.postassignmentaudit.save')}}</button>
    </div>
</form>

<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>
<script>

                                            $(document).ready(function () {
                                    $('#loading-image').hide();
                                    $("#trainingMode").change(function () {
                                    var selectVal = $(this).val();
                                        if (selectVal == 1) {
                                            $("div#trainingLocation").show();
                                            $("div#allowance").show();
                                            $("div#online-effort").hide();
                                        } else if (selectVal == 2) {
                                            $("div#trainingLocation").hide();
                                            $("div#allowance").hide();
                                            $("div#online-effort").show();
                                        } else {
                                            $("#online-effort").hide();
                                            $("div#trainingLocation").hide();
                                            $("div#allowance").hide();
                                        }
                                });
                                    });
                                            //get location list
                                                    function getLocationInfo(id, tbl) {
                                                    if (id == "states") {
                                                    $('#loading-image').show();
                                                            $('#city').val("");
                                                            $('#effort').val("");
                                                    } else {
                                                    $('#city').val("");
                                                            $('#effort').val("");
                                                    }
                                                    var selected_val = $('select#' + id + ' option:selected').val();
                                                            var url = "{{url('/getLocationInfo')}}";
                                                            $.post(url, {"selected_val": selected_val, "id": id, "tbl": tbl, "_token": '<?= csrf_token(); ?>'}, function (res) {
                                                            if (tbl == 'cities') {
                                                            availableTags = JSON.parse(res);
                                                                    $('#loading-image').hide();
                                                                    $("#city").autocomplete({
                                                            source: availableTags
                                                            });
                                                            } else {
                                                            $("#" + tbl).html(res);
                                                                    var total = $('select#' + tbl + ' option').length;
                                                                    $("#" + tbl + "_cnt").html(" <b>(" + (total - 1) + ")</b>");
                                                            }
                                                            });
                                                    }
//get location list

//delete loation info
                                            function deleteLocation(location_id) {
                                            if (location_id) {
                                            var url = "{{url('/deleteLocation')}}";
                                                    $.post(url, {'location_id': location_id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                                    if (data == "Success"){
                                                    $("#reloadDiv").load(" #locationCity");
                                                    setTimeout(function () {
                                                    var dataeffort = 0;
                                                    $("span.locationCity").each(function(){
                                                        var effort =$(this).attr("data-effort");
                                                        var eacheffort = parseInt(effort,10);
                                                        dataeffort = dataeffort+eacheffort;
                                                    })
                                                    $("#effort_tentative").val(dataeffort);
                                                    }, 2000);
                                                            return true;
                                                    } else {
                                                    return false;
                                                    }
                                                    });
                                                    } else {
                                            return false;
                                                    }
                                            }
//delete loation info

//add multiple effort & location
                                            $("#addLocation").click(function () {
                                            var city = $("#city").val();
                                                    var effort = $("#effort").val();
                                                    var stateid = $("#states").val();
                                                    var countryid = $("#countries").val();
                                                    var statename = $("#states option:selected").text();
                                                    var countryname = $("#countries option:selected").text();
                                                    if (city !== "" && countryid !== "" && stateid !== "" && effort != "") {
                                            $('#countries, #states').prop('selectedIndex', 0);
                                                    $('#effort, #city').val('');
                                                    $('#countries, #states, #city, #effort').parents('div.form-group').removeClass('has-error');
                                                    $("#locationCity").show();
                                                    var res = '<input type="hidden" name=cityname[] value="' + city + '" >\n\
                <input type="hidden" name=stateid[] value="' + stateid + '" >\n\
<input type="hidden" name=countryid[] value="' + countryid + '" >\n\
<input type="hidden" name=effort[] value="' + effort + '" >\n\
' + city + ', ' + statename + ', ' + countryname + " (Effort- " + effort + ")" + '<span class="removeLocation"><i class="fa fa-times"></i></span>';
                                                    $('<span class="locationCity" data-effort ="' + effort + '">' + res + '</span>&nbsp;').appendTo("#locationCity");
                                                    //assign effort value in total effort
                                                    var dataeffort = 0;
                                                    $(".locationCity").each(function(){
                                                        var effort =$(this).attr("data-effort");
                                                        var eacheffort = parseInt(effort,10);
                                                        dataeffort = dataeffort+eacheffort;
                                                    })
                                                    $('#effort_tentative').val(dataeffort);
                            
                                                    $(".removeLocation").click(function () {
                                                        //assign value in effort in location remove
                                                    var dataeffort = 0;
                                                    $(this).parent().remove();
                                                    $(".locationCity").each(function(){
                                                    var effort =$(this).attr("data-effort");
                                                    var eacheffort = parseInt(effort,10);
                                                    dataeffort = dataeffort+eacheffort;
                                                })
                                                $("input#effort_tentative").val(dataeffort);
                                            });
                                            } else {
                                            if (countryid == "") {
                                            $('#countries').parents('div.form-group').addClass('has-error');
                                            } else if (stateid == "") {
                                            $('#states').parents('div.form-group').addClass('has-error');
                                            } else if (city == "") {
                                            $('#city').parents('div.form-group').addClass('has-error');
                                            } else if (effort == "") {
                                            $('#effort').parents('div.form-group').addClass('has-error');
                                            }
                                            }
                                            });
//add multiple effort & location

//delete existing record
$(".removeLocation").click(function () {
     //get delete id
    var getattr = $(this).attr('id');
    if (typeof getattr !== typeof undefined && getattr !== false) {
        $('<input type="hidden" name=delete_location[] value="' + getattr + '" >').appendTo("#locationCity");
    }
    //assign value in effort in location remove
    var dataeffort = 0;
    $(this).parent().remove();
    $(".locationCity").each(function(){
    var effort =$(this).attr("data-effort");
    var eacheffort = parseInt(effort,10);
    dataeffort = dataeffort+eacheffort;
})
$("#effort_tentative").val(dataeffort);
});

//datepicker
                                                    $(function () {
                                                    $('#proposalEndDate').datepicker({
                                                    changeMonth: true,
                                                            changeYear: true,
                                                            dateFormat: 'dd-mm-yy',
                                                            minDate: 0,
                                                            onSelect: function (selected) {
//                                                                    $("#end_date").datepicker("setDate", null);
//                                                                    $("#start_date").datepicker("setDate", null);
                                                                    $("#end_date").datepicker("option", "minDate", selected)
                                                                    $("#start_date").datepicker("option", "minDate", selected)
                                                            $('#training-add').bootstrapValidator('revalidateField', 'proposalEndDate');
                                                            }
                                                    });
                                                            $('#start_date').datepicker({
                                                    changeMonth: true,
                                                            changeYear: true,
                                                            dateFormat: 'dd-mm-yy',
                                                            onSelect: function (selected) {
//                                                                    $("#end_date").datepicker("setDate", null);
                                                                    $("#end_date").datepicker("option", "minDate", selected)
                                                            $('#training-add').bootstrapValidator('revalidateField', 'startDate');
                                                            }
                                                    });
                                                            $('#end_date').datepicker({
                                                    changeMonth: true,
                                                            changeYear: true,
                                                            dateFormat: 'dd-mm-yy',
                                                            onSelect: function (selected) {
                                                            $('#training-add').bootstrapValidator('revalidateField', 'endDate');
                                                            }
                                                    });
                                                    });
//datepicker
</script>

<script>

                            function isNumberKey(evt) {
                            var charCode = (evt.which) ? evt.which : event.keyCode
                                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                                    return false;
                                    return true;
                            }

                    $(document).ready(function () {
                    $('#training-add').bootstrapValidator({
                    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                            feedbackIcons: {
                            valid: 'fa fa-check',
                                    invalid: 'fa fa-remove',
                                    validating: 'glyphicon glyphicon-refresh'
                            },
                            fields: {
                            "training[batch_no]": {
                            validators: {
                            notEmpty: {
                            message: "This field is required."
                            }
                            }
                            },
                                    "training[standard]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "training[training_type]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
//                                    "effort_tentative_online": {
//                                    validators: {
//                                    notEmpty: {
//                                    message: "This field is required."
//                                    }
//                                    }
//                                    },
                                    "training[fee_range]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "startDate": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "endDate": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "proposalEndDate": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
//                                    "training[local_travel_allowance]": {
//                                    validators: {
//                                    notEmpty: {
//                                    message: "This field is required."
//                                    }
//                                    }
//                                    },
//                                    "training[local_stay_allowance]": {
//                                    validators: {
//                                    notEmpty: {
//                                    message: "This field is required."
//                                    }
//                                    }
//                                    },
//                                    "training[air_fare]": {
//                                    validators: {
//                                    notEmpty: {
//                                    message: "This field is required."
//                                    }
//                                    }
//                                    },
                                    "training[mode]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "training[session_details]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "training[assignment_status]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    }
                            }
                    })
                            .on('error.form.bv', function (e) {
                                var selectVal = $("#trainingMode option:selected").val();
                                if (selectVal == 1) {
                                    if ($('input[name="training[local_travel_allowance]"]:checked').length == 0 ||
                                            $('input[name="training[local_stay_allowance]"]:checked').length == 0 ||
                                            $('input[name="training[air_fare]"]:checked').length == 0
                                            ) {
                                        alert("Kindly check allowance.");
                                        return false;
                                    }
//                            var dataeffort = 0;
//                                    $(".locationCity").each(function(){
//                            var effort = $(this).attr("data-effort");
//                                    var eacheffort = parseInt(effort, 10);
//                                    dataeffort = dataeffort + eacheffort;
//                            })
                                    if ($('#effort_tentative').val() == 0){
                                    alert("Effort should be more than 0 days.");
                                            return false;
                                    }
                                    if ($('.locationCity').text() == "") {
                                    return false;
                            } else {
                                    $('#countries, #states, #city, #effort').parents('div.form-group').removeClass('has-feedback has-error');
                                    $("small[data-bv-for='country").hide();
                                    $("small[data-bv-for='states").hide();
                                    $("small[data-bv-for='city[]").hide();
                                    $("small[data-bv-for='training_effort[]").hide();
                                    return true;
                            }
                                } else if (selectVal == 2) {
                                    if ($('#effort_tentative_online').val() == ""){
                                        alert("Effort should be more than 0 days.");
                                    return false;
                            }
                                }
                            })

                            .on('success.form.bv', function (e) {
                                if ($("#trainingMode").val() == 1) {
                                    //validate allowance
                                    if ($('input[name="training[local_travel_allowance]"]:checked').length == 0 ||
                                            $('input[name="training[local_stay_allowance]"]:checked').length == 0 ||
                                            $('input[name="training[air_fare]"]:checked').length == 0
                                            ) {
                                        alert("Kindly check allowance.");
                                        $( "button" ).attr( "disabled", false );
                                    return false;
                                    }
//                            var values = $("input[name='effort[]']").map(function () {
//                            return $(this).val();
//                            }).get();
//                                    var spliteffort = 0;
//                                    for (var i = 0; i < values.length; i++) {
//                            spliteffort += values[i] << 0;
//                            }
//                            var totalEffort = $("#effort_tentative").val();
//                                    console.log(totalEffort + "+++++" + spliteffort);
                                    if ($('#effort_tentative').val() > 0) {
                            return true;
                            } else {
                            alert("Effort should be more than 0 days.");
                            $( "button" ).attr( "disabled", false );
                                    return false;
                            }
                            } else {
                                if ($('#effort_tentative_online').val() == ""){
                                alert("Effort should be more than 0 days.");
                                $( "button" ).attr( "disabled", false );
                                return false;
                            }
                        }
                            return true;
                            });
                    });
</script>

<script>
    $(document).ready(function () {
                    $('#training-edit').bootstrapValidator({
                    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                            feedbackIcons: {
                            valid: 'fa fa-check',
                                    invalid: 'fa fa-remove',
                                    validating: 'glyphicon glyphicon-refresh'
                            },
                            fields: {
                            "training[batch_no]": {
                            validators: {
                            notEmpty: {
                            message: "This field is required."
                            }
                            }
                            },
                                    "training[standard]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "training[training_type]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
//                                    "effort_tentative_online": {
//                                    validators: {
//                                    notEmpty: {
//                                    message: "This field is required."
//                                    }
//                                    }
//                                    },
                                    "training[fee_range]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "startDate": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "endDate": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "proposalEndDate": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
//                                    "training[local_travel_allowance]": {
//                                    validators: {
//                                    notEmpty: {
//                                    message: "This field is required."
//                                    }
//                                    }
//                                    },
//                                    "training[local_stay_allowance]": {
//                                    validators: {
//                                    notEmpty: {
//                                    message: "This field is required."
//                                    }
//                                    }
//                                    },
//                                    "training[air_fare]": {
//                                    validators: {
//                                    notEmpty: {
//                                    message: "This field is required."
//                                    }
//                                    }
//                                    },
                                    "training[mode]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "training[session_details]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                    "training[assignment_status]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    }
                            }
                    })
                            .on('error.form.bv', function (e) {
                                var selectVal = $("#trainingMode").val();
                                if (selectVal == 1) {
                                    //validate allowance
                                    if ($('input[name="training[local_travel_allowance]"]:checked').length == 0 ||
                                            $('input[name="training[local_stay_allowance]"]:checked').length == 0 ||
                                            $('input[name="training[air_fare]"]:checked').length == 0
                                            ) {
                                        alert("Kindly check allowance.");
                                        $( "button" ).attr( "disabled", false );
                                    return false;
                                    }
                            if ($('.locationCity').text() == "") {
                        return false;
                    } 
                                }
                            })

                            .on('success.form.bv', function (e) {
                                if ($("#trainingMode").val() == 1) {
                                    //validate allowance
                                    if ($('input[name="training[local_travel_allowance]"]:checked').length == 0 ||
                                            $('input[name="training[local_stay_allowance]"]:checked').length == 0 ||
                                            $('input[name="training[air_fare]"]:checked').length == 0
                                            ) {
                                        alert("Kindly check allowance.");
                                        $( "button" ).attr( "disabled", false );
                                    return false;
                                    }
                    if($('#effort_tentative').val() == 0 ){
                        alert("Effort should be more than 0 days.");
                        $( "button" ).attr( "disabled", false );
                        return false;
                    }
                            } else {
                                if($('#effort_tentative_online').val() == 0 ){
                                    alert("Effort should be more than 0 days.");
                                    $( "button" ).attr( "disabled", false );
                                    return false;
                                }
                            }
                            return true;
                            });
                    });
</script>