@include('includes.header')
@php 
@$typelist = config('selecttype.trainingtypelist'); 
@$mode = config('selecttype.trainingmode'); 
@$standard = config('standards.standardlist');

//days left & budget
$now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
$end_date = Carbon\Carbon::parse(@$auditdetails->proposal_end_date);
$now_date = Carbon\Carbon::parse($now);
$daysleft = $now_date->diffInDays($end_date, false);
$assignmentBudget = @$trainingdetails->fee_range - @$trainingdetails->jobBudget;
//days left & budget
@endphp

<style>
    .tooltip {
        position: relative;
        display: inline-block;
        opacity: 1;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        width: 100%;
        background-color: black;
        color: #fff;
        text-align: left;
        border-radius: 6px;
        padding: 5px;

        /* Position the tooltip */
        position: absolute;
        top: 90%;
        left: 0%;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }
    #queryTooltip {
        width: 100% !important;
        top: 90%;
        right: 0%;
    }
    #queryTool .btn-sm {
        padding: 5% 9% !important;
    }
    #queryToolAdmin .btn-sm {
        padding: 2% 2.5% !important;
    }
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        padding: 2px 5px;
        border-radius: 5px;
    }
    .genderSelect p {
        margin-bottom: 0;
    }
</style>

<!--share modal-->
<div class="modal fade queryModal" id="queryModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Share Assignment</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="shareAssignment" name="shareAssignment" action="{{ asset('shareAssignment')}}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="assignment_id" id="assignment_id" value="{{@$trainingdetails->assignment_id}}">
                    <input type="hidden" name="ref_id" id="ref_id" value="{{@$trainingdetails->ref_id}}">
                    <label>{{trans('formlabel.shareassignment.enteremail')}}</label>
                    <div class="form-group">
                        <input type="email" class="form-control" id="share_email" name="share_email" required>
                    </div>
                    <button type="submit" class="btn btn-primary text-center">{{trans('formlabel.shareassignment.sharebutton')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!--share modal-->

<div class="container">
    <div class="main-content">
        <div class="row">
            <!---ref no, days left, posted username-->
            
            
            
            
            <div class="col-md-6 col-sm-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="details mt-4 border-right">
                            <h5 class="profile-no">{{@$trainingdetails->ref_id}} <small class="text-success">Open / 6days left</small></h5>
                            <div>Need ISO Trainer</div>
                            <div>Posted By  : <span class="text-primary"><b>{{@$trainingdetails->fname . " " . @$trainingdetails->lname}}</b></span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---ref no, days left, posted username-->
            
            
            <div class="col-md-6 col-sm-6">
                <div class="btnsection mt-5 pull-right">
                    <h6 class="text-center">Budget</h6>
                    <div class="buget-price">$ {{@$trainingdetails->fee_range}}</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container">
    <hr>
    <h5>Assignment Details</h5>
    <div class="row">
        <div class="col-md-12 mb30">
            <div class="card card-body">
                <p class="text-dark">{{@$trainingdetails->session_details}}</p>
                <h6 class="text-dark">Assignments Location</h6>
                <address>
                    {{@$trainingdetails->location}}
                </address>
                <hr>
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="inforamation-section">
                            <div>Batch No : <span class="text-dark">{{@$trainingdetails->batch_no}}</span> </div>
                            <div>Effort In man days : <span class="text-dark">{{@$trainingdetails->effort_tentative}}</span> </div>
                            <div>Budget in USD ($): <span class="text-dark">{{@$trainingdetails->fee_range}}</span> </div>
                            <div>Standard: <span class="text-dark">{{@$trainingdetails->standard ? @$trainingdetails->standard : "N/A" }}</span> </div>
                            <div>Training Type: <span class="text-dark">{{$typelist[@$trainingdetails->training_type]}}</span> </div>
                            <div>Training Mode : <span class="text-dark">{{$mode[@$trainingdetails->mode]}}</span> </div>
                        </div>
                    </div>
                    <!--/col-->
                    <div class="col-md-6 ">
                        <div class="inforamation-section">
                            <div>Start Date : <span class="text-dark">{{@$trainingdetails->start_date}}</span> </div>
                            <div>End Date : <span class="text-dark">{{@$trainingdetails->end_date}}</span> </div>
                            <div>Local Travels Allowance : 
                                <span class="text-dark">
                                    <i style="font-size: 1rem;" class="{{@$trainingdetails->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                </span> 
                            </div>
                            <div>Local Stay Allowance : <span class="text-dark">
                                    <i style="font-size: 1.2rem;" class="{{@$trainingdetails->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                </span> </div>
                            <div>Air Fare: <span class="text-dark">
                                    <i style="font-size: 1.2rem;" class="{{@$trainingdetails->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                </span> </div>
                        </div>
                    </div>
                </div>
                <!--/col--> 

            </div>
            <div class="clearfix"></div>
        </div>
        <!--/col--> 
    </div>
</div>
@include('includes.footer')
