@include('includes.header')
<?php
$now = time(); // or your date as well
$endate = strtotime(@$certificationdetails->proposal_end_date);
$datediff = $endate - $now;
$daysleft = round($datediff / (60 * 60 * 24));
?>
<style>
    .tooltip {
        position: relative;
        display: inline-block;
        opacity: 1;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        width: 100%;
        background-color: black;
        color: #fff;
        text-align: left;
        border-radius: 6px;
        padding: 5px;

        /* Position the tooltip */
        position: absolute;
        top: 90%;
        left: 0%;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }
    #queryTooltip {
        width: 100% !important;
        top: 90%;
        right: 0%;
    }
    #queryTool .btn-sm {
        padding: 5% 9% !important;
    }
    #queryToolAdmin .btn-sm {
        padding: 2% 2.5% !important;
    }
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        padding: 2px 5px;
        border-radius: 5px;
    }
</style>

<div class="container">
    <div class="main-content">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="details mt-4 border-right">
                            <h5 class="profile-no">{{@$certificationdetails->ref_id}} 
                                <small class="text-success">
                                    @if(@$userdetails[0]->approval_status == 1 && @Auth::user()->id == @$userdetails[0]->id)
                                    Awarded to <strong>{{ucfirst(@$awarddetails[0]->fname) . " " . ucfirst(@$awarddetails[0]->lname)}}</strong>
                                    @elseif(@$userdetails[0]->approval_status == 1 && @Auth::user()->id == @$awarddetails[0]->awarded_to)
                                    Awarded to <strong>you</strong>
                                    @elseif(@$userdetails[0]->approval_status == 1)
                                    Awarded
                                    @elseif(@$daysleft < 0) 
                                    <span class="text-danger">Closed</span>
                                    @else
                                    Open / {{@$daysleft}} days left
                                    @endif
                                </small>
                            </h5>
                            <div>{{@$certificationdetails->assignment_name}} 
                                @if(@$userdetails[0]->approval_status == 0)
                                (Proposals: {{count(@$proposaldetails)}})
                                @endif
                            </div>
                            <div>Posted By  : <span class="text-primary"><b>{{ucfirst(@$userdetails[0]->fname) . " " . ucfirst(@$userdetails[0]->lname)}}</b></span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---send, edit & delete proposal button-->
            <div class="col-md-6 col-sm-6">
                <div class="btnsection mt-2 pull-right">
                    <h6 class="text-center">Budget</h6>
                    <div class="buget-price">$ {{@$certificationdetails->budget_usd}}</div>

                    @if(@Auth::id())
                    @if(@Auth::user()->id == @$userdetails[0]->id && @$userdetails[0]->approval_status == 0)
                    <div class="btn-group">
                        <a href="javascript:void(0);" onclick="deleteAssignment('{{@$certificationdetails->assignment_id}}')">
                            <button type="button" class="btn btn-orange btn-sm btn-xs">Delete Assignment</button>
                        </a>
                    </div>
                    @elseif(@$editProposal && @$userdetails[0]->approval_status == 0)
                    <div class="btn-group">
                        <a data-toggle="modal" class="pr-2" data-target="#proposalModal" style="cursor: pointer;">
                            <button type="button" class="btn btn-orange btn-sm btn-xs mb3">Edit Proposal</button>
                        </a>
                        <a href="javascript:void(0);" onclick="deleteProposal('{{@$certificationdetails->assignment_id}}')">
                            <button type="button" class="btn btn-danger btn-sm btn-xs mb3">Delete Proposal</button>
                        </a>
                    </div>
                    @elseif(@Auth::user()->usertype == 6 && @$userdetails[0]->approval_status == 0)
                    <div class="btn-group">
                        <a data-toggle="modal" data-target="#proposalModal" style="cursor: pointer;">
                            <button type="button" class="btn btn-orange btn-sm btn-xs">Send Proposal</button>
                        </a>
                    </div>
                    @endif
                    @elseif(@$daysleft < 0)
                    @elseif(@$userdetails[0]->approval_status == 0)
                    <div class="btn-group">
                        <a href="{{asset('signin')}}/phone/{{@$certificationdetails->assignment_id}}">
                            <button type="button" class="btn btn-orange btn-sm btn-xs">Send Proposal</button>
                        </a>
                    </div>
                    @else

                    @endif
                </div>
            </div>
            <!---send, edit & delete proposal button-->
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container">

    <!--assignment details-->
    <hr>
    <h5>Assignment Details</h5>
    <div class="row">
        <div class="col-md-12 mb30">
            <div class="card card-body">
                <p class="text-grey" style="font-size:1.3rem;">{{@$certificationdetails->description}}</p>
                <h6 class="text-dark">Assignment Location :</h6>
                <address>
                    @foreach(@$locationname as $k=>$v)
                    <span class="locationCity"><i class="fa fa-map-marker pr-1 text-danger" aria-hidden="true"></i>{{@$v}}</span>
                    @endforeach
                </address>
                <hr>
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="inforamation-section">
                            <div>Section / Sector : <span class="text-dark">{{@$standardDeatil->code ." - (". @$standardDeatil->type ." / ". @$standardDeatil->name . ")"}}</span> </div>
                            <div>Effort In man days : <span class="text-dark">{{@$certificationdetails->effort_tentative}}</span> </div>
                            <div>Budget in USD ($): <span class="text-dark">{{@$certificationdetails->budget_usd}}</span> </div>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="inforamation-section">
                            <div>Local Travels Allowance : 
                                <span class="text-dark">
                                    <i style="font-size: 1rem;" class="{{@$certificationdetails->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                </span> 
                            </div>
                            <div>Local Stay Allowance : <span class="text-dark">
                                    <i style="font-size: 1.2rem;" class="{{@$certificationdetails->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                </span> </div>
                            <div>Air Fare: <span class="text-dark">
                                    <i style="font-size: 1.2rem;" class="{{@$certificationdetails->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                </span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!--assignment details-->

    <!--send proposal modal--->
    <div class="modal fade" id="proposalModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center">{{trans('formlabel.sendproposalaudit.nametitle')}}</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    @if (@$isicCode == 0)
                    <div class="text-danger">
                        You have not added any ISIC/ANZSIC/NACE code yet. Please add in profile section and send your proposal again.
                    </div>
                    @elseif(@$standard == 0)
                    <div class="text-danger">
                        You are not eligible to post proposal on {{@$standardDeatil->code ." - (". @$standardDeatil->type ." / ". @$standardDeatil->name . ")"}}. Please add in profile section and send your proposal again.
                    </div>
                    @elseif (@$address == 0)
                    <div class="text-danger">
                        Your current location is not matching with assignment location. Please add in profile section and send your proposal again.
                    </div>
                    @else
                    <form id="sendProposal" name="sendProposal" action="{{ asset('sendproposal')}}" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" name="user_id" value="{{@Auth::user()->id}}">
                        <input type="hidden" name="proposal_id" value="{{@$proposalEdit->id}}">
                        <input type="hidden" name="proposal[assignment_id]" value="{{@$certificationdetails->assignment_id}}">
                        <input type="hidden" name="ref_id" value="{{@$certificationdetails->ref_id}}">
                        <input type="hidden" name="fname" value="{{@$userdetails[0]->fname}}">
                        <input type="hidden" name="lname" value="{{@$userdetails[0]->lname}}">
                        <input type="hidden" name="email" value="{{@$userdetails[0]->email}}">
                        <input type="hidden" name="assignment_type" value="{{@$userdetails[0]->assignment_type}}">
                        <input type="hidden" id="query" value="{{@$certificationdetails->query_first}}">

                        <label>{{trans('formlabel.sendproposalaudit.industryclassification')}}</label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="industryclass" name="proposal[industry_classification]" value="{{@$proposalEdit->industry_classification}}">
                        </div>
                        <label>{{trans('formlabel.sendproposalaudit.headofficestrength')}}</label>
                        <div class="form-group">
                            <input type="number" class="form-control" id="headofcstrength" name="proposal[head_office_strength]" value="{{@$proposalEdit->head_office_strength}}">
                        </div>
                        <label>{{trans('formlabel.sendproposalaudit.branchofficestrength')}}</label>
                        <div class="form-group">
                            <input type="number" class="form-control" id="branchofcstrength" name="proposal[branch_office_strength]" value="{{@$proposalEdit->branch_office_strength}}">
                        </div>
                        <label>{{trans('formlabel.sendproposalaudit.estimatedeffort')}}</label>
                        <div class="form-group">
                            <input type="number" class="form-control" id="estimatedeffort" name="proposal[estimated_effort]" value="{{@$proposalEdit->estimated_effort}}">
                        </div>
                        <label>{{trans('formlabel.sendproposalaudit.estimatedbudget')}}</label>
                        <div class="form-group">
                            <input type="number" class="form-control" id="estimatedbudget" name="proposal[estimated_budget]" value="{{@$proposalEdit->estimated_budget}}">
                        </div>
                        <div id="queryBlock" style="display: none;">
                            <label>{{@$certificationdetails->query_first}}</label>
                            <div class="form-group">
                                <textarea class="form-control" name="proposal[query_ans]" id="query_ans" rows="5" cols="15">{{@$proposalEdit->query_ans}}</textarea>
                            </div>
                        </div>
                        <label>{{trans('formlabel.sendproposalaudit.description')}}</label>
                        <div class="form-group">
                            <textarea class="form-control" name="proposal[description]" id="description" rows="5" cols="15">{{@$proposalEdit->description}}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary text-center">{{trans('formlabel.sendproposalaudit.submitproposal')}}</button>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--send proposal modal--->
</div>

<!---proposal details-->
@if((@$editProposal) || (@Auth::user()->id == @$userdetails[0]->user_id))
<div class="container">
    <h5>Proposals({{count(@$proposaldetails)}})</h5>
    @if(count(@$proposaldetails) > 0)
    @foreach(@$proposaldetails as $key=>$val)
    <div class="card mb-2" style="{{@Auth::user()->id == $val->user_id ? 'border:1px solid green' : '' }}">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-7 col-12">
                @if(@$val->profile_img)
                <img src="{{asset('profile_image')}}/{{@$val->profile_img}}" width="170" class="img-fluid pull-left mr-4" />
                @else
                <img src="{{asset('images')}}/{{@$val->gender == 1 ? 'profile_default.jpg' : 'profile_female.jpg' }}" style="border: 1px solid rgba(0,0,0,.125);" width="140" class="img-fluid pull-left mr-4">
                @endif
                <h6 class="text-dark m-2"><strong>{{ucfirst(@$val->fname) . " " . ucfirst(@$val->lname)}}</strong></h6>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span><br>
                <div class="text-dark tooltip">
                    {{substr(ucfirst(@$val->description), 0, 60) . "..."}}
                    <span class="tooltiptext">{{ucfirst(@$val->description)}}</span>
                </div>
                <div class="small">{{@$val->add_date}}</div>
            </div>

            <div class="col-lg-5 col-md-5 col-sm-4 col-12">
                <!--seeker-->
                @if(@Auth::user()->id == @$userdetails[0]->id)
                <div class="d-flex pull-right pt-3 mr-3">
                    <div class="p-2"><strong>Budget :</strong> <span class="text-dark">{{"$ " .@$val->estimated_budget}}</span></div>
                    <div class="p-2"><strong>Time :</strong><span class="text-dark">{{@$val->estimated_effort . " Days"}}</span></div>
                </div>
                <div class="clearfix"></div>
                <div class="text-right mr-3" id="queryToolAdmin">
                    <button type="button" class="btn btn-orange btn-sm btn-xs mb3"><i class="fa fa-envelope"></i> Message</button>
                    @if(@$userdetails[0]->approval_status == 0)
                    <a href="javascript:void(0);" onclick="acceptProposal('{{@$val->id}}')">
                        <button type="button" class="btn btn-success  btn-sm btn-xs mb3"><i class="fa fa-check-circle"></i> Accept</button>
                    </a>
                    <a href="javascript:void(0);" onclick="rejectProposal('{{@$val->id}}')">
                        <button type="button" class="btn btn-danger btn-sm btn-xs mb3"><i class="fa fa-crosshairs"></i> Reject</button>
                    </a>
                    @endif
                    <a data-toggle="modal" data-target="#queryModal{{@$val->id}}" style="cursor: pointer;">
                        <button type="button" class="btn btn-danger btn-xs btn-sm mb3">
                            <i class="fa fa-question-circle">&nbsp;More Info</i> 
                        </button>
                    </a>
                </div>
                @else
                <!--seeker-->
                <!--provider-->
                <!--<div class="d-flex pull-right mt-4 mb-4 p-1">-->
                <div class="d-flex pull-right m-2 p-1">
                    <div class="text-dark p-2 pr-4"><strong>Budget :</strong> <span class="text-dark">{{"$ " .@$val->estimated_budget}}</span>
                        <br>
                        <strong>Time :</strong><span class="text-dark"> {{@$val->estimated_effort . " Days"}}</span>
                        <br>
                        @if(@Auth::user()->id == $val->user_id)
                        <div id="queryTool">
                            <a data-toggle="modal" data-target="#queryModal{{@$val->id}}" style="cursor: pointer;">
                                <button type="button" class="btn btn-danger btn-xs btn-sm mb3">
                                    <i class="fa fa-question-circle">&nbsp;More Info</i> 
                                </button>
                            </a>
                        </div>

                        @endif
                    </div>
                </div>
                <!--provider-->
                @endif
            </div>
        </div>
    </div>

    <!--- proposal modal-->
    <div class="modal fade queryModal" id="queryModal{{@$val->id}}" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center">{{ucfirst(@$val->fname) . " " . ucfirst(@$val->lname)}}</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 border-right">
                            <div class="inforamation-section">
                                <div>Industry Classification : <span class="text-dark">{{$val->industry_classification}}</span> </div>
                                <div>Head office Strength : <span class="text-dark">{{$val->head_office_strength}}</span> </div>
                                <div>Branch office Strength : <span class="text-dark">{{$val->branch_office_strength}}</span> </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div>Estimated Budget in USD : <span class="text-dark">{{$val->estimated_budget}}</span> </div>
                            <div>Estimated Effort (in days) : <span class="text-dark">{{$val->estimated_effort}}</span> </div>
                        </div>
                    </div>
                    <br>
                    <div><strong>Description :</strong> <span class="text-dark">{{$val->description}}</span> </div>
                    <br>
                    @if($val->query_ans)
                    <strong>Questions: {{ucfirst(@$certificationdetails->query_first)}}</strong>
                    <br>
                    Answers: {{ucfirst($val->query_ans)}}
                    @else
                    No query found.
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--- proposal modal-->
    @endforeach
    @else
    <div class="card mb-2">
        <div class="row">
            <div class="text-center col-12 p-5">
                No proposals found.
            </div>
        </div>
    </div>
    @endif
</div>
@endif
<!---proposal details-->
<script>
                                    $(document).ready(function () {
                            var queryVal = $("#query").val();
                                    if (queryVal) {
                            $("#queryBlock").show();
                            } else {
                            $("#queryBlock").hide();
                            }
                            });
                                    $('#sendProposal').bootstrapValidator({
                            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                            feedbackIcons: {
                            valid: 'fa fa-check',
                                    invalid: 'fa fa-remove',
                                    validating: 'glyphicon glyphicon-refresh'
                            },
                                    fields: {
                                    "proposal[industry_classification]": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                            "proposal[head_office_strength]": {
                                            validators: {
                                            notEmpty: {
                                            message: "This field is required."
                                            }
                                            }
                                            },
                                            "proposal[branch_office_strength]": {
                                            validators: {
                                            notEmpty: {
                                            message: "This field is required."
                                            }
                                            }
                                            },
                                            "proposal[estimated_effort]": {
                                            validators: {
                                            notEmpty: {
                                            message: "This field is required."
                                            }
                                            }
                                            },
                                            "proposal[estimated_budget]": {
                                            validators: {
                                            notEmpty: {
                                            message: "This field is required."
                                            }
                                            }
                                            },
                                            "proposal[query_ans]": {
                                            validators: {
                                            notEmpty: {
                                            message: "This field is required."
                                            }
                                            }
                                            },
                                            "proposal[description]": {
                                            validators: {
                                            notEmpty: {
                                            message: "This field is required."
                                            }
                                            }
                                            }
                                    }
                            }).on('success.form.bv', function (e) {
                            return true;
                            });
                                    function rejectProposal(id) {
                                    var del = confirm('Are you sure to reject this?');
                                            if (del) {
                                    var url = "{{url('/rejectProposal')}}"; //alert(url)
                                            $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                            location.reload();
                                            });
                                    } else {
                                    return false;
                                    }
                                    }
                            function acceptProposal(id) {
                            var del = confirm('Are you sure to accept this?');
                                    if (del) {
                            var url = "{{url('/acceptProposal')}}"; //alert(url)
                                    $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                    location.reload();
                                    });
                            } else {
                            return false;
                            }
                            }

                            function deleteProposal(id) {
                            var del = confirm('Are you sure to delete this?');
                                    if (del) {
                            var url = "{{url('/deleteProposal')}}"; //alert(url)
                                    $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                    location.reload();
                                    });
                            } else {
                            return false;
                            }
                            }

                            function deleteAssignment(id) {
                            var del = confirm('Are you sure to delete this?');
                                    if (del) {
                            var url = "{{url('/deleteAssignment')}}"; //alert(url)
                                    $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                    window.location.href = "{{asset('allassignment')}}";
                                    });
                            } else {
                            return false;
                            }
                            }
</script>
@include('includes.footer')
