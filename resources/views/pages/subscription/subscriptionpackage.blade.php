@include('includes.header')

<!--subscription page template-->
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="main-content">

                <div class="container-fluid no-padding mb70">
                    <div class="row no-margin mb70 align-items-center">
                        <div class="col-md-12 pt20 pb20">
                            <div class="container">
                                <div class="media pos-relative">

                                    <div class="media-body ">
                                       <div class="container pt90 pb50">
        <div class="mb30 text-center">
                                <h4 class="text-secondary">ISOMART Subscription Packages </h4>
                            </div>
        
            <div class="row">
                <div class="pricing-table  align-items-center">
                    <div class="col-lg-3 col-md-6 plan icon-box icon-box-center icon-border-box bg-white">
                        <div class="plan-header ">
                            <h4>Basic</h4>
                            <h1 class="text-info">$9.99 <small>/ PM</small></h1>
                            <h6>Single License</h6>
                        </div>
                        <a href="#" class="btn btn-primary btn-lg btn-block">BUY NOW</a>
                     
                        <ul class="list-unstyled">
                            <li><i class="fa fa-check-circle-o"></i> <strong>5Gb</strong> Disk Space</li>
                            <li><i class="fa fa-check-circle-o"></i> <strong>50Gb</strong> Monthly Bandwdith</li>
                            <li><i class="fa fa-check-circle-o"></i> <strong>10</strong> Email Accounts</li>
                            <li><i class="fa fa-check-circle-o"></i> <strong>Unlimited</strong> Sub domains</li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 plan icon-box icon-box-center icon-border-box bg-white">
                        <div class="plan-header">
                            <h4>Extended</h4>
                            <h1 class="text-info">$99.99 <small>/ PM</small></h1>
                            <h6>More power &amp; security</h6>
                        </div>
                       <a href="#" class="btn btn-primary btn-lg btn-block">BUY NOW</a>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-check-circle-o"></i> <strong>5Gb</strong> Disk Space</li>
                            <li><i class="fa fa-check-circle-o"></i> <strong>50Gb</strong> Monthly Bandwdith</li>
                            <li><i class="fa fa-check-circle-o"></i> <strong>10</strong> Email Accounts</li>
                            <li><i class="fa fa-check-circle-o"></i> <strong>Unlimited</strong> Sub domains</li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 plan icon-box icon-box-center icon-border-box bg-white">
                        <div class="plan-header">
                            <h4>Business</h4>
                            <h1 class="text-info">$69.99 <small>/ PM</small></h1>
                            <h6>Extended License</h6>
                        </div>
                        <a href="#" class="btn btn-primary btn-lg btn-block">BUY NOW</a>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-check-circle-o"></i> <strong>500Gb</strong> Disk Space</li>
                            <li><i class="fa fa-check-circle-o"></i> <strong>50Gb</strong> Monthly Bandwdith</li>
                            <li><i class="fa fa-check-circle-o"></i>  Email Accounts</li>
                            <li><i class="fa fa-check-circle-o"></i> <strong>Unlimited</strong> Sub domains</li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 plan icon-box icon-box-center icon-border-box bg-white">
                        <div class="plan-header">
                            <h4>Extended</h4>
                            <h1 class="text-info">$99.99 <small>/ PM</small></h1>
                            <h6>More power &amp; security</h6>
                        </div>
                     <a href="#" class="btn btn-primary btn-lg btn-block">BUY NOW</a>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-check-circle-o"></i> <strong>5Gb</strong> Disk Space</li>
                            <li><i class="fa fa-check-circle-o"></i> <strong>50Gb</strong> Monthly Bandwdith</li>
                            <li><i class="fa fa-check-circle-o"></i> <strong>10</strong> Email Accounts</li>
                            <li><i class="fa fa-check-circle-o"></i> <strong>Unlimited</strong> Sub domains</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="clearfix"></div>
                
            </div>
        </div>
    </div>
</div>
<!--subscription page template-->

@include('includes.footer')

