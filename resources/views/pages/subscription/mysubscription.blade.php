@include('includes.header')
<style>
    ul.pagination li{
        padding: 5px;
    }
    ul.pagination li.active span,
    ul.pagination li a {
        padding: 5px;
    }
</style>
<!--subscription page template-->
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="main-content">
                <div class="container-fluid no-padding">
                    <div class="row no-margin align-items-center">
                        <div class="col-md-12 pt20 pb20">
                            <div class="container">
                                <div class="media pos-relative">
                                    <div class="media-body">
                                        <div class="container">
                                            <h4 class="font-weight-bold mb-4">{{trans('formlabel.mysubscription.mysubscriptionheader')}}</h4>                                         
                                            @if (@$subscription_info->credit_points)
                                            <h6 class="">{{trans('formlabel.mysubscription.mysubscription')}}</h6>
                                            <div class="card mt-2">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-8">
                                                            <h6>{{trans('formlabel.mysubscription.currentpackage')}}</h6>
                                                            <div>
                                                                <h4 class="text-info ">{{@$currentpackagename->packageName}}</h4>
                                                                @if($currentpackagename->purchase_invoice)
                                                                <a href="{{asset('subscription/'.$currentpackagename->purchase_invoice)}}">
                                                                    <i class="fa fa-download" aria-hidden="true"></i> Download Invoice
                                                                </a>
                                                                @endif
                                                            </div>                                          
                                                        </div>
                                                        <div class="col-4 text-center">
                                                            {{trans('formlabel.mysubscription.availablepoints')}}
                                                            <h4 class="text-center text-info">{{@$subscription_info->credit_points}}</h4>
                                                            {{trans('formlabel.mysubscription.subscriptionexpiry')}} {{Carbon\Carbon::parse(@$subscription_info->expiry_date)->format('d-M-Y')}}
                                                        </div>
                                                    </div>
                                                </div>                 
                                            </div>
                                            <h6 class="mt-4">{{trans('formlabel.mysubscription.transactionhistoryhead')}}</h6>
                                            <div class="card">
                                                <!---search days-->
                                                <div class="card-body" style="display: none;">
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <select class="form-control">
                                                                <option>Last 30days</option>
                                                                <option>2</option>
                                                                <option>3</option>
                                                                <option>4</option>
                                                                <option>5</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-3">
                                                            <button type="button" class="btn btn-primary btn-sm">Apply</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---search days-->

                                                <table class="table  table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th width="20%">{{trans('formlabel.mysubscription.transactiondate')}}</th>
                                                            <th width="50%">{{trans('formlabel.mysubscription.transactionnarrate')}}</th>
                                                            <th width="10%" class="text-center">{{trans('formlabel.mysubscription.transactioncr')}}</th>
                                                            <th width="10%" class="text-center">{{trans('formlabel.mysubscription.transactiondr')}}</th>                               
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                         @if(count(@$transaction_list) > 0)
                                                         @foreach(@$transaction_list as $key => $val)
                                                        <tr>
                                                            <td>{{Carbon\Carbon::parse($val->activity_date)->format('d-M-Y')}}</td>
                                                            <td>
                                                                <div>{{$val->narration}}</div>
<!--                                                                <div class="text-info">Need ISO Auditor for automobile Industry</div>-->
                                                            </td>
                                                            <td class="text-center text-success">{{$val->credit_point ? $val->credit_point : ""}}</td>
                                                            <td class="text-center text-danger">{{$val->debit_point ? $val->debit_point : ""}}</td>                      
                                                        </tr>
                                                        @endforeach
                                                        @else
                                                        <tr class="text-center">
                                                            
                                                        </tr>
                                                        @endif
                                                    </tbody>
                                                </table>     
                                                <div>
                                                    <nav aria-label="Page navigation example" class="mb20 pull-right p-3">
                                                        {{@$transaction_list->links()}}
                                                    </nav>    
                                                </div>
                                            </div>
                                        @else
                                        <div class="text-center card p-3">
                                            {{trans('formlabel.mysubscription.notransaction')}}
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>

        </div>
    </div>
</div>
</div>
<!--subscription page template-->

@include('includes.footer')

