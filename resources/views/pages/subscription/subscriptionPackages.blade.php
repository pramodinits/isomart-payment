@include('includes.header')
@php
$packagetype = config('subscription.packagelist');
$packageduration = config('subscription.packageduration');
@endphp
<!--subscription page template-->
<style>
    #packageModal {
        width: 50%;
        margin: 0 auto;
    }
    #packageModal button {
        width: 15%;
    }
    ul.pagination li{
        padding: 5px;
    }
    ul.pagination li.active span,
    ul.pagination li a {
        padding: 5px;
    }
</style>
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="main-content">

                <div class="container-fluid no-padding mb70">
                    <div class="row no-margin mb70 align-items-center">
                        <div class="col-md-12 pb20">
                            <div class="container">
                                <div class="media pos-relative">

                                    <div class="media-body ">
                                        <div class="container">
                                            <div class="mb30 text-center">
                                                <h4 class="text-secondary">{{trans('formlabel.mysubscription.allpackages')}} </h4>
                                            </div> 
                                        </div>

                                        <div class="container">
                                            <div class="card p-4">
                                                <div class="row">
                                                    @if(count(@$package_list) > 0)
                                                    @foreach(@$package_list as $key => $val)
                                                    <div class="col-lg-4 col-md-4">
                                                        <div class="card text-center p-4">
                                                            <div class="plan-header ">
                                                                <h4>{{$val->package_name}}</h4>
                                                                <h1 class="text-info">{{config('assignment.price') . " ". $val->package_price}}</h1>
                                                            </div>
                                                            <a data-toggle="modal" data-target="#packageModal" 
                                                               data-packageid="{{$val->id}}"
                                                               data-packagename="{{$val->package_name}}" 
                                                               data-packageprice="{{$val->package_price}}"
                                                               data-packageduration="{{$packageduration[$val->package_duration]}}"
                                                               data-creditpoints="{{$val->credit_points}}"
                                                               onclick="buyPackage(this)" style="cursor: pointer;">
                                                                <button type="button" class="btn btn-primary btn-lg btn-block mb-3">
                                                                    @if ($val->package_price > 0)
                                                                    {{trans('bladelable.home.buypackage')}}
                                                                    @else
                                                                    {{trans('bladelable.home.subscribepackage')}}
                                                                    @endif
                                                                </button>
                                                            </a>
                                                            
                                                            <ul class="list-unstyled">
                                                                <li><i class="fa fa-check-circle-o"></i> {{$packageduration[$val->package_duration]}}</li>
                                                                <li><i class="fa fa-check-circle-o"></i> {{$val->credit_points . " " . config('assignment.creditpoint')}}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                    @else
                                                    <div class="col-md-12">
                                                        <div class="text-secondary text-center" style="text-align: center;">
                                                            {{trans('bladelable.home.nopackages')}}
                                                        </div>
                                                    </div>
                                                    @endif  

                                                    <!---package info-->
                                                    <div class="modal fade" id="packageModal" role="dialog">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <h4 id="modal-packagetitle">
                                                                        
                                                                    </h4>
                                                                    <hr>
                                                                    <form id="subscriptionPackage" name="subscriptionPackage" action="{{ asset('insertPackage')}}" method="post">
                                                                        {!! csrf_field() !!}
                                                                        <input type="hidden" name="package_id" value="">
                                                                        <input type="hidden" name="credits_points" value="">
                                                                        <input type="hidden" name="package_duration" value="">
                                                                        <input type="hidden" name="package_price" value="">

                                                                        <div class="col-lg-4 col-md-4 text-center" style="margin: 0 auto;">
                                                                            <div class="card text-center p-4">
                                                                                <div class="plan-header">
                                                                                    <h4 id="package-name"></h4>
                                                                                    <h1 class="text-info">{{config('assignment.price') . " "}}<span id="package-price"></span></h1>
                                                                                </div>
                                                                                <hr>
                                                                                <ul class="list-unstyled">
                                                                                    <li><i class="fa fa-check-circle-o"></i> <span id="package-duration"></span></li>
                                                                                    <li><i class="fa fa-check-circle-o"></i> <span id="package-credits"></span>{{" " . config('assignment.creditpoint')}}</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>

                                                                        <button type="submit" id="submitPackage" class="p-2 btn btn-primary text-center pull-right"></button>
                                                                        <button class="p-2 btn btn-primary text-center pull-left" id="cancelBuy" type="button">
                                                                            {{trans('formlabel.subscriptionmodal.cancalpackage')}}
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!---package info-->
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div>
                                            <nav aria-label="Page navigation example" class="mb20 pull-right p-3">
                                                {{$package_list->links()}}
                                            </nav>    
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>

            </div>
        </div>
    </div>
</div>
<!--subscription page template-->

<script>
     //cancel package buy
    $("#cancelBuy").click(function(){
    $('#packageModal').modal('hide');
//  localStorage.removeItem("buy_package");
//  location.reload();
}); 

function buyPackage(e) {
    var packageid = $(e).attr('data-packageid');
    var packageprice = $(e).attr('data-packageprice');
    var packageduration = $(e).attr('data-packageduration');
    var creditpoints = $(e).attr('data-creditpoints');
    var packagename = $(e).attr('data-packagename');
    
    $("#package-name").text(packagename);
    $("#package-price").text(packageprice);
    $("#package-duration").text(packageduration);
    $("#package-credits").text(creditpoints);
    if (packageprice > 0) {
        $("#submitPackage").attr('onclick', 'buySubscription()');
        $("#modal-packagetitle").html("{{trans('formlabel.subscriptionmodal.buymessage')}}");
        $("#submitPackage").html("{{trans('formlabel.subscriptionmodal.submitpackage')}}");
    } else {
        $("#modal-packagetitle").html("{{trans('formlabel.subscriptionmodal.subscribemessage')}}");
        $("#submitPackage").html("{{trans('formlabel.subscriptionmodal.subscribepackage')}}");
    }
    
    $("input[name='package_id']").val(packageid);
    $("input[name='package_price").val(packageprice);
    $("input[name='package_duration']").val(packageduration);
    $("input[name='credits_points']").val(creditpoints);
}

$("#subscriptionPackage").submit(function() {
    var del = confirm('Are you sure to subscribe this?');
    if (del) {
        return true;
    } else {
        return false;   
    }
});
</script>
@include('includes.footer')

