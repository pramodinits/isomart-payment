@include('includes.header')

<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="login-wrapper col-md-5 col-sm-12">
                    <div class="text-center text-dark">
                        <h4><strong>Mobile Number Verification</strong></h4>
                        <label>OTP has been sent to your mobile number {{"xxxxxxx".substr(@Auth::user()->phone, -3)}}. Please enter below</label>
                         <br>
                    </div>
                    <form id="verify_otp_form" name="verify_otp_form" action="{{ asset('verifyotp')}}" method="post"> 
                        {!! csrf_field() !!}
                        <div class="input-field form-group">
                            <label for="otp" class="">Enter OTP*</label>
                            <input type="text" class="form-control" id="otp" name="otp">
                        </div>
                        <button type="submit" id="sbmt" class="btn btn-primary login-wrapper-submit">Verify OTP</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        $(".input-field :input").focus(function () {
            $("label[for='" + this.name + "']").addClass("active");
        }).blur(function () {
            $("label").removeClass("active");
        });

        $('#verify_otp_form').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "otp": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        },
                        integer: {
                            message: 'Enter numeric values only.'
                        },
                        stringLength: {
                            min: 4,
                            max: 4,
                            message: 'OTP must be 4 characters length.'
                        }
                    }
                }
            }
        })
                .on('success.form.bv', function (e) {
//                    $('#custom_loader').show();
                    return true;
                });

    });
</script>