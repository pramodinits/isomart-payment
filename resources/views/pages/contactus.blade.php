@include('includes.header')
<link href="{{ asset('/')}}css/smart-forms.css" rel="stylesheet" type="text/css" media="all" />
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
                <div class="main-content">
                    <div class="container pb50">
                        <h4 class="mt-0 mb-5">Contact Us</h4>
                        <div class="row">
                            <div class="col-md-6 mb40">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6 class=""> Office Address </h6>
                                        <p>
                                            124, Munna Wali Dhani, Lalchandpura, <br> Jhotwara, Jaipur, 302012<br>
                                            support@assan.com<br>
                                            +120 234-449-3949
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6 class="">Business hours</h6>
                                        <p>
                                            Monday-Friday: 8am to 6pm<br>
                                            Saturday: 10am to 2pm<br>
                                            Sunday: Closed<br>
                                        </p>
                                    </div>
                                </div>
                                <hr>
                                <div>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14967.998821278126!2d85.80630377938604!3d20.300279332358254!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a1909c55970eb61%3A0x81f12808cc25b847!2sJayadev+Vihar%2C+Bhubaneswar%2C+Odisha!5e0!3m2!1sen!2sin!4v1542019951855" width="100%" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="smart-wrap p-0">
                                    <div class="smart-forms smart-container wrap-2 mt-0">
                                        <form method="post" id="smart-form">
                                            <div class="form-body">
                                                <div class="section">
                                                    <label class="field prepend-icon">
                                                        <input type="text" name="sendername" id="sendername" class="gui-input" placeholder="Enter name">
                                                        <span class="field-icon"><i class="fa fa-user"></i></span>  
                                                    </label>
                                                </div><!-- end section -->
                                                <div class="section">
                                                    <label class="field prepend-icon">
                                                        <input type="email" name="emailaddress" id="emailaddress" class="gui-input" placeholder="Email address">
                                                        <span class="field-icon"><i class="fa fa-envelope"></i></span>
                                                    </label>
                                                </div><!-- end section -->
                                                <div class="section">
                                                    <label class="field prepend-icon">
                                                        <input type="text" name="sendersubject" id="sendersubject" class="gui-input" placeholder="Enter subject">
                                                        <span class="field-icon"><i class="fa fa-lightbulb-o"></i></span>
                                                    </label>
                                                </div><!-- end section -->
                                                <div class="section">
                                                    <label class="field prepend-icon">
                                                        <textarea class="gui-textarea" id="sendermessage" name="sendermessage" placeholder="Enter message"></textarea>
                                                        <span class="field-icon"><i class="fa fa-comments"></i></span>
                                                        <span class="input-hint"> <strong>Hint:</strong> Please enter between 80 - 300 characters.</span>   
                                                    </label>
                                                </div><!-- end section -->
                                                <div class="section">  
                                                    <label class="field">
                                                        <input type="text" id="g-recaptcha-response" name="g-recaptcha-response" class="smart-hidden-elem">
                                                        <div id="g-recaptcha" class="g-recaptcha" ></div>
                                                    </label>
                                                </div><!-- end section -->
                                                <div class="result"></div><!-- end .result  section --> 
                                            </div><!-- end .form-body section -->
                                            <div class="form-footer">
                                                <button type="submit" data-btntext-sending="Sending..." class="button btn-primary">Submit</button>
                                                <button type="reset" class="button"> Cancel </button>
                                            </div><!-- end .form-footer section -->
                                        </form>
                                    </div><!-- end .smart-forms section -->
                                </div><!-- end .smart-wrap section -->


                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
@include('includes.footer')