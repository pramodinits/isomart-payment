@include('includes.header')

@php
$packagetype = config('subscription.packagelist');
$packageduration = config('subscription.packageduration');
@endphp

<script>
    $(document).ready(function () {
        localStorage.removeItem("buy_package");
//   localStorage.setItem('buy_package', "0");
        });
        
    </script>

<div class="container-slider">
    <div id="demo" class="carousel slide bg-green" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>

  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active mx-auto">
       <img src="http://192.168.1.18/isomart_web/public/images/main_1.png" class="img-fluid">     
    </div>
    <div class="carousel-item mx-auto">
         <img src="http://192.168.1.18/isomart_web/public/images/main_2.png" class="img-fluid">
    </div>
    <div class="carousel-item mx-auto">
         <img src="http://192.168.1.18/isomart_web/public/images/main_3.png" class="img-fluid">
    </div>
     <div class="text-slider"> 
                        <!--/.tlt-->
        <h2 class="text-white-gray"> FIND AN AUDITOR ANYTIME, ANYWHERE </h2>
        <p class="lead text-white-gray"> Get most trusted, Experience auditor from thousand of our registered Members! </p>
        <div>
            <a href="{{asset("signin")}}">
               <button type="button" class="btn btn-dark mb5 btn-3d btn-lg">POST YOUR ASSIGNMENT </button>
            </a>
<!--                            <button type="button" class="btn btn-light btn-3d mb5"><i class="fa fa-video-camera"></i> Watch Video</button>-->
        </div>
         </div>
  </div>
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
   
</div>
<div class="clearfix"></div>
<div class="cta bg-secondary">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-white font-weight-normal text-center mb-0">
                    Are you an ISO Auditor OR a Certification Body ? Join the largest community of Auditors 
                    <a href="{{asset("signin")}}" class="btn btn-dark text-yellow"> Click Here </a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container pt30 pb30">
    <h4 class="text-left text-secondary font-weight-normal">{{trans('bladelable.home.latestassignments')}} 
        <small class="pull-right">
            <a href="{{asset('assignmentlist')}}" class="text-danger">
                <button type="button" class="btn btn-dark btn-sm">
                    {{trans('bladelable.home.morelinks')}} <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                </button>
            </a>
        </small></h4>
</div>

<div class="container">
    <div class="">
        <div class="row">
            <div class="col-lg-12 mb40">
                <div id="tabAssign">
                    <ul class="nav nav-tabs tabs-default mb30" role="tablist bg-light">
                        <li class="nav-item" role="presentation" style="display: none;">
                            <a class="nav-link  custom-tabs" href="#certification" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">{{trans('bladelable.tabtitle.tabcertification')}}</a>
                        </li>
                        <li class="nav-item " role="presentation">
                            <a class="nav-link  custom-tabs active show" href="#audit" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">{{trans('bladelable.tabtitle.tabaudit')}}</a>
                        </li>
                        <li class="nav-item " role="presentation">
                            <a class="nav-link  custom-tabs " href="#training" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">{{trans('bladelable.tabtitle.tabtraining')}}</a>
                        </li>
                    </ul>

                    

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade show active" id="audit">
                            <div class="card-body card">
                                <div class="row">
                                    @if(count(@$assignmentaudit)> 0)
                                    @foreach(@$assignmentaudit as $k=>$v)
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb30 ">
<!--                                        <div class="card card-body text-center"></div>-->
                                            <div class="card-header bg-premiry text-white">
                                                <div class="row">
                                                  <div class="col-6">
                                                <h5 href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}" class=" mb-0 headings">
                                                    @if(strlen($v->assignment_name) > 20)
                                                    {{substr(ucfirst($v->assignment_name), 0, 20) . "..."}}
                                                    @else
                                                    {{ucfirst($v->assignment_name)}}
                                                    @endif
                                                </h5>
                                                      </div>
                                            
                                       <div class="col-6">
                                                 
                                                <ul class="post-meta list-inline pull-right mb-0 mt-1">
                                <li class="list-inline-item">
                                    <div class="center">
                                                    <i style="font-size: .8rem; background-color: #fff; border-radius:50%; padding: 5px;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                </div>
                                </li>
                                <li class="list-inline-item">
                                     <div class="center">
                                                    <i style="font-size: .8rem; background-color: #fff; border-radius:50%; padding: 5px; margin: 0 1px" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                </div>
                                </li>
                                <li class="list-inline-item">
                                     <div class="center">
                                                    <i style="font-size: .8rem; background-color: #fff; border-radius:50%; padding: 5px;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                </div>
                                </li>
                            </ul>          
                                           
                                       </div>
                                            </div>
                                            </div>
                                      
                                            <div class="card pt-2">
                                                <div class="d-flex flex-row mx-auto col-12">
                                                <div class="mx-auto">
                                                    <?php
                                                    /*days left*/
                                                    $now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
                                                    $end_date = Carbon\Carbon::parse($v->proposal_end_date);
                                                    $now_date = Carbon\Carbon::parse($now);
                                                    $daysleft = $now_date->diffInDays($end_date, false);
                                                    /*days left*/
                                                    /* budget counted with approved bid */
                                                    $assignmentBudget = $v->budget_usd - $v->jobBudget;
                                                    $assignmentEffort = $v->effort_tentative - $v->jobEffort;
                                                    /* budget counted with approved bid */
                                                    ?>
                                                    <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $assignmentBudget}}</span>
                                                    {{" ( " . trans('formlabel.assignmentinfoaudit.efforts') .  $assignmentEffort . config('assignment.effortdays') . ")" }}
                                                    @if($v->approval_status == 1)
                                                    <span class="text-success">{{trans('formlabel.auditdetail.awarded')}}</span>
                                                    @elseif(@$daysleft < 0)
                                                    <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                    @elseif(@$daysleft == 0)
                                                    <span class="text-success">{{trans('formlabel.auditdetail.openfortoday')}}</span>
                                                    @else
                                                    <span class="text-success">{{config('assignment.assignmentopen') . "" . @$daysleft . "" . config('assignment.assignmentdaysleft')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                                <div class="clearfix"></div>
                                                <div class="text-center">
                                                    <a href="{{ asset('assignmentAuditDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a>
                                                  
                                                    <div class="">
                                                        {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat')) . " to ". Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}
                                                    </div>
                                                    <?php
                                                $citynames = "";
                                                foreach (@$v->AssignmentLocation as $key => $val) {
                                                    if(!$val->whomtoassign_id) {
                                                    $citynames .= $val->city_name . ", ";
                                                    } elseif($v->approval_status == 1) {
                                                        $citynames .= $val->city_name . ", ";
                                                    }
                                                }
                                                ?>
                                                    <div style="width: 100%; height: 50px; overflow-x: auto">
                                                        {{rtrim($citynames, ", ")}}<br> 
                                                    </div>
                                                </div>
                                                <div class="text-center">
                                                    <h6 class="text-muted"> {{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</h6>
                                                </div>
                                            </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="col-md-12">
                                        <div class="text-secondary text-center" style="text-align: center;">
                                            {{trans('bladelable.home.noassignment')}}
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="training">
                            <div class="card-body card">
                                <div class="row">
                                    @if(count(@$assignmenttraining)> 0)
                                    @foreach(@$assignmenttraining as $k=>$v)
                                    <div class="col-md-4 mb-30">
                                         <div class="card-header bg-premiry text-white">
                                             <div class="row">
                                             <div class="col-6">
                                                 <h5 class="card-title mb-0">
                                                <a href="{{ asset('assignmentTrainingDetail/'.$v->assignment_id)}}">{{ucfirst($v->batch_no)}} </a>
                                            </h5>
                                             </div>
                                                 <div class="col-6">
                                                      @if($v->mode == 1)
                                  <ul class="post-meta list-inline pull-right mb-0 mt-1">
                                <li class="list-inline-item">
                                    <div class="center">
                                          <i style="font-size: .8rem; background-color: #fff; border-radius:50%; padding: 5px; margin: 0 1px" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                    </div>
                                </li>
                                <li class="list-inline-item">
                                    <div class="center">
                                          <i style="font-size: .8rem; background-color: #fff; border-radius:50%; padding: 5px; margin: 0 1px" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                    </div>
                                </li>
                                <li class="list-inline-item">
                                    <div class="center">
                                          <i style="font-size: .8rem; background-color: #fff; border-radius:50%; padding: 5px; margin: 0 1px" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                    </div>
                                </li>
                                  </ul>   
                                            @endif
                                                 </div>
                                         </div>
                                              
                                         </div>
                                        <div class="card text-center pt-2">
                                           
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="mx-auto">
                                                    <?php
                                                    /* days left */
                                                    $now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
                                                    $end_date = Carbon\Carbon::parse($v->proposal_end_date);
                                                    $now_date = Carbon\Carbon::parse($now);
                                                    $daysleft = $now_date->diffInDays($end_date, false);
                                                    /* days left */
                                                    /* budget counted with approved bid */
                                                    $assignmentBudget = $v->fee_range - $v->jobBudget;
                                                    $assignmentEffort = $v->effort_tentative - $v->jobEffort;
                                                    /* budget counted with approved bid */
                                                    ?>
                                                    <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $assignmentBudget}}</span>
                                                    {{" ( " . trans('formlabel.assignmentinfoaudit.efforts') .  $assignmentEffort . config('assignment.effortdays') . ")" }}
                                                    @if($v->approval_status == 1)
                                                    <span class="text-success">{{trans('formlabel.auditdetail.awarded')}}</span>
                                                    @elseif(@$daysleft < 0) 
                                                    <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                    @elseif(@$daysleft == 0)
                                                    <span class="text-success">{{trans('formlabel.auditdetail.openfortoday')}}</span>
                                                    @else
                                                    <span class="text-success">{{config('assignment.assignmentopen') . "" . @$daysleft . "" . config('assignment.assignmentdaysleft')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                           
                                            <hr/>
                                            <p class="card-text text-left col-12">
                                                {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('assignmentTrainingDetail/'.$v->assignment_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                                                {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                                                @if($v->mode == 1)
                                                <?php
                                                $citynames = "";
                                                foreach (@$v->AssignmentLocation as $key => $val) {
                                                    if (!$val->whomtoassign_id) {
                                                        $citynames .= $val->city_name . ", ";
                                                    } elseif ($v->approval_status == 1) {
                                                        $citynames .= $val->city_name . ", ";
                                                    }
                                                }
                                                ?>
                                                {{trans('formlabel.assignmentinfotraining.mode')}} {{config('assignment.modef2f')." (" . rtrim($citynames, ", ") . ")"}}<br>
                                                @else
                                                {{trans('formlabel.assignmentinfotraining.mode')}} {{config('assignment.modeonline')}}<br>
                                                @endif
                                                <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                                            </p>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="col-md-12">
                                        <div class="text-secondary text-center" style="text-align: center;">
                                            {{trans('bladelable.home.noassignment')}}
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="certification">
                            <div class="container">
                                <div class="row">
                                    @if(count(@$assignmentcertification)> 0)
                                    @foreach(@$assignmentcertification as $k=>$v)
                                    <div class="col-md-4 mb30">
                                        <div class="card text-center">
                                            <h4 class="card-title">
                                                <a href="{{ asset('assignmentCertificationDetail/'.$v->assignment_id)}}">{{substr(ucfirst($v->assignment_name), 0, 20) . "..."}}</a>
                                            </h4>
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="col-3"></div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-3"></div>
                                            </div>
                                            <hr/>
                                            <p class="card-text text-left col-12">{{substr($v->description, 0, 40) . "..."}}<br>
                                                Ref ID : <a href="{{ asset('assignmentCertificationDetail/'.$v->assignment_id)}}">{{"#".$v->ref_id}}</a><br>
                                                Duration : {{$v->effort_tentative . " days"}} <br>
                                                <?php
                                                $names = "";
                                                $cityArray = explode(",", $v->location);
                                                foreach ($cityArray as $key => $val) {
                                                    $names .= @$cityNameArray[$val] . ", ";
                                                }
                                                ?>
                                                Location : {{rtrim($names, ", ")}}<br>
                                                <strong>{{$v->add_date}}</strong>
                                            </p>

                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="d-flex text-secondary text-center" style="text-align: center;">
                                        No assignment found.
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Tab panes -->
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if(@$advtinfo && @$advtinfo[1])
            <img src="{{asset('training_image')}}/{{@$advtinfo[1]}}" class="img-fluid pt-2 pb-2">
            @else
            <img src="images/add_img.jpg" class="img-fluid pt-2 pb-2">
            @endif
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="container pt30">
    <h4 class="text-left text-secondary font-weight-normal pb30">{{trans('bladelable.home.latesttrainingassignments')}} 
        <small class="pull-right">
            <a href="{{asset('trainingPostList')}}" class="text-danger">
                <button type="button" class="btn btn-dark btn-sm">
                    {{trans('bladelable.home.morelinks')}} <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                </button>
            </a>
        </small>
    </h4>
</div>
<div class="container">
    <div class="card pt-4">
    <div class="row">
        <div class="col-lg-12">
            <div class="container">
            <div class="row">
                @if(count(@$trainingpostinfo) > 0)
                @foreach(@$trainingpostinfo as $k=>$v)
                <div class="col-md-4 mb30">
                    <div class="card-header bg-premiry text-white">
                        <h5 class="mb-0">
                            <a href="{{ asset('trainingPostDetail/'.$v->training_post_id)}}">
                                @if(strlen($v->traning_name) > 20)
                                {{substr(ucfirst($v->traning_name), 0, 20) . "..."}}
                                @else
                                {{ucfirst($v->traning_name)}}
                                @endif
                            </a>
                        </h5>
                        </div>
                    
                    <div class="card">
                        <div class="col-12">
                            <h4>
                                {{trans('messagelabel.training.feerange')}} : <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $v->fee_range}}</span>
                            </h4>
                        </div>
                          <hr/>

                        <p class="card-text text-left col-12">
                            {{trans('formlabel.assignmentinfoaudit.refid')}} <a href="{{ asset('trainingPostDetail/'.$v->training_post_id)}}">{{config('assignment.refid') . $v->ref_id}}</a><br>
                            {{trans('formlabel.assignmentinfoaudit.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                            {{trans('formlabel.assignmentinfoaudit.endadte')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                            <strong>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}</strong>
                        </p>
                    </div>
                </div>
                @endforeach
                @else 
                <div class="col-md-12">
                    <div class="text-secondary text-center" style="text-align: center;">
                        {{trans('bladelable.home.noadvertisement')}}
                    </div>
                </div>
                @endif
            </div>  
            </div>
        </div>
    </div>
    </div>

    <div class="row">
        <div class="col-12">
            @if(@$advtinfo && @$advtinfo[2])
            <img src="{{asset('training_image')}}/{{@$advtinfo[2]}}" class="img-fluid pt-2 pb-2">
            @else
            <img src="images/add_img.jpg" class="img-fluid pt-2 pb-2">
            @endif
        </div>
    </div>
</div>

<div class="container pt30">
    <h4 class="text-left text-secondary font-weight-normal pb30">{{trans('bladelable.home.latestqueries')}} 
        <small class="pull-right "> <a href="{{asset('queryPostList')}}" class="text-danger"><button type="button" class="btn btn-dark btn-sm">
                    {{trans('bladelable.home.morelinks')}} <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                </button></a></small>
    </h4>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="row">
                <div class="col-md-3 col-3">
                    @if(@$advtinfo && @$advtinfo[4])
                    <img src="{{asset('training_image')}}/{{@$advtinfo[4]}}" class="img-fluid pt-2 pb-2">
                    @else
                    <img src="images/iso.jpg" class="img-fluid">
                    @endif

                </div>
                <div class="col-md-9 col-9">
                    <div class="addsection_grey bg-gray">
                        <a href="{{ asset('queryPostDetail/'.@$querypostinfo['0']->query_id)}}">
                        <h5 class="sidebar-title pl-3">{{@$querypostinfo['0']->title}}</h5>
                        </a>
                        <p class="pl-3 mb-0">{{substr(@$querypostinfo['0']->description, 0, 50)."..."}} <br>
                            Effort in man days : {{@$querypostinfo['0']->effort_mandays}} days <br>
                            Location : {{@$querypostinfo['0']->city_name}},{{@$state[@$querypostinfo['0']->states]}},{{@$country[@$querypostinfo['0']->country]}}<br>
                           
                        </p>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6 col-xs-12">
            <div class="row">
                <div class="col-md-9 col-9">
                    <div class="addsection_grey bg-gray">
                        <a href="{{ asset('queryPostDetail/'.@$querypostinfo['1']->query_id)}}">
                        <h5 class="sidebar-title pl-3">{{@$querypostinfo['1']->title}}</h5>
                        </a>
                        <p class="pl-3 mb-0">{{substr(@$querypostinfo['1']->description, 0, 50)."..."}} <br>
                            Effort in man days : {{@$querypostinfo['1']->effort_mandays}} days <br>
                            Location : {{@$querypostinfo['1']->city_name}},{{@$state[@$querypostinfo['1']->states]}},{{@$country[@$querypostinfo['0']->country]}}<br>
                            
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-3">
                    @if(@$advtinfo && @$advtinfo[5])
                    <img src="{{asset('training_image')}}/{{@$advtinfo[5]}}" class="img-fluid pt-2 pb-2">
                    @else
                    <img src="images/iso.jpg" class="img-fluid">
                    @endif

                </div>
            </div>

        </div>
        <div class="col-12">
            @if(@$advtinfo && @$advtinfo[3])
            <img src="{{asset('training_image')}}/{{@$advtinfo[3]}}" class="img-fluid pt-2 pb-2">
            @else
            <img src="images/add_img.jpg" class="img-fluid pt-2 pb-2">
            @endif
        </div>
    </div>

</div>

<div  class="cta bg-faded pt30 p30">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="pt20  pb20">
                    <div class="mb30 text-center">
                        <h4 class="text-secondary">How It Work</h4>
                    </div>
                    <div class="hw-wrk">
                        <p>Organizations or CBs post assignment on specific date, location</p>
                        <p>System matches date, location & job with Audiors displays list, 
                            Notification send</p>
                        <p>All auditors maintain their schedule including travels & location 
                            availability</p>
                        <p>All auditors maintain their schedule including travels & location 
                            availability</p>
                        <p>Organizations & CBs evaluate and book an auditor.Auditor receives 
                            notification and confirm</p>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pt40  pb40">
                    <div class="container">
                        <div class="col-md-12 text-center">
                            <div class="mb30 text-center">
                                <h4 class="text-secondary">Testimonial</h4>
                            </div>
                        </div>
                        <!--title--> 
                        <!-- Carousel -->
                        <div class="carousel-testimonial-dark owl-carousel owl-theme carousel-dark">
                            <div class="item">
                                <div class="testimonial-dark testimonial">
                                    <div class="row align-items-center">
                                        <div class="col-md-12 text-center">
                                            <div class=""> <img src="images/customer-1.jpg" alt="" class="img-fluid circle"> </div>
                                            <p class=" p-2"> " This is a great theme! I love it and recommend it to anyone wanting to purchase it. " </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--item-->
                            <div class="item">
                                <div class="testimonial-dark testimonial">
                                    <div class="row align-items-center">
                                        <div class="col-md-12 text-center">
                                            <div class=""> <img src="images/customer-2.jpg" alt="" class="img-fluid circle"> </div>
                                            <p class=" p-4"> " Compared to others this was a very easy to use and adapt template. It didn't break as easy with minor changes. Thanks " </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--item-->
                            <div class="item">
                                <div class="testimonial-dark testimonial">
                                    <div class="row align-items-center">

                                        <div class="col-md-12 text-center">
                                            <div class=""> <img src="images/customer-3.jpg" alt="" class="img-fluid circle"> </div>
                                            <p class=" p-4"> " Hello,
                                                your theme gets better and better on every version. Gratulation to this wonderful theme and please keep on working this way (good once)!! " </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--item--> 
                        </div>
                        <!-- /Carousel --> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="benifits-parallax">
    <div class="container">
        <h4 class="text-center text-light p-4">Benifits of ISO Mart</h4>
        <div class="row">
            <div class="col-lg-6">
                <div class="benifitheading p-2 text-center">Organization/Certification Bodies</div>
                <ul>
                    <li>
                        <p>44% higher checkout conversion than other payment methods*.</p>
                    </li>
                    <li>
                        <p>Accept payments from 200+ markets with a single account.</p>
                    </li>
                    <li>
                        <p>Stay safer with Seller Protection and 24/7 real-time fraud intelligence.</p>
                    </li>
                </ul>
            </div>
            <div class="col-lg-6">
                <div class="benifitheading p-2 text-center">Auditors</div>
                <ul>
                    <li>
                        <p>Your customers can shop with confidence with return shipping refunds 
                            of up to ₹500 or US$15 per return.</p>
                    </li>
                    <li>
                        <p>Buyer Protection lets your customers pay with confidence.</p>
                    </li>
                    <li>
                        <p>Your customers' financial and personal information are securely 
                            encrypted.</p>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

    
<div class="container pt30">
    <h4 class="text-left text-secondary font-weight-normal pb30">
        {{trans('bladelable.home.packageheader')}}
    </h4>
</div>

<div class="container">
    <div class="card p-4">
                    <div class="row">
                            @if(count(@$package_list) > 0)
                            @foreach(@$package_list as $key => $val)
                            <div class="col-lg-4 col-md-4">
                                <div class="card text-center p-4">
                                    <input type="hidden" name="package_id" id="package<?= $val->id ?>" value="<?= $val->id ?>">
                                <div class="plan-header ">
                                    <h4>{{$val->package_name}}</h4>
                                    <h1 class="text-info">{{config('assignment.price') . " ". $val->package_price}}</h1>
                                </div>
                                    <a class="btn btn-primary btn-lg btn-block mb-3" onclick="packageInfo('{{$val->id}}', '{{$val->package_name}}', '{{$val->package_price}}', '{{$packageduration[$val->package_duration]}}', '{{$val->credit_points}}')">
                                    @if ($val->package_price > 0)
                                    {{trans('bladelable.home.buypackage')}}
                                    @else
                                    {{trans('bladelable.home.subscribepackage')}}
                                    @endif
                                    </a>
                                    
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check-circle-o"></i> {{$packageduration[$val->package_duration]}}</li>
                                    <li><i class="fa fa-check-circle-o"></i> {{$val->credit_points . " " . config('assignment.creditpoint')}}</li>
                                </ul>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="col-md-12">
                                <div class="text-secondary text-center" style="text-align: center;">
                                    {{trans('bladelable.home.nopackages')}}
                                </div>
                            </div>
                            @endif  
        </div>
    </div>
</div>
<!--</div>-->
<script>
    function packageInfo(id, package_name, package_price, package_duration, credit_points) {
        var package = {"id":id, "package_name":package_name, "package_price":package_price, "package_duration":package_duration, "credit_points":credit_points};
        localStorage.setItem("buy_package", JSON.stringify(package));
        window.location.href = "{{asset('/signin')}}";
    }
    </script>

@include('includes.footer')