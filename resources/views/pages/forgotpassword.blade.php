@include('includes.header')
<link rel="stylesheet" href="{{ asset('intltel/css/intlTelInput.css')}}">

<link rel="stylesheet" href="{{ asset('intltel/css/demo.css')}}">

<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="container">
                <div class="login-wrapper col-md-5 col-sm-12">
                    <div class="text-center text-dark">
                        <h4><strong>Forgot Password</strong></h4>
                        <label id="otpLabel">Please provide your registered phone number to reset your password.</label>
                        <br><br>
                    </div>
                    <form id="verifyotp_form" name="verifyotp_form" action="#" method="post"> 
                        {!! csrf_field() !!}
                        <div class="input-field form-group" id="show_phoneDiv">
                            <label for="Phone" class="active">Enter Phone*</label>
                            <input type="text" class="form-control" id="Phone" name="Phone" onkeypress="return isNumberKey(event)">
                            <input type="hidden" value="+91" name="countrycode" id="countrycode" />
                            <i style="display: none; color: #b24d4b;" class="form-control-feedback fa fa-remove" data-bv-icon-for="phone-check"></i>
                            <div class="help-block has-error small" style="color: #b24d4b;"></div>
                        </div>

                        <!---enter otp--->
                        <div class="input-field form-group" id="show_otpDiv" style="display: none;">
                            <label for="showOtp">OTP*</label>
                            <input type="text" class="form-control" id="showOtp" name="showOtp">
                            <div class="help-block has-error small" id="otp-error" style="color: #b24d4b;"></div>
                        </div> <!-- /password -->
                        <!---enter otp--->

                        <!---set password --->
                        <div id="set_pwd" style="display: none;">
                            <div class="input-field form-group">
                                <label for="setPassword">New Password*</label>
                                <input type="password" class="form-control" id="setPassword" name="setPassword">
                            </div>
                            <div class="input-field form-group">
                                <label for="resetPassword">Confirm Password*</label>
                                <input type="password" class="form-control" id="resetPassword" name="resetPassword">
                            </div>
                        </div>
                        <!---set password --->

                        <button type="submit" id="request_otp" class="btn btn-primary btn-block btn-lg">Request OTP</button>
                        <button type="submit" id="verify_otp" style="display: none;" class="btn btn-primary login-wrapper-submit">Verify OTP</button>
                        <div class="form-group col-md-12 mt-2" id="info-div" >
                            <span class="col-md-6 text-left"></span>
                            <span class="col-md-6 text-right">
                                <a href="{{asset('signin')}}" class="forgot-password">{{trans('formlabel.signup.loginlink')}}</a>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(".input-field :input").focus(function () {
        $("label[for='" + this.name + "']").addClass("active");
    }).blur(function () {
        $("label").removeClass("active");
    });

    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 1 second for example
    var $input_phone = $('#Phone');

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    $input_phone.on('keyup', function () {
        if ($input_phone.val().length >= 8) {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(checkPhone, doneTypingInterval);
        } else {
            $("div.help-block").hide();
            return false;
        }
    });

    function checkPhone() {
        var phone = $("#Phone").val();
        var countrycode = $("#countrycode").val();
        var _token = "<?php echo csrf_token(); ?>";
        $("#custom_loader").show();
        if (phone) {
            $.ajax({
                method: "POST",
                url: "{{url('/checkphone')}}",
                data: {
                    "phone": phone,
                    "countrycode": countrycode,
                    "_token": _token
                },
                cache: false,
                success: function (res) {
                    if (res === "success") {
                        $("#custom_loader_alert_box").hide();
                        $("div.help-block").hide();
                        $("i[data-bv-icon-for=Phone]").show();
                        $("i[data-bv-icon-for=phone-check]").hide();
                    } else {
                        $("i[data-bv-icon-for=Phone]").hide();
                        $("i[data-bv-icon-for=phone-check]").show();
                        $("div.help-block").html("Mobile number is not registered. Kindly <a href='{{asset('signup')}}/" + phone + "'>Register</a>").show();
                        $('#request_otp').prop('disabled', true);
                        $("#custom_loader").hide();
                        $("#custom_loader_alert_box").hide();
                    }
                }
            });
        }
    }

//    $('#setPassword, #resetPassword').on('keyup', function () {
//        if ($('#setPassword').val() === $('#resetPassword').val()) {
//            $('#message').html('Matching').css('color', 'green');
//        } else
//            $('#message').html('Not Matching').css('color', 'red');
//    });

    $(document).ready(function () {
        $('#verifyotp_form').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "Phone": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    },
                    integer: {
                        message: 'Enter digit only.'
                    }
                },
                "showOtp": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                },
                "setPassword": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        },
                        identical: {
                            field: 'resetPassword',
                            message: 'The password and its confirm are not the same'
                        },
                        stringLength: {
                            min: 6,
                            max: 20,
                            message: 'The password must be within 6 to 20 characters.'
                        }
                    }
                },
                "resetPassword": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        },
                        identical: {
                            field: 'setPassword',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                }
            }
        })
                .on('success.form.bv', function (e) {
                    e.preventDefault();
                    var $form = $(e.target);
                    var bv = $form.data('bootstrapValidator');

                    var phone = $("#Phone").val();
                    var countrycode = $("#countrycode").val();
                    var showOtp = $("#showOtp").val();
                    var setPassword = $("#setPassword").val();
                    var resetPassword = $("#resetPassword").val();
                    var _token = "<?php echo csrf_token(); ?>";

                    if (phone && setPassword) {
                        if (setPassword !== resetPassword) {
                            $("i[data-bv-icon-for=resetPassword]").addClass("fa fa-remove").css('color', '#b24d4b');
                            $("small[data-bv-validator=identical]").show();
                            $('.help-block').css('color', '#b24d4b');
                            return false;
                        } else {
//                            $("i[data-bv-icon-for=Confirm_password]").addClass("fa fa-check").css('color', '#3c763d');
                            $("i[data-bv-icon-for=setPassword]").addClass("fa fa-check").css('color', '#3c763d');
                        }
//                        alert("checked");
                        $.ajax({
                            method: "POST",
                            url: "{{url('/setPasswordOtp')}}",
                            data: {
                                "phone": phone,
                                "countrycode": countrycode,
                                "setPassword": setPassword,
                                "_token": _token
                            },
                            cache: false,
                            success: function (res) {
                                if (res === "success") {
                                    $("#custom_loader_alert_box").html("Password reset successful.").show();
                                    window.location.href = "{{asset('signin')}}";
                                } else {
                                    $("#otp-error").html("Error in password reset.").show();
                                }
                            }
                        });
                    } else if (phone && showOtp) {
                        $.ajax({
                            method: "POST",
                            url: "{{url('/setPasswordOtp')}}",
                            data: {
                                "phone": phone,
                                "countrycode": countrycode,
                                "showOtp": showOtp,
                                "_token": _token
                            },
                            cache: false,
                            success: function (res) {
                                if (res === "success") {
                                    $("#otpLabel").hide();
                                    $("#show_otpDiv").hide();
                                    $("#show_phoneDiv").hide();
                                    $("#info-div").hide();
                                    $("#set_pwd").show();
                                    $("#verify_otp").show().prop('disabled', false);
                                    $("#verify_otp").html("reset password");
                                    $("#request_otp").hide();
                                } else {
                                    $("#otp-error").html("OTP mismatched").show();
                                }
                            }
                        });
                    } else {
                        $.ajax({
                            method: "POST",
                            url: "{{url('/setPasswordOtp')}}",
                            data: {
                                "phone": phone,
                                "countrycode": countrycode,
                                "_token": _token
                            },
                            cache: false,
                            success: function (res) {
                                if (res === "success") {
                                    var phoneNo = phone.substr(phone.length - 4, phone.length - 1);
                                    $("#otpLabel").html('OTP has been sent to your mobile number ending with '+ phoneNo +'. Please enter below.');
                                    $("#show_otpDiv").show();
                                    $("#verify_otp").show().prop('disabled', false);
                                    $("#request_otp").hide();
                                    $("#custom_loader").show();
                                    $("#custom_loader_alert_box").html("OTP sent to your mobile number ending with "+ phoneNo +". Please enter below.").show();
                                    setTimeout(function () {
                                        $("#custom_loader_alert_box").hide();
                                    }, 2000);
                                } else {
                                    $("#phone-error").html("Recheck mobile no.").show();
                                }
                            }
                        });
                    }
                    $('#custom_loader').hide();
                    return true;

                });
    });
</script>
<script src="{{ asset('intltel/js/intlTelInput.js')}}"></script>
<script>
    $("#Phone").intlTelInput({
        // allowDropdown: false,
        // autoHideDialCode: false,
        // autoPlaceholder: "off",
        // dropdownContainer: "body",
        // excludeCountries: ["us"],
        // formatOnDisplay: false,
        // geoIpLookup: function(callback) {
        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //     var countryCode = (resp && resp.country) ? resp.country : "";
        //     callback(countryCode);
        //   });
        // },
        // hiddenInput: "full_number",
        initialCountry: "in",
        // nationalMode: false,
        //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        separateDialCode: true,
        utilsScript: "{{ asset('intltel/js/utils.js')}}"
    });
    // on blur: validate    
    $('.country').click(function () {
        $('#countrycode').val($(this).children('.dial-code').html());
    });
    $('.country').click(function () {
        if ($("#Phone").val().length >= 8) {
            checkPhone();
        }
    });
</script>

