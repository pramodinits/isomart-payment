<?php
$generalStatus = $generalstatuscolor;
if (@$generalDetails->iso_standards &&
        $regDateProof &&
        $generalDetails->brief_info &&
        $trainingCertificate &&
        $aggrement &&
        $verificationReport &&
        $cv &&
        $codeClaim &&
        $generalDetails->registration_date &&
        $regCertificate &&
        $auditLog &&
        $appreciationLetter &&
        $generalDetails->status
) {
    $generalStatus = "#5CB85C";
}
?>
<style>
    .generalstatus{
        color: <?= $generalStatus; ?> !important;
    }
</style>
<div class="col-md-12 col-sm-12 col-12">
    <form id="profile-general" name="profile-general" action="{{ asset('updateProfileGeneral')}}" method="post" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="row">
            <div class="col-md-6 col-sm-6 col-12">
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <input type="hidden" name="id" value="{{@$generalDetails->id}}">
                <label>{{trans('formlabel.profilegeneral.isocode')}}</label>
                <div class="form-group">
                    <select class="form-control" name="profile[iso_standards]" id="isoCode" value="{{@$generalDetails->iso_standards}}">
                        <option value="">Select Standard</option>
                        @foreach(@$standardlist as $k=>$v)
                        <option value="{{@$k}}" <?= (@$generalDetails->iso_standards == @$k) ? "selected" : ""; ?>>{{@$v}}</option>
                        @endforeach
                    </select>
                    <!--<input type="text" class="form-control" id="isoCode" name="profile[iso_standards]" value="{{@$generalDetails->iso_standards}}">-->
                </div>
                <label>{{trans('formlabel.profilegeneral.regdateproof')}}</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <input name="regDateProof" id="regDateProof" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept=".doc, .docx, .txt,.pdf">
                        <input type="hidden" name="regDateProof_prev" value="{{@$regDateProof[0]->name}}" />
                    </span>
                    <span class="form-control border-right-0"></span>
                    <span class="btn btn-light border " onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-paperclip fa-1x"></i></span>
                    <small class="text-success" id="hdocument_details"></small>
                </div>
                @if(@$regDateProof)
                <div class="mt-2">
                    <div class="d-inline-flex text-white bg-gray">
                        <div class="border-radius-right pl-2"><i class="fa fa-file-pdf-o font11"></i> 
                            <small class="font11">{{@$regDateProof[0]->name}}</small>
                        </div>
                        @if(@$regDateProof[0]->id)
                        <div class="border-radius-left pr-2 pl-1"> 
                            <a href="{{ asset('downloaddocument')}}/{{MD5(@$regDateProof[0]->id)}}/{{@$generalDetails->id}}" >
                                <i class="fa fa-download text-white font11" data-toggle="tooltip" title="{{@$regDateProof[0]->name}} Download" data-original-title="Download"></i> 
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
                <div>
                </div>
                @endif

                <label>{{trans('formlabel.profilegeneral.briefinfo')}}</label>
                <div class="input-group">
                    <textarea class="form-control" name="profile[brief_info]" id="briefInfo" rows="5" cols="25">{{@$generalDetails->brief_info}}</textarea>
                </div>
                <label>{{trans('formlabel.profilegeneral.trainingcertificate')}}</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <input name="trainingCertificate" id="trainingCertificate" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept=".doc, .docx, .txt,.pdf">
                        <input type="hidden" name="trainingCertificate_prev" value="{{@$trainingCertificate[0]->name}}" />
                    </span>
                    <span class="form-control border-right-0"></span>
                    <span class="btn btn-light border " onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-paperclip fa-1x"></i></span>
                </div>
                @if(@$trainingCertificate)
                <div class="mt-2">
                    <div class="d-inline-flex text-white bg-gray">
                        <div class="bg-info border-radius-right pl-2 font11">
                            <i class="fa fa-file-pdf-o"></i> 
                            <small class="font11">{{@$trainingCertificate[0]->name}}</small>
                        </div>
                        @if(@$trainingCertificate[0]->id)
                        <div class="bg-info border-radius-left pr-2 pl-1"> <a href="{{ asset('downloaddocument')}}/{{MD5(@$trainingCertificate[0]->id)}}/{{@$generalDetails->id}}" >
                                <i class="fa fa-download text-white font11" data-toggle="tooltip" title="{{@$trainingCertificate[0]->name}} Download" data-original-title="Download"></i> 
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
                @endif

                <label>{{trans('formlabel.profilegeneral.aggrement')}}</label>
                <div class="input-group">
                    <span class="input-group-btn">   
                        <input name="aggrement" id="aggrement" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept=".doc, .docx, .txt,.pdf">
                        <input type="hidden" name="aggrement_prev" value="{{@$aggrement[0]->name}}" />
                    </span>
                    <span class="form-control border-right-0"></span>
                    <span class="btn btn-light border " onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-paperclip fa-1x"></i></span>
                </div>
                @if(@$aggrement)
                <div class="mt-2">
                    <div class="d-inline-flex text-white bg-gray">
                        <div class="bg-info border-radius-right pl-2">
                            <i class="fa fa-file-pdf-o"></i> 
                            <small class="font11">{{@$aggrement[0]->name}}</small></div>
                        @if(@$aggrement[0]->id)
                        <div class="bg-info border-radius-left pr-2 pl-1"> <a href="{{ asset('downloaddocument')}}/{{MD5(@$aggrement[0]->id)}}/{{@$generalDetails->id}}" >
                                <i class="fa fa-download text-white font11" data-toggle="tooltip" title="{{@$aggrement[0]->name}} Download" data-original-title="Download"></i> 
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
                @endif

                <label>{{trans('formlabel.profilegeneral.verauditreport')}}</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <input name="verificationReport" id="verificationReport" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept=".doc, .docx, .txt,.pdf">
                        <input type="hidden" name="verificationReport_prev" value="{{@$verificationReport[0]->name}}" />
                    </span>
                    <span class="form-control border-right-0"></span>
                    <span class="btn btn-light border " onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-paperclip fa-1x"></i></span>
                </div>
                @if(@$verificationReport)
                <div class="mt-2">
                    <div class="d-inline-flex text-white bg-gray">
                        <div class="bg-info border-radius-right pl-2"><i class="fa fa-file-pdf-o"></i> <small class="font11">{{@$verificationReport[0]->name}}</small></div>
                        @if(@$verificationReport[0]->id)
                        <div class="bg-info border-radius-left pr-2 pl-1"> <a href="{{ asset('downloaddocument')}}/{{MD5(@$verificationReport[0]->id)}}/{{@$generalDetails->id}}" >
                                <i class="fa fa-download text-white font11" data-toggle="tooltip" title="{{@$verificationReport[0]->name}} Download" data-original-title="Download"></i> 
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
                @endif
            </div>

            <div class="col-md-6 col-sm-6 col-12">
                <label>{{trans('formlabel.profilegeneral.registrationdate')}}</label>
                <?php
                if (@$generalDetails->registration_date) {
                    @$registration_date = @$generalDetails->registration_date;
                }
                ?>
                <div class="form-group">
                    <input type="date" class="form-control" id="regDate" name="profile[registration_date]" onkeydown="return false" value="<?php echo @$registration_date; ?>">
                </div>
                <label>{{trans('formlabel.profilegeneral.cv')}}</label>
                <div class="input-group">
                    <span class="input-group-btn">   
                        <input name="cv" id="cv" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept=".doc, .docx, .txt,.pdf">
                        <input type="hidden" name="cv_prev" value="{{@$cv[0]->name}}" />
                    </span>
                    <span class="form-control border-right-0"></span>
                    <span class="btn btn-light border " onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-paperclip fa-1x"></i></span>
                </div>
                @if(@$cv)
                <div class="mt-2">
                    <div class="d-inline-flex text-white bg-gray">
                        <div class="bg-info border-radius-right pl-2"><i class="fa fa-file-pdf-o"></i> <small class="font11">{{@$cv[0]->name}}</small></div>
                        @if(@$cv[0]->id)
                        <div class="bg-info border-radius-left pr-2 pl-2"> <a href="{{ asset('downloaddocument')}}/{{MD5(@$cv[0]->id)}}/{{@$generalDetails->id}}" >
                                <i class="fa fa-download text-white font11" data-toggle="tooltip" title="{{@$cv[0]->name}} Download" data-original-title="Download"></i> 
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
                @endif

                <label>{{trans('formlabel.profilegeneral.codeclaim')}}</label>
                <div class="input-group">
                    <span class="input-group-btn">   
                        <input name="codeClaim" id="codeClaim" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept=".doc, .docx, .txt,.pdf">
                        <input type="hidden" name="codeClaim_prev" value="{{@$codeClaim[0]->name}}" />
                    </span>
                    <span class="form-control border-right-0"></span>
                    <span class="btn btn-light border" onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-paperclip fa-1x"></i></span>
                </div>
                @if(@$codeClaim)
                <div class="mt-2">
                    <div class="d-inline-flex text-white bg-gray">
                        <div class="bg-info border-radius-right pl-2"><i class="fa fa-file-pdf-o font11"></i> <small class="font11">{{@$codeClaim[0]->name}}</small></div>
                        @if(@$codeClaim[0]->id)
                        <div class="bg-info border-radius-left pr-2 pl-2"> <a href="{{ asset('downloaddocument')}}/{{MD5(@$codeClaim[0]->id)}}/{{@$generalDetails->id}}" >
                                <i class="fa fa-download text-white font11" data-toggle="tooltip" title="{{@$codeClaim[0]->name}} Download" data-original-title="Download"></i> 
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
                @endif

                <label>{{trans('formlabel.profilegeneral.regcertidficates')}}</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <input name="regCertificate[]" multiple id="regCertificate" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept=".doc, .docx, .txt,.pdf">
                    </span>
                    <span class="form-control border-right-0"></span>
                    <span class="btn btn-light border" onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-paperclip fa-1x"></i></span>
                </div>

                <div class="mt-2">
                    @if(@$regCertificate)
                    @foreach(@$regCertificate as $key=>$val)
                    <div class="d-inline-flex text-white bg-gray">
                        <div class="bg-info border-radius-right pl-2">
                            <i class="fa fa-file-pdf-o font11"></i> <small class="font11">{{@$val->name}}</small>
                        </div>
                        @if(@$val->id)
                        <div class="bg-info pl-2">
                            <a href="{{ asset('downloaddocument')}}/{{MD5(@$val->id)}}/{{@$generalDetails->id}}" >
                                <i class="fa fa-download text-white font11" data-toggle="tooltip" title="" data-original-title="Download"></i> 
                            </a>
                        </div>
                        <div class="bg-info border-radius-left pr-2 pl-2">
                            <a href="javascript:void(0);" onclick="deletedoc('{{$val->id}}','{{@$generalDetails->id}}')" >
                                <i class="fa fa-trash text-white font11" aria-hidden="true" title="Delete"></i> 
                            </a>
                        </div>
                        @endif
                    </div>
                    @endforeach
                    @endif
                </div>


                <label>{{trans('formlabel.profilegeneral.auditlogs')}}</label>
                <div class="input-group">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <input name="auditLog[]" id="auditLog" multiple onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept=".doc, .docx, .txt,.pdf">
                        </span>
                        <span class="form-control border-right-0"></span>
                        <span class="btn btn-light border" onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-paperclip fa-1x"></i></span>
                    </div>
                </div>
                <div class="mt-2">
                    @if(@$auditLog)
                    @foreach(@$auditLog as $key=>$val)
                    <div class="d-inline-flex text-white bg-gray">
                        <div class="bg-info border-radius-right pl-2">
                            <i class="fa fa-file-pdf-o font11"></i> <small class="font11">{{@$val->name}}</small>
                        </div>
                        @if(@$val->id)
                        <div class="bg-info pl-2"> 
                            <a href="{{ asset('downloaddocument')}}/{{MD5(@$val->id)}}/{{@$generalDetails->id}}" >
                                <i class="fa fa-download text-white font11" data-toggle="tooltip" title="" data-original-title="Download"></i> 
                            </a>
                        </div>
                        <div class="bg-info border-radius-left pr-2 pl-2">
                            <a href="javascript:void(0);" onclick="deletedoc('{{$val->id}}','{{@$generalDetails->id}}')" >
                                <i class="fa fa-trash text-white font11" aria-hidden="true" title="Delete"></i> 
                            </a>
                        </div>
                        @endif
                    </div>
                    @endforeach
                    @endif
                </div>

                <label>{{trans('formlabel.profilegeneral.appreciationletter')}}</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <input name="appreciationLetter" id="appreciationLetter" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept=".doc, .docx, .txt,.pdf">
                        <input type="hidden" name="appreciationLetter_prev" value="{{@$appreciationLetter[0]->name}}" />
                    </span>
                    <span class="form-control border-right-0"></span>
                    <span class="btn btn-light border" onclick="$(this).parent().find('input[type=file]').click();"><i class="fa fa-paperclip fa-1x"></i></span>
                </div>
                @if(@$appreciationLetter)
                <div class="mt-2">
                    <div class="d-inline-flex text-white bg-gray">
                        <div class="bg-info border-radius-right pl-2"><i class="fa fa-file-pdf-o"></i> <small class="font11">{{@$appreciationLetter[0]->name}}</small></div>
                        @if(@$appreciationLetter[0]->id)
                        <div class="bg-info border-radius-left pr-2 pl-2"> <a href="{{ asset('downloaddocument')}}/{{MD5(@$appreciationLetter[0]->id)}}/{{@$generalDetails->id}}" >
                                <i class="fa fa-download text-white font11" data-toggle="tooltip" title="{{@$appreciationLetter[0]->name}} Download" data-original-title="Download"></i> 
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
                @endif

                <label>{{trans('formlabel.profilegeneral.status')}}</label>
                <div class="input-group">
                    <select class="form-control" name="profile[status]" id="status">
                        <option value="1" <?= (@$generalDetails->status == 1) ? "selected" : ""; ?>>{{trans('formlabel.profilegeneral.statuspublic')}}</option>
                        <option value="3" <?= (@$generalDetails->status == 3) ? "selected" : ""; ?>>{{trans('formlabel.profilegeneral.statusrestricted')}}</option>
                        <option value="2" <?= (@$generalDetails->status == 2) ? "selected" : ""; ?>>{{trans('formlabel.profilegeneral.statusinactive')}}</option>
                    </select>
                </div>
                <div class="mt-3">
                    <!--<button type="button" class="btn btn-dark btn-md pull-right">Save</button>-->
                    <button type="submit" class="btn btn-primary text-center btn-md pull-right">{{trans('formlabel.profilegeneral.generalsubmit')}}</button>
                </div>
            </div>
        </div>
    </form> 
</div>

<script>
                            $('#profile-general').bootstrapValidator({
                    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                    feedbackIcons: {
                    valid: 'fa fa-check',
                            invalid: 'fa fa-remove',
                            validating: 'glyphicon glyphicon-refresh'
                    },
                            fields: {
                            "profile[iso_standards]": {
                            validators: {
                            notEmpty: {
                            message: "This field is required."
                            }
                            }
                            },
                            "profile[registration_date]": {
                            validators: {
                            notEmpty: {
                            message: "This field is required."
                            }
                            }
                            }
                            }
                    }).on('success.form.bv', function (e) {
                    return true;
                    });
                            function deletedoc(id, uid) {
                            var del = confirm('Are you sure to remove this?');
                                    if (del) {

                            var url = "{{url('/deletedocument')}}"; //alert(url)
                                    $.post(url, {'id': id, 'uid': uid, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                    window.location.href = "{{asset('profile')}}";
                                    });
                            } else {
                            return false;
                            }
                            }
</script>