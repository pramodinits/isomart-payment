
<?php
$sicStatus = $sicstatuscolor;
if (count($sicCodeDetails) > 0 &&
        count($sicDocDetails) > 0) {
    $sicStatus = "#5CB85C";
}
?>
<style>
    .sicstatus{
        color: <?= $sicStatus; ?> !important;
    }
    .ui-datepicker {
        z-index: 2 !important;
    }
    a.add_field_button i.fa-plus, 
    a.remove_field i.fa-times
    {
        border: 1px solid;
        border-radius: 10px;
        padding: 2px;
    }
    .contact-address {
        padding: 0 !important;
    }
</style>
<div class="col-md-12 col-sm-12 col-12">
    <form id="profile_siccode" name="profile_siccode" action="{{asset('addIsicCode')}}" method="post" enctype="multipart/form-data"> 
        {!! csrf_field() !!}
        <input type="hidden" name="user_id" id="user_id" value="{{Auth::user()->id}}">
        <input type="hidden" name="id_code" id="id_code" value="">
        <div class="row" id="field">
            <div class="col-md-6 col-sm-6 col-12">
                <div class="input-field form-group" id="codeType">
                    <input type="radio" checked class="p-1" value="2" name="codetype"> NACE&nbsp;&nbsp;
                    <input type="radio" class="p-1" value="1" name="codetype"> ISIC&nbsp;&nbsp;
                    <input type="radio" class="p-1" value="3" name="codetype"> ANZSIC
                </div>
                <label>{{trans('formlabel.profilesiccode.codenumber')}}</label>
                <div class="form-group">
                    <select class="form-control" required name="codeNumber" id="codeNumber" data-bv-notempty-message="Please enter code.">
                        <option value="">{{trans('formlabel.profilesiccode.codenumber')}}</option>
                    </select>
                </div>
                <label>{{trans('formlabel.profilesiccode.applieddate')}}</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="applied_date" id="applied_date" data-bv-notempty-message="Please enter applied date." readonly />
                </div>
                <label>{{trans('formlabel.profilesiccode.approveddate')}}</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="approved_date" id="approved_date" data-bv-notempty-message="Please enter approved date." readonly />
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <label>{{trans('formlabel.profilesiccode.description')}}</label>
                <div class="input-group">
                    <textarea class="form-control" name="code_description" id="description" rows="5" cols="25"></textarea>
                </div>
                <label>{{trans('formlabel.profilesiccode.document')}}</label>
                <div class="input_fields_wrap">
                    <div class="input-group mb-2">
                        <input type="file" name="codeDocument[]" class="form-control" accept=".doc, .docx, .txt,.pdf">
                        <a class="add_field_button" href="#">
                            <i class="fa fa-plus font16 ml-1 text-success" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                
                    <div class="mt-2" id="doc_details">
                        
                    </div>
            </div>
        </div>
        <div class="mt-2 mb-2">
            <button type="submit" class="btn btn-primary text-center btn-md pull-right">{{trans('formlabel.profilesiccode.savesiccode')}}</button>
            <div class="clearfix"></div>
        </div>
    </form>
    <div class="row">
    <div class="col-lg-12 mb40">
                    <table class="table table-bordered table-responsive">
                        <thead class="bg-light">
                            <tr class="text-center">
                                <th>{{trans('formlabel.profilesiccode.rowtype')}}</th>
                                <th>{{trans('formlabel.profilesiccode.rowcode')}}</th>
                                <th>{{trans('formlabel.profilesiccode.rowcodeno')}}</th>
                                <th>{{trans('formlabel.profilesiccode.rowapplieddate')}}</th>
                                <th>{{trans('formlabel.profilesiccode.rowapproveddate')}}</th>
                                <th>{{trans('formlabel.profilesiccode.rowdescription')}}</th>
                                <th>{{trans('formlabel.profilesiccode.rowdocument')}}</th>
                                <th>{{trans('formlabel.profilesiccode.rowaction')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                             @if(@$sicCodeDetails)
        <?php
        $i = 1;
        ?>
        @foreach(@$sicCodeDetails as $key=>$val)
                            
                            <tr>
                                <th scope="row">{{@$val->type}}</th>
                                <td>{{@$val->code}}</td>
                                <td>{{@$val->codeName}}</td>
                                <td>{{Carbon\Carbon::parse(@$val->applied_date)->format(config('assignment.dateformat'))}}</td>
                                <td>{{Carbon\Carbon::parse(@$val->approved_date)->format(config('assignment.dateformat'))}}</td>
                                <td>{{$val->code_description}}</td>
                                <td> @if(@$sicDocDetails)
                @foreach(@$sicDocDetails as $k=>$v)
                @if($val->id == $v->code_id)
                <a href="{{ asset('downloaddocument')}}/{{MD5(@$v->id)}}/{{@$val->user_id}}" >
                    <i class="fa fa-file-pdf-o font16"></i>&nbsp;
                </a>
                @endif
                @endforeach
                @else
                N/A
                @endif</td>
                                 <td> <a href="javascript:void(0);" onclick="getIsicCode('{{$val->id}}')">
                    <i class="fa fa-edit text-success" title="Edit Code"></i>
                </a>&nbsp;&nbsp;
                <a href="javascript:void(0);" onclick="deleteSicCode('{{$val->id}}')">
                    <i class="fa fa-remove text-danger" aria-hidden="true" title="Delete Code"></i>
                </a></td>
                            </tr>
                             <?php $i++; ?>
        @endforeach
        @else
         <div class="d-flex contactdetails text-secondary">
            <div class="col-12 text-center">No data found.</div>
        </div>
        @endif
                         
                         
                        </tbody>
                    </table>
                </div>
    </div>
    
<!--    <div class="contact-address col-md-12">
        <div class="d-flex text-white">
            <div class="p-1 bg-warning col-1 pl-0 border-right"></div>
            <div class="p-1 bg-warning col-1 pl-0 border-right"></div>
            <div class="p-1 bg-warning col-2 border-right"></div>
            <div class="p-1 bg-warning col-2 border-right"></div>
            <div class="p-1 bg-warning col-2 border-right"></div>
            <div class="p-1 bg-warning col-2 border-right"></div>
            <div class="p-1 bg-warning col-1 border-right"></div>
            <div class="p-0 bg-warning col-1 text-center border-right"></div> 
        </div>
       
        <div class="d-flex text-secondary contactdetails">
            <div class="p-1 col-1 pl-0"></div>
            <div class="p-1 col-1 pl-0"></div>
            <div class="p-1 col-2 pl-0"></div>
            <div class="p-1 col-2 pl-0"></div>
            <div class="p-1 col-2 pl-0"></div>
            <div class="p-1 col-2 pl-0"></div>
            <div class="p-1 col-1 pl-0">
               
            </div>
            <div class="p-0 col-1 text-center">
               
            </div>
        </div>
       
       
    </div>-->
</div>

<!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>
<script>
        $(document).ready(function() {
            //trigger click event selected check box
             $('#codeType').find(':radio[name=codetype][value="2"]').prop('checked', true).trigger("click");
            //trigger click event selected check box
          
var max_fields = 5; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
e.preventDefault();
        if (x < max_fields){ //max input box allowed
x++; //text box increment
        $(wrapper).append('<div class="input-group mb-2">\n\
<input type="file" class="form-control" name="codeDocument[]" accept=".doc, .docx, .txt,.pdf"/>\n\
<a href="#" class="remove_field"><i class="fa fa-times font16 text-danger ml-1" aria-hidden="true"></i></a>\n\
</div>'); //add input box
}
});
        $(wrapper).on("click", ".remove_field", function(e){ //user click on remove text
e.preventDefault(); $(this).parent('div').remove(); x--;
})
});
</script>

<!---edit get address-->
<script>
                                    function getIsicCode(id) {
                                    if (id) {
                                    $.ajax({
                                    type: "POST",
                                            url: "{{url('/getIsicCode')}}",
                                            dataType: 'json',
                                            data: {'id': id, '_token': "<?= csrf_token(); ?>"},
                                            cache: false,
                                            success: function (res) {
                                                $('#codeNumber, #applied_date, #approved_date').parents('div.form-group').removeClass('has-feedback has-error');   
                                                $("small.help-block").hide();
                                                $("i.form-control-feedback").hide();
                                            var document = res.document;
                                                    var code = res.code;
                                                    $("#id_code").val(code.id);
                                                    $("#approved_date").val(code.approved_date);
                                                    $("#applied_date").val(code.applied_date);
                                                    $("#description").val(code.code_description);
                                                    if (code.type == "ISIC") {
                                            $('#codeType').find(':radio[name=codetype][value="1"]').prop('checked', true).trigger("click");
                                            } else if (code.type == "NACE") {
                                            $('#codeType').find(':radio[name=codetype][value="2"]').prop('checked', true).trigger("click");
                                            } else {
                                            $('#codeType').find(':radio[name=codetype][value="3"]').prop('checked', true).trigger("click");
                                            }
                                            setTimeout(function(){
                                            $("#codeNumber option[value='" + code.code_sector + "']").attr("selected", "selected");
                                            }, 600);
                                                    if (document.length > 0) {
                                            var output = "";
                                                    for (i = 0; i < document.length; i++){
                                                        output += '<div class="d-inline-flex text-white bg-gray pr-2 pb-2">';
                                    output += '<div class="bg-warning border-radius-right pl-2">';
                                    output += '<i class="fa fa-file-pdf-o font11"></i> <small class="font11">';
                                                    output += document[i].name + "&nbsp;";
                                                   output += '</small>'; 
                                                   output += '</div>';
                                                   output += '<div class="bg-warning border-radius-left pr-2 pl-2">';
                                            output += '<a href="javascript:void(0);" onclick="deletedocIsic(' + document[i].id + ',' + document[i].user_id + ')" >';
                                                    output += '<i class="fa fa-trash text-white font11" aria-hidden="true" title="Delete"></i>';
                                                    output += '</a>';
                                                    output += '</div>';
                                                    output += '</div>';
                                            }
                                            $("#doc_details").html(output);
                                            }
                                            }
                                    });
                                    }
                                    }

                            function deletedocIsic(id, user_id) {
                            var del = confirm('Are you sure to remove this?');
                                    if (del) {

                            var url = "{{url('/deletedocIsic')}}"; //alert(url)
                                    $.post(url, {'id': id, 'user_id': user_id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                    window.location.href = "{{asset('profile')}}";
                                    });
                            } else {
                            return false;
                            }
                            }
</script>
<!---edit get address-->

<script>
                            $(function () {
                            $('#applied_date').datepicker({
                            changeMonth: true,
                                    changeYear: true,
                                    dateFormat: 'dd-mm-yy',
                                    maxDate: -1, //(disable current date)
                                    onSelect: function (selected) {
                                    $("#approved_date").datepicker("option", "minDate", selected)
                                    $('#profile_siccode').bootstrapValidator('revalidateField', 'applied_date');
                                    }
                            });
                                    $('#approved_date').datepicker({
                            changeMonth: true,
                                    changeYear: true,
                                    dateFormat: 'dd-mm-yy',
                                    maxDate: 0,
                                    onSelect: function (selected) {
                                    $('#profile_siccode').bootstrapValidator('revalidateField', 'approved_date');
                                    }
                            });
                            });
                                    $("input[type='radio']").click(function(){
                            var radioValue = $("input[name='codetype']:checked").val();
                                    var url = "{{url('/getStandards')}}";
                                    if (radioValue){
                            $('#codeNumber').html("<option value='' >Select Code</option>");
                                    $.post(url, {"selected_val": radioValue, "_token": '<?= csrf_token(); ?>'}, function (res) {
                                    $("#codeNumber").html(res);
                                            var total = $('select#codeNumber option').length;
                                            $("#codeNumber_cnt").html(" <b>(" + (total - 1) + ")</b>");
                                    });
                            }
                            });
                                    $('#profile_siccode').bootstrapValidator({
                            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                            feedbackIcons: {
                            valid: 'fa fa-check',
                                    invalid: 'fa fa-remove',
                                    validating: 'glyphicon glyphicon-refresh'
                            },
                                    fields: {
                                    "codeNumber": {
                                    validators: {
                                    notEmpty: {
                                    message: "This field is required."
                                    }
                                    }
                                    },
                                            "applied_date": {
                                            validators: {
                                            notEmpty: {
                                            message: "This field is required."
                                            }
                                            }
                                            },
                                            "approved_date": {
                                            validators: {
                                            notEmpty: {
                                            message: "This field is required."
                                            }
                                            }
                                            }
                                    }
                            }).on('success.form.bv', function (e) {
                            $('#custom_loader').hide();
                                    return true;
                            });
                                    function editSicCode(id, type, codeSector, appliedDate, approvedDate, description) {
                                    console.log(codeSector);
                                            $("#id_code").attr('value', id);
                                            if (type == "ISIC") {
                                    $('#codeType').find(':radio[name=codetype][value="1"]').prop('checked', true).trigger("click");
                                    } else if (type == "NACE") {
                                    $('#codeType').find(':radio[name=codetype][value="2"]').prop('checked', true).trigger("click");
                                    } else {
                                    $('#codeType').find(':radio[name=codetype][value="3"]').prop('checked', true).trigger("click");
                                    }
                                    setTimeout(function(){
                                    $("#codeNumber option[value='" + codeSector + "']").attr("selected", "selected");
                                    }, 600);
                                            $("#applied_date").attr('value', appliedDate);
                                            $("#approved_date").attr('value', approvedDate);
                                            $("#description").html(description);
                                    }

                            function deleteSicCode(id) {
                            var del = confirm('Are you sure to remove this?');
                                    if (del) {
                            var url = "{{url('/deleteSicCode')}}";
                                    $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                                    window.location.href = "{{asset('profile')}}";
                                    });
                            } else {
                            return false;
                            }
                            }
</script>