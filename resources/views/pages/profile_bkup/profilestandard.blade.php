<?php
$standardStatus = $standardstatuscolor;
if (count($standardDetails) > 0) {
    $standardStatus = "#5CB85C";
}
?>
<style>
    .standardstatus{
        color: <?= $standardStatus; ?> !important;
    }
</style>

<div class="col-md-12 col-sm-12 col-12">
    <form id="profile_standard" name="profile_standard" action="{{asset('addStandard')}}" method="post"> 
        {!! csrf_field() !!}
        <input type="hidden" name="user_id" id="user_id" value="{{Auth::user()->id}}">
        <input type="hidden" name="id_standard" id="id_standard" value="">
        <div class="row" id="field">
            <div class="col-md-6 col-sm-6 col-12">
                <label>{{trans('formlabel.profilestandards.standardnumber')}}</label>
                <div class="form-group">
                    <input type="text" required class="form-control" id="standardNo" name="standardNo" value="">
                </div>
                <label>{{trans('formlabel.profilestandards.approveddate')}}</label>
                <div class="form-group">
                    <input type="text" required class="form-control" id="approvedDate" name="approvedDate" value="" readonly>
                </div>
                <label>{{trans('formlabel.profilestandards.expirydate')}}</label>
                <div class="form-group">
                    <input type="text" required class="form-control" id="expiryDate" name="expiryDate" value="" readonly>
                </div>
                <label>{{trans('formlabel.profilestandards.reinstateddate')}}</label>
                <div class="form-group">
                    <input type="text" required class="form-control" id="reinstatedDate" name="reinstatedDate" value="" readonly>
                </div>
                <label>{{trans('formlabel.profilestandards.verificationauditdate')}}</label>
                <div class="form-group">
                    <input type="text" required class="form-control" id="verAuditdDate" name="verAuditdDate" value="" readonly>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <label>{{trans('formlabel.profilestandards.removeddate')}}</label>
                <div class="form-group">
                    <input type="text" required class="form-control" id="removedDate" name="removedDate" value="" readonly>
                </div>
                <label>{{trans('formlabel.profilestandards.description')}}</label>
                <div class="input-group">
                    <textarea class="form-control" name="stdDescription" id="stdDescription" rows="5" cols="25"></textarea>
                </div>
                <label>{{trans('formlabel.profilestandards.status')}}</label>
                <div class="input-group">
                    <select class="form-control" name="stdStatus" id="stdStatus">
                        <option value="1">{{trans('formlabel.profilestandards.statusactive')}}</option>
                        <option value="2">{{trans('formlabel.profilestandards.statusinactive')}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="mt-2 mb-2">
            <button type="submit" class="btn btn-primary text-center btn-md pull-right">{{trans('formlabel.profilestandards.savestandards')}}</button>
            <div class="clearfix"></div>
        </div>
    </form>
    <div class="contact-address row">
        <div class="col-12 mb40">
            <div class="table table-responsive">
                <table class="table table-bordered">
                    <thead class="bg-light">
                        <tr class="text-center">
                            <th width="2%">{{trans('formlabel.profilestandards.rowslno')}}</th>
                            <th width="6%">{{trans('formlabel.profilestandards.rowstandardnumber')}}</th>
                            <th width="12%">{{trans('formlabel.profilestandards.rowapproveddate')}}</th>
                            <th width="12%">{{trans('formlabel.profilestandards.rowexpirydate')}}</th>
                            <th width="10%">{{trans('formlabel.profilestandards.rowreinstateddate')}}</th>
                            <th width="10%">{{trans('formlabel.profilestandards.rowauditdate')}}</th>
                            <th width="10%">{{trans('formlabel.profilestandards.rowremoveddate')}}</th>
                            <th width="26%">{{trans('formlabel.profilestandards.rowdescription')}}</th>
                            <th width="4%">{{trans('formlabel.profilestandards.rowstatus')}}</th>
                            <th width="8%">{{trans('formlabel.profilestandards.rowaction')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(@$standardDetails)
                        <?php
                        $i = 1;
                        ?>
                        @foreach(@$standardDetails as $key=>$val)
                        <tr>
                            <th scope="row"><?= $i ?></th>
                            <td>{{$val->standard_no}}</td>
                            <td>{{Carbon\Carbon::parse(@$val->approved_date)->format(config('assignment.dateformat'))}}</td>
                            <td>{{Carbon\Carbon::parse(@$val->expired_date)->format(config('assignment.dateformat'))}}</td>
                            <th scope="row">{{Carbon\Carbon::parse(@$val->reinstated_date)->format(config('assignment.dateformat'))}}</th>
                            <td>{{Carbon\Carbon::parse(@$val->verification_audit_date)->format(config('assignment.dateformat'))}}</td>
                            <td>{{Carbon\Carbon::parse(@$val->removed_date)->format(config('assignment.dateformat'))}}</td>
                            <td>{{@$val->standard_description}}</td>
                            <td> @if($val->standard_status == 1)
                                <i class="fa fa-check text-success p-1" title="Active"></i>
                                @else
                                <i class="fa fa-remove text-danger p-1" title="In-Active"></i>
                                @endif</td>
                            <td><a href="javascript:void(0);" onclick="editStandard('{{$val->id}}','{{$val->standard_no}}','{{$val->approved_date}}','{{$val->expired_date}}','{{$val->reinstated_date}}','{{$val->verification_audit_date}}','{{$val->removed_date}}','{{$val->standard_description}}','{{$val->standard_status}}')">
                                    <i class="fa fa-edit p-1" title="Edit Standard"></i>
                                </a>
                                <a href="javascript:void(0);" onclick="deleteStandard('{{$val->id}}')">
                                    <i class="fa fa-remove text-danger p-1" aria-hidden="true" title="Delete Standard"></i>
                                </a></td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                        @else
                    <div class="d-flex contactdetails text-secondary">
                        <div class="col-12 text-center">No data found.</div>
                    </div>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>


        <!--        <div class="d-flex text-white">  
                    <div class="p-2 bg-warning col-1 pl-0 border-right"></div>
                    <div class="p-2 bg-warning col-1 border-right"></div>
                    <div class="p-2 bg-warning col-1 border-right"></div>
                    <div class="p-2 bg-warning col-1 border-right"></div>
                    <div class="p-2 bg-warning col-1 border-right"></div>
                    <div class="p-2 bg-warning col-2 border-right"></div>
                    <div class="p-2 bg-warning col-1 border-right"></div>
                    <div class="p-2 bg-warning col-2 border-right"></div>
                    <div class="p-2 bg-warning col-1 border-right"></div>
                    <div class="p-2 bg-warning col-1 text-center border-right"></div> 
                </div>-->

<!--        <div class="d-flex text-secondary contactdetails">
            <div class="p-2 col-1 pl-0"></div>
            <div class="p-2 col-1 pl-0"></div>
            <div class="p-2 col-1 pl-0"></div>
            <div class="p-2 col-1 pl-0"></div>
            <div class="p-2 col-1 pl-0"></div>
            <div class="p-2 col-2 pl-0"></div>
            <div class="p-2 col-1 pl-0"></div>
            <div class="p-2 col-2 pl-0"></div>
            <div class="p-2 col-1 pl-0">

            </div>
            <div class="p-2 col-1 pl-0">

            </div>
        </div>-->

    </div>
</div>

<!--datepicker-->
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>
<script>
                                    $(function () {
                                    $('#approvedDate').datepicker({
                                    changeMonth: true,
                                            changeYear: true,
                                            dateFormat: 'dd-mm-yy',
                                            maxDate: - 1, //(disable current date)
                                            onSelect: function (selected) {
                                            $('#profile_standard').bootstrapValidator('revalidateField', 'approvedDate');
                                            }
                                    });
                                            $('#expiryDate').datepicker({
                                    changeMonth: true,
                                            changeYear: true,
                                            dateFormat: 'dd-mm-yy',
                                            minDate: 0,
                                            onSelect: function (selected) {
                                            $('#profile_standard').bootstrapValidator('revalidateField', 'expiryDate');
                                            }
                                    });
                                            $('#reinstatedDate').datepicker({
                                    changeMonth: true,
                                            changeYear: true,
                                            dateFormat: 'dd-mm-yy',
                                            minDate: 0,
                                            onSelect: function (selected) {
                                            $('#profile_standard').bootstrapValidator('revalidateField', 'reinstatedDate');
                                            }
                                    });
                                            $('#verAuditdDate').datepicker({
                                    changeMonth: true,
                                            changeYear: true,
                                            dateFormat: 'dd-mm-yy',
                                            minDate: 0,
                                            onSelect: function (selected) {
                                            $('#profile_standard').bootstrapValidator('revalidateField', 'verAuditdDate');
                                            }
                                    });
                                            $('#removedDate').datepicker({
                                    changeMonth: true,
                                            changeYear: true,
                                            dateFormat: 'dd-mm-yy',
                                            minDate: 0,
                                            onSelect: function (selected) {
                                            $('#profile_standard').bootstrapValidator('revalidateField', 'removedDate');
                                            }
                                    });
                                    });</script>
<!--datepicker-->

<script>
                    $('#profile_standard').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            feedbackIcons: {
            valid: 'fa fa-check',
                    invalid: 'fa fa-remove',
                    validating: 'glyphicon glyphicon-refresh'
            },
                    fields: {
                    "standardNo": {
                    validators: {
                    notEmpty: {
                    message: "This field is required."
                    }
                    }
                    }
                    }
            }).on('success.form.bv', function (e) {
            $('#custom_loader').hide();
                    return true;
            });
                    function editStandard(id, standardNo, approvedDate, expiryDate, reinstatedDate, verAuditdDate, removedDate, stdDescription, stdStatus) {
                    $('#standardNo, #approvedDate, #expiryDate, #reinstatedDate, #verAuditdDate, #removedDate').parents('div.form-group').removeClass('has-feedback has-error');
                            $("small.help-block").hide();
                            $("i.form-control-feedback").hide();
                            $("#id_standard").attr('value', id);
                            $("#standardNo").attr('value', standardNo);
                            $("#approvedDate").attr('value', approvedDate);
                            $("#expiryDate").attr('value', expiryDate);
                            $("#reinstatedDate").attr('value', reinstatedDate);
                            $("#verAuditdDate").attr('value', verAuditdDate);
                            $("#removedDate").attr('value', removedDate);
                            $("#stdDescription").html(stdDescription);
                            $("#stdStatus").val(stdStatus);
                    }

            function deleteStandard(id) {
            var del = confirm('Are you sure to remove this?');
                    if (del) {
            var url = "{{url('/deleteStandard')}}";
                    $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                    window.location.href = "{{asset('profile')}}";
                    });
            } else {
            return false;
            }
            }
</script>