@include('includes.header')
<style>
    i.form-control-feedback {
        display: none !important;
    }
</style>
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="clearfix"></div>
            <div class="container">
                <div class="main-content">

                    <div class="">
                        <!-- Tab panes -->
                        <div class="tab-content bg-light">
                            <section id="section-3" class="content-current">
                                <div class="card p-3">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <form id="addcpdhours" name="addcpdhours" action="{{asset('addcpdHours')}}" method="post">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="id_cpd" id="id_cpd" value="">
                                            <div class="row" id="field">
                                                <div class="col-md-6 col-sm-6 col-12">
                                                    <label>{{trans('formlabel.cpdhours.title')}}</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="cpd_title" name="cpd_title" value="">
                                                    </div>
                                                    <label>{{trans('formlabel.cpdhours.description')}}</label>
                                                    <div class="form-group">
                                                        <textarea class="form-control" name="cpd_description" id="cpd_description" rows="4" cols="20"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-12">
                                                    <label>{{trans('formlabel.cpdhours.date')}}</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="cpd_date" id="cpd_date" readonly />
                                                    </div>
                                                    <label>{{trans('formlabel.cpdhours.links')}}</label>
                                                    <div class="input_fields_wrap">
                                                        <div class="input-group mb-2" id="firstInput">
                                                            <input type="text" name="cpdlinks[]" id="cpdlinks" class="form-control">
                                                            <a class="add_field_button" href="#">
                                                                <i class="fa fa-plus font16 ml-1 text-success" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-2 mb-2">
                                                <button type="submit" class="btn btn-primary text-center btn-md pull-right">{{trans('formlabel.assignjobdetails.trackformsave')}}</button>
                                                <div class="clearfix"></div>
                                            </div>
                                        </form>
                                    </div>
                                    <hr>
                                    <!--job track form-->
                                    <div class="contact-address col-md-12 col-sm-12 col-12">
                                        <div class="d-flex text-white">
                                            <div class="p-2 bg-warning col-3 border-right">{{trans('formlabel.cpdhours.title')}}</div>
                                            <div class="p-2 bg-warning col-3 border-right">{{trans('formlabel.cpdhours.description')}}</div>
                                            <div class="p-2 bg-warning col-2 border-right">{{trans('formlabel.cpdhours.date')}}</div>
                                            <div class="p-2 bg-warning col-3 border-right">{{trans('formlabel.cpdhours.links')}}</div>
                                            <div class="p-2 bg-warning col-1 border-right">{{trans('formlabel.cpdhours.action')}}</div>
                                        </div>
                                        @if(count(@$getcpddetails) > 0)
                                        @foreach(@$getcpddetails as $k => $v) 
                                        <div class="d-flex text-secondary contactdetails">
                                            <div class="p-2 col-3">{{$v->cpd_title}}</div>
                                            <div class="p-2 col-3">{{$v->cpd_description}}</div>
                                            <div class="p-2 col-2">{{Carbon\Carbon::parse($v->cpd_date)->format(config('assignment.dateformat'))}}</div>
                                            <div class="p-2 col-3">
                                                @if($v->cpd_links)
                                                @php
                                                $links = explode(',', $v->cpd_links);
                                                $i = 1;
                                                @endphp
                                                @foreach ($links as $key => $val)
                                                <a href="{{$val}}" target="_blank">{{$val}}</a>
                                                @if($i != count($links)),&nbsp; @endif
                                                @php $i++; @endphp
                                                @endforeach
                                                @else
                                                N/A
                                                @endif
                                            </div>
                                            <div class="p-2 col-1 text-center">
                                                <a href="javascript:void(0);" onclick="getCPD('{{$v->id}}')">
                                                    <i class="fa fa-edit text-success" title="Edit CPD Hour"></i>
                                                </a>&nbsp;&nbsp;
                                                <a href="javascript:void(0);" onclick="deleteCPD('{{$v->id}}')">
                                                    <i class="fa fa-remove text-danger" aria-hidden="true" title="Delete CPD Hour"></i>
                                                </a>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="col-md-12 mt-2">
                                            <div class="text-secondary text-center" style="text-align: center;">
                                                No information found.
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    <!--/map--> 
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('/')}}js/jquery-ui.js"></script>
<script>
                                                                                $(document).ready(function() {
                                                                        var max_fields = 3; //maximum input boxes allowed
                                                                                var wrapper = $(".input_fields_wrap"); //Fields wrapper
                                                                                var add_button = $(".add_field_button"); //Add button ID
                                                                                var x = 1; //initlal text box count
                                                                                $(add_button).click(function(e){ //on add input button click
                                                                        e.preventDefault();
                                                                                if (x < max_fields){ //max input box allowed
                                                                        x++; //text box increment
                                                                                $(wrapper).append('<div class="input-group mb-2">\n\
            <input type="text" class="form-control" name="cpdlinks[]" id="cpdlinks"/>\n\
            <a href="#" class="remove_field"><i class="fa fa-times font16 text-danger ml-1" aria-hidden="true"></i></a>\n\
            </div>'); //add input box
                                                                        }
                                                                        });
                                                                                $(wrapper).on("click", ".remove_field", function(e){ //user click on remove text
                                                                        e.preventDefault(); $(this).parent('div').remove(); x--;
                                                                        })
                                                                                });
                                                                                $(function () {
                                                                                $('#cpd_date').datepicker({
                                                                                changeMonth: true,
                                                                                        changeYear: true,
                                                                                        dateFormat: 'dd-mm-yy',
                                                                                        maxDate: 0,
                                                                                        onSelect: function (selected) {
                                                                                        $('#addcpdhours').bootstrapValidator('revalidateField', 'cpd_date');
                                                                                        }
                                                                                });
                                                                                        });
                                                                                $('#addcpdhours').bootstrapValidator({
// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                                                                        feedbackIcons: {
                                                                        valid: 'fa fa-check',
                                                                                invalid: 'fa fa-remove',
                                                                                validating: 'glyphicon glyphicon-refresh'
                                                                                },
                                                                                fields: {
                                                                                "cpd_title": {
                                                                                validators: {
                                                                                notEmpty: {
                                                                                message: "This field is required."
                                                                                }
                                                                                }
                                                                                },
                                                                                        "cpd_description": {
                                                                                        validators: {
                                                                                        notEmpty: {
                                                                                        message: "This field is required."
                                                                                        },
                                                                                        stringLength: {
                                                                                        max: 250,
                                                                                        message: 'Maximum length is 250 characters.'
                                                                                    }
                                                                                        }
                                                                                        },
                                                                                        "cpd_date": {
                                                                                        validators: {
                                                                                        notEmpty: {
                                                                                        message: "This field is required."
                                                                                        }
                                                                                        }
                                                                                        }
                                                                                }
                                                                        }).on('success.form.bv', function (e) {
                                                                        $('#custom_loader').hide();
                                                                                return true;
                                                                                });</script>    
<!---edit get cpd hours-->
<script>
                    function getCPD(id) {
                    if (id) {
                    $.ajax({
                    type: "POST",
                            url: "{{url('/getCPD')}}",
                            dataType: 'json',
                            data: {'id': id, '_token': "<?= csrf_token(); ?>"},
                            cache: false,
                            success: function (res) {
                            $('#cpd_title, #cpd_description, #cpd_date').parents('div.form-group').removeClass('has-feedback has-error');   
                            $("small.help-block").hide();
                                var output = "";
                            var cpdDetails = res.cpdDetails;
                            var cpdLinks = cpdDetails.cpd_links;
                            var spiltLinks = cpdLinks.split(',');
                            $('.input_fields_wrap div:not(#firstInput)').remove();
                            $("#cpdlinks").val(spiltLinks[0]);
                            if (spiltLinks.length > 0) {
                            for (i = 1; i <= (spiltLinks.length)-1; i++){
                               output += '<div class="input-group mb-2">\n\
                                <input type="text" class="form-control" name="cpdlinks[]" id="cpdlinksedit" value="'+spiltLinks[i]+'"/>\n\
                                <a href="#" class="remove_field"><i class="fa fa-times font16 text-danger ml-1" aria-hidden="true"></i></a>\n\
                                </div>'; //add input box                 
                            }
                            }
                            $('.input_fields_wrap').append(output);
                                    $("#id_cpd").val(cpdDetails.id);
                                    $("#cpd_title").val(cpdDetails.cpd_title);
                                    $("#cpd_description").val(cpdDetails.cpd_description);
                                    $("#cpd_date").val(cpdDetails.cpd_date);
                            }
                    });
                    }
                    }

            function deletedocCPD(id, user_id) {
            var del = confirm('Are you sure to remove this?');
                    if (del) {

            var url = "{{url('/deletedocIsic')}}"; //alert(url)
                    $.post(url, {'id': id, 'user_id': user_id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                    window.location.href = "{{asset('profile')}}";
                    });
            } else {
            return false;
            }
            }
            function deleteCPD(id) {
            var del = confirm('Are you sure to remove this?');
                    if (del) {
            var url = "{{url('/deleteCPD')}}";
                    $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                    window.location.href = "{{asset('cpdHours')}}";
                    });
            } else {
            return false;
            }
            }
</script>
<!---edit get cpd hours-->
@include('includes.footer')


