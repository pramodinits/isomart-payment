@include('includes.header')
<br>
<div class="clearfix"></div>
<div class="container pt30  mt-5">
    <h3 class="text-left text-secondary font-weight-normal p-4">
        {{trans('formlabel.jobinfo.jobheading')}}
    </h3>
</div>
<div class="container">
    <div class="card">
        <div class="row">
            <div class="col-lg-12 mb40">
                <div id="tabAssign">
                    <!---nav tabs-->
                    <ul class="nav nav-tabs tabs-default justify-content-left mb30 " role="tablist">
                         @if(@Auth::user()->usertype == 6)
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active show" href="#profile" aria-controls="profile" role="tab" data-toggle="tab" aria-selected="true">
                                {{trans('formlabel.jobinfo.tabcreatejob')}}</a>
                        </li>
                        @elseif(@Auth::user()->usertype == 8)
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" href="{{asset('assignedjobDetails')}}">
                                {{trans('formlabel.jobinfo.tabassignjob')}}</a>
                        </li>
                        @elseif(@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1)
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active show" href="#jobtraining" aria-controls="jobtraining" role="tab" data-toggle="tab" aria-selected="true">
                                {{trans('formlabel.jobinfo.tabcreatejob')}}</a>
                        </li>
                        @endif
                    </ul>
                    <!---nav tabs-->
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @if(@Auth::user()->usertype == 6)
                        <div role="tabpanel" class="tab-pane fade active show" id="profile">
                            <div class="container">
                                <div class="row">
                                    @if(count($jobslist) > 0)
                                    @foreach(@$jobslist as $k=>$v)
                                    <div class="col-md-4 mb30">
                                        <div class="card card-body text-center">
                                            <h4 class="card-title">
                                                <a href="{{asset('myjobDetails/')}}/{{$v->assignment_id}}">
                                                @if(strlen($v->assignment_name) > 20)
                                                {{substr(ucfirst($v->assignment_name), 0, 20) . "..."}}
                                                @else
                                                {{ucfirst($v->assignment_name)}}
                                                @endif
                                                </a>
                                            </h4>
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="text-center">
                                                    <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $v->budget_usd}}</span>&nbsp;
                                                    {{" ( " . trans('formlabel.assignmentinfoaudit.efforts') .  $v->effort_tentative . config('assignment.effortdays') . ")" }}
                                                    @if($v->close_job == 1)
                                                    <span class="text-success">{{trans('formlabel.jobinfo.closejob')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="col-md-3"></div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-md-3"></div>
                                            </div>
                                            <hr/>
                                            <p class="card-text text-left">
                                                {{trans('formlabel.jobinfo.refid')}} {{config('assignment.refid') . $v->ref_id}}<br>
                                                {{trans('formlabel.jobinfo.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.jobinfo.enddate')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.jobinfo.acceptedbidcount')}} {{$v->jobAcceptedBidCount}}<br>
                                                {{trans('formlabel.jobinfo.tataljobbudget')}} {{config('assignment.price') . " ". $v->jobBudget}}<br>
                                            </p>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else 
                                    <div class="col-md-12">
                                        <div class="text-secondary text-center" style="text-align: center;">
                                            No job found.
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @elseif(@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1)
                        <div role="tabpanel" class="tab-pane fade active show" id="jobtraining">
                            <div class="container">
                                <div class="row">
                                    @if(count($jobslist) > 0)
                                    @foreach(@$jobslist as $k=>$v)
                                    <div class="col-md-4 mb30">
                                        <div class="card card-body text-center">
                                            <h4 class="card-title">
                                                <a href="{{asset('myjobDetails/')}}/{{$v->assignment_id}}">
                                                {{ucfirst($v->batch_no)}}
                                                </a>
                                            </h4>
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="text-center">
                                                    <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $v->fee_range}}</span>&nbsp;
                                                    {{" ( " . trans('formlabel.assignmentinfoaudit.efforts') .  $v->effort_tentative . config('assignment.effortdays') . ")" }}
                                                    @if($v->close_job == 1)
                                                    <span class="text-success">{{trans('formlabel.jobinfo.closejob')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="col-md-3"></div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-md-3"></div>
                                            </div>
                                            <hr/>
                                            <p class="card-text text-left">
                                                {{trans('formlabel.jobinfo.refid')}} {{config('assignment.refid') . $v->ref_id}}<br>
                                                {{trans('formlabel.jobinfo.startdate')}} {{Carbon\Carbon::parse(@$v->start_date)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.jobinfo.enddate')}} {{Carbon\Carbon::parse(@$v->end_date)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.jobinfo.acceptedbidcount')}} {{$v->jobAcceptedBidCount}}<br>
                                                {{trans('formlabel.jobinfo.tataljobbudget')}} {{config('assignment.price') . " ". $v->jobBudget}}<br>
                                            </p>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else 
                                    <div class="col-md-12">
                                        <div class="text-secondary text-center" style="text-align: center;">
                                            No job found.
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div>
                    <nav aria-label="Page navigation example pull-right" class="mb20">
                        {{$jobslist->links()}}
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.footer')