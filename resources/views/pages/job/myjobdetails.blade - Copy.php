@include('includes.header')
<style>
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        padding: 2px 5px;
        border-radius: 5px;
    }
</style>
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">

            <div class="clearfix"></div>
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="">
                                <h4>{{config('assignment.refid') . @$assignmentdetail->ref_id}}
                                    <i style="font-size: 1rem;" class="p-1 {{@$assignmentdetail->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                    <i style="font-size: 1.2rem;" class="p-1 {{@$assignmentdetail->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                    <i style="font-size: 1.2rem;" class="p-1 {{@$assignmentdetail->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                </h4>
                                <div><h5>{{ucfirst(@$assignmentdetail->assignment_name)}}</h5>
                                    {{trans('formlabel.jobinfo.startdate')}} {{Carbon\Carbon::parse(@$assignmentdetail->start_date)->format(config('assignment.dateformat'))}}<br>
                                    {{trans('formlabel.jobinfo.enddate')}} {{Carbon\Carbon::parse(@$assignmentdetail->end_date)->format(config('assignment.dateformat'))}}</div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="text-right">
                                <h3>{{config('assignment.price') . " ". @$assignmentdetail->budget_usd}}</h3>
                                <h5>{{@$assignmentdetail->effort_tentative. config('assignment.effortdays')}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @foreach(@$locationname as $key=>$val)
                            <span class="locationCity">
                                <i class="fa fa-map-marker pr-1 text-danger" aria-hidden="true"></i>
                                {{$val->city_name . ", " . $val->stateName . ", " . $val->countryName . " (Effort- " . $val->effort . " days)"}}
                            </span>&nbsp;
                            @endforeach
                        </div>
                    </div>
                    <hr>
                    <div class=""> 
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-default justify-content-left mb30 " role="tablist">
                            <li class="nav-item" role="presentation"> <a class="nav-link active show" href="#auditor" aria-controls="home" role="tab" data-toggle="tab" aria-selected="true">{{trans('formlabel.myjobdetails.auditortab')}}</a></li>
                            <li class="nav-item" role="presentation"> <a class="nav-link" href="#jobTracking" aria-controls="home" role="tab" data-toggle="tab" aria-selected="true">{{trans('formlabel.myjobdetails.tracktab')}}</a></li>
                            <li class="nav-item" role="presentation"> <a class="nav-link" href="#rate" aria-controls="home" role="tab" data-toggle="tab" aria-selected="true">{{trans('formlabel.myjobdetails.ratingtab')}}</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content bg-light">
                            <div role="tabpanel" class="tab-pane fade active show" id="auditor">
                                <section id="section-3" class="content-current">
                                    <div class="card p-3">
                                        @foreach(@$auditorlists as $k=>$v)
                                        <div class="taborder2">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <h4>
                                                        <a href="{{ asset('publicProfileView/'.@$v->userId)}}">
                                                            {{$v->fname . " " . $v->lname}}
                                                        </a>
                                                    </h4>
                                                    <p>{{trans('formlabel.myjobdetails.auditorlocation')}}
                                                        <?php
                                                        $locationArray = explode(',', @$v->assignment_location_id);
                                                        ?>
                                                        @foreach(@$locationname as $key=>$val)
                                                        @if((in_array($val->id, $locationArray)))
                                                        <span class="locationCity">
                                                            <i class="fa fa-map-marker pr-1 text-danger" aria-hidden="true"></i>
                                                            {{$val->city_name . ", " . $val->stateName . ", " . $val->countryName . " (Effort-" . $val->effort . " days)"}}
                                                        </span>
                                                        @endif
                                                        @endforeach
                                                    </p>
                                                    <p>{{trans('formlabel.myjobdetails.auditorbudget')}} {{config('assignment.price') . " ". $v->estimated_budget}}</p>
                                                </div>
                                                <div class="col-md-4 text-right">
                                                    <br>
                                                    <div class="btn-group">
                                                        <a href="{{ url("/sendMessage/".@$v->user_id."/".@$v->id."/".@$assignmentdetail->ref_id) }}">
                                                            <button type="button" class="btn btn-orange btn-sm btn-xs mb3 pull-right"><i class="fa fa-envelope"></i> {{trans('formlabel.myjobdetails.message')}}</button>
                                                        </a>&nbsp;
                                                        @if(count(@$ownsendrating[@$v->user_id]) > 0)
                                                        <span class="btn btn-orange btn-sm btn-xs mb3 pull-right" data-toggle="modal" data-target="#viewratingModal{{@$v->user_id}}" style="cursor: pointer;"
                                                              data-userid="{{@$v->user_id}}" 
                                                              data-bidid="{{@$v->id}}" 
                                                              data-referenceid="{{@$assignmentdetail->ref_id}}" 
                                                              data-assignment="{{@$assignmentdetail->assignment_id}}" 
                                                              onclick="replyQuote(this)"><i class="fa fa-star"></i> {{trans('messagelabel.view')}}</span>
                                                        @else
                                                        <span class="btn btn-orange btn-sm btn-xs mb3 pull-right" data-toggle="modal" data-target="#ratingModal" style="cursor: pointer;"
                                                              data-userid="{{@$v->user_id}}" 
                                                              data-bidid="{{@$v->id}}" 
                                                              data-referenceid="{{@$assignmentdetail->ref_id}}" 
                                                              data-assignment="{{@$assignmentdetail->assignment_id}}" 
                                                              onclick="replyQuote(this)"><i class="fa fa-star"></i> {{trans('messagelabel.rating')}}</span>
                                                        @endif
                                                        &nbsp;
                                                        @if(@$v->endjob_status == 0)
                                                        <a data-toggle="modal" data-target="#proposalModal" data-bidid="{{@$v->id}}" 
                                                           data-auditorid="{{@$v->user_id}}" 
                                                           data-assignmentid="{{@$v->assignment_id}}"
                                                           data-assignmentrefid="{{@$assignmentdetail->ref_id}}"
                                                           data-auditorfname="{{@$v->fname}}"
                                                           data-auditorlname="{{@$v->lname}}"
                                                           data-auditoremail="{{@$v->email}}"
                                                           onclick="endJob(this)" style="cursor: pointer;">
                                                            <button type="button" class="btn btn-orange btn-sm btn-xs">
                                                                {{trans('formlabel.myjobdetails.endjob')}}
                                                            </button>
                                                        </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="modal fade queryModal" id="viewratingModal{{@$v->user_id}}" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-center">{{trans('messagelabel.ratingreview')}}</h5>
                                                        <div id="successmsg"></div>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <div id="hidebody">

                                                        <div class="modal-body">
                                                            <div class="row">
                                                                @foreach(@$ownsendrating[@$v->user_id] as $key => $val) 
                                                                <div class="col-md-12 profile-info">
                                                                    <div class="card p-3 mb10">


                                                                        <div class="p-skill">
                                                                            <p><strong><i class="fa fa-comments text-secondary"></i> : </strong> {{@$val->message}}</p>
                                                                            <hr>
                                                                            @foreach($rate[$key]['RatingAnswer'] as $k => $v)

                                                                            <div class="row">
                                                                                <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                                                                    <p><span class="mr-4"><i class="fa fa-star text-danger" style="font-size:10px;"></i></span> {{@$queryname[$v->query_id]}}</p>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                                    <p class="text-danger" >{{@$v->answer}} <span style="color:#ccc;">out of 5</span></p>
                                                                                </div>
                                                                            </div>
                                                                            <div style="clear:both;"></div>

                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                    <!--/map--> 

                                                                </div>
                                                                <div class="clearfix"></div>
                                                                @endforeach
                                                            </div>

                                                        </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @endforeach
                                    </div>
                                </section>

                                <div class="modal fade queryModal" id="ratingModal" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title text-center">{{trans('messagelabel.givefeedback')}}</h5>
                                                <div id="successmsg"></div>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div id="hidebody">
                                                <form name="msgform" id="msgform" method="post" action="{{url('/insertRating')}}" onsubmit="return checkvalidate()">

                                                    <div class="modal-body">
                                                        <div class="row">
                                                            {!! csrf_field() !!}
                                                            <input type="hidden" name="user_id" id="user_id" value="">
                                                            <input type="hidden" name="bid_id" id="bid_id" value="">
                                                            <input type="hidden" name="reference_id" id="reference_id" value="">
                                                            <input type="hidden" name="assignment_id" id="assignment_id" value="">
                                                            <input type="hidden" name="type" id="type" value="1">
                                                            @foreach(@$querylists as $key=>$val)
                                                            <div class="col-md-12 border-right">
                                                                <label class="mt-2">{{@$val->query}}</label>
                                                                <input type="hidden" name="query_id[{{@$val->id}}]" class="queryid" value="{{@$val->id}}">
                                                                <div class="form-group">
                                                                    <div class="d-flex">
                                                                        <label class="container-radio pr-4">
                                                                            <input type="radio" value="1" name="answer[{{@$val->id}}]" class="answer" >1
                                                                            <span class="checkmark"></span> 
                                                                        </label>
                                                                        <label class="container-radio pr-4">
                                                                            <input type="radio" value="2" name="answer[{{@$val->id}}]" class="answer" >2
                                                                            <span class="checkmark"></span> 
                                                                        </label>
                                                                        <label class="container-radio pr-4">
                                                                            <input type="radio" value="3" name="answer[{{@$val->id}}]" class="answer" >3
                                                                            <span class="checkmark"></span> 
                                                                        </label>
                                                                        <label class="container-radio pr-4">
                                                                            <input type="radio" value="4" name="answer[{{@$val->id}}]" class="answer" >4
                                                                            <span class="checkmark"></span> 
                                                                        </label>
                                                                        <label class="container-radio pr-4">
                                                                            <input type="radio" value="5" name="answer[{{@$val->id}}]" class="answer" >5
                                                                            <span class="checkmark"></span> 
                                                                        </label>
                                                                        <small class="help-block"></small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                            <div class="col-md-12 border-right">
                                                                <label class="mt-2">{{trans('messagelabel.message')}}</label>
                                                                <div class="form-group">
                                                                    <textarea class="form-control" name="message" id="message" rows="2" cols="25"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 border-right">
                                                                <button  class="btn btn-primary text-center btn-md pull-right mt-2" type="submit">Send</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="jobTracking">
                                <section id="section-3" class="content-current">
                                    <div class="card p-3"> 
                                        <!---728x90--->
                                        <div class="col-md-12 profile-info">
                                            <div class="p-skill">
                                                @if(count(@$jobtracklists) > 0)
                                                <ul class="timeline">
                                                    @foreach(@$jobtracklists as $k => $v) 
                                                    <li>
                                                        <div class="timeline-badge info"><i class="fa fa-check-circle-o"></i></div>
                                                        <div class="timeline-panel2">
                                                            <div class="timeline-body">
                                                                <h5>{{$v->fname . " " . $v->lname}}</h5>
                                                                <p class="time">{{$v->message}}</p>
                                                                <p>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}
                                                                    @if(@$v->document)
                                                                    <a href="{{ asset('downloadDocumentJob')}}/{{@$v->assignment_id}}/{{@$v->document}}">
                                                                        <i class="fa fa-paperclip"></i>
                                                                    </a>
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No job track found.
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <!--/map--> 

                                    </div>
                                </section>
                            </div>

                            <!--rating tab-->
                            <div role="tabpanel" class="tab-pane fade " id="rate">
                                <section id="section-3" class="content-current">

                                    <div class="row">
                                        @if(count(@$rate) > 0)
                                        @foreach($rate as $key => $val) 

                                        <div class="col-md-6">
                                            <div class="card p-3 mb10">

                                                <div class="col-md-12 profile-info">
                                                    <div class="p-skill">
                                                        <h5>{{@$fname[$val->sender_id]}} {{@$lname[$val->sender_id]}}</h5>
                                                        <p><strong><i class="fa fa-comments text-secondary"></i> : </strong> {{@$val->message}}</p>
                                                        <hr>
                                                        @foreach($rate[$key]['RatingAnswer'] as $k => $v)

                                                        <div class="row">
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                                                <p><span class="mr-4"><i class="fa fa-star text-danger" style="font-size:10px;"></i></span> {{@$queryname[$v->query_id]}}</p>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <p class="text-danger">{{@$v->answer}}<span style="color:#ccc;"> out of 5</span></p>
                                                            </div>
                                                        </div>
                                                        <div style="clear:both;"></div>

                                                        @endforeach
                                                    </div>
                                                </div>
                                                <!--/map--> 

                                            </div>
                                        </div>

                                        @endforeach

                                        @else
                                        <div class="col-md-12 card p-3">
                                            <div class="text-secondary text-center" style="text-align: center;">
                                                No data found.
                                            </div>
                                        </div>
                                        @endif
                                    </div>

                                </section>
                            </div>
                            <!--rating tab-->

                            <!---end job comment-->
                            <div class="modal fade" id="proposalModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title text-center">{{trans('formlabel.myjobdetails.endjobcommentheading')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="endJobReview" name="endJobReview" action="{{ asset('endJobReview')}}" method="post">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="proposal_id" id="proposal_id" value="">
                                                <input type="hidden" name="auditor_id" id="auditor_id" value="">
                                                <input type="hidden" name="assignment_id" id="assignment_id" value="">
                                                <input type="hidden" name="assignment_refid" id="assignment_refid" value="">
                                                <input type="hidden" name="auditor_fname" id="auditor_fname" value="">
                                                <input type="hidden" name="auditor_lname" id="auditor_lname" value="">
                                                <input type="hidden" name="auditor_email" id="auditor_email" value="">
                                                <label>{{trans('formlabel.myjobdetails.endjobcomment')}}</label>
                                                <div class="form-group">
                                                    <textarea class="form-control" name="job_comment" id="job_comment" rows="5" cols="15" required></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-primary text-center">{{trans('formlabel.myjobdetails.submitcomment')}}</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!---end job comment-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function endJob(e) {
        var bidId = $(e).attr('data-bidid');
        var auditorId = $(e).attr('data-auditorid');
        var assignmentId = $(e).attr('data-assignmentid');
        var assignmentRefId = $(e).attr('data-assignmentrefid');
        var auditorFname = $(e).attr('data-auditorfname');
        var auditorLname = $(e).attr('data-auditorlname');
        var auditorEmail = $(e).attr('data-auditoremail');

        $("#proposal_id").val(bidId);
        $("#auditor_id").val(auditorId);
        $("#assignment_id").val(assignmentId);
        $("#assignment_refid").val(assignmentRefId);
        $("#auditor_fname").val(auditorFname);
        $("#auditor_lname").val(auditorLname);
        $("#auditor_email").val(auditorEmail);
    }

    $('#endJobReview').bootstrapValidator({
        fields: {
            "job_comment": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            }
        }
    })
            .on('success.form.bv', function (e) {
                return true;
            });
</script>
<script>
function replyQuote(e) {
        var assignment = $(e).attr('data-assignment');
        var referenceid = $(e).attr('data-referenceid');
        var user_id = $(e).attr('data-userid');
        var bidid = $(e).attr('data-bidid');
        $('#assignment_id').val(assignment);
        $('#reference_id').val(referenceid);
        $('#user_id').val(user_id);
        $('#bid_id').val(bidid);
        }
</script>
<script>
    function checkvalidate()
    {
        var chx = document.getElementsByTagName('input');
            for (var i=0; i<chx.length; i++) {
              if (chx[i].type == 'radio' && chx[i].checked) {
                  okay=true;
                break;
              } else {
                  okay=false;
              }
            }
        if(!okay)
            {
                alert("Atleast choose one query.");
                return false;
            }
    }
</script>
@include('includes.footer')
