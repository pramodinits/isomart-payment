@include('includes.header')
<br>
<div class="clearfix"></div>
<div class="container pt30  mt-5">
    <h3 class="text-left text-secondary font-weight-normal p-4">
        {{trans('formlabel.jobinfo.jobheading')}}
    </h3>
</div>
<div class="container">
    <div class="card">
        <div class="row">
            <div class="col-lg-12 mb40">
                <div id="tabAssign">
                    <!---nav tabs-->
                    <ul class="nav nav-tabs tabs-default justify-content-left mb30 " role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active show" href="#canceljobaudit">
                                {{trans('formlabel.canceljob.tabcanceljob')}}</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" href="{{asset('cancelledjoblisttraining')}}">
                                {{trans('formlabel.canceljob.tabcanceljobtraining')}}</a>
                        </li>
                    </ul>
                    <!---nav tabs-->
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active show" id="canceljobaudit">
                            <div class="container">
                                <div class="row">
                                    @if(count($jobslist) > 0)
                                    @foreach(@$jobslist as $k=>$v)
                                    <div class="col-md-4 mb30">
                                        <div class="card card-body text-center">
                                            <h4 class="card-title">
                                                <a href="{{asset('cancelledjobDetails/')}}/{{$v->assignment_id}}">
                                                @if(strlen($v->assignment_name) > 20)
                                                {{substr(ucfirst($v->assignment_name), 0, 20) . "..."}}
                                                @else
                                                {{ucfirst($v->assignment_name)}}
                                                @endif
                                                </a>
                                            </h4>
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="text-center">
                                                    <span class="font700" style="font-size: 1rem; color: #000;">{{config('assignment.price') . " ". $v->estimated_budget}}</span>&nbsp;
                                                    {{" ( " . trans('formlabel.assignmentinfoaudit.efforts') .  $v->estimated_effort . config('assignment.effortdays') . ")" }}
                                                    @if($v->endjob_status == 1)
                                                    <span class="text-success">{{trans('formlabel.jobinfo.closejob')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="d-flex flex-row mx-auto col-12">
                                                <div class="col-md-3"></div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1rem;" class="{{$v->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                </div>
                                                <div class="p-1 col-2">
                                                    <i style="font-size: 1.2rem;" class="{{$v->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-md-3"></div>
                                            </div>
                                            <hr/>
                                            <p class="card-text text-left">
                                                {{trans('formlabel.jobinfo.refid')}} {{config('assignment.refid') . $v->ref_id}}<br>
                                                {{trans('formlabel.jobinfo.startdate')}} {{Carbon\Carbon::parse(@$v->assignment_startdate)->format(config('assignment.dateformat'))}}<br>
                                                {{trans('formlabel.jobinfo.enddate')}} {{Carbon\Carbon::parse(@$v->assignment_enddate)->format(config('assignment.dateformat'))}}<br>
                                            </p>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else 
                                    <div class="col-md-12">
                                        <div class="text-secondary text-center" style="text-align: center;">
                                            No job found.
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <nav aria-label="Page navigation example pull-right" class="mb20">
                        {{$jobslist->links()}}
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.footer')


