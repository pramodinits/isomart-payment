@include('includes.header')
<style>
    .locationCity {
        border: 1px solid rgba(0,0,0,.125);
        padding: 2px 5px;
        border-radius: 5px;
    }
</style>
<div id="content-wrapper-parent">
    <div id="content-wrapper">
        <div id="content" class="clearfix">
            <div class="clearfix"></div>
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="">
                                <h4>{{config('assignment.refid') . @$jobslist->ref_id}}
                                    <i style="font-size: 1rem;" class="p-1 {{@$jobslist->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                    <i style="font-size: 1.2rem;" class="p-1 {{@$jobslist->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                    <i style="font-size: 1.2rem;" class="p-1 {{@$jobslist->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                </h4>
                                <div><h5>{{ucfirst(@$jobslist->assignment_name)}}</h5>
                                    {{trans('formlabel.jobinfo.startdate')}} {{Carbon\Carbon::parse(@$jobslist->start_date)->format(config('assignment.dateformat'))}}<br>
                                    {{trans('formlabel.jobinfo.enddate')}} {{Carbon\Carbon::parse(@$jobslist->end_date)->format(config('assignment.dateformat'))}}</div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="text-right">
                                <h3>{{config('assignment.price') . " ". @$jobslist->estimated_budget}}</h3>
                                <h5>{{@$jobslist->estimated_effort. config('assignment.effortdays')}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @foreach(@$locationname as $key=>$val)
                            <span class="locationCity">
                                <i class="fa fa-map-marker pr-1 text-danger" aria-hidden="true"></i>
                                {{$val->city_name . ", " . $val->stateName . ", " . $val->countryName . " (Effort- " . $val->effort . " days)"}}
                            </span>&nbsp;
                            @endforeach
                        </div>
                    </div>
                    <hr>
                    <div class="">
                        <h4>{{trans('formlabel.assignjobdetails.creatorheading')}}</h4>
                        <div class="row">
                            <div class="col-md-1">
                                <p>
                                    @if(@$jobslist->profile_img)
                                    <img src="{{asset('profile_image')}}/{{@$jobslist->profile_img}}" style="height: 77px; width: 97px;" class="img-thumbnail" />
                                    @else
                                    <img src="{{asset('images')}}/{{@$jobslist->gender == 1 ? 'profile_default.jpg' : 'profile_female.jpg' }}" style="height: 100px; width: 100px;" class="img-thumbnail">
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-8">
                                <p>
                                    <strong>
                                        <a href="{{ asset('publicProfileView/'.@$jobslist->userId)}}">
                                        {{@$jobslist->fname . " " . @$jobslist->lname}}
                                        </a>
                                    </strong>
                                </p>
                                {{@$addressCb->city_name . ", " . @$addressCb->stateName . ", " . @$addressCb->countryName}}
                            </div>
                            <div class="col-md-3">
                                <a href="{{ url("/sendMessage/".@$jobslist->assignment_user_id."/".@$jobslist->proposal_id."/".@$jobslist->ref_id) }}">
                                    <button type="button" class="btn btn-orange btn-sm btn-xs mb3 pull-right ml-1"><i class="fa fa-envelope"></i> {{trans('formlabel.auditdetail.message')}}</button>
                                </a>
                                @if(count(@$ownsendrating) > 0)
                                <span class="btn btn-orange btn-sm btn-xs mb3 pull-right" data-toggle="modal" data-target="#viewratingModal" style="cursor: pointer;"
                                      data-userid="{{@$jobslist->userId}}" 
                                      data-bidid="{{@$jobslist->proposal_id}}" 
                                      data-assignment="{{@$jobslist->assignment_id}}" 
                                      data-referenceid="{{@$jobslist->ref_id}}" 
                                      onclick="replyQuote(this)"><i class="fa fa-star"></i> {{trans('messagelabel.view')}}</span>
                                @elseif(@$jobslist->endjob_status == 1)
                                <span class="btn btn-orange btn-sm btn-xs mb3 pull-right" data-toggle="modal" data-target="#ratingModal" style="cursor: pointer;"
                                      data-userid="{{@$jobslist->userId}}" 
                                      data-bidid="{{@$jobslist->proposal_id}}" 
                                      data-assignment="{{@$jobslist->assignment_id}}" 
                                      data-referenceid="{{@$jobslist->ref_id}}" 
                                      data-devicetoken="{{@$jobslist->fcm_device_token}}" 
                                      onclick="replyQuote(this)"><i class="fa fa-star"></i> {{trans('messagelabel.rating')}}</span>
                                @endif  
                            </div>
                        </div>
                        <hr>
                    </div>

                    <div class="modal fade queryModal" id="viewratingModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title text-center">{{trans('messagelabel.ratingreview')}}</h5>
                                    <div id="successmsg"></div>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div id="hidebody">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12 profile-info">
                                                <div class="card p-3 mb10">
                                                    <div class="p-skill">
                                                        <p><strong><i class="fa fa-comments text-secondary"></i> :</strong> {{@$ownsendrating->message ? @$ownsendrating->message : "No comments available."}}</p>
                                                        <hr>
                                                        @if(@$ownsendrating)
                                                        @foreach(@$ownsendrating['RatingAnswer'] as $k => $v)
                                                        <div class="row">
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                                                <p><span class="mr-4"><i class="fa fa-star text-danger" style="font-size:10px;"></i></span> {{@$queryname[$v->query_id]}}</p>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                                <p class="text-danger" >{{@$v->answer}} <span style="color:#ccc;">out of 5</span></p>
                                                            </div>
                                                        </div>
                                                        <div style="clear:both;"></div>
                                                        @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--/map--> 
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade queryModal" id="ratingModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title text-center">{{trans('messagelabel.givefeedback')}}</h5>
                                    <div id="successmsg"></div>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div id="hidebody">
                                    <form name="msgform" id="msgform" method="post" action="{{url('/insertRating')}}" onsubmit="return checkvalidate()">

                                        <div class="modal-body">
                                            <div class="row">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="user_id" id="user_id" value="">
                                                <input type="hidden" name="bid_id" id="bid_id" value="">
                                                <input type="hidden" name="reference_id" id="reference_id" value="">
                                                <input type="hidden" name="assignment_id" id="assignment_id" value="">
                                                <input type="hidden" name="assignment_type" id="assignment_type" value="{{@$assignment_type}}">
                                                <input type="hidden" name="type" id="type" value="2">
                                                <input type="hidden" name="device_token" id="device_token" value="">
                                                @foreach(@$querylists as $key=>$val)
                                                <div class="col-md-12 border-right">
                                                    <label class="mt-2">{{@$val->query}}</label>
                                                    <input type="hidden" name="query_id[{{@$val->id}}]" class="queryid" value="{{@$val->id}}">
                                                    <div class="form-group">
                                                        <div class="d-flex">
                                                            <label class="container-radio pr-4">
                                                                <input type="radio" value="1" name="answer[{{@$val->id}}]" class="answer" >1
                                                                <span class="checkmark"></span> 
                                                            </label>
                                                            <label class="container-radio pr-4">
                                                                <input type="radio" value="2" name="answer[{{@$val->id}}]" class="answer" >2
                                                                <span class="checkmark"></span>
                                                            </label>
                                                            <label class="container-radio pr-4">
                                                                <input type="radio" value="3" name="answer[{{@$val->id}}]" class="answer" >3
                                                                <span class="checkmark"></span> 
                                                            </label>
                                                            <label class="container-radio pr-4">
                                                                <input type="radio" value="4" name="answer[{{@$val->id}}]" class="answer" >4
                                                                <span class="checkmark"></span>
                                                            </label>
                                                            <label class="container-radio pr-4">
                                                                <input type="radio" value="5" name="answer[{{@$val->id}}]" class="answer" >5
                                                                <span class="checkmark"></span>
                                                            </label>
                                                            <small class="help-block"></small>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                <div class="col-md-12 border-right">
                                                    <label class="mt-2">{{trans('messagelabel.message')}}</label>
                                                    <div class="form-group">
                                                        <textarea class="form-control" name="message" id="message" rows="2" cols="25"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 border-right">
                                                    <button  class="btn btn-primary text-center btn-md pull-right mt-2" type="submit">Send</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-default justify-content-left mb30 " role="tablist">
                            <li class="nav-item" role="presentation"> <a class="nav-link active show" href="#jobTracking" aria-controls="home" role="tab" data-toggle="tab" aria-selected="true">{{trans('formlabel.assignjobdetails.trackformtab')}}</a> </li>
                            @if(@$jobslist->endjob_status == 1)
                            <li class="nav-item" role="presentation"> <a class="nav-link" href="#rate" aria-controls="home" role="tab" data-toggle="tab" aria-selected="true">{{trans('formlabel.assignjobdetails.ratingtab')}}</a></li>
                            @endif
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content bg-light">
                            <div role="tabpanel" class="tab-pane fade active show" id="jobTracking">
                                <section id="section-3" class="content-current">
                                    <div class="card p-3">
                                        <!--job track form-->
                                        @if(@$jobslist->endjob_status == 1)
                                        <div class="text-center">
                                            <h5>{{trans('formlabel.assignjobdetails.jobended')}}</h5>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <strong>{{trans('formlabel.assignjobdetails.jobendcomment')}}</strong>{{@$jobslist->endjob_comment}}
                                        </div>
                                        @else
                                        <div class="col-md-6">
                                            <form id="track_job" name="track_job" action="{{asset('addJobTrack')}}" method="post" enctype="multipart/form-data"> 
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="tracking[assignment_id]" id="assignment_id" value="{{@$jobslist->assignment_id}}">
                                                <input type="hidden" name="tracking[proposal_id]" id="proposal_id" value="{{@$jobslist->proposal_id}}">
                                                <input type="hidden" name="tracking[sender_id]" id="sender_id" value="{{Auth::user()->id}}">
                                                <input type="hidden" name="device_token" value="{{@$jobslist->fcm_device_token}}">
                                                <input type="hidden" name="assignment_type" value="{{@$assignment_type}}">
                                                <input type="hidden" name="ref_id" value="{{@$jobslist->ref_id}}">
                                                <input type="hidden" name="id_update" id="id_update" value="">
                                                <label>{{trans('formlabel.assignjobdetails.trackformmessage')}}</label>
                                                <div class="form-group">
                                                    <textarea class="form-control" name="tracking[message]" id="message" rows="4" cols="20"></textarea>
                                                </div>
                                                <label>{{trans('formlabel.assignjobdetails.trackformdocument')}}</label>
                                                <div class="form-group">
                                                    <input type="file" name="trackDocuemnt" class="form-control" accept=".doc, .docx, .txt,.pdf">
                                                </div>
                                                <div class="mt-2 mb-2">
                                                    <button type="submit" class="btn btn-primary text-center btn-md pull-right">{{trans('formlabel.assignjobdetails.trackformsave')}}</button>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </form>
                                        </div>
                                        @endif
                                        <hr>
                                        <!--job track form-->
                                        <div class="col-md-12 profile-info">
                                            <div class="p-skill">
                                                @if(count(@$jobtracklist) > 0)
                                                <ul class="timeline">
                                                    @foreach(@$jobtracklist as $k => $v) 
                                                    <li>
                                                        <div class="timeline-badge info"><i class="fa fa-check-circle-o"></i></div>
                                                        <div class="timeline-panel2">
                                                            <div class="timeline-body">
                                                                <p class="time">{{$v->message}}</p>
                                                                <p>{{Carbon\Carbon::parse(@$v->add_date)->format(config('assignment.adddateformat'))}}
                                                                    @if(@$v->document)
                                                                    <a href="{{ asset('downloadDocumentJob')}}/{{@$v->assignment_id}}/{{@$v->document}}">
                                                                        <i class="fa fa-paperclip"></i>
                                                                    </a>
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No job track found.
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <!--/map--> 

                                    </div>
                                </section>
                            </div>

                            <div role="tabpanel" class="tab-pane fade " id="rate">
                                <section id="section-3" class="content-current">
                                    <div class="card p-3">
                                        <div class="col-md-12 profile-info">
                                            <div class="p-skill">
                                                @if(count(@$rate) > 0)
                                                <p><strong><i class="fa fa-comments text-secondary"></i> : </strong> {{@$rate->message ? @$rate->message : "No comments available."}}</p>
                                                <hr>
                                                @foreach($rate['RatingAnswer'] as $k => $v) 
                                                <div class="row">
                                                    <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                                        <p><span class="mr-4"><i class="fa fa-star text-danger" style="font-size:10px;"></i></span> {{@$queryname[$v->query_id]}}</p>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                                        <p class="text-danger">{{@$v->answer}} <span style="color:#ccc;">out of 5</span></p>
                                                    </div>

                                                </div>
                                                <div style="clear:both;"></div>
                                                @endforeach

                                                @else
                                                <div class="col-md-12">
                                                    <div class="text-secondary text-center" style="text-align: center;">
                                                        No data found.
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <!--/map--> 
                                    </div>
                                </section>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#track_job').bootstrapValidator({
            fields: {
                "tracking[message]": {
                    validators: {
                        notEmpty: {
                            message: "This field is required."
                        }
                    }
                }
            }
        })
                .on('success.form.bv', function (e) {
                    return true;
                });
    });
</script>
<script>
function replyQuote(e) {
        var assignment = $(e).attr('data-assignment');
        var referenceid = $(e).attr('data-referenceid');
        var user_id = $(e).attr('data-userid');
        var bidid = $(e).attr('data-bidid');
        var devicetoken = $(e).attr('data-devicetoken');
        $('#assignment_id').val(assignment);
        $('#reference_id').val(referenceid);
        $('#user_id').val(user_id);
        $('#bid_id').val(bidid);
        $('#device_token').val(devicetoken);
        }
function checkvalidate()
    {
        var chx = document.getElementsByTagName('input');
            for (var i=0; i<chx.length; i++) {
              if (chx[i].type == 'radio' && chx[i].checked) {
                  okay=true;
                break;
              } else {
                  okay=false;
              }
            }
        if(!okay)
            {
                alert("Atleast choose one query.");
                return false;
            }
    }
</script>
@include('includes.footer')
