<?php 
ini_set('session.gc_maxlifetime', 3600); // server should keep session data for AT LEAST 1 hour
session_set_cookie_params(3600); // each client should remember their session id for EXACTLY 1 hour
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title_prefix', config('adminlte.title'))
        @yield('title')
        </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <!-- Bootstrap 3.3.7 -->
        <!--        <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
                <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
                <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.min.css') }}">-->
<script src="{{ asset('vendor/adminlte/js/jquery.min.js')}}"></script>
        <link rel="icon" href="<?php echo env('APP_URL'); ?>public/images/favicon.ico" type="image/x-icon"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('vendor/adminlte/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('vendor/adminlte/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('vendor/adminlte/css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('vendor/adminlte/css/component.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Lato|Roboto" rel="stylesheet">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.min.css') }}">

        @if(config('adminlte.plugins.datatables'))
        <!-- DataTables -->
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
        @endif

        @yield('adminlte_css')

    </head>
    <body class="hold-transition @yield('body_class')">

        <div class="theme-loader">
            <div class="ball-scale">
                <div></div>
            </div>
        </div>
        <div id="pcoded" class="pcoded">
            <div class="pcoded-overlay-box"></div>
            <div class="pcoded-container navbar-wrapper">
                @yield('body')
            </div>
        </div>

        

        
        <script src="{{ asset('vendor/adminlte/js/jquery-ui.min.js')}}"></script>
        <script src="{{ asset('vendor/adminlte/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('vendor/adminlte/js/script.js')}}"></script>
        <script src="{{ asset('vendor/adminlte/js/pcoded.min.js')}}"></script>
        <script src="{{ asset('vendor/adminlte/js/demo-12.js')}}"></script>

<!--        <script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>-->

        @if(config('adminlte.plugins.chartjs'))
        <!-- ChartJS -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js"></script>
        @endif

        @yield('adminlte_js')

    </body>
</html>
