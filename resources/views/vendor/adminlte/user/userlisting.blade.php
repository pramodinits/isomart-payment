@extends('adminlte::master')

@section('title', ': User Listing')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

@php
$user_type = config('selecttype.usertypelist');
@endphp

<style>
    .searchform .col-md-2 {
        padding-right: 0px !important;
    }
    .table .toggle.btn {
        max-width: 59px !important;
        max-height: 25px !important;
        min-height: 10px !important;
        padding: 5px 13px 0px 10px !important;
    }
    .fa-phone-square {
        font-size: 1.3rem;
    }
    .toggle-group .btn {
        padding: 0 15px !important;
    }
    .table .btn {
       line-height: 1.85 !important; 
    }
</style>
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--user content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
<!--                                <div class="card-header">
                                    <h5>{{trans('adminlabel.userlisting.listingheader')}}</h5>
                                </div>-->
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <div class="dt-responsive table-responsive">
                                            <form action="{{url('admin/userListing')}}" method="GET">
                                                <div class="row searchform">
                                                    <div class="col-md-2">
                                                        <div class="input-group">
                                                            <input type="text" name="email" placeholder="{{trans('adminlabel.userlisting.searchemail')}}" value="{{@$email}}" class="form-control input-sm" aria-controls="res-config">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="input-group">
                                                            <input type="text" placeholder="{{trans('adminlabel.userlisting.searchphone')}}" name="phone" value="{{@$phone}}" class="form-control input-sm" aria-controls="res-config">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <select name="usertype" class="form-control input-sm">
                                                                <option value="">-- {{trans('adminlabel.userlisting.searchusertype')}} --</option>
                                                                <?php $i = 0; ?>
                                                                @foreach($user_type as $k=>$v)
                                                                <option value="{{@$k}}" @if(@$usertype == @$k) selected @endif >{{@$v}}</option>
                                                                <?php $i++; ?>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button type="submit" class="btn btn-primary">{{trans('adminlabel.userlisting.searchbutton')}}</button>
                                                        <div class="btn-group">
                                                            <a href="{{url('/admin/userListing')}}" class="btn btn-danger">{{trans('adminlabel.userlisting.resetbutton')}}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                                                <!--table-->
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table id="res-config" class="table table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 100%;">
                                                            <thead>
                                                                <tr role="row">
                                                                    <th rowspan="1" colspan="1" >#</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-sort="ascending" aria-label="User name">{{trans('adminlabel.userlisting.rowusername')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Email">{{trans('adminlabel.userlisting.rowemail')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Phone">{{trans('adminlabel.userlisting.rowphone')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Company/Organization">{{trans('adminlabel.userlisting.rowcompany')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Usertype">{{trans('adminlabel.userlisting.rowusertype')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Registration Date">{{trans('adminlabel.userlisting.rowregdate')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.userlisting.rowstatus')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.userlisting.rowaction')}}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($userlist as $key => $val)
                                                                <tr role="row" class="odd">
                                                                    <td>
                                                                        {{(($userlist->currentPage()-1) * $userlist->perPage())+($key+1)}}
                                                                    </td>
                                                                    <td tabindex="0" class="sorting_1">
                                                                        @if($val->fname)
                                                                        {{$val->fname . " " . $val->lname}}
                                                                        @else
                                                                        {{$val->name}}
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if($val->email)
                                                                        {{$val->email}}
                                                                        @else
                                                                        {{"N/A"}}
                                                                        @endif
                                                                    </td>
                                                                    <td>{{"+" . $val->countrycode . "-" .$val->phone}}</td>
                                                                    <td>
                                                                        @if($val->company_name)
                                                                        {{$val->company_name}}
                                                                        @else
                                                                        {{"N/A"}}
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        {{$user_type[$val->usertype]}}
                                                                    </td>
                                                                    <td>{{$val->reg_date}}</td>
                                                                    <td>
                                                                        <input type="hidden" name="uid_{{$val->id}}" id="uid_{{$val->id}}" value="{{$val->id}}">
                                                                        <?php
                                                                        if ($val->status == 1) { ?>
                                                                        <span id="status_{{$val->id}}">
                                                                                <input id="toggle-event_{{$val->id}}" type="checkbox" checked data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger">
                                                                            </span>
                                                                            <script>
                                                                                $(function () {
                                                                                    $('#toggle-event_<?php echo $val->id ?>').bootstrapToggle('Active');
                                                                                })
                                                                            </script>
                                                                       <?php } else if ($val->status == 0) { ?>
                                                                            <span id="status_{{$val->id}}">
                                                                                    <input id="toggle-event_{{$val->id}}" type="checkbox"  data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger"> 
                                                                                </span>
                                                                                <script>
                                                                                    $(function () {
                                                                                        $('#toggle-event_<?php echo $val->id ?>').bootstrapToggle('Inactive');
                                                                                    })
                                                                                </script>
                                                                        <?php }
//                                                                        if ($val->status == 1) {
//                                                                            echo "<i class='fa fa-user text-success' data-toggle='tooltip' title='Active User' data-original-title='Active User'></i>";
//                                                                        } else if ($val->status == 2) {
//                                                                            echo "<i class='fa fa-user text-gray' data-toggle='tooltip' title='Pending User' data-original-title='Pending User'></i>";
//                                                                        } else if ($val->status == 0) {
//                                                                            echo "<i class='fa fa-user text-danger' data-toggle='tooltip' title='In-Active User' data-original-title='In-Active User'></i>";
//                                                                        }
                                                                        ?>
                                                                        <?php
                                                                        if ($val->phone_verify_status == 1) {
                                                                            echo "<i class='fa fa-phone-square text-success' data-toggle='tooltip' title='Phone Verified'></i>";
                                                                        } else if ($val->phone_verify_status == 0) {
                                                                            echo "<i class='fa fa-phone-square text-gray' data-toggle='tooltip' title='Phone Verified Pending'></i>";
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                    <td>
                                                                        <a href="{{ url('/admin/userDetail/'.$val->id)}}">
                                                                            <i class="fa fa-eye" title="View Detail" aria-hidden="true"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            <script>
                                                                $(function () {
                                                                    $('#toggle-event_<?php echo $val->id ?>').change(function () {
                                                                        //alert($(this).prop('checked'));
                                                                        if ($(this).prop('checked') == true)
                                                                        {
                                                                            var val = 1;
                                                                        } else {
                                                                            var val = 0;
                                                                        }
//                                                                        alert(val);
//                                                                        return false;
                                                                        var id = $('#uid_<?php echo $val->id ?>').val();
                                                                        var _token = "<?php echo csrf_token(); ?>";
//                                                                        var table = "program";
                                                                            $.ajax({
                                                                                            method: "POST",
                                                                                            url: "{{url('/admin/updateUserStatus')}}",
                                                                                            data: {
                                                                                                "user_id": id,
                                                                                                "status_val": val,
                                                                                                "_token": _token
                                                                                            },
                                                                                            cache: false,
                                                                                            success: function (res) {
                                                                                                if (res === "success") {
                                                                                                    location.reload();
                                                                                                } else {
                                                                                                    alert("Error in Status update!");
                                                                                                    return false;
                                                                                                }
                                                                                            }
                                                                                        });
                                                                        
                                                                    })
                                                                })
                                                            </script> 
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-5">
                                                        <div class="dataTables_info" id="res-config_info" role="status" aria-live="polite">
                                                            @if(@$userlist->total())
                                                            Showing 
                                                            {{(($userlist->currentPage()-1) * $userlist->perPage())+1}}
                                                            to 
                                                            {{(($userlist->currentPage()-1) * $userlist->perPage())+$userlist->count() }}

                                                            of {{$userlist->total()}} entries
                                                            @else
                                                            No Record Found !!!
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-7">
                                                        <div class="dataTables_paginate paging_simple_numbers" id="res-config_paginate">
                                                            <ul class="pagination">
                                                                <li class="paginate_button page-item ">
                                                                    {{$userlist->links()}}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!--table-->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--user content--->

                </div>
            </div>
        </div>
    </div>
</div>

   

@stop


