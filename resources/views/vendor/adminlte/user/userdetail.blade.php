@extends('adminlte::master')

@section('title', ': User Listing')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')

<style>
    .nav-link {
        padding: .5rem 3.3rem !important;
    }
    .viewPrfle{
    font-size: 15px;
}
.My_Services{
    border: 1px solid #ddd;
    padding: 10px 15px;
    text-align: center;
    border-radius:  0 30px;
    line-height: 10px;
    background: #fdf9f8;
    margin-bottom: 10px;
    min-height: 230px;
}
.My_Services h3{
    font-size: 17px;
    font-weight: bold;
    color: #868686;
}
.My_Services h4{
    font-size: 12px;
    font-weight: bold;
}
.My_Services p{
    font-size: 12px;
}
.My_Services span{
    font-size: 21px;
    /* font-weight: bold; */
    line-height: 25px;
    color: #b9b9b9;
}
.Rating_review{
    border-bottom: 1px solid #ddd;
    padding: 10px 15px;
    text-align: left;
}
.profle{
    padding: 10px;
    border: 1px dotted #ddd;
}
.profle img{
    padding:5px;
    margin-bottom: 20px;
}
.social{
    font-size:12px;
}
.social ul{
    list-style:none;
    padding-left: 0;
}
.social ul li{
    border-bottom:1px dotted #ddd;
    margin:10px 0;
}
.social ul li a{
    color: #807e7e;
}
.social ul li a:hover{
    color: #F79C15;
}
.social i{
    font-size:14px;
    padding: 5px 8px;
    color: #959595
}
.social i:hover{
    color: #000;
}
.profle .sent_msg {
    float: right;
    width: 100%;
    margin-bottom:5px;
}
.profle .sent_msg p{
    background: #ff590d ;
}
.profle .sent_msg a{
    color: #fff;
    border: 1px solid #fdca7d;
    padding:0 8px;
    float: right;
    font-size: 12px;
}
.profle .sent_msg a:hover{
    color: #fff;
    border: 1px solid #fdca7d;
    background: #d04606;
}
.mb30 {
    margin-bottom: 30px !important;
}
.userdetail {
    border: 1px solid rgba(0,0,0,.125) !important;
}
#rating .card {
    border: 1px solid rgba(0,0,0,.125) !important;
}
</style>
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')
                    
                    <!--user details-->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
                                <div class="card-block">
                                <div class="container">
                                    <div class="main-content mb-5">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="profle">
                                                    @if(@$userdetails->profile_img)
                                                    <img src="{{asset('profile_image')}}/{{@$userdetails->profile_img}}" style="height:146px" width="140" class="img-thumbnail mx-auto d-block" />
                                                    @else
                                                    <img src="{{asset('images')}}/{{@$userdetails->gender == 1 ? 'profile_default.jpg' : 'profile_female.jpg' }}" width="170" class="img-thumbnail mx-auto d-block">
                                                    @endif
                                                    <hr>
                                                    <p><i class="fa fa-envelope pr-2"></i>{{@$userdetails->email}}</p>
                                                    <p><i class="fa fa-phone pr-2"></i>{{@$userdetails->phone}}</p>
                                                    <hr>
                                                    <div class="social">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-twitter"></i> {{@$contactdetails->twitter ? @$contactdetails->twitter : "N/A"}}</a></li>
                                                            <li><a href="#"><i class="fa fa-instagram"></i> {{@$contactdetails->instagram ? @$contactdetails->instagram : "N/A"}}</a></li>
                                                            <li><a href="#"><i class="fa fa-skype"></i> {{@$contactdetails->skype ? @$contactdetails->skype : "N/A"}}</a></li>
                                                            <li><a href="#"><i class="fa fa-whatsapp"></i> {{@$contactdetails->whatsapp ? @$contactdetails->whatsapp : "N/A"}}</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="viewPrfle">
                                                    <h4>{{ucfirst(@$userdetails->fname) . " " . ucfirst(@$userdetails->lname)}}</h4>
                                                    @for ($i = 1; $i <= 5; $i++)
                                                    @if($i <= @$gettotalaverage)
                                                    <span class="fa fa-star checked"></span>
                                                    @else
                                                    <span class="fa fa-star"></span>
                                                    @endif
                                                    @endfor {{"(". @$jobCount . " assignment & " . count(@$ratingreview)." ratings)"}}
                                                    <div class="clearfix"></div>
                                                    @php $usertype = config('selecttype.usertypelist'); @endphp
                                                    <p>{{$usertype[@$userdetails->usertype]}}<br>
                                                        <span class="text-success">
                                                            @if (@$addressdetails)
                                                            {{"Available Address : ". @$addressdetails->city_name . ", " . @$addressdetails->stateName . ", " . @$addressdetails->countryName . ""}}
                                                            @endif
                                                        </span>
                                                    </p>
                                                    <hr>
                                                    <h5>{{trans('formlabel.profileview.aboutme')}}</h5>
                                                    <p>{{(@$generaldetails->brief_info) ? @$generaldetails->brief_info : 'N/A' }}</p>
                                                    <hr>

                                                    <!--tab section--->
                                                    <div class="card userdetail">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div id="tabAssign">
                                                                    <ul class="nav nav-tabs tabs-default mb30" role="tablist bg-light">
                                                                        <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs active show" href="#services" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">{{trans('formlabel.profileview.myservices')}}</a></li>
                                                                        <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs " href="#cpdhours" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">{{trans('formlabel.profileview.cpdhours')}}</a></li>
                                                                        <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs " href="#rating" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">{{trans('formlabel.profileview.ratingreview')}}</a></li>
                                                                    </ul>

                                                                    <!--tab content-->
                                                                    <div class="tab-content">
                                                                        <div role="tabpanel" class="tab-pane fade active show" id="services">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    @foreach(@$isiccodedetails as $k => $v)
                                                                                    <div class="col-md-4">
                                                                                        <div class="My_Services">
                                                                                            <h3>AUDIT</h3>
                                                                                            <hr>
                                                                                            <h4>{{$v->type}}</h4>
                                                                                            <span>{{$v->codeName}}</span>
                                                                                            <hr>
                                                                                            <p>{{trans('formlabel.profileview.approved')}} {{Carbon\Carbon::parse($v->approved_date)->format(config('assignment.dateformat'))}}</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div role="tabpanel" class="tab-pane fade" id="cpdhours">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    @if(count(@$cpddetails) > 0)
                                                                                    @foreach(@$cpddetails as $k => $v)
                                                                                    <div class="col-md-4">
                                                                                        <div class="My_Services">
                                                                                            <h3>{{$v->cpd_title}}</h3>
                                                                                            <hr>
                                                                                            @if($v->cpd_links)
                                                                                            @php
                                                                                            $links = explode(', ', $v->cpd_links);
                                                                                            @endphp
                                                                                            @foreach ($links as $key => $val)
                                                                                            <span style="font-size: 1.1em"><a href="{{$val}}">{{$val}}</a><br></span>
                                                                                            @endforeach
                                                                                            @else
                                                                                            N/A
                                                                                            @endif
                                                                                            <hr>
                                                                                            <p>{{trans('formlabel.profileview.cpddate')}} {{Carbon\Carbon::parse($v->cpd_date)->format(config('assignment.dateformat'))}}</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    @endforeach
                                                                                    @else
                                                                                    <div class="col-md-12 p-3">
                                                                                        <div class="text-secondary text-center" style="text-align: center;">
                                                                                            No data found.
                                                                                        </div>
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div role="tabpanel" class="tab-pane fade" id="rating">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    @if(count(@$ratingreview) > 0)
                                                                                    @foreach(@$ratingreview as $key => $val) 
                                                                                    <div class="col-md-6">
                                                                                        <div class="card p-3 mb10">
                                                                                            <div class="col-md-12 profile-info">
                                                                                                <div class="p-skill">
                                                                                                    <p><strong>
                                                                                                            <a data-toggle="modal" data-target="#assignmentModal{{@$val->id}}" style="cursor: pointer;">
                                                                                                                {{$val->assignment_name ? $val->assignment_name : $val->batch_no}}
                                                                                                            </a>
                                                                                                        </strong>
                                                                                                        <span style="color: #3f62db; font-size: 0.85em;">By {{ucfirst($val->fname) . " " . ucfirst($val->lname)}}</span>
                                                                                                    </p>
                                                                                                    <p><strong><i class="fa fa-comments text-secondary"></i></strong> {{@$val->message}}</p>
                                                                                                    <hr>
                                                                                                    @foreach(@$ratingreview[$key]['RatingAnswer'] as $k => $v)

                                                                                                    <div class="row">
                                                                                                        <div class="col-lg-8 col-md-9 col-sm-9 col-12">
                                                                                                            <p><span class="mr-4"><i class="fa fa-star text-danger" style="font-size:10px;"></i></span> {{@$queryname[$v->query_id]}}</p>
                                                                                                        </div>
                                                                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-12">
                                                                                                            <p class="text-danger">{{@$v->answer}}<span style="color:#ccc;"> out of 5</span></p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div style="clear:both;"></div>

                                                                                                    @endforeach
                                                                                                </div>
                                                                                            </div>
                                                                                            <!--/map--> 
                                                                                        </div>
                                                                                    </div>

                                                                                    <!--- assignment modal-->
                                                                                    <div class="modal fade queryModal" id="assignmentModal{{@$val->id}}" role="dialog">
                                                                                        <div class="modal-dialog">
                                                                                            <div class="modal-content">
                                                                                                <div class="modal-header">
                                                                                                    <h5 class="modal-title text-center">{{@$val->assignment_name ? @$val->assignment_name : @$val->batch_no}}</h5>
                                                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                </div>
                                                                                                <div class="modal-body">
                                                                                                    <div class="inforamation-section" style="width: 95%;">
                                                                                                        <p>
                                                                                                            {{trans('formlabel.profileview.assignmentdescription')}} {{@$val->description ? @$val->description : @$val->session_details}}
                                                                                                        </p>
                                                                                                        <p>
                                                                                                            @php
                                                                                                            @$mode = config('selecttype.trainingmode'); 
                                                                                                            @$trainingtype = config('selecttype.trainingtypelist');
                                                                                                            @endphp
                                                                                                            {{trans('formlabel.profileview.assignmentstartdate')}} {{@$val->auditStart ? Carbon\Carbon::parse(@$val->auditStart)->format(config('assignment.dateformat')) : Carbon\Carbon::parse(@$val->trainingStart)->format(config('assignment.dateformat'))}}<br>
                                                                                                            {{trans('formlabel.profileview.assignmentenddate')}} {{@$val->auditEnd ? Carbon\Carbon::parse(@$val->auditEnd)->format(config('assignment.dateformat')) : Carbon\Carbon::parse(@$val->trainingEnd)->format(config('assignment.dateformat'))}}<br>
                                                                                                            @if(@$val->code)
                                                                                                            {{trans('formlabel.profileview.assignmentsiccode')}} {{@$val->code ." - (". @$val->type ." / ". @$val->name . ")"}}<br>
                                                                                                            @else
                                                                                                            {{trans('formlabel.profileview.assignmenttrainingtype')}} {{@$trainingtype[@$val->training_type]}}<br>
                                                                                                            @endif
                                                                                                            @if(@$val->cities)
                                                                                                            {{trans('formlabel.profileview.assignmentcities')}} {{@$val->cities}}
                                                                                                            @else
                                                                                                            {{trans('formlabel.profileview.assignmentmode')}} {{@$mode[@$val->mode]}}<br>
                                                                                                            @endif
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!--- assignment modal-->

                                                                                    @endforeach
                                                                                    @else
                                                                                    <div class="col-md-12 p-3">
                                                                                        <div class="text-secondary text-center" style="text-align: center;">
                                                                                            No data found.
                                                                                        </div>
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--tab content-->

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--tab section--->
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--user details-->
                </div>
            </div>
        </div>
    </div>
</div>


@stop