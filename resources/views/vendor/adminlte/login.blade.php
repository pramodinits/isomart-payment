@extends('adminlte::master')

@section('adminlte_css')
@section('title', ': Login')
@yield('css')
@stop

@section('body_class', 'login-page')

@section('body')

<style>
    .form-actions {
    margin: 0;
    background-color: transparent;
    text-align: center;
}
</style>

<div class="login-box">


    <p class="login-box-msg">Admin Sign In</p>
    <div class="login-box-body">
        <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
            {!! csrf_field() !!}

            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                <input type="email" name="email" class="form-control" value=""
                       placeholder="{{ trans('adminlte::adminlte.email') }}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <span class="tooltip tooltip-top-right">Email Address</span>

                @if ($errors->has('email'))
                <span class="help-block">
                    {{ $errors->first('email') }}
                </span>
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                <input type="password" name="password" class="form-control"
                       placeholder="{{ trans('adminlte::adminlte.password') }}">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
                <b class="tooltip tooltip-top-right">Type your Password</b>
            </div>
            <div class="form-group form-actions">
                <button type="submit"
                        class="btn btn-primary">{{ trans('adminlte::adminlte.sign_in') }}</button>

                <!-- /.col -->
            </div>
        </form>
        <div class="auth-links" style="visibility: hidden;">
            <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}"
               class="text-center">{{ trans('adminlte::adminlte.i_forgot_my_password') }}</a>
        </div>
    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
@stop

@section('adminlte_js')
<script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});
</script>
@yield('js')
@stop
