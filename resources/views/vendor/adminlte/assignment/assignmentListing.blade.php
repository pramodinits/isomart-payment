@extends('adminlte::master')

@section('title', ': Assignment Listing')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')

@php
$user_type = config('selecttype.usertypelist');
@endphp
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--user content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Assignment Listing</h5>
                                </div>
                                <div class="col-lg-12 mb40">
                                    <div id="tabAssign" class="mt-2">
                                        <ul class="nav nav-tabs tabs-default mb30" role="tablist bg-light">
                                            <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs active show" href="#audit" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Audit</a></li>
                                            <li class="nav-item " role="presentation"><a class="nav-link  custom-tabs " href="#training" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">Training</a></li>
                                        </ul>

                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade show active" id="audit">
                                                @include('vendor.adminlte.assignment.auditlisting')
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="training">
                                                @include('vendor.adminlte.assignment.traininglisting')
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--user content--->

                </div>
            </div>
        </div>
    </div>
</div>

@stop


