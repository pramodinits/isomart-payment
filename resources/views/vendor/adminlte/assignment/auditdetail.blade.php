@extends('adminlte::master')

@section('title', ': Assignment Detail')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')

<?php
$now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
$end_date = Carbon\Carbon::parse(@$auditdetails->proposal_end_date);
$now_date = Carbon\Carbon::parse($now);
$daysleft = $now_date->diffInDays($end_date, false);
?>
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--user content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
                                <div class="card-header">
                                    <h4>{{trans('adminlabel.auditdetail.assignmenttitle')}}</h4>
                                </div>
                                <div class="card-block">
                                    <!---top details-->
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="col-md-12 mb40">
                                                    <div class="details border-right">
                                                        <h5 class="profile-no">{{config('assignment.refid') . @$auditdetails->ref_id}} 
                                                            <!--- assignment status for send bid--->
                                                            <small class="text-success">
                                                                @if(@$auditdetails->assignment_status == 2)
                                                                <span class="text-danger">{{trans('formlabel.auditdetail.draft')}}</span>
                                                                @elseif(@$userdetails[0]->approval_status == 1)
                                                                {{trans('formlabel.auditdetail.awarded')}}
                                                                @elseif(@$daysleft < 0) 
                                                                <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                                @elseif(@$daysleft == 0)
                                                                {{trans('formlabel.auditdetail.openfortoday')}}
                                                                @else
                                                                Open / {{@$daysleft}} days left
                                                                @endif
                                                            </small>
                                                            <!--- assignment status for send bid--->
                                                        </h5>
                                                        <div>{{@$auditdetails->assignment_name}} 
                                                            @if(@$userdetails[0]->approval_status == 0)
                                                            ({{trans('formlabel.auditdetail.proposals')}}{{@$countTotalBid}})
                                                            @endif
                                                        </div>
                                                        <div>{{trans('formlabel.auditdetail.postedby')}}<span class="text-primary"><b>{{ucfirst(@$userdetails[0]->fname) . " " . ucfirst(@$userdetails[0]->lname)}}</b></span> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="btnsection mt-2 pull-right">
                                                    <h6 class="text-center">{{trans('formlabel.auditdetail.budget')}}</h6>
                                                    <div class="buget-price">
                                                        {{config('assignment.price') . " ". @$auditdetails->budget_usd}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!---top details-->
                                    
                                    <!--below details-->
                                    <div class="container">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="">
                                                    <p class="text-grey" style="font-size:1.3rem;">{{@$auditdetails->description}}</p>
                                                    <h6 class="text-dark">{{trans('formlabel.auditdetail.assignmentlocation')}}</h6>
                                                    <address>
                                                        @foreach(@$locationname as $key=>$val)
                                                        <span class="locationCity">
                                                            <i class="fa fa-map-marker pr-1 text-danger" aria-hidden="true"></i>
                                                            {{$val->city_name . ", " . $val->stateName . ", " . $val->countryName . " (Effort- " . $val->effort . " days)"}}
                                                            @if($val->whomtoassign_id)
                                                            <span class="text-success">Awarded</span>
                                                            @endif
                                                        </span>&nbsp;
                                                        @endforeach
                                                    </address>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-md-6 border-right">
                                                            <div class="inforamation-section">
                                                                <div>{{trans('formlabel.auditdetail.section')}}<span class="text-dark">{{@$standardDeatil->code ." - (". @$standardDeatil->type ." / ". @$standardDeatil->name . ")"}}</span> </div>
                                                                <div>{{trans('formlabel.auditdetail.efforts')}}<span class="text-dark">{{@$auditdetails->effort_tentative . config('assignment.effortdays')}}</span> </div>
                                                                <div>{{trans('formlabel.auditdetail.startdate')}}<span class="text-dark">{{Carbon\Carbon::parse(@$auditdetails->start_date)->format(config('assignment.dateformat'))}}</span> </div>
                                                                <div>{{trans('formlabel.auditdetail.endadte')}}<span class="text-dark">{{Carbon\Carbon::parse(@$auditdetails->end_date)->format(config('assignment.dateformat'))}}</span> </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <div class="inforamation-section">
                                                                <div>{{trans('formlabel.auditdetail.localtravel')}} 
                                                                    <span class="text-dark">
                                                                        <i style="font-size: 1rem;" class="{{@$auditdetails->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                                    </span> 
                                                                </div>
                                                                <div>{{trans('formlabel.auditdetail.localstay')}}<span class="text-dark">
                                                                        <i style="font-size: 1.2rem;" class="{{@$auditdetails->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                                    </span> </div>
                                                                <div>{{trans('formlabel.auditdetail.airfare')}}<span class="text-dark">
                                                                        <i style="font-size: 1.2rem;" class="{{@$auditdetails->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                                    </span> </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--below details-->
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--user content--->

                </div>
            </div>
        </div>
    </div>
</div>

@stop