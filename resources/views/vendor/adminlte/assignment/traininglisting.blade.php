@php
@$trainingtype = config('selecttype.trainingtypelist');
@$trainingmode = config('selecttype.trainingmode');
@$standard = config('standards.standardlist');
@endphp
<style>
    table.dataTable thead th {
        padding: 0.5% !important;
    }
    .modal-dialog {
        max-width: 700px !important;
    }
</style>
<div class="card-block">
    <div class="table-responsive">
        <div class="dt-responsive table-responsive">
            <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <!--table-->
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <table id="res-config" class="table table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 100%;">
                            <thead>
                                <tr role="row">
                                    <th rowspan="1" colspan="1" >#</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-sort="ascending" aria-label="User name">{{trans('adminlabel.traininglisting.refid')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 100px;" aria-label="Phone">{{trans('adminlabel.traininglisting.batchno')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.traininglisting.mode')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.traininglisting.effort')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.traininglisting.budget')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.traininglisting.allowance')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.traininglisting.proposalenddate')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.traininglisting.startdate')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.traininglisting.enddate')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.traininglisting.status')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.traininglisting.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($traininglist as $key => $val)
                                <tr role="row" class="odd">
                                    <td>
                                        {{(($traininglist->currentPage()-1) * $traininglist->perPage())+($key+1)}}
                                    </td>
                                    <td tabindex="0" class="sorting_1">{{$val->ref_id}}</td>
                                    <td tabindex="0" class="sorting_1">{{$val->batch_no}}</td>
                                    <td tabindex="0" class="sorting_1">{{$trainingmode[$val->mode]}}</td>
                                    <td tabindex="0" class="sorting_1">{{$val->effort_tentative}}</td>
                                    <td tabindex="0" class="sorting_1">{{$val->fee_range}}</td>
                                    <td tabindex="0" class="sorting_1">
                                        @if($val->mode == 1)
                                        <i class="pr-1 {{$val->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                        <i class="pr-1 {{$val->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                        <i class="{{$val->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                        @else
                                        {{"N/A"}}
                                        @endif
                                    </td>
                                    <td tabindex="0" class="sorting_1">{{Carbon\Carbon::parse($val->proposal_end_date)->format(config('assignment.dateformat'))}}</td>
                                    <td tabindex="0" class="sorting_1">{{Carbon\Carbon::parse($val->start_date)->format(config('assignment.dateformat'))}}</td>
                                    <td tabindex="0" class="sorting_1">{{Carbon\Carbon::parse(@$val->end_date)->format(config('assignment.dateformat'))}}</td>
                                    <td tabindex="0" class="sorting_1"> 
                                        <i class="{{$val->status == "1" ? "text-success fa fa-check" : "text-danger fa fa-times" }}" title="Status" aria-hidden="true"></i>
                                    </td>
                                    <td tabindex="0" class="sorting_1">
                                        <a href="{{ url('/admin/trainingDetail/'.$val->assignment_id)}}">
                                        <i class="fa fa-eye" title="View Detail" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-5">
                        <div class="dataTables_info" id="res-config_info" role="status" aria-live="polite">
                            @if(@$traininglist->total())
                            Showing 
                            {{(($traininglist->currentPage()-1) * $traininglist->perPage())+1}}
                            to 
                            {{(($traininglist->currentPage()-1) * $traininglist->perPage())+$traininglist->count() }}

                            of {{$traininglist->total()}} entries
                            @else
                            No Record Found !!!
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="res-config_paginate">
                            <ul class="pagination">
                                <li class="paginate_button page-item ">
                                    {{$traininglist->links()}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--table-->
            </div>
        </div>
    </div>
</div>