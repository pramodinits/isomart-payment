@php
@$trainingtype = config('selecttype.trainingtypelist');
@$trainingmode = config('selecttype.trainingmode');
@$standard = config('standards.standardlist');
@endphp
<style>
    table.dataTable thead th {
        padding: 0.5% !important;
    }
    .modal-dialog {
        max-width: 700px !important;
    }
</style>
<div class="card-block">
    <div class="table-responsive">
        <div class="dt-responsive table-responsive">
            <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <!--table-->
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <table id="res-config" class="table table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 100%;">
                            <thead>
                                <tr role="row">
                                    <th rowspan="1" colspan="1" >#</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-sort="ascending" aria-label="User name">{{trans('adminlabel.trainingdetail.refid')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 100px;" aria-label="Phone">{{trans('adminlabel.trainingdetail.batchno')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.trainingdetail.mode')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.trainingdetail.effort')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.trainingdetail.budget')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.trainingdetail.allowance')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.trainingdetail.proposalenddate')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.trainingdetail.startdate')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.trainingdetail.enddate')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.trainingdetail.status')}}</th>
                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" aria-label="Status">{{trans('adminlabel.trainingdetail.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($traininglist as $key => $val)
                                <tr role="row" class="odd">
                                    <td>
                                        {{(($traininglist->currentPage()-1) * $traininglist->perPage())+($key+1)}}
                                    </td>
                                    <td tabindex="0" class="sorting_1">{{$val->ref_id}}</td>
                                    <td tabindex="0" class="sorting_1">{{$val->batch_no}}</td>
                                    <td tabindex="0" class="sorting_1">{{$trainingmode[$val->mode]}}</td>
                                    <td tabindex="0" class="sorting_1">{{$val->effort_tentative}}</td>
                                    <td tabindex="0" class="sorting_1">{{$val->fee_range}}</td>
                                    <td tabindex="0" class="sorting_1">
                                        @if($val->mode == 1)
                                        <i class="pr-1 {{$val->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                        <i class="pr-1 {{$val->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                        <i class="{{$val->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                        @else
                                        {{"N/A"}}
                                        @endif
                                    </td>
                                    <td tabindex="0" class="sorting_1">{{Carbon\Carbon::parse($val->proposal_end_date)->format(config('assignment.dateformat'))}}</td>
                                    <td tabindex="0" class="sorting_1">{{Carbon\Carbon::parse($val->start_date)->format(config('assignment.dateformat'))}}</td>
                                    <td tabindex="0" class="sorting_1">{{Carbon\Carbon::parse(@$val->end_date)->format(config('assignment.dateformat'))}}</td>
                                    <td tabindex="0" class="sorting_1"> 
                                        <i class="{{$val->status == "1" ? "text-success fa fa-check" : "text-danger fa fa-times" }}" title="Status" aria-hidden="true"></i>
                                    </td>
                                    <td tabindex="0" class="sorting_1">
                                        <?php
                                        $address = "";
                                        foreach($val->AssignmentLocation as $k => $v){
//                                            $address .= $val->country_id."###".$val->state_id."###".$val->city_id."@@@";
                                            $address .= $v->city_name.",";
                                        }
                                        $address = rtrim($address,",");
                                        ?>
                                        <a data-toggle="modal" data-target="#proposalModal"
                                           data-refid="{{$val->ref_id}}" 
                                           data-batchno="{{$val->batch_no}}" 
                                           data-startdate="{{$val->start_date}}"
                                           data-enddate="{{$val->end_date}}"
                                           data-proposaldate="{{$val->proposal_end_date}}"
                                           data-standard="{{@$standard[$val->standard]}}"
                                           data-trainingtype="{{@$trainingtype[$val->training_type]}}"
                                           data-sessiondetails="{{$val->session_details}}"
                                           data-mode="{{$trainingmode[$val->mode]}}"
                                           data-location="{{$address}}"
                                           data-effort="{{$val->effort_tentative}}"
                                           data-localtravel="{{$val->local_travel_allowance}}"
                                           data-localstay="{{$val->local_stay_allowance}}"
                                           data-airfare="{{$val->air_fare}}"
                                           data-feerange="{{$val->fee_range}}"
                                           data-query="{{$val->query}}"
                                           onclick="trainingDetails(this)" style="cursor: pointer;">
                                        <i class="fa fa-eye" title="View Detail" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-5">
                        <div class="dataTables_info" id="res-config_info" role="status" aria-live="polite">
                            @if(@$traininglist->total())
                            Showing 
                            {{(($traininglist->currentPage()-1) * $traininglist->perPage())+1}}
                            to 
                            {{(($traininglist->currentPage()-1) * $traininglist->perPage())+$traininglist->count() }}

                            of {{$traininglist->total()}} entries
                            @else
                            No Record Found !!!
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="res-config_paginate">
                            <ul class="pagination">
                                <li class="paginate_button page-item ">
                                    {{$traininglist->links()}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--table-->
                
                <!---details modal-->
                <div class="modal fade" id="proposalModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title text-center" id="refid" style="font-weight: bold;"></h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12 mb-3">
                                        <strong>{{trans('adminlabel.detailmodal.assignmentdetails')}}</strong><br>
                                        <span id="sessiondetails"></span>
                                    </div>
                                    <div class="col-md-6 border-right">
                                        <strong>{{trans('adminlabel.detailmodal.batchno')}}</strong><span id="batchno"></span><br>
                                        <strong>{{trans('adminlabel.detailmodal.mode')}}</strong><span id="mode"></span><br>
                                        <strong>{{trans('adminlabel.detailmodal.standard')}}</strong><span id="standard"></span><br>
                                        <strong>{{trans('adminlabel.detailmodal.mode')}}</strong><span id="batchno"></span><br>
                                        <strong>{{trans('adminlabel.detailmodal.mode')}}</strong><span id="batchno"></span><br>
                                        <strong>{{trans('adminlabel.detailmodal.mode')}}</strong><span id="batchno"></span><br>
                                        <strong>{{trans('adminlabel.detailmodal.mode')}}</strong><span id="batchno"></span><br>
                                    </div>
                                    <div class="col-md-6">
                                        
                                    </div>
                                </div>
                                <br>
                                <div><strong></strong>
                                    <span class="text-dark"></span>
                                </div>
                                <br>
                                <strong></strong>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                <!---details modal-->
                
            </div>
        </div>
    </div>
</div>

<script>
    function trainingDetails(e) {
        var refid = $(e).attr('data-refid');
        var batchno = $(e).attr('data-batchno');
        var startdate = $(e).attr('data-startdate');
        var enddate = $(e).attr('data-enddate');
        var proposaldate = $(e).attr('data-proposaldate');
        var standard = $(e).attr('data-standard');
        var trainingtype = $(e).attr('data-trainingtype');
        var sessiondetails = $(e).attr('data-sessiondetails');
        var mode = $(e).attr('data-mode');
        var location = $(e).attr('data-location');
        var effort = $(e).attr('data-effort');
        var localtravel = $(e).attr('data-localtravel');
        var localstay = $(e).attr('data-localstay');
        var airfare = $(e).attr('data-airfare');
        var feerange = $(e).attr('data-feerange');
        var query = $(e).attr('data-query');
        
        $("#refid").html(refid);
        $("#sessiondetails").html(sessiondetails);
        $("#batchno").html(batchno);
    }
</script>