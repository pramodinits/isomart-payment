@extends('adminlte::master')

@section('title', ': Assignment Detail')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')

<?php
@$typelist = config('selecttype.trainingtypelist'); 
@$mode = config('selecttype.trainingmode'); 
@$standard = config('standards.standardlist');

$now = Carbon\Carbon::now()->format('Y-m-d'); // or your date as well
$end_date = Carbon\Carbon::parse(@$trainingdetails->proposal_end_date);
$now_date = Carbon\Carbon::parse($now);
$daysleft = $now_date->diffInDays($end_date, false);
?>
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--user content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
                                <div class="card-header">
                                    <h4>{{trans('adminlabel.auditdetail.assignmenttitle')}}</h4>
                                </div>
                                <div class="card-block">
                                    <!---top details-->
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="col-md-12 mb40">
                                                    <div class="details mt-4 border-right">
                                                        <h5 class="profile-no">{{config('assignment.refid') . @$trainingdetails->ref_id}}
                                                            <!--- assignment status for send bid--->
                                                            <small class="text-success">
                                                                @if(@$trainingdetails->assignment_status == 2)
                                                                <span class="text-danger">{{trans('formlabel.auditdetail.draft')}}</span>
                                                                @elseif(@$userdetails[0]->approval_status == 1)
                                                                {{trans('formlabel.auditdetail.awarded')}}
                                                                @elseif(@$daysleft < 0) 
                                                                <span class="text-danger">{{trans('formlabel.auditdetail.closed')}}</span>
                                                                @elseif(@$daysleft == 0)
                                                                {{trans('formlabel.auditdetail.openfortoday')}}
                                                                @else
                                                                {{config('assignment.assignmentopen') . @$daysleft . config('assignment.assignmentdaysleft')}} 
                                                                @endif
                                                            </small>
                                                            <!--- assignment status for send bid--->
                                                        </h5>
                                                        <div>{{@$trainingdetails->batch_no}} 
                                                            @if(@$userdetails[0]->approval_status == 0)
                                                            ({{trans('formlabel.auditdetail.proposals')}}{{@$countTotalBid}})
                                                            @endif
                                                        </div>
                                                        <div>{{trans('formlabel.auditdetail.postedby')}}<span class="text-primary"><b>{{ucfirst(@$userdetails[0]->fname) . " " . ucfirst(@$userdetails[0]->lname)}}</b></span> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="btnsection mt-2 pull-right">
                                                    <h6 class="text-center">{{trans('formlabel.auditdetail.budget')}}</h6>
                                                    <div class="buget-price">
                                                        {{config('assignment.price') . " ". @$trainingdetails->fee_range}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!---top details-->
                                    
                                    <!--below details-->
                                    <div class="container">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="">
                                                    <p class="text-grey" style="font-size:1.3rem;">{{@$trainingdetails->session_details}}</p>
                                                    @if(@$trainingdetails->mode == 1)
                                                    <h6 class="text-dark">{{trans('formlabel.auditdetail.assignmentlocation')}}</h6>
                                                    <address>
                                                        @foreach(@$locationname as $key=>$val)
                                                        <span class="locationCity">
                                                            <i class="fa fa-map-marker pr-1 text-danger" aria-hidden="true"></i>
                                                            {{$val->city_name . ", " . $val->stateName . ", " . $val->countryName . " (Effort- " . $val->effort . " days)"}}
                                                            @if($val->whomtoassign_id)
                                                            <span class="text-success">Awarded</span>
                                                            @endif
                                                        </span>&nbsp;
                                                        @endforeach
                                                    </address>
                                                    @endif
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-md-6 border-right">
                                                            <div class="inforamation-section">
                                                                <div>{{trans('formlabel.trainingdetail.standard')}}<span class="text-dark">{{@$trainingdetails->standard ? @$standard[@$trainingdetails->standard] : "N/A" }}</span></div>
                                                                <div>{{trans('formlabel.trainingdetail.efforts')}}<span class="text-dark">{{@$trainingdetails->effort_tentative}}</span></div>
                                                                <div>{{trans('formlabel.trainingdetail.trainingtype')}}<span class="text-dark">{{$typelist[@$trainingdetails->training_type]}}</span></div>
                                                                <div>{{trans('formlabel.trainingdetail.trainingmode')}}<span class="text-dark">{{$mode[@$trainingdetails->mode]}}</span></div>
                                                                <div>{{trans('formlabel.trainingdetail.startdate')}}<span class="text-dark">{{Carbon\Carbon::parse(@$trainingdetails->start_date)->format(config('assignment.dateformat'))}}</span> </div>
                                                                <div>{{trans('formlabel.trainingdetail.enddate')}}<span class="text-dark">{{Carbon\Carbon::parse(@$trainingdetails->end_date)->format(config('assignment.dateformat'))}}</span> </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <div class="inforamation-section">
                                                                <div>{{trans('formlabel.auditdetail.localtravel')}} 
                                                                    <span class="text-dark">
                                                                        <i style="font-size: 1rem;" class="{{@$trainingdetails->local_travel_allowance == "1" ? "text-success" : "text-danger" }} fa fa-car" title="Local Travel Allowance" aria-hidden="true"></i>
                                                                    </span> 
                                                                </div>
                                                                <div>{{trans('formlabel.auditdetail.localstay')}}<span class="text-dark">
                                                                        <i style="font-size: 1.2rem;" class="{{@$trainingdetails->local_stay_allowance == "1" ? "text-success" : "text-danger" }} fa fa-home" title="Local Stay Allowance" aria-hidden="true"></i>
                                                                    </span> </div>
                                                                <div>{{trans('formlabel.auditdetail.airfare')}}<span class="text-dark">
                                                                        <i style="font-size: 1.2rem;" class="{{@$trainingdetails->air_fare == "1" ? "text-success" : "text-danger" }} fa fa-plane" title="Air Fare" aria-hidden="true"></i>
                                                                    </span> </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--below details-->
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--user content--->

                </div>
            </div>
        </div>
    </div>
</div>

@stop