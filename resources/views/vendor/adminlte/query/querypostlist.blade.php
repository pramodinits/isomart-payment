@extends('adminlte::master')

@section('title', ': User Listing')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')

@php
$user_type = config('selecttype.usertypelist');
@endphp

<style>
    .searchform .col-md-2 {
        padding-right: 0px !important;
    }
</style>
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--user content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{trans('messagelabel.query.listingheader')}}</h5>
                                </div>
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <div class="dt-responsive table-responsive">
                                            
                                            <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                                                <!--table-->
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table id="res-config" class="table table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 100%;">
                                                            <thead>
                                                                <tr role="row">
                                                                    <th rowspan="1" colspan="1" >#</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 90px;" aria-sort="ascending" aria-label="User name">{{trans('adminlabel.userlisting.rowusername')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 88px;" aria-label="Title">{{trans('messagelabel.query.title')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 191px;" aria-label="Type">{{trans('messagelabel.query.type')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 87px;" aria-label="Section/Sector">{{trans('messagelabel.query.section/sector')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 35px;" aria-label="Start Date">{{trans('messagelabel.training.start_date')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="End Date">{{trans('messagelabel.training.end_date')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Location">{{trans('messagelabel.advt.location')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Effort">{{trans('messagelabel.query.effort')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Description">{{trans('messagelabel.query.description')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Looking For">{{trans('messagelabel.query.lookingfor')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="" aria-label="Travel info">{{trans('messagelabel.query.travelinfo')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Budget info">{{trans('messagelabel.query.budgetinfo')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Perday Charge">{{trans('messagelabel.query.perdaycharge')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Radious">{{trans('messagelabel.query.preferradiouslocation')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Status">{{trans('messagelabel.query.status')}}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach(@$querypostinfo as $key => $val)
                                                                <tr role="row" class="odd">
                                                                    <td>
                                                                        {{$key+1}}
                                                                    </td>
                                                                    <td tabindex="0" class="sorting_1">{{@$userdetails[@$val->user_id]}}</td>
                                                                    <td>{{@$val->title}}</td>
                                                                    <td>{{@$codetype[@$val->codetype]}}</td>
                                                                    <td>{{@$codeDetail[@$val->sic_code]}}</td>
                                                                    <td>{{Carbon\Carbon::parse(@$val->startDate)->format(config('assignment.dateformat'))}}</td>
                                                                    <td>{{Carbon\Carbon::parse(@$val->endDate)->format(config('assignment.dateformat'))}}</td>
                                                                    <td>{{@$val->city_name}},{{@$state[@$val->states]}},{{@$country[@$val->country]}}</td>
                                                                    <td>{{@$val->effort_mandays}}</td>
                                                                    <td style=""> {{@$val->description}}</td>
                                                                    <td>
                                                                        @if(@$val->lookingfor == 1)
                                                                        {{"Seeker"}} @else {{"Provider"}} @endif
                                                                    </td>
                                                                    <td wrap>{{@$val->travelinfo}}</td>
                                                                    <td wrap>{{@$val->budgetinfo}}</td>
                                                                    <td>{{@$val->perday_charge}}</td>
                                                                    <td wrap>{{@$val->radious}}</td>
                                                                    <td>
                                                                        @if(@$val->status == 1)
                                                                        {{"Active"}} @else {{"Deactive"}} @endif
                                                                    </td>
                                                                    
                                                                    
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                                <!--table-->
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-5">
                                                        <div class="dataTables_info" id="res-config_info" role="status" aria-live="polite">
                                                            @if(@$querypostinfo->total())
                                                            Showing 
                                                            {{((@$querypostinfo->currentPage()-1) * @$querypostinfo->perPage())+1}}
                                                            to 
                                                            {{((@$querypostinfo->currentPage()-1) * @$querypostinfo->perPage())+@$querypostinfo->count() }}

                                                            of {{@$querypostinfo->total()}} entries
                                                            @else
                                                            No Record Found !!!
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-7">
                                                        <div class="dataTables_paginate paging_simple_numbers" id="res-config_paginate">
                                                            <ul class="pagination">
                                                                <li class="paginate_button page-item ">
                                                                    {{@$querypostinfo->links()}}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--user content--->

                </div>
            </div>
        </div>
    </div>
</div>

@stop


