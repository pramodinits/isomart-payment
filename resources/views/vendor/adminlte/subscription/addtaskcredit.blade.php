@extends('adminlte::master')

@section('title', ': Add Task Credit')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')

@php
$task_list = config('subscription.tasknamelist');
@endphp
<style>
    .form-group i.form-control-feedback {
        display: none !important;
    }
    .form-control {
        display: inline-block;
        /*width: 30%;*/
    }
    .row label {
        width: 30%;
    }
    .form-group {
        display: inline-block;
    }
</style>
@include('includes.message')
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--user content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{trans('adminlabel.header.menusubscription')}}</h5>
                                </div>
                                
                                <!--tab section-->
                                <div class="col-lg-12 mb40">
                                <div id="tabAssign" class="mt-2">
                                    <ul class="nav nav-tabs tabs-default mb30" role="tablist bg-light">
                                        <li class="nav-item " role="presentation">
                                            <a class="nav-link custom-tabs active show" href="#addtask" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">{{trans('adminlabel.taskcreditadd.taskaddheader')}}</a>
                                        </li>
                                        <li class="nav-item " role="presentation">
                                            <a class="nav-link custom-tabs" href="{{asset('admin/addPackage')}}">{{trans('adminlabel.packageadd.packageheader')}}</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade show active" id="addtask">
                                            <div class="col-xs-12 col-sm-12 card-block">
                                                <form name="credittask"  id="credittask" method="post" action="{{ asset('admin/insertCreditTask') }}">
                                                    {!! csrf_field() !!}
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-12">
                                                            <div class="mb-2">
                                                                <label>{{trans('adminlabel.taskcreditadd.addassignment')}}</label>
                                                                <div class="form-group" id="shape-div">
                                                                    <input type="hidden" name="task_name[]" class="form-control" value="{{trans('adminlabel.taskcreditadd.addassignment')}}" onkeypress="return isNumberKey(event)">
                                                                    <input type="text" name="task_point[]" class="form-control" value="{{@$taskcreditlist[0]->task_point}}" placeholder="{{trans('adminlabel.taskcreditadd.taskpoint')}}" onkeypress="return isNumberKey(event)">
                                                                    <input type="hidden" name="id_update[]" class="form-control" value="{{@$taskcreditlist[0]->id}}">
                                                                    <div id="shape-error"></div>
                                                                </div>
                                                            </div>
                                                            <div class="mb-2">
                                                                <label>{{trans('adminlabel.taskcreditadd.addproposal')}}</label>
                                                                <div class="form-group">
                                                                    <input type="hidden" name="task_name[]" class="form-control" value="{{trans('adminlabel.taskcreditadd.addproposal')}}" onkeypress="return isNumberKey(event)">
                                                                    <input type="text" name="task_point[]" class="form-control" value="{{@$taskcreditlist[1]->task_point}}" placeholder="{{trans('adminlabel.taskcreditadd.taskpoint')}}" onkeypress="return isNumberKey(event)">
                                                                    <input type="hidden" name="id_update[]" class="form-control" value="{{@$taskcreditlist[1]->id}}">
                                                                    <div id="shape-error"></div>
                                                                </div>  
                                                            </div>
                                                            <div class="mb-2">
                                                                <label>{{trans('adminlabel.taskcreditadd.addadvertisement')}}</label>
                                                                <div class="form-group">
                                                                    <input type="hidden" name="task_name[]" class="form-control" value="{{trans('adminlabel.taskcreditadd.addadvertisement')}}" onkeypress="return isNumberKey(event)">
                                                                    <input type="text" name="task_point[]" class="form-control" value="{{@$taskcreditlist[2]->task_point}}" placeholder="{{trans('adminlabel.taskcreditadd.taskpoint')}}" onkeypress="return isNumberKey(event)">
                                                                    <input type="hidden" name="id_update[]" class="form-control" value="{{@$taskcreditlist[2]->id}}">
                                                                    <div id="shape-error"></div>
                                                                </div>
                                                            </div>
                                                            <div class="mb-2">
                                                                <label>{{trans('adminlabel.taskcreditadd.addquery')}}</label>
                                                                <div class="form-group">
                                                                    <input type="hidden" name="task_name[]" class="form-control" value="{{trans('adminlabel.taskcreditadd.addquery')}}" onkeypress="return isNumberKey(event)">
                                                                    <input type="text" name="task_point[]" class="form-control" value="{{@$taskcreditlist[3]->task_point}}" placeholder="{{trans('adminlabel.taskcreditadd.taskpoint')}}" onkeypress="return isNumberKey(event)">
                                                                    <input type="hidden" name="id_update[]" class="form-control" value="{{@$taskcreditlist[3]->id}}">
                                                                    <div id="shape-error"></div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <button type="submit" class="hide-btn btn btn-primary text-center btn-md mt-2" >{{trans('adminlabel.taskcreditadd.savetaskcredit')}}</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                </div>                                            

                            </div>
                        </div>
                    </div>
                    <!--user content--->

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('/')}}js/bootstrapValidator.js"></script>
@if(@$advtinfo)
<script>
function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    
    $('#advertisement').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "advt[shape]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "advt[location]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            }
        }
    })

            .on('success.form.bv', function (e) {
//                $('#confirmModal').show();
//                $("#content-form").html($("#assignment-request").serialize());
                return true;
            });
    
</script>
@else
<script>
    $('#credittask').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "task_point[]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            }
        }
    })

            .on('success.form.bv', function (e) {
//                $('#confirmModal').show();
                return true;
            });
    
</script>
@endif
<script>
    function changenote(shape) {
        var url = "{{url('/admin/changeNote')}}";
            $.post(url, {'shape': shape, '_token': "<?= csrf_token(); ?>"}, function (data) {
            $("#note").html(data);
            
            });
        } 
</script>
@if(@$advtinfo)
<script>
    $(document).ready(function(){
    $("#note").html("<div style='font-weight:bold;margin-bottom: 10px;'>NOTE:Banner size should be "+{{@$shapeinfo->width}}+" * "+{{@$shapeinfo->height}}+"</div><input type='hidden' class='form-control' name='img-height' id='img-height' value="+{{@$shapeinfo->height}}+"><input type='hidden' class='form-control' name='img-width'  id='img-width' value="+{{@$shapeinfo->width}}+">");
});
</script>
@endif
<!--<script type="text/javascript">
    function imgSize()
    {
        var myImg = document.getElementById("sky");
        var currWidth = myImg.offsetWidth;
        var currHeight = myImg.clientHeight;
        var imgheight = $("#img-height").val();
        var imgwidth = $("#img-width").val();
       alert("Current width=" + currWidth + ", " + "Original width=" + imgwidth);
       alert("Current height=" + currHeight + ", " + "Original height=" + imgheight);
        if($("#shape").val() == "")
        {
            $("#shape-div").addClass("has-error");
            $("#shape-error").html("<small style='' class='help-block' data-bv-validator='notEmpty' data-bv-for='advt[shape]' data-bv-result='INVALID'>This field is required.</small>");
            $(".sky").val("");
            return false;
        }
        else if((currWidth > imgwidth) || (currHeight > imgheight))
        {
            $(".sky").val("");
            $("#image-div").addClass("has-error");
            $("#shape-image").delay(2000).show().fadeOut('slow').html("<small style='' class='help-block' data-bv-validator='notEmpty' data-bv-for='image' data-bv-result='INVALID'>Banner size should not be greater than "+ imgwidth + " * " + imgheight +"</small>");
            return false;
            
        } 
    }
</script>-->

<script type="text/javascript">
function imgSize()
    {
        //Get reference of FileUpload.
        var fileUpload = $("#sky")[0];
        
        //Check whether the file is valid Image.
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        //if (regex.test(fileUpload.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if($("#shape").val() == "")
            {
                $("#shape-div").addClass("has-error");
                $("#shape-error").html("<small style='' class='help-block' data-bv-validator='notEmpty' data-bv-for='advt[shape]' data-bv-result='INVALID'>This field is required.</small>");
                $(".sky").val("");
                return false;
            }
            else if (typeof (fileUpload.files) != "undefined") {
                //Initiate the FileReader object.
                var reader = new FileReader();
                //Read the contents of Image File.
                reader.readAsDataURL(fileUpload.files[0]);
                reader.onload = function (e) {
                    //Initiate the JavaScript Image object.
                    var image = new Image();
                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;
                    image.onload = function () {
                        //Determine the Height and Width.
                        var height = this.height;
                        var width = this.width;
                        var imgheight = $("#img-height").val();
                        var imgwidth = $("#img-width").val();
                        if (height > imgheight || width > imgwidth) {
                            $(".sky").val("");
                            $("#image-div").addClass("has-error");
                            $("#shape-image").delay(2000).show().fadeOut('slow').html("<small style='' class='help-block' data-bv-validator='notEmpty' data-bv-for='image' data-bv-result='INVALID'>Banner size should not be greater than "+ imgwidth + " * " + imgheight +"</small>");
                            //$(".hide-btn").prop('disabled', true);
			    if($("#id_update").val())
                            {
                            setTimeout(function () {
                                $("#image-div").removeClass("has-error");
                            }, 2000);
                            }
                            return false;
                        } else {
                            $("#image-div").removeClass("has-error");
                            $("#image-div").addClass("has-success");
//                        alert("Uploaded image has valid Height and Width.");
                            //$(".hide-btn").prop('disabled', false);
                            return true;
                        }
                    };
                }
            } else {
                alert("This browser does not support HTML5.");
                return false;
            }
//        } else {
//            $("#image-div").addClass("has-error");
//            $("#shape-image").delay(2000).show().fadeOut('slow').html("<small style='' class='help-block' data-bv-validator='notEmpty' data-bv-for='image' data-bv-result='INVALID'>Please upload valid image file.</small>");
//            return false;
//        }
    }
</script>
@stop


