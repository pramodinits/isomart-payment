@extends('adminlte::master')

@section('title', ': Add Package')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')

@php
$packagetype = config('subscription.packagelist');
$packageduration = config('subscription.packageduration');
@endphp
<style>
    .form-group i.form-control-feedback {
        display: none !important;
    }
    .icon-border-box {
    border: 1px solid #eee;
    padding: 5px;
}
    .plan {
    background-color: #fbfbfb;
    border-radius: 0px;
    color: #333;
    margin-bottom: 35px;
    position: relative;
    z-index: 1;
}
    .plan .plan-header {
    padding: 1em 0 0.5em 0em;
    border-bottom: 1px solid #dee2e6;
    text-align: center;
}
.plan .plan-header h1 {
    font-weight: 700;
    color: #5bc0de !important;
}
.plan h6 {
    color: #aeaeae;
    font-weight: 400;
}
.plan ul {
    padding: 0.7em 0 0em 1.5em;
    margin: 0px;
}
.plan ul li {
    padding: 5px 0;
    font-size: 15px;
    color: #999;
}
.plan ul li i {
    color: #4782d3;
    display: inline-block;
    margin-right: 10px;
}
.pricing-table {
    margin: 10px 0;
padding: 0 15px;
width: 50%;
/*display: flex;
overflow-x: scroll;*/
}
.package-lists {
    display: flex;
overflow-x: scroll;
}
</style>
@include('includes.message')
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--user content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{trans('adminlabel.header.menusubscription')}}</h5>
                                </div>
                                
                                <!--tab section-->
                                <div class="col-lg-12 mb40">
                                <div id="tabAssign" class="mt-2">
                                    <ul class="nav nav-tabs tabs-default mb30" role="tablist bg-light">
                                        <li class="nav-item " role="presentation">
                                            <a class="nav-link custom-tabs" href="{{asset('admin/addTaskcredit')}}">{{trans('adminlabel.taskcreditadd.taskaddheader')}}</a>
                                        </li>
                                        <li class="nav-item " role="presentation">
                                            <a class="nav-link custom-tabs active show" href="#addpackage" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">{{trans('adminlabel.packageadd.packageheader')}}</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade show active" id="addpackage">
                                            <div class="row">
                                            <div class="col-xs-4 col-sm-4 card-block border-right">
                                                <form name="package" id="package" method="post" action="{{ asset('admin/insertPackage') }}">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="id_update" id="id_update" value="{{@$packageinfo->id}}">
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-12">
                                                                <label>{{trans('adminlabel.packageadd.packagetype')}}</label>
                                                                <div class="form-group">
                                                                    <select class="form-control" name="package[package_type]" id="package_type">
                                                                        <option value="">{{trans('adminlabel.packageadd.typeselect')}}</option>
                                                                        @foreach(@$packagetype as $k=>$v)
                                                                        <option value="{{@$k}}" @if(@$k == @$packageinfo->package_type) selected @endif>{{@$v}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <label>{{trans('adminlabel.packageadd.packagename')}}</label>
                                                                <div class="form-group">
                                                                    <input type="text" name="package[package_name]" class="form-control" value="{{@$packageinfo->package_name}}" placeholder="">
                                                                </div>
                                                                <label>{{trans('adminlabel.packageadd.packageprice')}}</label>
                                                                <div class="form-group">
                                                                    <input type="text" name="package[package_price]" class="form-control" value="{{@$packageinfo->package_price}}"  onkeypress="return isNumberKey(event)">
                                                                </div>
                                                                <label>{{trans('adminlabel.packageadd.packageduration')}}</label>
                                                                <div class="form-group">
                                                                    <select class="form-control" name="package[package_duration]" id="package_duration">
                                                                        <option value="">{{trans('adminlabel.packageadd.durationselect')}}</option>
                                                                        @foreach(@$packageduration as $k=>$v)
                                                                        <option value="{{@$k}}" @if(@$k == @$packageinfo->package_duration) selected @endif>{{@$v}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <label>{{trans('adminlabel.packageadd.packagepoints')}}</label>
                                                                <div class="form-group">
                                                                    <input type="text" name="package[credit_points]" class="form-control" value="{{@$packageinfo->credit_points}}" placeholder="" onkeypress="return isNumberKey(event)">
                                                                </div>
                                                            <div>
                                                                <button type="submit" class="hide-btn btn btn-primary text-center btn-md mt-2" >{{trans('adminlabel.packageadd.packagesave')}}</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 card-block pricing-table align-items-center">
                                                    <div class="col-md-12 col-lg-12 text-center mb-3 mt-3">
                                                    <h4>{{trans('adminlabel.packageadd.packagelist')}}</h4>
                                                    <hr>
                                                </div>
                                                <div class="col-md-12 col-lg-12 package-lists">
                                                @if(@$package_list)
                                                @foreach(@$package_list as $key => $val)
                                                <div class="mr-2 col-lg-3 col-md-6 plan icon-box icon-box-center icon-border-box bg-white">
                                                    <div class="plan-header">
                                                        <h4>{{$val->package_name}}</h4>
                                                        <h1 class="text-info">{{config('assignment.price') . " ". $val->package_price}} </h1>
                                                        <h6>{{$packagetype[$val->package_type]}}</h6>
                                                    </div>
                                                    <ul class="list-unstyled">
                                                        <li><i class="fa fa-check-circle-o"></i> {{$packageduration[$val->package_duration]}}</li>
                                                        <li><i class="fa fa-check-circle-o"></i> {{$val->credit_points . " " . config('assignment.creditpoint')}}</li>
                                                    </ul>
                                                    <hr>
                                                    <div class="text-center">
                                                        <a href="{{ asset('admin/addPackage/'.@$val->id)}}" class="mr-2">
                                                        <i class="fa fa-edit" style="font-size: 1.3em;"></i>
                                                    </a>
                                                    <a href="javascript:void(0);" onclick="deletepackage('{{@$val->id}}')">
                                                    <i class="fa fa-trash text-danger" style="font-size: 1.3em;"></i>
                                                    </a>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="col-md-12 col-lg-12 text-center mb-3 mt-3">
                                                No data available!
                                                </div>
                                                @endif
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                </div>                                            

                            </div>
                        </div>
                    </div>
                    <!--user content--->

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('/')}}js/bootstrapValidator.js"></script>

<script>
function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    
    $('#package').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "package[package_type]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "package[package_name]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "package[package_price]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "package[package_duration]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "package[credit_points]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            }
        }
    })

            .on('success.form.bv', function (e) {
//                $('#confirmModal').show();
                return true;
            });
    
    
    function deletepackage(id) {
        var del = confirm('Are you sure to remove this?');
            if (del) {
            var url = "{{url('/admin/deletePackage')}}";
            $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                //alert(data);
            //location.reload();
            if(data == 'Success')
            {
                $("#msg").delay(3000).show().fadeOut('slow').html("<div class='alert alert-success-cust text-center' id='msg_alert_box'>Package Deleted Successfully!.</div>");
                location.reload();
            } 
            else if(data == 'Error')
            {
                $("#msg").delay(3000).show().fadeOut('slow').html("<div class='alert alert-success-cust text-center' id='msg_alert_box'>Failure in delete.</div>");
                location.reload();
            }
            });
        } else {
            return false;
        }
    }
</script>
@stop


