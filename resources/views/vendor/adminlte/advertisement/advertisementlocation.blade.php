@extends('adminlte::master')

@section('title', ': Assignment Listing')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')

@php
$user_type = config('selecttype.usertypelist');
@endphp
<style>
    .form-group i.form-control-feedback {
        display: none !important;
    }
</style>
@include('includes.message')
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--user content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Advertisement Location</h5>
                                </div>
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <div class="dt-responsive table-responsive">
                                            <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                                <!--table-->
                                                <div class="col-xs-12 col-sm-12">
                                                    <form name="advtloc" id="advtloc" method="post" action="{{ asset('admin/insertLocation') }}" enctype="multipart/form-data">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="id_update" id="id_update" value="{{@$locinfo->loc_id}}">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-12">
                                                            <label>{{trans('messagelabel.advt.name')}}</label>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="location[loc_name]" value="{{@$locinfo->loc_name}}">
                                                            </div>
                                                            <div>
                                                                <button type="submit" class="btn btn-primary text-center btn-md pull-right mt-2">{{trans('formlabel.postassignmenttraining.rowaction')}}</button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-12">

                                                        </div>
                                                    </div>

                                                    </form>
                                                </div>
                                                <div style="clear:both;"></div>
                                                <br/>
                                                <div class="col-xs-12 col-sm-12">
                                                    <table id="res-config" class="table table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 100%;">
                                                        <thead>
                                                            <tr role="row">
<!--                                                                <th rowspan="1" colspan="1" >#</th>-->
                                                                <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="User name">{{trans('messagelabel.advt.name')}}</th>
                                                                <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1"  aria-label="Status">{{trans('messagelabel.advt.action')}}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(count(@$loclist) > 0)
                                                            @foreach($loclist as $key => $val)
                                                            <tr role="row" class="odd" id="{{@$val->loc_id}}" class="documentcls{{@$val->loc_id}}">
<!--                                                                <td>
                                                                    {{$key+1}}
                                                                </td>-->
                                                                <td>{{@$val->loc_name}}</td>
                                                                <td>
                                                                    <a href="{{ asset('admin/advertisementLocation/'.@$val->loc_id)}}">
                                                                        <i class="fa fa-edit text-success" title="Edit"></i>
                                                                    </a>&nbsp;&nbsp;
                                                                    <a href="javascript:void(0);" onclick="deletelocation('{{@$val->loc_id}}')">
                                                                        <i class="fa fa-remove text-danger" aria-hidden="true" title="Delete"></i>
                                                                    </a>
                                                                </td>


                                                            </tr>
                                                            @endforeach
                                                            @else
                                                            <tr>
                                                                <td colspan="2">
                                                                    <div style="text-align: center;">No Record Found .. </div>
                                                                </td>
                                                                </tr>
                                                            @endif
                                                            <tr class="loc{{@$val->loc_id}}"></tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                            

                            </div>
                        </div>
                    </div>
                    <!--user content--->

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('/')}}js/bootstrapValidator.js"></script>
<script>
    $('#advtloc').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "location[loc_name]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            }
        }
    })

            .on('success.form.bv', function (e) {
//                $('#confirmModal').show();
//                $("#content-form").html($("#assignment-request").serialize());
                return true;
            });
    
</script>
<script>
    function deletelocation(id) {
        var del = confirm('Are you sure to remove this?');
            if (del) {
                $("#"+ id).fadeOut(400, function() {
                $("#"+ id).remove();
                if($('.documentcls'+id).length == 0)
                    {
                        $('.loc'+id).html("<td colspan='2'><div style='text-align:center;'>No Record Found..</div></td>");
                    }
            });
            var url = "{{url('/admin/deleteLocation')}}";
            $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                //alert(data);
            //location.reload();
            });
        } else {
            return false;
        }
    }
</script>
@stop


