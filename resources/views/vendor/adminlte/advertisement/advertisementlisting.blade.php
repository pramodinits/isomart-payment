@extends('adminlte::master')

@section('title', ': Assignment Listing')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')

@php
$user_type = config('selecttype.usertypelist');
@endphp
<style>
    .form-group i.form-control-feedback {
        display: none !important;
    }
    table.dataTable thead th, table.dataTable thead td{
        padding: 10px 10px  !important;
    }
</style>
@include('includes.message')
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--user content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Advertisement List</h5>
                                </div>
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <div class="dt-responsive table-responsive">
                                            <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                                <!--table-->
                                                
                                                <div class="col-xs-12 col-sm-12">
                                                    <div style="float:right;"><a href="{{ asset('admin/addAdvertisement/')}}" class="btn btn-primary text-center btn-sm mb-2">ADD</a></div>
                                                    
                                                    <div id="msg"></div>
                                                    <table id="res-config" class="table table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 100%;">
                                                        <thead>
                                                            <tr role="row">
<!--                                                                <th rowspan="1" colspan="1" >#</th>-->
                                                                <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Shape">{{trans('messagelabel.advt.shape')}}</th>
                                                                <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1"  aria-label="Location">{{trans('messagelabel.advt.location')}}</th>
                                                                <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1"  aria-label="Advertisement Image">{{trans('messagelabel.advt.advertisementimage')}}</th>
                                                                <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1"  aria-label="Status">{{trans('messagelabel.advt.status')}}</th>
                                                                <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1"  aria-label="Action">{{trans('messagelabel.advt.action')}}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(count(@$advtlist) > 0)
                                                            @foreach(@$advtlist as $key => $val)
                                                            <tr role="row" class="odd documentcls" id="{{@$val->id}}" >
<!--                                                                <td>
                                                                    {{$key+1}}
                                                                </td>-->
                                                                <td>{{@$shapelist[@$val->shape]}}</td>
                                                                <td>{{@$locationlist[@$val->location]}}</td>
                                                                <td><img src="{{asset('training_image')}}/{{@$val->image}}"  width="100" /></td>
                                                                <td>{{@$status[@$val->status]}}</td>
                                                                <td>
                                                                    <a href="{{ asset('admin/addAdvertisement/'.@$val->id)}}">
                                                                        <i class="fa fa-edit text-success" title="Edit"></i>
                                                                    </a>&nbsp;&nbsp;
                                                                    <a href="javascript:void(0);" onclick="deleteadvertisement('{{@$val->id}}')">
                                                                        <i class="fa fa-remove text-danger" aria-hidden="true" title="Delete"></i>
                                                                    </a>
                                                                </td>


                                                            </tr>
                                                            @endforeach
                                                            @else
                                                            <tr>
                                                                <td colspan="7">
                                                                    <div style="text-align: center;">No Record Found .. </div>
                                                                </td>
                                                                </tr>
                                                            @endif
                                                            <tr class="advt{{@$val->id}}"></tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                            

                            </div>
                        </div>
                    </div>
                    <!--user content--->

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('/')}}js/bootstrapValidator.js"></script>
<script>
    $('#advtloc').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "location[loc_name]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            }
        }
    })

            .on('success.form.bv', function (e) {
//                $('#confirmModal').show();
//                $("#content-form").html($("#assignment-request").serialize());
                return true;
            });
    
</script>
<script>
    function deleteadvertisement(id) {
        var del = confirm('Are you sure to remove this?');
            if (del) {
                $("#"+ id).fadeOut(400, function() {
                $("#"+ id).remove();
                if($('.documentcls').length == 0)
                    {
                        $('.advt'+id).html("<td colspan='7'><div style='text-align:center;'>No Record Found..</div></td>");
                    }
            });
            var url = "{{url('/admin/deleteAdvertisement')}}";
            $.post(url, {'id': id, '_token': "<?= csrf_token(); ?>"}, function (data) {
                //alert(data);
            //location.reload();
            if(data == 'Success')
            {
                $("#msg").delay(3000).show().fadeOut('slow').html("<div class='alert alert-success-cust text-center' id='msg_alert_box'>Advertisement Deleted Successfully!.</div>");
            } 
            else if(data == 'Error')
            {
                $("#msg").delay(3000).show().fadeOut('slow').html("<div class='alert alert-success-cust text-center' id='msg_alert_box'>Failure in delete.</div>");
            }
            });
        } else {
            return false;
        }
    }
</script>
@stop


