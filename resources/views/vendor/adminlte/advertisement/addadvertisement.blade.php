@extends('adminlte::master')

@section('title', ': Assignment Listing')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')

@php
$user_type = config('selecttype.usertypelist');
@endphp
<style>
    .form-group i.form-control-feedback {
        display: none !important;
    }
</style>
@include('includes.message')
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--user content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Banner Advertisement</h5>
                                </div>
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <div class="dt-responsive table-responsive">
                                            <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                                <!--table-->
                                                <div class="col-xs-12 col-sm-12">
                                                    <form name="advertisement"  id="advertisement" method="post" action="{{ asset('admin/insertAdvertisement') }}" enctype="multipart/form-data">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="id_update" id="id_update" value="{{@$advtinfo->id}}">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-12">
                                                            <label>{{trans('messagelabel.advt.shape')}}</label>
                                                            <div class="form-group" id="shape-div">
                                                                <select class="form-control" name="advt[shape]" id="shape" onchange="changenote(this.value);">
                                                                    <option value="">Select Shape</option>
                                                                    @foreach(@$shapelist as $k=>$v)
                                                                    <option value="{{@$k}}" @if(@$k == @$advtinfo->shape) selected @endif>{{@$v}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <div id="shape-error"></div>
                                                            </div>
                                                            <label>{{trans('messagelabel.advt.location')}}</label>
                                                            <div class="form-group">
                                                                <select class="form-control" name="advt[location]" id="location">
                                                                    <option value="">Select Location</option>
                                                                    @foreach(@$locationlist as $k=>$v)
                                                                    <option value="{{@$k}}" @if(@$k == @$advtinfo->location) selected @endif>{{@$v}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <label>{{trans('messagelabel.advt.advertisementimage')}}</label>
                                                            <div class="form-group" id="image-div">
                                                                <input type="file" class="form-control sky" id="sky" name="image" accept="image/*" onchange="imgSize();">
                                                                @if(@$advtinfo->image)
                                                                <br/>
                                                                <input type="hidden" name='photo_hid' value="{{@$advtinfo->image}}" class="form-control" />
                                                                <img src="{{asset('training_image')}}/{{@$advtinfo->image}}" height="50" width="50" />
                                                                @endif
                                                                <div id="shape-image"></div>
                                                            </div>
                                                            <div id="note"></div>
                                                            
                                                            
                                                            <label>{{trans('formlabel.postassignmenttraining.status')}}</label>
                                                            <div class="form-group">
                                                                <select class="form-control" name="advt[status]">
                                                                    @foreach(@$status as $k=>$v)
                                                                    <option value="{{@$k}}" @if(@$k == @$advtinfo->status) selected @endif>{{@$v}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            
                                                            <div>
                                                                <button type="submit" class="hide-btn btn btn-primary text-center btn-md pull-right mt-2" >{{trans('formlabel.postassignmenttraining.rowaction')}}</button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-12">

                                                        </div>
                                                    </div>

                                                    </form>
                                                </div>
                                                <div style="clear:both;"></div>
                                               


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                            

                            </div>
                        </div>
                    </div>
                    <!--user content--->

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('/')}}js/bootstrapValidator.js"></script>
@if(@$advtinfo)
<script>
    $('#advertisement').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "advt[shape]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "advt[location]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            }
        }
    })

            .on('success.form.bv', function (e) {
//                $('#confirmModal').show();
//                $("#content-form").html($("#assignment-request").serialize());
                return true;
            });
    
</script>
@else
<script>
    $('#advertisement').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "advt[shape]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "advt[location]": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            },
            "image": {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            }
        }
    })

            .on('success.form.bv', function (e) {
//                $('#confirmModal').show();
                if($("#sky").val() == "")
                {
                    $("#image-div").addClass("has-error");
                    $("#shape-image").delay(2000).show().fadeOut('slow').html("<small style='' class='help-block' data-bv-validator='notEmpty' data-bv-for='image' data-bv-result='INVALID'>Banner size should not be greater than "+ imgwidth + " * " + imgheight +"</small>");
                    return false;
                }
                return true;
            });
    
</script>
@endif
<script>
    function changenote(shape) {
        var url = "{{url('/admin/changeNote')}}";
            $.post(url, {'shape': shape, '_token': "<?= csrf_token(); ?>"}, function (data) {
            $("#note").html(data);
            
            });
        } 
</script>
@if(@$advtinfo)
<script>
    $(document).ready(function(){
    $("#note").html("<div style='font-weight:bold;margin-bottom: 10px;'>NOTE:Banner size should be "+{{@$shapeinfo->width}}+" * "+{{@$shapeinfo->height}}+"</div><input type='hidden' class='form-control' name='img-height' id='img-height' value="+{{@$shapeinfo->height}}+"><input type='hidden' class='form-control' name='img-width'  id='img-width' value="+{{@$shapeinfo->width}}+">");
});
</script>
@endif
<!--<script type="text/javascript">
    function imgSize()
    {
        var myImg = document.getElementById("sky");
        var currWidth = myImg.offsetWidth;
        var currHeight = myImg.clientHeight;
        var imgheight = $("#img-height").val();
        var imgwidth = $("#img-width").val();
       alert("Current width=" + currWidth + ", " + "Original width=" + imgwidth);
       alert("Current height=" + currHeight + ", " + "Original height=" + imgheight);
        if($("#shape").val() == "")
        {
            $("#shape-div").addClass("has-error");
            $("#shape-error").html("<small style='' class='help-block' data-bv-validator='notEmpty' data-bv-for='advt[shape]' data-bv-result='INVALID'>This field is required.</small>");
            $(".sky").val("");
            return false;
        }
        else if((currWidth > imgwidth) || (currHeight > imgheight))
        {
            $(".sky").val("");
            $("#image-div").addClass("has-error");
            $("#shape-image").delay(2000).show().fadeOut('slow').html("<small style='' class='help-block' data-bv-validator='notEmpty' data-bv-for='image' data-bv-result='INVALID'>Banner size should not be greater than "+ imgwidth + " * " + imgheight +"</small>");
            return false;
            
        } 
    }
</script>-->

<script type="text/javascript">
function imgSize()
    {
        //Get reference of FileUpload.
        var fileUpload = $("#sky")[0];
        
        //Check whether the file is valid Image.
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        //if (regex.test(fileUpload.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if($("#shape").val() == "")
            {
                $("#shape-div").addClass("has-error");
                $("#shape-error").html("<small style='' class='help-block' data-bv-validator='notEmpty' data-bv-for='advt[shape]' data-bv-result='INVALID'>This field is required.</small>");
                $(".sky").val("");
                return false;
            }
            else if (typeof (fileUpload.files) != "undefined") {
                //Initiate the FileReader object.
                var reader = new FileReader();
                //Read the contents of Image File.
                reader.readAsDataURL(fileUpload.files[0]);
                reader.onload = function (e) {
                    //Initiate the JavaScript Image object.
                    var image = new Image();
                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;
                    image.onload = function () {
                        //Determine the Height and Width.
                        var height = this.height;
                        var width = this.width;
                        var imgheight = $("#img-height").val();
                        var imgwidth = $("#img-width").val();
                        if (height > imgheight || width > imgwidth) {
                            $(".sky").val("");
                            $("#image-div").addClass("has-error");
                            $("#shape-image").delay(2000).show().fadeOut('slow').html("<small style='' class='help-block' data-bv-validator='notEmpty' data-bv-for='image' data-bv-result='INVALID'>Banner size should not be greater than "+ imgwidth + " * " + imgheight +"</small>");
                            //$(".hide-btn").prop('disabled', true);
			    if($("#id_update").val())
                            {
                            setTimeout(function () {
                                $("#image-div").removeClass("has-error");
                            }, 2000);
                            }
                            return false;
                        } else {
                            $("#image-div").removeClass("has-error");
                            $("#image-div").addClass("has-success");
//                        alert("Uploaded image has valid Height and Width.");
                            //$(".hide-btn").prop('disabled', false);
                            return true;
                        }
                    };
                }
            } else {
                alert("This browser does not support HTML5.");
                return false;
            }
//        } else {
//            $("#image-div").addClass("has-error");
//            $("#shape-image").delay(2000).show().fadeOut('slow').html("<small style='' class='help-block' data-bv-validator='notEmpty' data-bv-for='image' data-bv-result='INVALID'>Please upload valid image file.</small>");
//            return false;
//        }
    }
</script>
@stop


