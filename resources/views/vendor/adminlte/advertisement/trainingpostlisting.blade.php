@extends('adminlte::master')

@section('title', ': User Listing')

@section('adminlte_css')
@yield('css')
@stop

@section('body')
@include('layouts.header')

@php
$user_type = config('selecttype.usertypelist');
@endphp

<style>
    .searchform .col-md-2 {
        padding-right: 0px !important;
    }
</style>
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="inner-body">
            <div class="pcoded-inner-content">
                <div class="main-body">
                    @include('layouts.menu')

                    <!--user content--->
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{trans('messagelabel.training.listingheader')}}</h5>
                                </div>
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <div class="dt-responsive table-responsive">
                                            
                                            <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                                                <!--table-->
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table id="res-config" class="table table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 100%;">
                                                            <thead>
                                                                <tr role="row">
                                                                    <th rowspan="1" colspan="1" >#</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 90px;" aria-sort="ascending" aria-label="User name">{{trans('adminlabel.userlisting.rowusername')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 88px;" aria-label="Ref ID">{{trans('messagelabel.training.ref_id')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 191px;" aria-label="Training Name">{{trans('messagelabel.training.trainingname')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 87px;" aria-label="Batch No">{{trans('messagelabel.training.batchno')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 35px;" aria-label="Start Date">{{trans('messagelabel.training.start_date')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="End Date">{{trans('messagelabel.training.end_date')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Standard">{{trans('messagelabel.training.standard')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Training Type">{{trans('messagelabel.training.training_type')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Training Mode">{{trans('messagelabel.training.training_mode')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="" aria-label="Session Details">{{trans('messagelabel.training.session_details')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Fee Ranage">{{trans('messagelabel.training.feerange')}}</th>
                                                                    <th tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 85px;" aria-label="Status">{{trans('messagelabel.training.status')}}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($traininglist as $key => $val)
                                                                <tr role="row" class="odd">
                                                                    <td>
                                                                        {{$key+1}}
                                                                    </td>
                                                                    <td tabindex="0" class="sorting_1">{{@$userdetails[@$val->user_id]}}</td>
                                                                    <td>{{@$val->ref_id}}</td>
                                                                    <td>{{@$val->traning_name}}</td>
                                                                    <td>{{@$val->batch_no}}</td>
                                                                    <td>{{Carbon\Carbon::parse(@$val->start_date)->format(config('assignment.dateformat'))}}</td>
                                                                    <td>{{Carbon\Carbon::parse(@$val->end_date)->format(config('assignment.dateformat'))}}</td>
                                                                    <td>{{@$standardlist[@$val->standard]}}</td>
                                                                    <td>{{@$trainingtypelist[@$val->training_type]}}</td>
                                                                    <td>
                                                                        @if(@$val->mode == 1)
                                                                        {{"Face to Face"}} @else {{"Online"}} @endif
                                                                    </td>
                                                                    <td wrap>
                                                                        {{@$val->session_details}}
                                                                    </td>
                                                                    <td>$ {{@$val->fee_range}}</td>
                                                                    <td>
                                                                        @if(@$val->mode == 1)
                                                                        {{"Active"}} @else {{"Deactive"}} @endif
                                                                    </td>
                                                                    
                                                                    
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                                <!--table-->
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-5">
                                                        <div class="dataTables_info" id="res-config_info" role="status" aria-live="polite">
                                                            @if(@$traininglist->total())
                                                            Showing 
                                                            {{(($traininglist->currentPage()-1) * $traininglist->perPage())+1}}
                                                            to 
                                                            {{(($traininglist->currentPage()-1) * $traininglist->perPage())+$traininglist->count() }}

                                                            of {{$traininglist->total()}} entries
                                                            @else
                                                            No Record Found !!!
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-7">
                                                        <div class="dataTables_paginate paging_simple_numbers" id="res-config_paginate">
                                                            <ul class="pagination">
                                                                <li class="paginate_button page-item ">
                                                                    {{$traininglist->links()}}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--user content--->

                </div>
            </div>
        </div>
    </div>
</div>

@stop


