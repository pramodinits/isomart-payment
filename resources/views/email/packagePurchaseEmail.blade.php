Hello {{@$fname}}, 
<br/>

<p>You have successfully purchased <strong>{{@$credits_points}}</strong> {{config('assignment.creditpoint')}}.</p>

Available Credit Points: <strong>{{@$totalcredits_points}}</strong><br />
Expiry Date: <strong>{{Carbon\Carbon::parse(@$expiry_date)->format('d-M-Y')}}</strong><br /><br />

Thanks,<br />
ISOMart Team