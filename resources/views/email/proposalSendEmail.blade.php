<?php 
if (@$assignment_type == 1) {
    $assignment_url = asset('assignmentAuditDetail/'.@$assignment_id);
} else if (@$assignment_type == 2) {
    $assignment_url = asset('assignmentTrainingDetail/'.@$assignment_id);
}
?>
Hello {{@$fname}}, 
<br/>

<p>We have received a new response to your assignment {{@$ref_id}}.</p>
<br/>

Assignment Title : {{@$assignment_title}}
<br/>
<strong>Bid Details:</strong><br/>
Bid Budget : {{@$bid_price}}
Bid Effort : {{@$bid_effort}}

<br />

<p>Please click <a href="{{ @$assignment_url }}"><strong>here</strong></a> to respond the bid.</p>
<br/>

Thanks,<br />
ISOMart Team