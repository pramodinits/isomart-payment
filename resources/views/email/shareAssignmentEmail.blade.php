Hi,
<br/>
<p>This is {{@$fname . " " . @$lname}}, I am sharing with you an assignment via <a href="{{asset('/')}}">ISOMart</a> which might be interesting for you. You can check the details by clicking <a href="{{ asset('assignmentAuditDetail/'.@$assignment_id)}}"><strong>{{@$ref_id}}</strong></a>.</p>

<br />

Thanks,<br />
{{@$fname}}<br/>
(Via ISOMART TEAM)