Hello {{@$fname}},

<?php if (@$jobtype == 1) { //seeker ?>

    <p>You have cancelled a job on Ref ID : <strong>{{@$ref_id}}</strong>.</p>
    
<?php } else if (@$jobtype == 2) { //provider ?>

    <p>Job <strong>{{@$ref_id}}</strong> has been cancelled.</p><br/>
    
    <p>Please click <a href="{{ asset('assignedjobDetails/'.@$assignment_id)}}"><strong>here</strong></a> to view the details.</p>

<?php }
?>

<br />

Regards,<br /><br />
ISOMart Team