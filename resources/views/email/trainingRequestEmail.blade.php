Dear {{@$fname}}, 
<br/><br />
<p>There is a new Training program from {{@$company_name}} Training Organization.</p>

<p>
    Batch No : {{@$batch_no}}<br/>
    Duration : {{@$duration . "days"}}<br/>
    Price : {{"$" . @$price . ".00"}}
</p>
<br />

Regards,<br />
ISOMart Team