Hello {{@$fname}}, 
<br/>

<p>Your bid against assignment : <strong>{{@$ref_id}}</strong> has been accepted by the owner.</p>
<br/>
<p>Please click <a href="{{ asset('assignedjobDetails/'.@$assignment_id) }}"><strong>here</strong></a> to view the details.</p>

<br />

Regards,<br />
ISOMart Team