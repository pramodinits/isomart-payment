<?php 
if (@$assignment_type == 1) {
    $assignment_url = asset('assignmentAuditDetail/'.@$assignment_id);
} else if (@$assignment_type == 2) {
    $assignment_url = asset('assignmentTrainingDetail/'.@$assignment_id);
}
?>
Hello {{@$fname}},
<br/>

<p>Your bid against assignment : <strong>{{@$ref_id}}</strong> has been rejected by the owner.</p>
    <br/>
    <p>Please click <a href="{{ @$assignment_url }}"><strong>here</strong></a> to view the details.</p>
    
<br/>
Thanks,<br />
ISOMart Team