<?php
if (@$assignment == 1) {
   $assignmentFor = "Audit";
} else if (@$assignment == 2) {
    $assignmentFor = "Training";
} else if (@$assignment == 3) {
    $assignmentFor = "Consulting";
}
?>

Dear {{@Auth::user()->fname}}, 
<br/><br />
<p>You have successfully registered for a new assignment for {{@$assignmentFor}} program.</p>

<p>Ref ID : {{"#".@$ref_id}}</p>

<p>You are requested to wait for bids from our service providers in response to your assignment request.</p>

<br />

Regards,<br />
ISOMart Team