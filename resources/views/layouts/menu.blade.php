<div class="container">
    <div class="menusection">
        <ul>
            <li class="custom-border"><i class="fa fa-user  text-warning"></i>
                <a href="{{ url('/admin/userListing') }}"><h6 class="text-white">{{trans('adminlabel.header.menuuserlisting')}}</h6></a>
            </li>
            <li class="custom-border"><i class="fa fa-user  text-warning"></i>
                <a href="{{ url('/admin/assignmentListing') }}"><h6 class="text-white">{{trans('adminlabel.header.menuassignment')}}</h6></a>
            </li>
            <li class="custom-border"><i class="fa fa-user  text-warning"></i>
                <a href="{{ url('/admin/queryListing') }}"><h6 class="text-white">{{trans('adminlabel.header.menuquery')}}</h6></a>
            </li>
<!--            <li class="custom-border"><i class="fa fa-user  text-warning"></i>
                <a href="#"><h6 class="text-white">Banner</h6></a>
            </li>-->
            <li class="custom-border"><i class="fa fa-user  text-warning"></i>
                <a href="{{ url('/admin/advertisementListing') }}"><h6 class="text-white">{{trans('adminlabel.header.menubanneradv')}}</h6></a>
            </li>
            <li class="custom-border"><i class="fa fa-user  text-warning"></i>
                <a href="{{ url('/admin/trainingPostListing') }}"><h6 class="text-white ">{{trans('adminlabel.header.menutrainingadv')}}</h6></a>
            </li>
            <li class="border-0"><i class="fa fa-user text-warning"></i>
                <a href="{{ url('/admin/addTaskcredit') }}"><h6 class="text-white ">{{trans('adminlabel.header.menusubscription')}}</h6></a>
            </li>
        </ul>
        <div style="clear:both"></div>
    </div>

</div>
<style>
    .menusection ul li { padding:0px 30px}
</style>