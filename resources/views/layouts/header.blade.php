<nav class="navbar header-navbar pcoded-header">
    <div class="navbar-wrapper">
        <div class="navbar-logo"> <a class="mobile-menu" id="mobile-collapse" href="#!"> 
                <i class="ti-menu"></i> </a> <a class="mobile-search morphsearch-search" href="#"> <i class="fa fa"></i> </a> 
            <a href="{{ url('/admin/home') }}"><h3>{{trans('adminlabel.header.iconheader')}}</h3>
            </a> 
            <a class="mobile-options"> <i class="ti-more"></i> </a> </div>
        <div class="navbar-container container-fluid">
            <div>
                <ul class="nav-right">
                    <li class="user-profile header-notification"> <a href="#!"> <img src="{{asset('images/user.png')}}" alt="User-Profile-Image"> <span>{{@Auth::user()->name}}</span> <i class="ti-angle-down"></i> </a>
                        <ul class="show-notification profile-notification">
                            <li> <a href="#"> <i class="fa fa-user"></i>{{trans('adminlabel.header.menuprofile')}}</a> </li>
                            <li> <a href="#"> <i class="fa fa-envelope"></i>{{trans('adminlabel.header.menumessages')}}</a></li>
                                    <li> <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                            <i class="fa fa-sign-out"></i>{{trans('adminlabel.header.menulogout')}} 
                                        </a>
                                    </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>


