<nav class="navbar header-navbar pcoded-header">
    <div class="navbar-wrapper">
        <div class="navbar-logo"> <a class="mobile-menu" id="mobile-collapse" href="#!"> 
                <i class="ti-menu"></i> </a> <a class="mobile-search morphsearch-search" href="#"> <i class="fa fa"></i> </a> 
            <a href="index.html"><h3>ISOMART</h3> 
            </a> 
            <a class="mobile-options"> <i class="ti-more"></i> </a> </div>
        <div class="navbar-container container-fluid">
            <div>
                <ul class="nav-right">
                    <li class="header-notification"> <a href="#!"> 
                            <i class="fa fa-bell"></i> <span class="badge">5</span> </a>
                        <ul class="show-notification">
                            <li>
                                <h6>Notifications</h6>
                                <label class="label label-danger">New</label>
                            </li>
                            <li>
                                <div class="media"> <img class="d-flex align-self-center" src="images/user.png" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="notification-user">John Doe</h5>
                                        <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                        <span class="notification-time">30 minutes ago</span> </div>
                                </div>
                            </li>
                            <li>
                                <div class="media"> <img class="d-flex align-self-center" src="images/user.png" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="notification-user">Joseph William</h5>
                                        <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                        <span class="notification-time">30 minutes ago</span> </div>
                                </div>
                            </li>
                            <li>
                                <div class="media"> <img class="d-flex align-self-center" src="images/user.png" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="notification-user">Sara Soudein</h5>
                                        <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                        <span class="notification-time">30 minutes ago</span> </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="header-notification"> <a href="#!" class="displayChatbox"> <i class="fa fa-comment"></i> <span class="badge">9</span> </a> </li>
                    <li class="user-profile header-notification"> <a href="#!"> <img src="images/user.png" alt="User-Profile-Image"> <span>John Doe</span> <i class="ti-angle-down"></i> </a>
                        <ul class="show-notification profile-notification">
                            <li> <a href="#!"> <i class="ti-settings"></i> Settings </a> </li>
                            <li> <a href="user-profile.html"> <i class="fa fa-user"></i> Profile </a> </li>
                            <li> <a href="email-inbox.html"> <i class="fa fa-envelope"></i> My Messages </a> </li>
                            <li> <a href="auth-lock-screen.html"> <i class="fa fa-key"></i> Lock Screen </a> </li>
                            <li> <a href="#!"> <i class="fa fa-sign-out"></i> Logout </a> </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>