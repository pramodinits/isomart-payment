<style>
    #modalcustom{
        top:12% !important;
        width: 60% !important;
        margin: auto;
        overflow: hidden;
        position: fixed;
        right: 0;
        left: 0;
        z-index: 1040;
        -webkit-overflow-scrolling: touch;
        outline: 0;
    }
    #msg_alert_box{
        border: solid 1px #00436a;
        font-size: 14px;
    }
    .alert-danger-cust{
        border: 1px solid #560d07 !important;
        background-color: #FFF !important;        
        box-shadow: 5px 5px 5px #ddb6b1;
        color:#560d07 !important;
    }
    .alert-success-cust{
        border: 1px solid #0c4f06 !important;
        background-color: #FFF !important;        
        box-shadow: 5px 5px 5px #b5ddb1;
        color:#0c4f06 !important;
    }
    #custom_loader{
        top:18% !important;
        width: 60% !important;
        margin: auto;
        overflow: hidden;
        position: fixed;
        right: 0;
        left: 0;
        z-index: 1040;
        -webkit-overflow-scrolling: touch;
        outline: 0;
    }
    #custom_loader_alert_box{
        border: 1px solid #2b77d7 !important;
        background: rgba(255,255,255,0.5);        
        box-shadow: 5px 5px 5px #ddb6b1;
        color:#2b77d7 !important;
        padding: 10px;
        margin-bottom: 20px;
        border-radius: 4px;
    }
</style>
<div id="custom_loader" style="display: none;">
    <div class="text-center" id="custom_loader_alert_box">
        <i class="fa fa-lock" aria-hidden="true"></i>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Loading ....
    </div>
</div>

<div  id="modalcustom" role="dialog" aria-hidden="true" tabindex="-1" data-width="80" style="display: block;">
    <div id="msg" class="alert alert-mini  margin-bottom-30" style="display: none;" ></div>   



    @if (Session::has('error_message'))
    <div class="alert alert-danger-cust text-center" id="msg_alert_box">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close text-danger"></i></button>

        <i class="fa fa-thumbs-down" aria-hidden="true"></i>
        &nbsp;&nbsp;&nbsp;&nbsp;
        {!! Session::get('error_message') !!}
    </div>

    @endif

    @if (Session::has('success_message'))
    <div class="alert alert-success-cust text-center" id="msg_alert_box">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close text-success"></i></button>

        <i class="fa fa-thumbs-up" aria-hidden="true"></i>
        &nbsp;&nbsp;&nbsp;&nbsp;

        {!! Session::get('success_message') !!}
    </div>

    @endif



</div>

<script>
    $(function () {
        if ($("#msg_alert_box").length) {
            setTimeout(function () {
                $("#msg_alert_box").hide();
            }, 5000);
        }
    });

//    history.pushState(null, document.title, location.href);
//    window.addEventListener('popstate', function (event)
//    {
//        history.pushState(null, document.title, location.href);
//    });

</script>




