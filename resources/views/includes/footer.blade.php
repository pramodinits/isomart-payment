<footer class="footer pt50 pb30">
    <div class="container">
        <div class="row">


            <div class="col-lg-8 ml-auto mr-auto text-center mb30">    

                <ul class="social-icons list-inline ">
                    <li class="list-inline-item custom-border-black">
                        <a href="{{asset('aboutus')}}">
                            About Us
                        </a>
                    </li>

                    <li class="list-inline-item custom-border-black">
                        <a href="{{asset('contactus')}}">
                            Contact Us
                        </a>
                    </li>

                    <li class="list-inline-item custom-border-black">
                        <a href="{{asset('support')}}">
                            Support
                        </a>
                    </li>

                    <li class="list-inline-item custom-border-black">
                        <a href="{{asset('faq')}}">
                            FaQ
                        </a>
                    </li>


                    <li class="list-inline-item ">
                        <a href="{{asset('termcondition')}}">
                            Terms & Condition
                        </a>
                    </li>
                </ul>

                <p>&copy; Copyright 2018 2019 Privacy T&C Feedback</p>
            </div>
        </div>
    </div>
</footer>


</body>
</html>