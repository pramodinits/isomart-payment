
<?php 
ini_set('session.gc_maxlifetime', 3600); // server should keep session data for AT LEAST 1 hour
session_set_cookie_params(3600); // each client should remember their session id for EXACTLY 1 hour
session_start();
?>
<!doctype html>
<html lang="en" class="no-js">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <!--meta tags-->
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="" />
        <!--meta tags-->
        <title>ISOMart</title>

        <!---style sheets-->
        <meta property="" content="" />
        <link href="{{ asset('/')}}css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('/')}}css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('/')}}css/styles.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Lato|Roboto" rel="stylesheet">
        <!---style sheets-->

        <!---scripts--->
        <script src="{{ asset('/')}}js/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="{{ asset('/')}}js/custom.js"></script>
        <script type="text/javascript" src="{{ asset('/')}}js/jquery.js"></script>
        <script type="text/javascript" src="{{ asset('/')}}js/bootstrapValidator.js"></script>
        

        <!---scripts-->
        
       

        <script>
$(function () {
    $('#searchSubmit').click(function () {
        if ($("#searchValue").val() === '') {
            return true;
        }
    });
    $('#tabAssign ul li a').click(function () {
        var getId = $(this).attr('href');
        if (getId === "#training") {
            $("#tabActive").val('2');
        } else {
            $("#tabActive").val('1');
        }
    });
});
        </script>
        @if(url()->previous() == url('/signin'))
        <script>
            history.pushState(null, document.title, location.href);
            window.addEventListener('popstate', function (event)
            {
                history.pushState(null, document.title, location.href);
            });
        </script>
        @elseif(url()->previous() == url('/signin/*'))
        <script>
            history.pushState(null, document.title, location.href);
            window.addEventListener('popstate', function (event)
            {
                history.pushState(null, document.title, location.href);
            });
        </script>
        @endif
        
        <style>
            li.nav-item a i.fa-info-circle {
                position: relative;
                top: 7%;
                left: 2%;
                font-size: 0.9rem;
            }
            #tooltip-header {
                position: relative;
                display: inline-block;
                opacity: 1;
            }

            #tooltip-header .tooltiptext {
                visibility: hidden;
                width: 190px;
/*                width: 100%;*/
                background-color: black;
                color: #fff;
                text-align: left;
                border-radius: 6px;
                padding: 5px;

                /* Position the tooltip */
                position: absolute;
                top: 95%;
                left: -75px;
                text-align: center;
            }

            #tooltip-header:hover .tooltiptext {
                visibility: visible;
            }
            #queryTooltip {
                width: 100% !important;
                top: 90%;
                right: 0%;
            }
        </style>
    </head>
    <!--    {{ @$controller === "UserController@index" ? "login-bg" : "" }}-->
    <!--    <body class='{{ 
            @$controller == "UserController@signin" ? "login-bg" : (@$controller == "UserController@signup" ? "login-bg" : "") }}'>-->
    <!--    <body class="login-bg">-->
    <body class='{{ (@$selectmenu === "signin") || (@$selectmenu === "signup")|| (@$selectmenu === "forgotpassword") || (@$selectmenu === "verifyotp")
                ? "login-bg" : "" }}'>
        <header id="top" class="fadeInDown clearfix">
            <div class="site-overlay"></div>
            @if (@Auth::user()->id && Auth::user()->phone_verify_status == 1)
            <nav class="navbar navbar-expand-lg navbar-light navbar-transparent bg-faded nav-sticky pt-2 pb-2">
                <!--/search form-->
                <div class="container-fluid">
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                    <a class="navbar-brand" href="{{asset('dashboard')}}">
                        <h4 class="">{{trans('bladelable.header.sitetitle')}}</h4>
                    </a>
                    <form class="navbar-form input-group-searchsection ml-atuo" action="{{ asset('searchContent')}}" method="get"> 
                        <div class="input-group">
                            <div class="selectParent txtbox">
                            <select class="textbox" name="selectSearch" style="border-radius: 2px 0 0 2px;">
                                <option value="1" <?= (@$selectSearch ==1) ? "selected" : ""; ?>>{{trans('bladelable.header.searchassignment')}}</option>
                                <option value="2" <?= (@$selectSearch ==2) ? "selected" : ""; ?>>{{trans('bladelable.header.searchquery')}}</option>
                            </select>
                            </div>
<!--                            <div class="input-group-btn">
                                <button type="button" class="btn btn-light " data-toggle="dropdown" style="border-radius: 2px 0 0 2px;">Assignments <i class="fa fa-angle-down text-warning"></i></button>                          
                            </div>-->
                            <input type="text" class="form-control textbox" placeholder="Search" name="searchValue" id="searchValue" value="{{@$searchInput}}">                
                            <input type="hidden" class="form-control" name="tabActive" id="tabActive" value="1">       
                            <div class="input-group-btn">
                                <button type="submit" id="searchSubmit" class="btn btn-light btn-shrap"><i class="fa fa-search text-info"></i></button>                    
                            </div>
                        </div>
                    </form>
                    <div style="margin-right:12%;">
                        <div id="navbarNavDropdown" class="navbar-collapse collapse">
                        <ul class="navbar-nav mr-auto" style="z-index: 100000;">
                            <li class="nav-item dropdown active" style='
                                {{ (@Auth::user()->usertype == 6 || @Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) ? "" : "visibility:hidden;" }}
                                '>
                                @if (Auth::user()->usertype == 6)
                                <a class="nav-link dropdown-toggle text-secondary" href="{{asset('requestassignment')}}">
                                    {{trans('bladelable.menu.newassignmentaudit')}}
                                    <div class="tooltip" id="tooltip-header">
                                        <i class="fa fa-info-circle n-icons text-primary" aria-hidden="true"></i>
                                        <span class="tooltiptext">{{trans('bladelable.menu.newassignmentaudit')}}</span>
                                    </div>
                                </a>
                                @elseif(@Auth::user()->usertype == 5 || @Auth::user()->usertype == 1)
                                <a class="nav-link dropdown-toggle text-secondary" href="{{asset('requestassignment')}}">
                                    {{trans('bladelable.menu.newassignmenttraining')}}
                                    <div class="tooltip" id="tooltip-header">
                                        <i class="fa fa-info-circle n-icons text-primary" aria-hidden="true"></i>
                                        <span class="tooltiptext">{{trans('bladelable.menu.newassignmenttraining')}}</span>
                                    </div>
                                </a>
                                @endif
                            </li>
                            <li class="nav-item dropdown active">
                                <a class="nav-link  dropdown-toggle text-secondary" href="{{asset('addQuery')}}">
                                    {{trans('bladelable.menu.newquery')}}
                                    <div class="tooltip" id="tooltip-header">
                                        <i class="fa fa-info-circle ml-1 n-icons text-primary" aria-hidden="true"></i>
                                        <span class="tooltiptext">{{trans('bladelable.menu.newquery')}}</span>
                                    </div>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="nav-link  icon-border n-icons" href="{{ url("/messageDetails") }}">
                                    <i class="fa fa-envelope n-icons"></i> 
                                </a>
                            </li>
                        </ul>
                    </div>
                    </div>
                    
                </div>
                <div class="navbar-right-elements">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <div class="dropdown text-secondary">
                                    <button class="dropbtn text-secondary mt-1">Hi {{Auth::user()->fname}}&nbsp;<i class="fa fa-angle-down font12"></i></button><br>
                                    <div class="small text-primary">
                                    @php
                                    $usertype = config('selecttype.usertypelist');
                                    echo $usertype[@Auth::user()->usertype];
                                    @endphp
                                    </div>
                                    <div class="dropdown-content bg-light show">
                                        <ul class="dropdown-menu dropdown-menu-right bg-light show">
                                            <li><a href="{{asset('profile')}}" class="dropdown-item border-bottom {{@$selectmenu == "profile" ? "text-warning" : ""}}">{{trans('bladelable.menu.myprofile')}}</a></li>
                                            @if(@Auth::user()->usertype == 6 || @Auth::user()->usertype == 5 || @Auth::user()->usertype == 1) 
                                            <li><a href="{{asset('allassignment')}}" class="dropdown-item border-bottom {{@$selectmenu == "myassignment" ? "text-warning" : ""}}">{{trans('bladelable.menu.myassignment')}}</a></li>
                                            <li><a href="{{asset('myjobslist')}}" class="dropdown-item border-bottom {{@$selectmenu == "myjoblist" ? "text-warning" : ""}}">{{trans('bladelable.menu.myjobs')}}</a></li>
                                            @elseif(@Auth::user()->usertype == 8)
                                            <li><a href="{{asset('allproposal')}}" class="dropdown-item border-bottom {{@$selectmenu == "allproposal" ? "text-warning" : ""}}">{{trans('bladelable.menu.mybids')}}</a></li>
                                            <li><a href="{{asset('assignedjoblist')}}" class="dropdown-item border-bottom {{@$selectmenu == "assignedjobslist" ? "text-warning" : ""}}">{{trans('bladelable.menu.myjobs')}}</a></li>
                                            <li><a href="{{asset('cancelledjoblist')}}" class="dropdown-item border-bottom {{@$selectmenu == "cancelledjobslist" ? "text-warning" : ""}}">{{trans('bladelable.menu.cancelledjobs')}}</a></li>
                                            <li><a href="{{asset('myCalender')}}" class="dropdown-item border-bottom {{@$selectmenu == "calendar" ? "text-warning" : ""}}">{{trans('bladelable.menu.mycalendars')}}</a></li>
                                            @endif
                                            <li><a href="{{asset('myQuery')}}" class="dropdown-item border-bottom {{(@$selectmenu == "myquery") || (@$selectmenu === "querydetail") ? "text-warning" : ""}}">{{trans('bladelable.menu.myquery')}}</a></li>
                                            <li><a href="{{asset('mySubscription')}}" class="dropdown-item border-bottom {{(@$selectmenu == "mysubscription") ? "text-warning" : ""}}">{{trans('bladelable.menu.mysubscription')}}</a></li>
                                            <li><a href="{{asset('subscriptionPackages')}}" class="dropdown-item border-bottom {{(@$selectmenu == "subscriptionpackages") ? "text-warning" : ""}}">{{trans('bladelable.menu.subscriptionpackages')}}</a></li>
                                            @if(@Auth::user()->usertype == 7)
                                            <li><a href="{{asset('addTrainingPost')}}" class="dropdown-item border-bottom {{@$selectmenu == "addtrainingpost" ? "text-warning" : ""}}">{{trans('bladelable.menu.trainingadv')}}</a></li>
                                            @endif
                                            <li><a href="{{asset('cpdHours')}}" class="dropdown-item border-bottom {{@$selectmenu == "cpdhours" ? "text-warning" : ""}}">{{trans('bladelable.menu.cpdhours')}}</a></li>
                                            <li><a href="{{asset('logout')}}" class="dropdown-item ">{{trans('bladelable.menu.logout')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

            @elseif(@$selectmenu == "signin" || @$selectmenu == "signup" || @$selectmenu == "forgotpassword" || @$selectmenu == "verifyotp")

            @else 
            </nav>
            
            <nav class="navbar navbar-expand-lg navbar-light navbar-transparent bg-faded nav-sticky pt-2 pb-2 bg-white">
                <!--/search form-->
                <div class="container-fluid">
                    <!--<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>-->
                    <a class="navbar-brand" href="{{asset('/')}}">
                        <h4 class="font-weight-bold">{{trans('bladelable.header.sitetitle')}}</h4>
                    </a>
                    <form class="navbar-form input-group-searchsection ml-atuo" action="{{ asset('searchContent')}}" method="get"> 
                        <div class="input-group">
                            <div class="selectParent txtbox">
                            <select class="textbox" name="selectSearch" style="border-radius: 2px 0 0 2px;">
                                <option value="1" <?= (@$selectSearch ==1) ? "selected" : ""; ?>>{{trans('bladelable.header.searchassignment')}}</option>
                                <option value="2" <?= (@$selectSearch ==2) ? "selected" : ""; ?>>{{trans('bladelable.header.searchquery')}}</option>
                            </select>
                            </div>
<!--                            <div class="input-group-btn">                                
                                <div class="dropdown">
                                    
                                <button type="button" class="btn btn-light " data-toggle="dropdown" style="border-radius: 2px 0 0 2px;">Assignments <i class="fa fa-angle-down text-warning"></i></button>  
                                </div>
                            </div>        -->
                            <input type="text" class="form-control textbox" placeholder="Search" name="searchValue" id="searchValue" value="{{@$searchInput}}">                
                            <input type="hidden" class="form-control" name="tabActive" id="tabActive" value="1">       
                            <div class="input-group-btn">
                                <button type="submit" id="searchSubmit" class="btn btn-light btn-shrap"><i class="fa fa-search text-info"></i></button>                        
                            </div>
                        </div>
                    </form>

                    <div id="navbarNavDropdown" class="navbar-collapse">
                        <ul class="navbar-nav nav-position">
                            <li class="nav-item nav-button"><a href="{{asset("signin")}}" class="btn btn-light nav-item btn-border">{{trans('bladelable.menu.signin')}}</a></li>
                            <li class="nav-item nav-button"><a href="{{asset("signup")}}" class="btn nav-item btn-ctm-premiry">{{trans('bladelable.menu.signup')}}</a></li>
                        </ul>
                    </div>
                    <!--right nav icons--> 
                </div>
            </nav>
            @endif
        </header>
        @include('includes.message')

