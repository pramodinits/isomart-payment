<?php
return [
    'header' => [
        'sitetitle' => "ISOMART",
        'searchassignment' => "Assignment",
        'searchquery' => "Query"
    ],
    'menu' => [
        'newassignmentaudit' => "New Audit Assignment",
        'newassignmenttraining' => "New Training Assignment",
        'newquery' => "New Query",
        'newtraining' => "New Training",
        'myprofile' => "My Profile",
        'myassignment' => "My Assignments",
        'myjobs' => "My Jobs",
        'mybids' => "My Bids",
        'cancelledjobs' => "Cancelled Jobs",
        'mycalendars' => "My Calendars",
        'myquery' => "My Query",
        'mysubscription' => "My Subscription",
        'subscriptionpackages' => "Subscription Packages",
        'trainingadv' => "Training Advertisement",
        'cpdhours' => "CPD Hours",
        'logout' => "Log Out",
        'signin' => "Sign In",
        'signup' => "Sign Up"
    ],
    'home' => [
        "buypackage" => "BUY NOW",
        "subscribepackage" => "SUBSCRIBE",
        "latestassignments" => "Latest Assignments :",
        "latesttrainingassignments" => "Latest Training Advertisement :",
        "latestqueries" => "Latest Queries :",
        "morelinks" => "More",
        "noassignment" => "No assignment found.",
        "noadvertisement" => "No advertisement found.",
        "packageheader" => "Subscription Packages :",
        "nopackages" => "No Package available."
    ],
    'tabtitle' => [
        "tabaudit" => "Audit",
        "tabtraining" => "Training",
        "tabcertification" => "Certification"
    ]
]

?>

